<!-- BEGIN page_title_block -->
PostgreSQL: Introduction and Concepts - One Year Later
<!-- END page_title_block -->

<h1>PostgreSQL: Introduction and Concepts - One Year Later</h1>

<h3>Bruce Momjian</h3>

<p>It has been a year since the printing of my book so I thought I would
reflect on the process. The book has sold eleven thousand copies in
the first year, which is good, and is available in three languages.
There are also now five other PostgreSQL books on the market, which
is also good news.</p>

<p>Having the book content accessible on the Internet while I was writing
the book was a great benefit, both to the quality of the book and
to my morale while writing it. Keeping the content available even
after publication has been a great boost too, so people who don't
want to purchase the book can still benefit from it. (I am pleased
to see content of the book <em>Practical PostgreSQL,</em>
<a href="http://www.commandprompt.com/ppbook/">http://www.commandprompt.com/ppbook/</a>,
has also remained online after publication.)</p>

<p>Writing a computer book is different from writing a general book because
computer books require an excruciating amount of detail to convey
information to the reader. It is easy to see why this is the case.
Most items people use in their daily lives perform a limited number
of functions. A dishwasher, a television, and even a car perform only
a few basic functions and are fairly limited in how they can be controlled
by the user. Certain devices, like <small>VCR'</small>s, stretch the capabilities
of their users, e.g. programming your <small>VCR</small>. Computers in some
ways are alone in their ability to morph themselves to perform almost
limitless functions. Of course, the computer doesn't move or make
toast, but the input, processing, storage, and display devices can
be manipulated in an infinite number of ways to perform a variety
of functions. It is this <em>open-ended</em> nature that make computers
so powerful, but it also makes them complicated to control.</p>

<p>In writing a computer book, it was my goal to take the limited topic
of PostgreSQL and present it in a way that would make its form and
function clear and controllable. Though I required the software be
installed (more on that later), I did not make any other assumptions,
and this allowed me to coherently present PostgreSQL's capabilities.
The trick was to make PostgreSQL's capabilities, even the complex
ones, almost obvious to users with no previous database experience.</p>

<p>In fact, one great challenge of the book was to order items in a way
that there were few forward references to material that appeared later
in the book. Another challenge was deciding what <em>not</em> to mention,
or perhaps mention later, for fear that it could confuse readers more
than help them. I also found the writing process to be similar to
the process of programming enhancements to PostgreSQL -- my first
attempt usually gets the job done, but is far too large and unorganized;
my second attempt is closer but still doesn't fit cleanly into the
existing code; and my final patch is usually the smallest, slipping
into the code as though it was meant to be there all along. In writing
my book, my goal was to fit together the pieces of PostgreSQL with
the same clarity that exists in our source code.</p>

<p>Anyway, that was my goal, and I think I was fairly successful. However,
this emphasis probably surprised some people. Most computer books
fall into several general categories:</p>

<ul>
<li>Reference material with narrative</li>
<li>Tips and tricks for experienced users</li>
<li>Practical examples showing common tasks</li>
</ul>

<p>My book doesn't fit into any of these categories. It does have a reference
section at the back, and tips and some practical material, but its
emphasis is not really any of these. The emphasis is on introducing
PostgreSQL and explaining the database system with a conceptual emphasis.
Though PostgreSQL has fine documentation, it does not have material
that can be easily digested by inexperienced database users. Though
it documents the nuts and bolts of PostgreSQL, it doesn't show you
how those nuts and bolts fit together. My book was meant to fill that
need.</p>

<p>Writing an open-source computer book is also slightly different from
a general computer book. Open source software, and particularly PostgreSQL,
is rapidly changing thanks to the many talented developers around
the world connected via the Internet. However, a book, once printed,
is hard to change, so care must be taken so the book remains relevant
long after printing. The delay for translation into other languages
makes longevity even more important. By placing reference material
into a separate section, users can easily refer to it while reading
the book <em>and</em> they can refer to more current reference material
when future PostgreSQL releases are made. This is also why I did not
cover installation in detail in the book -- PostgreSQL installation
changes in subtle ways from release to release, so rather than provide
material that may fail to work in future releases, I felt it was better
for people to refer to installation instructions that <em>exactly</em>
match the PostgreSQL version they are using.</p>

<p>The book is now in its third printing. The number of errors were minimal,
thanks to the many reviewers of the online version. I continue to
update the book's web page, <a href="/docs/books/awbook">http://www.postgresql.org/docs/books/awbook</a>,
with changes relevant to PostgreSQL releases and with additional chapters.
I am glad people are enjoying it. Writing a book was a great experience
for me, and maybe someday, I will do it again.</p>
