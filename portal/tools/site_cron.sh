#!/usr/local/bin/bash

# $Id$

# This script is intended to be run from cron say every 5 minutes.
# It will poll the database for requests to rebuild the whole or
# parts of the site, and will call the actual buildsite script as
# necessary to perform these updates.


PSQL="/usr/local/bin/psql -h pgsql74.hub.org 186_www -U 186_pgsql"
BS="/usr/local/www/wwwmaster.postgresql.org/www/tools/buildsite /usr/local/www/wwwmaster.postgresql.org/www"
RSYNC="/usr/local/bin/rsync"
WWWROOT="/usr/local/www/wwwmaster.postgresql.org/static/"

# Interlocking goes here
if [ ! -e /tmp/.website_update.lck ]; then
    trap "rm -f /tmp/.website_update.lck" INT TERM EXIT
    touch /tmp/.website_update.lck

    # Check if there are updates to be run
    R=$(${PSQL} -At -F_ -c "SELECT max(t),max(docs),max(ftp) FROM sync_request \
                            WHERE completed IS NULL AND t < CURRENT_TIMESTAMP")
    if [ "$R" ]; then
        TIME=$(echo $R | cut -f1 -d_)
        if [ "$TIME" != "" ]; then
            # Figure out if we can skip docs and/or ftp
            DOCS=$(echo $R | cut -f2 -d_)
            FTP=$(echo $R | cut -f3 -d_)

            PRM=""
            [ "$DOCS" == "0" ] && PRM="$PRM -nodocs"
            [ "$FTP" == "0" ] && PRM="$PRM -noftp"

            ${BS} $PRM

            # Ok, site is built. Log this fact, and remove the queued
            # jobs for it.
            ${PSQL} -q -c "INSERT INTO sync_log VALUES \
                             (CURRENT_TIMESTAMP, 'sync', 'wwwmaster', '$TIME'); \
                           UPDATE sync_request SET completed=CURRENT_TIMESTAMP \
                             WHERE completed IS NULL AND t <= '$TIME';"


            T=$(date "+%Y-%m-%d %H:%M:%S")
            # Now fire off mirroring tasks as subcommands
            for IP in $(${PSQL} -At -c "SELECT ip FROM frontends") ; do
                (
                    # Push the sync through
                    ${RSYNC} -azH --delete --exclude=.svn ${WWWROOT} ${IP}::pgsql-www
                    ${PSQL} -q -c "INSERT INTO sync_log VALUES \
                                   (CURRENT_TIMESTAMP, 'push', '${IP}', '$TIME')"
                ) &
            done
            wait
        fi
    fi
#else
    # Script is already running. Just don't do anything,
    # if we log something here it's going to end up spamming
    # the list...
fi

