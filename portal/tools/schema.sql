--
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: cvslog; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA cvslog;


ALTER SCHEMA cvslog OWNER TO postgres;

--
-- Name: pgcrypto; Type: SCHEMA; Schema: -; Owner: pgsql
--

CREATE SCHEMA pgcrypto;


ALTER SCHEMA pgcrypto OWNER TO pgsql;

--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: stefan
--

CREATE PROCEDURAL LANGUAGE plpgsql;


ALTER PROCEDURAL LANGUAGE plpgsql OWNER TO stefan;

SET search_path = cvslog, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: branches; Type: TABLE; Schema: cvslog; Owner: postgres; Tablespace: 
--

CREATE TABLE branches (
    branchid integer NOT NULL,
    branchname text NOT NULL,
    lastcheck timestamp with time zone DEFAULT '1996-01-01 00:00:00+00'::timestamp with time zone NOT NULL,
    active boolean DEFAULT false NOT NULL
);


ALTER TABLE cvslog.branches OWNER TO postgres;

--
-- Name: commits; Type: TABLE; Schema: cvslog; Owner: postgres; Tablespace: 
--

CREATE TABLE commits (
    commitid integer NOT NULL,
    branch integer NOT NULL,
    "time" timestamp with time zone NOT NULL,
    author text NOT NULL,
    message text NOT NULL
);


ALTER TABLE cvslog.commits OWNER TO postgres;

--
-- Name: files; Type: TABLE; Schema: cvslog; Owner: postgres; Tablespace: 
--

CREATE TABLE files (
    fileid integer NOT NULL,
    commitid integer NOT NULL,
    filename text NOT NULL,
    revision text NOT NULL,
    linesadded integer NOT NULL,
    linesremoved integer NOT NULL
);


ALTER TABLE cvslog.files OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- Name: applications; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE applications (
    id character varying(100) NOT NULL,
    version character varying(20) NOT NULL,
    name text NOT NULL,
    active boolean DEFAULT true NOT NULL,
    description text NOT NULL,
    category text NOT NULL,
    pgversion character varying(5),
    edbversion character varying(5),
    format character varying(4) NOT NULL,
    installoptions text,
    upgradeoptions text,
    checksum character varying(32) NOT NULL,
    mirrorpath text,
    alturl text,
    dependencies character varying(100)[],
    versionkey text NOT NULL,
    platform character varying(20) NOT NULL,
    secondaryplatform character varying(20)
);


ALTER TABLE public.applications OWNER TO "186_pgsql";

SET default_with_oids = true;

--
-- Name: clickthrus; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE clickthrus (
    id bigint NOT NULL,
    ts timestamp without time zone DEFAULT ('now'::text)::timestamp(6) without time zone,
    scheme character varying(4),
    host character varying(100),
    path text,
    country character(2)
);


ALTER TABLE public.clickthrus OWNER TO "186_pgsql";

--
-- Name: clickthrus_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE clickthrus_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.clickthrus_id_seq OWNER TO "186_pgsql";

--
-- Name: clickthrus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: 186_pgsql
--

ALTER SEQUENCE clickthrus_id_seq OWNED BY clickthrus.id;


SET default_with_oids = false;

--
-- Name: clickthrus2; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE clickthrus2 (
    id bigint DEFAULT nextval('clickthrus_id_seq'::regclass) NOT NULL,
    ts timestamp with time zone DEFAULT now() NOT NULL,
    mirror integer NOT NULL,
    protocol character(1) NOT NULL,
    client inet,
    path text
);


ALTER TABLE public.clickthrus2 OWNER TO "186_pgsql";

SET default_with_oids = true;

--
-- Name: comment_rejects; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE comment_rejects (
    re character varying(128) NOT NULL
);


ALTER TABLE public.comment_rejects OWNER TO "186_pgsql";

SET default_with_oids = false;

--
-- Name: comments; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE comments (
    id integer DEFAULT nextval(('"comments_id_seq"'::text)::regclass) NOT NULL,
    version numeric(2,1),
    file character varying(64),
    comment text,
    posted_at timestamp with time zone DEFAULT now(),
    posted_by text,
    posted_ip character varying(16),
    approved boolean DEFAULT false NOT NULL,
    processed boolean DEFAULT false NOT NULL
);


ALTER TABLE public.comments OWNER TO "186_pgsql";

--
-- Name: communitypages; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE communitypages (
    pageid integer NOT NULL,
    version integer DEFAULT 0 NOT NULL,
    syspage integer DEFAULT 0 NOT NULL,
    parent integer NOT NULL,
    author character varying(16) NOT NULL,
    origauthor character varying(16) NOT NULL,
    savedate timestamp with time zone DEFAULT now() NOT NULL,
    title character varying(128) NOT NULL,
    shorttitle character varying(128) NOT NULL,
    contents text NOT NULL
);


ALTER TABLE public.communitypages OWNER TO "186_pgsql";

--
-- Name: communitypages_breadcrumb_type; Type: TYPE; Schema: public; Owner: 186_pgsql
--

CREATE TYPE communitypages_breadcrumb_type AS (
	pageid integer,
	title character varying(128)
);


ALTER TYPE public.communitypages_breadcrumb_type OWNER TO "186_pgsql";

--
-- Name: communitypages_files; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE communitypages_files (
    fileid integer NOT NULL,
    pageid integer NOT NULL,
    author character varying(16) NOT NULL,
    title character varying(128) NOT NULL,
    mimetype character varying(64) NOT NULL,
    image bytea
);


ALTER TABLE public.communitypages_files OWNER TO "186_pgsql";

--
-- Name: communitypages_history; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE communitypages_history (
    pageid integer NOT NULL,
    version integer NOT NULL,
    author character varying(16) NOT NULL,
    savedate timestamp with time zone NOT NULL,
    title character varying(128) NOT NULL,
    shorttitle character varying(128) NOT NULL,
    contents text NOT NULL
);


ALTER TABLE public.communitypages_history OWNER TO "186_pgsql";

--
-- Name: communitypages_root; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE communitypages_root (
    pageid integer NOT NULL,
    template character varying(128)
);


ALTER TABLE public.communitypages_root OWNER TO "186_pgsql";

--
-- Name: communitypages_work; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE communitypages_work (
    pageid integer NOT NULL,
    parent integer NOT NULL,
    syspage integer DEFAULT 0 NOT NULL,
    author character varying(16) NOT NULL,
    ready integer DEFAULT 0 NOT NULL,
    savedate timestamp with time zone DEFAULT now() NOT NULL,
    title character varying(128) NOT NULL,
    shorttitle character varying(128) NOT NULL,
    contents text NOT NULL
);


ALTER TABLE public.communitypages_work OWNER TO "186_pgsql";

--
-- Name: communitypages_work_files; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE communitypages_work_files (
    fileid integer NOT NULL,
    pageid integer NOT NULL,
    author character varying(16) NOT NULL,
    title character varying(128) NOT NULL,
    mimetype character varying(64) NOT NULL,
    image bytea
);


ALTER TABLE public.communitypages_work_files OWNER TO "186_pgsql";

SET default_with_oids = true;

--
-- Name: countries; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE countries (
    id integer NOT NULL,
    name text,
    tld character varying(3)
);


ALTER TABLE public.countries OWNER TO "186_pgsql";

--
-- Name: developers_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE developers_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.developers_id_seq OWNER TO "186_pgsql";

SET default_with_oids = false;

--
-- Name: developers; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE developers (
    id integer DEFAULT nextval('developers_id_seq'::regclass) NOT NULL,
    type integer NOT NULL,
    lastname text NOT NULL,
    firstname text NOT NULL,
    email text NOT NULL,
    company text,
    companyurl text,
    location text,
    contribution text
);


ALTER TABLE public.developers OWNER TO "186_pgsql";

--
-- Name: developers_types_type_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE developers_types_type_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.developers_types_type_seq OWNER TO "186_pgsql";

--
-- Name: developers_types; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE developers_types (
    type integer DEFAULT nextval('developers_types_type_seq'::regclass) NOT NULL,
    typename character varying(32),
    sortorder integer NOT NULL,
    extrainfo text,
    detailed integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.developers_types OWNER TO "186_pgsql";

SET default_with_oids = true;

--
-- Name: docs; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE docs (
    id integer DEFAULT nextval(('docs_id_seq'::text)::regclass) NOT NULL,
    file character varying(64) NOT NULL,
    version numeric(2,1) NOT NULL,
    title character varying(256),
    content text,
    dirty boolean
);


ALTER TABLE public.docs OWNER TO "186_pgsql";

--
-- Name: events; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE events (
    id integer DEFAULT nextval(('"events_id_seq"'::text)::regclass) NOT NULL,
    posted date DEFAULT ('now'::text)::date,
    posted_by character varying(64),
    approved boolean DEFAULT false,
    start_date date,
    end_date date,
    training boolean DEFAULT false,
    organisation text
);


ALTER TABLE public.events OWNER TO "186_pgsql";

--
-- Name: events_location; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE events_location (
    eventid integer NOT NULL,
    country integer NOT NULL,
    state text,
    city text
);


ALTER TABLE public.events_location OWNER TO "186_pgsql";

--
-- Name: events_text; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE events_text (
    eventid integer NOT NULL,
    language character varying(5) DEFAULT 'en'::character varying NOT NULL,
    event text NOT NULL,
    summary text NOT NULL,
    details text NOT NULL,
    modified timestamp(0) with time zone DEFAULT now() NOT NULL,
    CONSTRAINT events_text_language CHECK (((language)::text ~ '[a-z]{2}(-[a-z]{2})?'::text))
);


ALTER TABLE public.events_text OWNER TO "186_pgsql";

SET default_with_oids = false;

--
-- Name: features_categories; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE features_categories (
    categoryid integer NOT NULL,
    categoryname text NOT NULL,
    categorydescription text NOT NULL
);


ALTER TABLE public.features_categories OWNER TO "186_pgsql";

--
-- Name: features_features; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE features_features (
    featureid integer NOT NULL,
    groupid integer NOT NULL,
    featurename text NOT NULL,
    featuredescription text DEFAULT ''::text
);


ALTER TABLE public.features_features OWNER TO "186_pgsql";

--
-- Name: features_groups; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE features_groups (
    groupid integer NOT NULL,
    groupsort integer NOT NULL,
    groupname text NOT NULL,
    category integer NOT NULL
);


ALTER TABLE public.features_groups OWNER TO "186_pgsql";

--
-- Name: features_log; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE features_log (
    ts timestamp with time zone DEFAULT now() NOT NULL,
    userid character varying(16) NOT NULL,
    txt text NOT NULL
);


ALTER TABLE public.features_log OWNER TO "186_pgsql";

--
-- Name: features_matrix; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE features_matrix (
    version integer NOT NULL,
    feature integer NOT NULL,
    state integer NOT NULL
);


ALTER TABLE public.features_matrix OWNER TO "186_pgsql";

--
-- Name: features_versions; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE features_versions (
    versionid integer NOT NULL,
    versionname text NOT NULL,
    active boolean DEFAULT false NOT NULL,
    category integer NOT NULL
);


ALTER TABLE public.features_versions OWNER TO "186_pgsql";

--
-- Name: frontends; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE frontends (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    ip character varying(64) NOT NULL
);


ALTER TABLE public.frontends OWNER TO "186_pgsql";

SET default_with_oids = true;

--
-- Name: iptocountry; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE iptocountry (
    id integer DEFAULT nextval(('iptocountry_id_seq'::text)::regclass) NOT NULL,
    startip bigint NOT NULL,
    endip bigint NOT NULL,
    countrycode character(2) NOT NULL,
    country character varying(100) NOT NULL
);


ALTER TABLE public.iptocountry OWNER TO "186_pgsql";

SET default_with_oids = false;

--
-- Name: listgroups; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE listgroups (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    sortkey integer NOT NULL
);


ALTER TABLE public.listgroups OWNER TO "186_pgsql";

--
-- Name: lists; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE lists (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    active integer NOT NULL,
    grp integer NOT NULL,
    description text NOT NULL,
    shortdesc text
);


ALTER TABLE public.lists OWNER TO "186_pgsql";

SET default_with_oids = true;

--
-- Name: mirrors; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE mirrors (
    id integer DEFAULT nextval(('"mirrors_id_seq"'::text)::regclass) NOT NULL,
    country_name character varying(50) NOT NULL,
    country_code character varying(2) NOT NULL,
    mirror_created timestamp with time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    mirror_last_rsync timestamp with time zone DEFAULT '1970-01-01 00:00:00+00'::timestamp with time zone NOT NULL,
    mirror_type character varying(10) NOT NULL,
    mirror_index smallint NOT NULL,
    host_addr inet DEFAULT '0.0.0.0'::inet,
    host_path character varying(100),
    host_sponsor character varying(100),
    host_contact character varying(100),
    host_email character varying(100),
    host_notes text,
    rsync_host1 character varying(100),
    rsync_host2 character varying(100),
    host_port character varying(5),
    mirror_active boolean DEFAULT true NOT NULL,
    mirror_dns boolean DEFAULT false NOT NULL,
    mirror_private boolean DEFAULT false NOT NULL,
    host_use_cname boolean DEFAULT false,
    host_cname_host character varying(100),
    mirror_primary boolean DEFAULT false NOT NULL,
    error_count integer DEFAULT 0 NOT NULL,
    alternate_protocol boolean DEFAULT false NOT NULL,
    alternate_at_root boolean DEFAULT false NOT NULL
);


ALTER TABLE public.mirrors OWNER TO "186_pgsql";

--
-- Name: news; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE news (
    id integer DEFAULT nextval(('"news_id_seq"'::text)::regclass) NOT NULL,
    posted date DEFAULT ('now'::text)::date,
    posted_by character varying(64),
    active boolean DEFAULT false,
    approved boolean DEFAULT false
);


ALTER TABLE public.news OWNER TO "186_pgsql";

--
-- Name: news_text; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE news_text (
    newsid integer NOT NULL,
    language character varying(5) DEFAULT 'en'::character varying NOT NULL,
    headline text NOT NULL,
    summary text NOT NULL,
    story text NOT NULL,
    modified timestamp(0) with time zone DEFAULT now() NOT NULL,
    CONSTRAINT news_text_language CHECK (((language)::text ~ '[a-z]{2}(-[a-z]{2})?'::text))
);


ALTER TABLE public.news_text OWNER TO "186_pgsql";

SET default_with_oids = false;

--
-- Name: organisations; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE organisations (
    id integer NOT NULL,
    name text NOT NULL,
    address text,
    url text,
    email text,
    phone text,
    orgtype character(1) NOT NULL,
    contact character varying(16) NOT NULL,
    approved boolean DEFAULT false NOT NULL,
    lastconfirmed timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT check_orgtype CHECK ((orgtype = ANY (ARRAY['p'::bpchar, 'c'::bpchar, 'i'::bpchar, 'n'::bpchar])))
);


ALTER TABLE public.organisations OWNER TO "186_pgsql";

--
-- Name: product_categories; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE product_categories (
    id integer NOT NULL,
    name text,
    blurb text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.product_categories OWNER TO "186_pgsql";

--
-- Name: products; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE products (
    id integer NOT NULL,
    publisher integer NOT NULL,
    name text NOT NULL,
    url text NOT NULL,
    category integer NOT NULL,
    description text NOT NULL,
    price text,
    licence character(1) NOT NULL,
    contact character varying(16) NOT NULL,
    approved boolean DEFAULT false NOT NULL,
    lastconfirmed timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT products_licence_check CHECK ((licence = ANY (ARRAY['o'::bpchar, 'c'::bpchar, 'f'::bpchar, 'm'::bpchar])))
);


ALTER TABLE public.products OWNER TO "186_pgsql";

SET default_with_oids = true;

--
-- Name: profserv; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE profserv (
    id integer NOT NULL,
    email character varying(246) NOT NULL,
    lastconfirmed timestamp with time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    name character varying(256) NOT NULL,
    description character varying(1024) NOT NULL,
    employees character varying(32),
    locations character varying(128),
    region_africa boolean NOT NULL,
    region_asia boolean NOT NULL,
    region_europe boolean NOT NULL,
    region_northamerica boolean NOT NULL,
    region_oceania boolean NOT NULL,
    region_southamerica boolean NOT NULL,
    hours character varying(128),
    languages character varying(256),
    customerexample character varying(4096),
    experience character varying(2048),
    contact character varying(1024),
    url character varying(256),
    provides_support boolean NOT NULL,
    provides_hosting boolean NOT NULL,
    interfaces character varying(512),
    approved boolean DEFAULT false NOT NULL
);


ALTER TABLE public.profserv OWNER TO "186_pgsql";

--
-- Name: quotes; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE quotes (
    id integer NOT NULL,
    active boolean,
    approved boolean
);


ALTER TABLE public.quotes OWNER TO "186_pgsql";

--
-- Name: quotes_text; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE quotes_text (
    quoteid integer NOT NULL,
    language character varying(5) DEFAULT 'en'::character varying NOT NULL,
    quote text,
    tagline text,
    modified timestamp with time zone DEFAULT now(),
    CONSTRAINT "$2" CHECK (((language)::text ~ '[a-z]{2}(-[a-z]{2})?'::text))
);


ALTER TABLE public.quotes_text OWNER TO "186_pgsql";

--
-- Name: survey_lock; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE survey_lock (
    ipaddr character varying(15) NOT NULL,
    voted timestamp with time zone DEFAULT now()
);


ALTER TABLE public.survey_lock OWNER TO "186_pgsql";

--
-- Name: survey_questions; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE survey_questions (
    surveyid integer NOT NULL,
    language character varying(5) DEFAULT 'en'::character varying NOT NULL,
    question text NOT NULL,
    opt1 text,
    opt2 text,
    opt3 text,
    opt4 text,
    opt5 text,
    opt6 text,
    opt7 text,
    opt8 text,
    modified timestamp(0) with time zone DEFAULT now() NOT NULL,
    CONSTRAINT survey_questions_language CHECK (((language)::text ~ '[a-z]{2}(-[a-z]{2})?'::text))
);


ALTER TABLE public.survey_questions OWNER TO "186_pgsql";

--
-- Name: surveys; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE surveys (
    id integer DEFAULT nextval(('"survey_id_seq"'::text)::regclass) NOT NULL,
    tot1 integer DEFAULT 0 NOT NULL,
    tot2 integer DEFAULT 0 NOT NULL,
    tot3 integer DEFAULT 0 NOT NULL,
    tot4 integer DEFAULT 0 NOT NULL,
    tot5 integer DEFAULT 0 NOT NULL,
    tot6 integer DEFAULT 0 NOT NULL,
    tot7 integer DEFAULT 0 NOT NULL,
    tot8 integer DEFAULT 0 NOT NULL,
    respondants integer DEFAULT 0 NOT NULL,
    current boolean DEFAULT false NOT NULL
);


ALTER TABLE public.surveys OWNER TO "186_pgsql";

SET default_with_oids = false;

--
-- Name: sync_log; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE sync_log (
    t timestamp with time zone DEFAULT now() NOT NULL,
    op character varying(8) NOT NULL,
    node character varying(32) NOT NULL,
    start timestamp with time zone NOT NULL
);


ALTER TABLE public.sync_log OWNER TO "186_pgsql";

--
-- Name: sync_request; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE sync_request (
    t timestamp with time zone DEFAULT now() NOT NULL,
    docs integer DEFAULT 0 NOT NULL,
    ftp integer DEFAULT 0 NOT NULL,
    requestby character varying(32) NOT NULL,
    completed timestamp with time zone
);


ALTER TABLE public.sync_request OWNER TO "186_pgsql";

--
-- Name: users; Type: TABLE; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE TABLE users (
    userid character varying(16) NOT NULL,
    fullname character varying(128) NOT NULL,
    authorblurb text DEFAULT ''::character varying NOT NULL,
    email character varying(128) NOT NULL,
    communitydoc_superuser integer DEFAULT 0 NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    lastlogin timestamp with time zone,
    matrixeditor integer DEFAULT 0 NOT NULL,
    pwdhash text NOT NULL,
    resethash text,
    resethashtime timestamp with time zone,
    sshkey text,
    sshkey_last_update timestamp with time zone
);


ALTER TABLE public.users OWNER TO "186_pgsql";

--
-- Name: users_keys; Type: VIEW; Schema: public; Owner: 186_pgsql
--

CREATE VIEW users_keys AS
    SELECT users.userid, users.sshkey, users.sshkey_last_update FROM users WHERE ((users.sshkey IS NOT NULL) AND (NOT (users.sshkey = ''::text)));


ALTER TABLE public.users_keys OWNER TO "186_pgsql";

SET search_path = pgcrypto, pg_catalog;

--
-- Name: armor(bytea); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION armor(bytea) RETURNS text
    AS '$libdir/pgcrypto', 'pg_armor'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.armor(bytea) OWNER TO postgres;

--
-- Name: crypt(text, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION crypt(text, text) RETURNS text
    AS '$libdir/pgcrypto', 'pg_crypt'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.crypt(text, text) OWNER TO postgres;

--
-- Name: dearmor(text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION dearmor(text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pg_dearmor'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.dearmor(text) OWNER TO postgres;

--
-- Name: decrypt(bytea, bytea, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION decrypt(bytea, bytea, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pg_decrypt'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.decrypt(bytea, bytea, text) OWNER TO postgres;

--
-- Name: decrypt_iv(bytea, bytea, bytea, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION decrypt_iv(bytea, bytea, bytea, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pg_decrypt_iv'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.decrypt_iv(bytea, bytea, bytea, text) OWNER TO postgres;

--
-- Name: digest(text, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION digest(text, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pg_digest'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.digest(text, text) OWNER TO postgres;

--
-- Name: digest(bytea, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION digest(bytea, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pg_digest'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.digest(bytea, text) OWNER TO postgres;

--
-- Name: encrypt(bytea, bytea, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION encrypt(bytea, bytea, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pg_encrypt'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.encrypt(bytea, bytea, text) OWNER TO postgres;

--
-- Name: encrypt_iv(bytea, bytea, bytea, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION encrypt_iv(bytea, bytea, bytea, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pg_encrypt_iv'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.encrypt_iv(bytea, bytea, bytea, text) OWNER TO postgres;

--
-- Name: gen_random_bytes(integer); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION gen_random_bytes(integer) RETURNS bytea
    AS '$libdir/pgcrypto', 'pg_random_bytes'
    LANGUAGE c STRICT;


ALTER FUNCTION pgcrypto.gen_random_bytes(integer) OWNER TO postgres;

--
-- Name: gen_salt(text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION gen_salt(text) RETURNS text
    AS '$libdir/pgcrypto', 'pg_gen_salt'
    LANGUAGE c STRICT;


ALTER FUNCTION pgcrypto.gen_salt(text) OWNER TO postgres;

--
-- Name: gen_salt(text, integer); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION gen_salt(text, integer) RETURNS text
    AS '$libdir/pgcrypto', 'pg_gen_salt_rounds'
    LANGUAGE c STRICT;


ALTER FUNCTION pgcrypto.gen_salt(text, integer) OWNER TO postgres;

--
-- Name: hmac(text, text, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION hmac(text, text, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pg_hmac'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.hmac(text, text, text) OWNER TO postgres;

--
-- Name: hmac(bytea, bytea, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION hmac(bytea, bytea, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pg_hmac'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.hmac(bytea, bytea, text) OWNER TO postgres;

--
-- Name: pgp_key_id(bytea); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_key_id(bytea) RETURNS text
    AS '$libdir/pgcrypto', 'pgp_key_id_w'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.pgp_key_id(bytea) OWNER TO postgres;

--
-- Name: pgp_pub_decrypt(bytea, bytea); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_pub_decrypt(bytea, bytea) RETURNS text
    AS '$libdir/pgcrypto', 'pgp_pub_decrypt_text'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.pgp_pub_decrypt(bytea, bytea) OWNER TO postgres;

--
-- Name: pgp_pub_decrypt(bytea, bytea, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_pub_decrypt(bytea, bytea, text) RETURNS text
    AS '$libdir/pgcrypto', 'pgp_pub_decrypt_text'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.pgp_pub_decrypt(bytea, bytea, text) OWNER TO postgres;

--
-- Name: pgp_pub_decrypt(bytea, bytea, text, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_pub_decrypt(bytea, bytea, text, text) RETURNS text
    AS '$libdir/pgcrypto', 'pgp_pub_decrypt_text'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.pgp_pub_decrypt(bytea, bytea, text, text) OWNER TO postgres;

--
-- Name: pgp_pub_decrypt_bytea(bytea, bytea); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_pub_decrypt_bytea(bytea, bytea) RETURNS bytea
    AS '$libdir/pgcrypto', 'pgp_pub_decrypt_bytea'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.pgp_pub_decrypt_bytea(bytea, bytea) OWNER TO postgres;

--
-- Name: pgp_pub_decrypt_bytea(bytea, bytea, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_pub_decrypt_bytea(bytea, bytea, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pgp_pub_decrypt_bytea'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.pgp_pub_decrypt_bytea(bytea, bytea, text) OWNER TO postgres;

--
-- Name: pgp_pub_decrypt_bytea(bytea, bytea, text, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_pub_decrypt_bytea(bytea, bytea, text, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pgp_pub_decrypt_bytea'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.pgp_pub_decrypt_bytea(bytea, bytea, text, text) OWNER TO postgres;

--
-- Name: pgp_pub_encrypt(text, bytea); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_pub_encrypt(text, bytea) RETURNS bytea
    AS '$libdir/pgcrypto', 'pgp_pub_encrypt_text'
    LANGUAGE c STRICT;


ALTER FUNCTION pgcrypto.pgp_pub_encrypt(text, bytea) OWNER TO postgres;

--
-- Name: pgp_pub_encrypt(text, bytea, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_pub_encrypt(text, bytea, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pgp_pub_encrypt_text'
    LANGUAGE c STRICT;


ALTER FUNCTION pgcrypto.pgp_pub_encrypt(text, bytea, text) OWNER TO postgres;

--
-- Name: pgp_pub_encrypt_bytea(bytea, bytea); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_pub_encrypt_bytea(bytea, bytea) RETURNS bytea
    AS '$libdir/pgcrypto', 'pgp_pub_encrypt_bytea'
    LANGUAGE c STRICT;


ALTER FUNCTION pgcrypto.pgp_pub_encrypt_bytea(bytea, bytea) OWNER TO postgres;

--
-- Name: pgp_pub_encrypt_bytea(bytea, bytea, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_pub_encrypt_bytea(bytea, bytea, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pgp_pub_encrypt_bytea'
    LANGUAGE c STRICT;


ALTER FUNCTION pgcrypto.pgp_pub_encrypt_bytea(bytea, bytea, text) OWNER TO postgres;

--
-- Name: pgp_sym_decrypt(bytea, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_sym_decrypt(bytea, text) RETURNS text
    AS '$libdir/pgcrypto', 'pgp_sym_decrypt_text'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.pgp_sym_decrypt(bytea, text) OWNER TO postgres;

--
-- Name: pgp_sym_decrypt(bytea, text, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_sym_decrypt(bytea, text, text) RETURNS text
    AS '$libdir/pgcrypto', 'pgp_sym_decrypt_text'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.pgp_sym_decrypt(bytea, text, text) OWNER TO postgres;

--
-- Name: pgp_sym_decrypt_bytea(bytea, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_sym_decrypt_bytea(bytea, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pgp_sym_decrypt_bytea'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.pgp_sym_decrypt_bytea(bytea, text) OWNER TO postgres;

--
-- Name: pgp_sym_decrypt_bytea(bytea, text, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_sym_decrypt_bytea(bytea, text, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pgp_sym_decrypt_bytea'
    LANGUAGE c IMMUTABLE STRICT;


ALTER FUNCTION pgcrypto.pgp_sym_decrypt_bytea(bytea, text, text) OWNER TO postgres;

--
-- Name: pgp_sym_encrypt(text, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_sym_encrypt(text, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pgp_sym_encrypt_text'
    LANGUAGE c STRICT;


ALTER FUNCTION pgcrypto.pgp_sym_encrypt(text, text) OWNER TO postgres;

--
-- Name: pgp_sym_encrypt(text, text, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_sym_encrypt(text, text, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pgp_sym_encrypt_text'
    LANGUAGE c STRICT;


ALTER FUNCTION pgcrypto.pgp_sym_encrypt(text, text, text) OWNER TO postgres;

--
-- Name: pgp_sym_encrypt_bytea(bytea, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_sym_encrypt_bytea(bytea, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pgp_sym_encrypt_bytea'
    LANGUAGE c STRICT;


ALTER FUNCTION pgcrypto.pgp_sym_encrypt_bytea(bytea, text) OWNER TO postgres;

--
-- Name: pgp_sym_encrypt_bytea(bytea, text, text); Type: FUNCTION; Schema: pgcrypto; Owner: postgres
--

CREATE FUNCTION pgp_sym_encrypt_bytea(bytea, text, text) RETURNS bytea
    AS '$libdir/pgcrypto', 'pgp_sym_encrypt_bytea'
    LANGUAGE c STRICT;


ALTER FUNCTION pgcrypto.pgp_sym_encrypt_bytea(bytea, text, text) OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- Name: clickstats_platform(character varying); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION clickstats_platform(character varying) RETURNS character varying
    AS $_$
   SELECT CASE
    WHEN $1 LIKE '%projects/%' THEN 'Projects'
    WHEN $1 LIKE '%.sig' OR $1 LIKE '%.md5' THEN 'Signature'
    WHEN $1 LIKE '%binary/%/win32/%.zip' THEN 'Win32'
    WHEN $1 LIKE '%binary/%.rpm' THEN 'Linux binary'
    WHEN $1 LIKE '%source%' OR $1 LIKE '%/latest/%' THEN 'Source'
    WHEN $1 LIKE '%psqlodbc%' THEN 'ODBC'
    WHEN $1 LIKE '%pgadmin%' THEN 'pgAdmin'
    ELSE 'Other' END
$_$
    LANGUAGE sql;


ALTER FUNCTION public.clickstats_platform(character varying) OWNER TO "186_pgsql";

--
-- Name: clickstats_version(character varying); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION clickstats_version(character varying) RETURNS character varying
    AS $_$
   SELECT CASE
    WHEN $1 LIKE '%.sig' OR $1 LIKE '%.md5' THEN NULL
    WHEN $1 LIKE '%odbc%' OR $1 LIKE '%pgadmin%' OR $1 LIKE '%projects/%' THEN NULL
    WHEN $1 NOT LIKE '%.tar.%' AND $1 NOT LIKE '%.zip' AND $1 NOT LIKE '%.rpm' THEN NULL
    ELSE substring($1, '([678]\\.\\d+)\\.\\d+')
   END
$_$
    LANGUAGE sql;


ALTER FUNCTION public.clickstats_version(character varying) OWNER TO "186_pgsql";

--
-- Name: community_login(text, text); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION community_login(INOUT userid text, password text, OUT success integer, OUT fullname text, OUT email text, OUT authorblurb text, OUT communitydoc_superuser integer, OUT last_login timestamp with time zone, OUT matrixeditor integer) RETURNS record
    AS $$
BEGIN
   SELECT users.userid,users.fullname,users.email,users.authorblurb,users.communitydoc_superuser,users.lastlogin,users.matrixeditor
     INTO userid,fullname,email,authorblurb,communitydoc_superuser,last_login,matrixeditor
     FROM users WHERE lower(users.userid)=lower(userid) AND 
     substring(users.pwdhash, 30) = pgcrypto.crypt(password, substring(users.pwdhash, 1, 29));
-- bf salts are always 29 chars!
   IF FOUND THEN
      success := 1;
      UPDATE users SET lastlogin=CURRENT_TIMESTAMP WHERE users.userid=userid;
   ELSE
      success := 0;
   END IF;
END
$$
    LANGUAGE plpgsql SECURITY DEFINER;


ALTER FUNCTION public.community_login(INOUT userid text, password text, OUT success integer, OUT fullname text, OUT email text, OUT authorblurb text, OUT communitydoc_superuser integer, OUT last_login timestamp with time zone, OUT matrixeditor integer) OWNER TO "186_pgsql";

--
-- Name: community_login_create(text, text, text, text); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION community_login_create(_userid text, _password text, _fullname text, _email text) RETURNS boolean
    AS $$
BEGIN
   IF _userid IS NULL OR _userid='' OR _password IS NULL OR _password='' OR _email IS NULL OR _email='' THEN
      RETURN false;
   END IF;

   PERFORM userid FROM users WHERE lower(userid)=lower(_userid) OR lower(email)=lower(_email);
   IF FOUND THEN
      RETURN false;
   END IF;
   INSERT INTO users (userid,pwdhash,fullname,email)
    VALUES (_userid,community_login_make_password(_password),_fullname,_email);
   RETURN true;
END;
$$
    LANGUAGE plpgsql SECURITY DEFINER;


ALTER FUNCTION public.community_login_create(_userid text, _password text, _fullname text, _email text) OWNER TO "186_pgsql";

--
-- Name: community_login_exists(text); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION community_login_exists(userid text) RETURNS boolean
    AS $$
BEGIN
   PERFORM userid
     FROM users WHERE lower(users.userid)=lower(userid);
   IF FOUND THEN
      RETURN true;
   END IF;
   
   RETURN false;
END
$$
    LANGUAGE plpgsql SECURITY DEFINER;


ALTER FUNCTION public.community_login_exists(userid text) OWNER TO "186_pgsql";

--
-- Name: community_login_make_password(text); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION community_login_make_password(pwd text) RETURNS text
    AS $$
DECLARE
   salt text;
   i int;
BEGIN
   -- Note: bf salts are always 29 characters
   salt = pgcrypto.gen_salt('bf');
   RETURN salt || pgcrypto.crypt(pwd, salt);
END
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.community_login_make_password(pwd text) OWNER TO "186_pgsql";

--
-- Name: community_login_setinfo(text, text, text, text, text); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION community_login_setinfo(_userid text, _password text, _fullname text, _email text, _authorblurb text) RETURNS boolean
    AS $$
BEGIN
   IF _userid IS NULL OR _userid='' OR _email IS NULL OR _email='' THEN
      RETURN false;
   END IF;

   IF _password IS NOT NULL AND NOT _password='' THEN
      UPDATE users SET fullname=_fullname,email=_email,authorblurb=_authorblurb,
                       pwdhash=community_login_make_password(_password)
         WHERE lower(userid)=lower(_userid);
   ELSE
      UPDATE users SET fullname=_fullname,email=_email,authorblurb=_authorblurb
         WHERE lower(userid)=lower(_userid);
   END IF;
   RETURN FOUND;
END;
$$
    LANGUAGE plpgsql SECURITY DEFINER;


ALTER FUNCTION public.community_login_setinfo(_userid text, _password text, _fullname text, _email text, _authorblurb text) OWNER TO "186_pgsql";

--
-- Name: community_login_setinfo(text, text, text, text); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION community_login_setinfo(_userid text, _password text, _fullname text, _email text) RETURNS boolean
    AS $$
BEGIN
   IF _userid IS NULL OR _userid='' OR _email IS NULL OR _email='' THEN
      RETURN false;
   END IF;

   IF _password IS NOT NULL AND NOT _password='' THEN
      UPDATE users SET fullname=_fullname,email=_email,
                       pwdhash=community_login_make_password(_password)
         WHERE lower(userid)=lower(_userid);
   ELSE
      UPDATE users SET fullname=_fullname,email=_email
         WHERE lower(userid)=lower(_userid);
   END IF;
   RETURN FOUND;
END;
$$
    LANGUAGE plpgsql SECURITY DEFINER;


ALTER FUNCTION public.community_login_setinfo(_userid text, _password text, _fullname text, _email text) OWNER TO "186_pgsql";

--
-- Name: community_login_setinfo(text, text, text, text, text, text); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION community_login_setinfo(_userid text, _password text, _fullname text, _email text, _authorblurb text, _sshkey text) RETURNS boolean
    AS $$
BEGIN
   IF _userid IS NULL OR _userid='' OR _email IS NULL OR _email='' THEN
      RETURN false;
   END IF;

   IF _password IS NOT NULL AND NOT _password='' THEN
      UPDATE users SET fullname=_fullname,email=_email,authorblurb=_authorblurb,
                       pwdhash=community_login_make_password(_password)
         WHERE lower(userid)=lower(_userid);
   ELSE
      UPDATE users SET fullname=_fullname,email=_email,authorblurb=_authorblurb,sshkey=_sshkey
         WHERE lower(userid)=lower(_userid);
   END IF;
   RETURN FOUND;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.community_login_setinfo(_userid text, _password text, _fullname text, _email text, _authorblurb text, _sshkey text) OWNER TO "186_pgsql";

--
-- Name: community_login_setpassword(text, text); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION community_login_setpassword(_userid text, _password text) RETURNS boolean
    AS $$
BEGIN
   IF _password IS NOT NULL AND NOT _password='' THEN
      UPDATE users SET pwdhash=community_login_make_password(_password) WHERE lower(userid)=lower(_userid);
   END IF;
   RETURN FOUND;
END;
$$
    LANGUAGE plpgsql SECURITY DEFINER;


ALTER FUNCTION public.community_login_setpassword(_userid text, _password text) OWNER TO "186_pgsql";

--
-- Name: community_login_trigger_sshkey(); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION community_login_trigger_sshkey() RETURNS trigger
    AS $$
BEGIN
   IF NEW.sshkey IS DISTINCT FROM OLD.sshkey THEN
      NEW.sshkey_last_update=CURRENT_TIMESTAMP;
   END IF;
   RETURN NEW;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.community_login_trigger_sshkey() OWNER TO "186_pgsql";

--
-- Name: communitypages_approve(integer); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION communitypages_approve(integer) RETURNS integer
    AS $_$
   DECLARE lastver int;
   BEGIN
      SELECT INTO lastver version FROM communitypages WHERE pageid=$1;
      IF NOT FOUND THEN
         -- Completley new page, add it
         INSERT INTO communitypages (pageid, parent, author, origauthor, savedate, title, shorttitle, contents) SELECT pageid,parent,author,author,savedate,title,shorttitle,contents FROM communitypages_work WHERE pageid=$1;
      ELSE
         -- Pre-existing page, copy to history and then overwrite
         INSERT INTO communitypages_history (pageid, version, author, savedate, title, shorttitle, contents) SELECT pageid,version,author,savedate,title,shorttitle,contents FROM communitypages WHERE pageid=$1;
         UPDATE communitypages SET version=version+1,author=w.author,savedate=w.savedate,title=w.title,shorttitle=w.shorttitle,contents=w.contents FROM communitypages_work w WHERE w.pageid=$1 AND communitypages.pageid=$1;
      END IF;
      -- Transfer any new files
      INSERT INTO communitypages_files (fileid, pageid, title, author, mimetype, image) SELECT fileid, pageid, title, author, mimetype, image FROM communitypages_work_files WHERE pageid=$1;
      DELETE FROM communitypages_work_files WHERE pageid=$1;
      -- Now remove from the pending table
      DELETE FROM communitypages_work WHERE pageid=$1;
      -- Placeholder return value
      RETURN 0;
   END;
$_$
    LANGUAGE plpgsql;


ALTER FUNCTION public.communitypages_approve(integer) OWNER TO "186_pgsql";

--
-- Name: communitypages_breadcrumbs(integer); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION communitypages_breadcrumbs(integer) RETURNS SETOF communitypages_breadcrumb_type
    AS $_$
   DECLARE r communitypages_breadcrumb_type%rowtype;
   DECLARE currpageid int;
   DECLARE currparent int;
   BEGIN
      currpageid := $1;
      LOOP
         SELECT INTO r.pageid,currparent,r.title pageid,parent,shorttitle FROM communitypages WHERE pageid=currpageid;
         EXIT WHEN NOT FOUND;
         RETURN NEXT r;
         EXIT WHEN r.pageid = currparent;
         currpageid = currparent;
      END LOOP;
      RETURN;
   END;
$_$
    LANGUAGE plpgsql;


ALTER FUNCTION public.communitypages_breadcrumbs(integer) OWNER TO "186_pgsql";

--
-- Name: communitypages_findroot(integer); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION communitypages_findroot(integer) RETURNS integer
    AS $_$
   DECLARE currpageid int;
   DECLARE currparent int;
   BEGIN
      currpageid := $1;
      LOOP
         SELECT INTO currparent parent FROM communitypages WHERE pageid=currpageid;
         IF NOT FOUND THEN
            SELECT INTO currparent parent FROM communitypages_work WHERE pageid=currpageid;
            IF NOT FOUND THEN
                RETURN NULL;
            END IF;
         END IF;
         IF currparent=currpageid THEN
            RETURN currpageid;
         END IF;
         currpageid = currparent;
      END LOOP;
      RETURN NULL;
   END;
$_$
    LANGUAGE plpgsql;


ALTER FUNCTION public.communitypages_findroot(integer) OWNER TO "186_pgsql";

--
-- Name: event_translation_modified(); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION event_translation_modified() RETURNS trigger
    AS $$
    BEGIN
        IF NEW.language = 'en' THEN
            IF NEW.event <> OLD.event OR NEW.summary <> OLD.summary OR NEW.details <> OLD.details THEN
                NEW.modified := 'now';
            END IF;
        ELSE
            NEW.modified := 'now';
        END IF;
        RETURN NEW;
    END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.event_translation_modified() OWNER TO "186_pgsql";

--
-- Name: news_translation_modified(); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION news_translation_modified() RETURNS trigger
    AS $$
    BEGIN
        IF NEW.language = 'en' THEN
            IF NEW.headline <> OLD.headline OR NEW.summary <> OLD.summary OR NEW.story <> OLD.story THEN
                NEW.modified := 'now';
            END IF;
        ELSE
            NEW.modified := 'now';
        END IF;
        RETURN NEW;
    END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.news_translation_modified() OWNER TO "186_pgsql";

--
-- Name: plpgsql_call_handler(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION plpgsql_call_handler() RETURNS language_handler
    AS '$libdir/plpgsql', 'plpgsql_call_handler'
    LANGUAGE c;


ALTER FUNCTION public.plpgsql_call_handler() OWNER TO postgres;

--
-- Name: quotes_translation_modified(); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION quotes_translation_modified() RETURNS trigger
    AS $$
 BEGIN
        IF NEW.language = 'en' THEN
            IF NEW.quote <> OLD.quote OR NEW.tagline <> OLD.tagline THEN
                NEW.modified := 'now';
            END IF;
        ELSE
            NEW.modified := 'now';
        END IF;
        RETURN NEW;
    END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.quotes_translation_modified() OWNER TO "186_pgsql";

--
-- Name: survey_translation_modified(); Type: FUNCTION; Schema: public; Owner: 186_pgsql
--

CREATE FUNCTION survey_translation_modified() RETURNS trigger
    AS $$
    BEGIN
        IF NEW.language = 'en' THEN
            IF NEW.question <> OLD.question OR COALESCE(NEW.opt1, '') <> COALESCE(OLD.opt1, '') OR
               COALESCE(NEW.opt2, '') <> COALESCE(OLD.opt2, '') OR COALESCE(NEW.opt3, '') <> COALESCE(OLD.opt3, '') OR
               COALESCE(NEW.opt4, '') <> COALESCE(OLD.opt4, '') OR COALESCE(NEW.opt5, '') <> COALESCE(OLD.opt5, '') OR
               COALESCE(NEW.opt6, '') <> COALESCE(OLD.opt6, '') OR COALESCE(NEW.opt7, '') <> COALESCE(OLD.opt7, '') OR
               COALESCE(NEW.opt8, '') <> COALESCE(OLD.opt8, '') THEN

                NEW.modified := 'now';
            END IF;
        ELSE
            NEW.modified := 'now';
        END IF;
        RETURN NEW;
    END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.survey_translation_modified() OWNER TO "186_pgsql";

SET search_path = cvslog, pg_catalog;

--
-- Name: branches_branchid_seq; Type: SEQUENCE; Schema: cvslog; Owner: postgres
--

CREATE SEQUENCE branches_branchid_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE cvslog.branches_branchid_seq OWNER TO postgres;

--
-- Name: branches_branchid_seq; Type: SEQUENCE OWNED BY; Schema: cvslog; Owner: postgres
--

ALTER SEQUENCE branches_branchid_seq OWNED BY branches.branchid;


--
-- Name: commits_commitid_seq; Type: SEQUENCE; Schema: cvslog; Owner: postgres
--

CREATE SEQUENCE commits_commitid_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE cvslog.commits_commitid_seq OWNER TO postgres;

--
-- Name: commits_commitid_seq; Type: SEQUENCE OWNED BY; Schema: cvslog; Owner: postgres
--

ALTER SEQUENCE commits_commitid_seq OWNED BY commits.commitid;


--
-- Name: files_fileid_seq; Type: SEQUENCE; Schema: cvslog; Owner: postgres
--

CREATE SEQUENCE files_fileid_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE cvslog.files_fileid_seq OWNER TO postgres;

--
-- Name: files_fileid_seq; Type: SEQUENCE OWNED BY; Schema: cvslog; Owner: postgres
--

ALTER SEQUENCE files_fileid_seq OWNED BY files.fileid;


SET search_path = public, pg_catalog;

--
-- Name: bug_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE bug_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.bug_id_seq OWNER TO "186_pgsql";

--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE comments_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.comments_id_seq OWNER TO "186_pgsql";

--
-- Name: communitypages_files_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE communitypages_files_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.communitypages_files_id_seq OWNER TO "186_pgsql";

--
-- Name: communitypages_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE communitypages_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.communitypages_id_seq OWNER TO "186_pgsql";

--
-- Name: countries_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.countries_id_seq OWNER TO "186_pgsql";

--
-- Name: countries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: 186_pgsql
--

ALTER SEQUENCE countries_id_seq OWNED BY countries.id;


--
-- Name: docs_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE docs_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.docs_id_seq OWNER TO "186_pgsql";

--
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE events_id_seq
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.events_id_seq OWNER TO "186_pgsql";

--
-- Name: features_features_featureid_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE features_features_featureid_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.features_features_featureid_seq OWNER TO "186_pgsql";

--
-- Name: features_features_featureid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: 186_pgsql
--

ALTER SEQUENCE features_features_featureid_seq OWNED BY features_features.featureid;


--
-- Name: features_groups_groupid_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE features_groups_groupid_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.features_groups_groupid_seq OWNER TO "186_pgsql";

--
-- Name: features_groups_groupid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: 186_pgsql
--

ALTER SEQUENCE features_groups_groupid_seq OWNED BY features_groups.groupid;


--
-- Name: features_versions_versionid_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE features_versions_versionid_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.features_versions_versionid_seq OWNER TO "186_pgsql";

--
-- Name: features_versions_versionid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: 186_pgsql
--

ALTER SEQUENCE features_versions_versionid_seq OWNED BY features_versions.versionid;


--
-- Name: iptocountry_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE iptocountry_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.iptocountry_id_seq OWNER TO "186_pgsql";

--
-- Name: mirrors_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE mirrors_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.mirrors_id_seq OWNER TO "186_pgsql";

--
-- Name: news_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE news_id_seq
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.news_id_seq OWNER TO "186_pgsql";

--
-- Name: organisations_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE organisations_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.organisations_id_seq OWNER TO "186_pgsql";

--
-- Name: organisations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: 186_pgsql
--

ALTER SEQUENCE organisations_id_seq OWNED BY organisations.id;


--
-- Name: product_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE product_categories_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.product_categories_id_seq OWNER TO "186_pgsql";

--
-- Name: product_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: 186_pgsql
--

ALTER SEQUENCE product_categories_id_seq OWNED BY product_categories.id;


--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE products_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO "186_pgsql";

--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: 186_pgsql
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- Name: profserv_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE profserv_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.profserv_id_seq OWNER TO "186_pgsql";

--
-- Name: profserv_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: 186_pgsql
--

ALTER SEQUENCE profserv_id_seq OWNED BY profserv.id;


--
-- Name: quotes_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE quotes_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.quotes_id_seq OWNER TO "186_pgsql";

--
-- Name: quotes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: 186_pgsql
--

ALTER SEQUENCE quotes_id_seq OWNED BY quotes.id;


--
-- Name: survey_id_seq; Type: SEQUENCE; Schema: public; Owner: 186_pgsql
--

CREATE SEQUENCE survey_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.survey_id_seq OWNER TO "186_pgsql";

SET search_path = cvslog, pg_catalog;

--
-- Name: branchid; Type: DEFAULT; Schema: cvslog; Owner: postgres
--

ALTER TABLE branches ALTER COLUMN branchid SET DEFAULT nextval('branches_branchid_seq'::regclass);


--
-- Name: commitid; Type: DEFAULT; Schema: cvslog; Owner: postgres
--

ALTER TABLE commits ALTER COLUMN commitid SET DEFAULT nextval('commits_commitid_seq'::regclass);


--
-- Name: fileid; Type: DEFAULT; Schema: cvslog; Owner: postgres
--

ALTER TABLE files ALTER COLUMN fileid SET DEFAULT nextval('files_fileid_seq'::regclass);


SET search_path = public, pg_catalog;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE clickthrus ALTER COLUMN id SET DEFAULT nextval('clickthrus_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE countries ALTER COLUMN id SET DEFAULT nextval('countries_id_seq'::regclass);


--
-- Name: featureid; Type: DEFAULT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE features_features ALTER COLUMN featureid SET DEFAULT nextval('features_features_featureid_seq'::regclass);


--
-- Name: groupid; Type: DEFAULT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE features_groups ALTER COLUMN groupid SET DEFAULT nextval('features_groups_groupid_seq'::regclass);


--
-- Name: versionid; Type: DEFAULT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE features_versions ALTER COLUMN versionid SET DEFAULT nextval('features_versions_versionid_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE organisations ALTER COLUMN id SET DEFAULT nextval('organisations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE product_categories ALTER COLUMN id SET DEFAULT nextval('product_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE profserv ALTER COLUMN id SET DEFAULT nextval('profserv_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE quotes ALTER COLUMN id SET DEFAULT nextval('quotes_id_seq'::regclass);


SET search_path = cvslog, pg_catalog;

--
-- Name: branches_pkey; Type: CONSTRAINT; Schema: cvslog; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY branches
    ADD CONSTRAINT branches_pkey PRIMARY KEY (branchid);


--
-- Name: commits_pkey; Type: CONSTRAINT; Schema: cvslog; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY commits
    ADD CONSTRAINT commits_pkey PRIMARY KEY (commitid);


--
-- Name: files_pkey; Type: CONSTRAINT; Schema: cvslog; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_pkey PRIMARY KEY (fileid);


SET search_path = public, pg_catalog;

--
-- Name: applications_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY applications
    ADD CONSTRAINT applications_pkey PRIMARY KEY (id, version, platform);


--
-- Name: clickthrus2_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY clickthrus2
    ADD CONSTRAINT clickthrus2_pkey PRIMARY KEY (id);


--
-- Name: clickthrus_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY clickthrus
    ADD CONSTRAINT clickthrus_pkey PRIMARY KEY (id);


--
-- Name: comments_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: communitypages_files_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY communitypages_files
    ADD CONSTRAINT communitypages_files_pkey PRIMARY KEY (fileid);


--
-- Name: communitypages_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY communitypages
    ADD CONSTRAINT communitypages_pkey PRIMARY KEY (pageid);


--
-- Name: communitypages_root_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY communitypages_root
    ADD CONSTRAINT communitypages_root_pkey PRIMARY KEY (pageid);


--
-- Name: communitypages_work_files_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY communitypages_work_files
    ADD CONSTRAINT communitypages_work_files_pkey PRIMARY KEY (fileid);


--
-- Name: communitypages_work_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY communitypages_work
    ADD CONSTRAINT communitypages_work_pkey PRIMARY KEY (pageid);


--
-- Name: countries_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (id);


--
-- Name: developers_email_key; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY developers
    ADD CONSTRAINT developers_email_key UNIQUE (email);


--
-- Name: developers_name_key; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY developers
    ADD CONSTRAINT developers_name_key UNIQUE (lastname, firstname);


--
-- Name: developers_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY developers
    ADD CONSTRAINT developers_pkey PRIMARY KEY (id);


--
-- Name: developers_types_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY developers_types
    ADD CONSTRAINT developers_types_pkey PRIMARY KEY (type);


--
-- Name: doc_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY docs
    ADD CONSTRAINT doc_pkey PRIMARY KEY (id);


--
-- Name: events_location_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY events_location
    ADD CONSTRAINT events_location_pkey PRIMARY KEY (eventid);


--
-- Name: events_text_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY events_text
    ADD CONSTRAINT events_text_pkey PRIMARY KEY (eventid, language);


--
-- Name: features_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY features_categories
    ADD CONSTRAINT features_categories_pkey PRIMARY KEY (categoryid);


--
-- Name: features_features_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY features_features
    ADD CONSTRAINT features_features_pkey PRIMARY KEY (featureid);


--
-- Name: features_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY features_groups
    ADD CONSTRAINT features_groups_pkey PRIMARY KEY (groupid);


--
-- Name: features_matrix_pk; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY features_matrix
    ADD CONSTRAINT features_matrix_pk PRIMARY KEY (version, feature);


--
-- Name: features_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY features_versions
    ADD CONSTRAINT features_versions_pkey PRIMARY KEY (versionid);


--
-- Name: frontends_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY frontends
    ADD CONSTRAINT frontends_pkey PRIMARY KEY (id);


--
-- Name: iptocountry_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY iptocountry
    ADD CONSTRAINT iptocountry_pkey PRIMARY KEY (id);


--
-- Name: listgroups_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY listgroups
    ADD CONSTRAINT listgroups_pkey PRIMARY KEY (id);


--
-- Name: lists_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY lists
    ADD CONSTRAINT lists_pkey PRIMARY KEY (id);


--
-- Name: mirrors_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY mirrors
    ADD CONSTRAINT mirrors_pkey PRIMARY KEY (id);


--
-- Name: news_text_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY news_text
    ADD CONSTRAINT news_text_pkey PRIMARY KEY (newsid, language);


--
-- Name: organisations_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY organisations
    ADD CONSTRAINT organisations_pkey PRIMARY KEY (id);


--
-- Name: product_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY product_categories
    ADD CONSTRAINT product_categories_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: profserv_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY profserv
    ADD CONSTRAINT profserv_pkey PRIMARY KEY (id);


--
-- Name: quotes_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY quotes
    ADD CONSTRAINT quotes_pkey PRIMARY KEY (id);


--
-- Name: quotes_text_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY quotes_text
    ADD CONSTRAINT quotes_text_pkey PRIMARY KEY (quoteid, language);


--
-- Name: survey_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY survey_questions
    ADD CONSTRAINT survey_questions_pkey PRIMARY KEY (surveyid, language);


--
-- Name: sync_log_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY sync_log
    ADD CONSTRAINT sync_log_pkey PRIMARY KEY (t);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: 186_pgsql; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (userid);


SET search_path = cvslog, pg_catalog;

--
-- Name: commits_branch_time; Type: INDEX; Schema: cvslog; Owner: postgres; Tablespace: 
--

CREATE INDEX commits_branch_time ON commits USING btree (branch, "time");


SET search_path = public, pg_catalog;

--
-- Name: clickthrus2_ts_idx; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE INDEX clickthrus2_ts_idx ON clickthrus2 USING btree (ts);


--
-- Name: clickthrus_ts_idx; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE INDEX clickthrus_ts_idx ON clickthrus USING btree (ts);


--
-- Name: comment_rejects_ui_re; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE UNIQUE INDEX comment_rejects_ui_re ON comment_rejects USING btree (re);


--
-- Name: comments_idx; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE INDEX comments_idx ON comments USING btree (version, file);


--
-- Name: doc_idx; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE UNIQUE INDEX doc_idx ON docs USING btree (file, version);


--
-- Name: events_id_key; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE UNIQUE INDEX events_id_key ON events USING btree (id);


--
-- Name: features_features_idx_grp; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE INDEX features_features_idx_grp ON features_features USING btree (groupid);


--
-- Name: features_groups_idx_cat; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE INDEX features_groups_idx_cat ON features_groups USING btree (category);


--
-- Name: features_versions_idx_cat; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE INDEX features_versions_idx_cat ON features_versions USING btree (category);


--
-- Name: lists_name_unique; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE UNIQUE INDEX lists_name_unique ON lists USING btree (name);


--
-- Name: mirrors_hosts_idx; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE UNIQUE INDEX mirrors_hosts_idx ON mirrors USING btree (country_code, mirror_type, mirror_index);


--
-- Name: mirrors_status_idx; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE INDEX mirrors_status_idx ON mirrors USING btree (mirror_type, mirror_last_rsync, rsync_host1, rsync_host2, mirror_active, mirror_dns, mirror_private);


--
-- Name: news_id_key; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE UNIQUE INDEX news_id_key ON news USING btree (id);


--
-- Name: news_idx; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE INDEX news_idx ON news USING btree (active, approved);


--
-- Name: organisations_name; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE UNIQUE INDEX organisations_name ON organisations USING btree (lower(name));


--
-- Name: product_categories_name; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE UNIQUE INDEX product_categories_name ON product_categories USING btree (lower(name));


--
-- Name: products_publisher_name_idx; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE UNIQUE INDEX products_publisher_name_idx ON products USING btree (publisher, lower(name));


--
-- Name: survey_id_key; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE UNIQUE INDEX survey_id_key ON surveys USING btree (id);


--
-- Name: survey_lock_idx; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE UNIQUE INDEX survey_lock_idx ON survey_lock USING btree (ipaddr);


--
-- Name: sync_request_completed; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE INDEX sync_request_completed ON sync_request USING btree (t) WHERE (completed IS NULL);


--
-- Name: sync_request_t; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE INDEX sync_request_t ON sync_request USING btree (t);


--
-- Name: users_email_idx; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE UNIQUE INDEX users_email_idx ON users USING btree (email);


--
-- Name: users_email_unique_idx; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE UNIQUE INDEX users_email_unique_idx ON users USING btree (lower((email)::text));


--
-- Name: users_userid_unique_idx; Type: INDEX; Schema: public; Owner: 186_pgsql; Tablespace: 
--

CREATE UNIQUE INDEX users_userid_unique_idx ON users USING btree (lower((userid)::text));


--
-- Name: community_login_trigger_sshkey; Type: TRIGGER; Schema: public; Owner: 186_pgsql
--

CREATE TRIGGER community_login_trigger_sshkey
    BEFORE UPDATE ON users
    FOR EACH ROW
    EXECUTE PROCEDURE community_login_trigger_sshkey();


--
-- Name: events_text_update; Type: TRIGGER; Schema: public; Owner: 186_pgsql
--

CREATE TRIGGER events_text_update
    BEFORE UPDATE ON events_text
    FOR EACH ROW
    EXECUTE PROCEDURE event_translation_modified();


--
-- Name: news_text_update; Type: TRIGGER; Schema: public; Owner: 186_pgsql
--

CREATE TRIGGER news_text_update
    BEFORE UPDATE ON news_text
    FOR EACH ROW
    EXECUTE PROCEDURE news_translation_modified();


--
-- Name: quotes_text_update; Type: TRIGGER; Schema: public; Owner: 186_pgsql
--

CREATE TRIGGER quotes_text_update
    BEFORE UPDATE ON quotes_text
    FOR EACH ROW
    EXECUTE PROCEDURE quotes_translation_modified();


--
-- Name: survey_questions_update; Type: TRIGGER; Schema: public; Owner: 186_pgsql
--

CREATE TRIGGER survey_questions_update
    BEFORE UPDATE ON survey_questions
    FOR EACH ROW
    EXECUTE PROCEDURE survey_translation_modified();


SET search_path = cvslog, pg_catalog;

--
-- Name: commits_branch_fkey; Type: FK CONSTRAINT; Schema: cvslog; Owner: postgres
--

ALTER TABLE ONLY commits
    ADD CONSTRAINT commits_branch_fkey FOREIGN KEY (branch) REFERENCES branches(branchid);


--
-- Name: files_commitid_fkey; Type: FK CONSTRAINT; Schema: cvslog; Owner: postgres
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_commitid_fkey FOREIGN KEY (commitid) REFERENCES commits(commitid);


SET search_path = public, pg_catalog;

--
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY quotes_text
    ADD CONSTRAINT "$1" FOREIGN KEY (quoteid) REFERENCES quotes(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: communitypages_author_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY communitypages
    ADD CONSTRAINT communitypages_author_fkey FOREIGN KEY (author) REFERENCES users(userid);


--
-- Name: communitypages_files_author_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY communitypages_files
    ADD CONSTRAINT communitypages_files_author_fkey FOREIGN KEY (author) REFERENCES users(userid);


--
-- Name: communitypages_files_pageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY communitypages_files
    ADD CONSTRAINT communitypages_files_pageid_fkey FOREIGN KEY (pageid) REFERENCES communitypages(pageid);


--
-- Name: communitypages_history_author_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY communitypages_history
    ADD CONSTRAINT communitypages_history_author_fkey FOREIGN KEY (author) REFERENCES users(userid);


--
-- Name: communitypages_origauthor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY communitypages
    ADD CONSTRAINT communitypages_origauthor_fkey FOREIGN KEY (origauthor) REFERENCES users(userid);


--
-- Name: communitypages_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY communitypages
    ADD CONSTRAINT communitypages_parent_fkey FOREIGN KEY (parent) REFERENCES communitypages(pageid);


--
-- Name: communitypages_root_pageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY communitypages_root
    ADD CONSTRAINT communitypages_root_pageid_fkey FOREIGN KEY (pageid) REFERENCES communitypages(pageid);


--
-- Name: communitypages_work_author_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY communitypages_work
    ADD CONSTRAINT communitypages_work_author_fkey FOREIGN KEY (author) REFERENCES users(userid);


--
-- Name: communitypages_work_files_author_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY communitypages_work_files
    ADD CONSTRAINT communitypages_work_files_author_fkey FOREIGN KEY (author) REFERENCES users(userid);


--
-- Name: communitypages_work_files_pageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY communitypages_work_files
    ADD CONSTRAINT communitypages_work_files_pageid_fkey FOREIGN KEY (pageid) REFERENCES communitypages_work(pageid);


--
-- Name: communitypages_work_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY communitypages_work
    ADD CONSTRAINT communitypages_work_parent_fkey FOREIGN KEY (parent) REFERENCES communitypages(pageid);


--
-- Name: developers_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY developers
    ADD CONSTRAINT developers_type_fkey FOREIGN KEY (type) REFERENCES developers_types(type);


--
-- Name: events_location_country_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY events_location
    ADD CONSTRAINT events_location_country_fkey FOREIGN KEY (country) REFERENCES countries(id);


--
-- Name: events_location_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY events_location
    ADD CONSTRAINT events_location_fkey FOREIGN KEY (eventid) REFERENCES events(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: events_text_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY events_text
    ADD CONSTRAINT events_text_fkey FOREIGN KEY (eventid) REFERENCES events(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: features_features_groupid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY features_features
    ADD CONSTRAINT features_features_groupid_fkey FOREIGN KEY (groupid) REFERENCES features_groups(groupid);


--
-- Name: features_groups_category_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY features_groups
    ADD CONSTRAINT features_groups_category_fkey FOREIGN KEY (category) REFERENCES features_categories(categoryid);


--
-- Name: features_matrix_feature_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY features_matrix
    ADD CONSTRAINT features_matrix_feature_fkey FOREIGN KEY (feature) REFERENCES features_features(featureid);


--
-- Name: features_matrix_version_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY features_matrix
    ADD CONSTRAINT features_matrix_version_fkey FOREIGN KEY (version) REFERENCES features_versions(versionid);


--
-- Name: features_versions_category_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY features_versions
    ADD CONSTRAINT features_versions_category_fkey FOREIGN KEY (category) REFERENCES features_categories(categoryid);


--
-- Name: lists_grp_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY lists
    ADD CONSTRAINT lists_grp_fkey FOREIGN KEY (grp) REFERENCES listgroups(id);


--
-- Name: news_text_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY news_text
    ADD CONSTRAINT news_text_fkey FOREIGN KEY (newsid) REFERENCES news(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: products_category_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_category_fkey FOREIGN KEY (category) REFERENCES product_categories(id);


--
-- Name: products_contact_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_contact_fkey FOREIGN KEY (contact) REFERENCES users(userid);


--
-- Name: products_publisher_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_publisher_fkey FOREIGN KEY (publisher) REFERENCES organisations(id);


--
-- Name: survey_questions_fkey; Type: FK CONSTRAINT; Schema: public; Owner: 186_pgsql
--

ALTER TABLE ONLY survey_questions
    ADD CONSTRAINT survey_questions_fkey FOREIGN KEY (surveyid) REFERENCES surveys(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: cvslog; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA cvslog FROM PUBLIC;
REVOKE ALL ON SCHEMA cvslog FROM postgres;
GRANT ALL ON SCHEMA cvslog TO postgres;
GRANT USAGE ON SCHEMA cvslog TO cvslog;


--
-- Name: pgcrypto; Type: ACL; Schema: -; Owner: pgsql
--

REVOKE ALL ON SCHEMA pgcrypto FROM PUBLIC;
REVOKE ALL ON SCHEMA pgcrypto FROM pgsql;
GRANT ALL ON SCHEMA pgcrypto TO pgsql;
GRANT USAGE ON SCHEMA pgcrypto TO PUBLIC;


--
-- Name: public; Type: ACL; Schema: -; Owner: pgsql
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM pgsql;
GRANT ALL ON SCHEMA public TO pgsql;
GRANT ALL ON SCHEMA public TO stefan;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


SET search_path = cvslog, pg_catalog;

--
-- Name: branches; Type: ACL; Schema: cvslog; Owner: postgres
--

REVOKE ALL ON TABLE branches FROM PUBLIC;
REVOKE ALL ON TABLE branches FROM postgres;
GRANT ALL ON TABLE branches TO postgres;
GRANT ALL ON TABLE branches TO cvslog;


--
-- Name: commits; Type: ACL; Schema: cvslog; Owner: postgres
--

REVOKE ALL ON TABLE commits FROM PUBLIC;
REVOKE ALL ON TABLE commits FROM postgres;
GRANT ALL ON TABLE commits TO postgres;
GRANT ALL ON TABLE commits TO cvslog;


--
-- Name: files; Type: ACL; Schema: cvslog; Owner: postgres
--

REVOKE ALL ON TABLE files FROM PUBLIC;
REVOKE ALL ON TABLE files FROM postgres;
GRANT ALL ON TABLE files TO postgres;
GRANT ALL ON TABLE files TO cvslog;


SET search_path = public, pg_catalog;

--
-- Name: listgroups; Type: ACL; Schema: public; Owner: 186_pgsql
--

REVOKE ALL ON TABLE listgroups FROM PUBLIC;
REVOKE ALL ON TABLE listgroups FROM "186_pgsql";
GRANT ALL ON TABLE listgroups TO "186_pgsql";
GRANT ALL ON TABLE listgroups TO search;
GRANT SELECT ON TABLE listgroups TO archives;


--
-- Name: lists; Type: ACL; Schema: public; Owner: 186_pgsql
--

REVOKE ALL ON TABLE lists FROM PUBLIC;
REVOKE ALL ON TABLE lists FROM "186_pgsql";
GRANT ALL ON TABLE lists TO "186_pgsql";
GRANT ALL ON TABLE lists TO search;
GRANT SELECT ON TABLE lists TO archives;


--
-- Name: mirrors; Type: ACL; Schema: public; Owner: 186_pgsql
--

REVOKE ALL ON TABLE mirrors FROM PUBLIC;
REVOKE ALL ON TABLE mirrors FROM "186_pgsql";
GRANT ALL ON TABLE mirrors TO "186_pgsql";
GRANT SELECT,UPDATE ON TABLE mirrors TO dns_svc;
GRANT SELECT ON TABLE mirrors TO rsync_svc;


--
-- Name: users; Type: ACL; Schema: public; Owner: 186_pgsql
--

REVOKE ALL ON TABLE users FROM PUBLIC;
REVOKE ALL ON TABLE users FROM "186_pgsql";
GRANT ALL ON TABLE users TO "186_pgsql";


--
-- Name: users_keys; Type: ACL; Schema: public; Owner: 186_pgsql
--

REVOKE ALL ON TABLE users_keys FROM PUBLIC;
REVOKE ALL ON TABLE users_keys FROM "186_pgsql";
GRANT ALL ON TABLE users_keys TO "186_pgsql";
GRANT SELECT ON TABLE users_keys TO auth_svc;


SET search_path = cvslog, pg_catalog;

--
-- Name: branches_branchid_seq; Type: ACL; Schema: cvslog; Owner: postgres
--

REVOKE ALL ON SEQUENCE branches_branchid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE branches_branchid_seq FROM postgres;
GRANT ALL ON SEQUENCE branches_branchid_seq TO postgres;
GRANT ALL ON SEQUENCE branches_branchid_seq TO cvslog;


--
-- Name: commits_commitid_seq; Type: ACL; Schema: cvslog; Owner: postgres
--

REVOKE ALL ON SEQUENCE commits_commitid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE commits_commitid_seq FROM postgres;
GRANT ALL ON SEQUENCE commits_commitid_seq TO postgres;
GRANT ALL ON SEQUENCE commits_commitid_seq TO cvslog;


--
-- Name: files_fileid_seq; Type: ACL; Schema: cvslog; Owner: postgres
--

REVOKE ALL ON SEQUENCE files_fileid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE files_fileid_seq FROM postgres;
GRANT ALL ON SEQUENCE files_fileid_seq TO postgres;
GRANT ALL ON SEQUENCE files_fileid_seq TO cvslog;


--
-- PostgreSQL database dump complete
--

