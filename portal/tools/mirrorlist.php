#! /usr/local/bin/php -c /usr/local/etc/php.ini
<?php

   $file = isset($_SERVER['argv'][1])?$_SERVER['argv'][1]:'mirrors.xml';

   $DB = "host=pgsql74.hub.org port=5432 dbname=186_www user=186_pgsql"; 

   // connect to database
   $db = @pg_pconnect($DB);
   if (isset($php_errormsg) && $php_errormsg != "") {
      echo "Error: Couldn't connect to the database server - " . $php_errormsg . "\n";
      exit;
   }

   // Set the encoding
   pg_exec($db, "SET client_encoding = 'utf-8';");

   // Create the XML doc
   $xw = new xmlWriter();
   $xw->openMemory();
   $xw->startDocument('1.0','UTF-8');

   $xw->startElement ('mirrors');

   // Get the mirrors, and loop round them, adding each one to the doc.
   echo "Querying mirrors...\n";
   $sql = "SELECT * FROM mirrors WHERE mirror_active = TRUE AND mirror_dns = TRUE AND mirror_private = FALSE AND mirror_last_rsync > (now() - '48 Hours'::interval);";
   $rs = @pg_exec($db, $sql);
   if (pg_result_status($rs) != PGSQL_TUPLES_OK && pg_result_status($rs) != PGSQL_COMMAND_OK) {
      echo "Error: Could not query the mirrors table - " . $php_errormsg . "\n";
      exit;
   }
  
   $rows = pg_numrows($rs);
  
   for ($x = 0; $x < $rows; $x++) {
      WriteMirror($xw, $rs, $x, 'ftp');
      if (pg_result($rs, $x, 'alternate_protocol') == 't')
          WriteMirror($xw, $rs, $x, 'http');

   }

   pg_close($db);

   // Close tags
   $xw->endElement(); // mirrors
   $xw->endDtd();

   $fp = fopen($file, "w");
   if (!$fp) {
       echo "Couldn't open the output file: " . $file . ".\n";
       exit;
   }

   if (!fwrite($fp, $xw->outputMemory(true)))
   {
       echo "Couldn't write to output file: " . $file . ".\n";
       fclose($fp);
       exit;
   }

   fclose($fp);
   echo "Wrote " . $x . " mirror records to " . $file . ".\n";


   function WriteMirror($xw, $rs, $x, $proto) {
      $xw->startElement('mirror');

      $xw->writeElement('country', htmlentities(pg_result($rs, $x, "country_name"), ENT_QUOTES, 'UTF-8'));

      $path = pg_result($rs, $x, "host_path");
      if ($path{0} != "/") 
          $path = "/" . $path;
      if ($path{strlen($path)-1} != "/") 
          $path = $path . "/";
      if ($proto == 'http' && pg_result($rs, $x, 'alternate_at_root') == 't')
          $path = '/';
      $xw->writeElement('path', htmlentities($path, ENT_QUOTES, 'UTF-8'));

      $port = pg_result($rs, $x, "host_port");
      if (strlen($port) > 0) 
          $xw->writeElement('port', htmlentities($port, ENT_QUOTES, 'UTF-8'));

      $xw->writeElement('protocol', $proto);

      if (pg_result($rs, $x, "mirror_index") == "0") {
          $xw->writeElement('hostname', htmlentities(pg_result($rs, $x, "mirror_type") . "." . pg_result($rs, $x, "country_code") . ".postgresql.org", ENT_QUOTES, 'UTF-8'));
      } else {
          $xw->writeElement('hostname', htmlentities(pg_result($rs, $x, "mirror_type") . pg_result($rs, $x, "mirror_index")  . "." . pg_result($rs, $x, "country_code") . ".postgresql.org", ENT_QUOTES, 'UTF-8'));
      }

      $xw->endElement(); // mirror
   }
?>

