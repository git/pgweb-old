<?php
	// Locate all directory symlinks in the ftp tree and 
	// generate a .htaccess file to redirect them
	if ($argc != 2) {
		print "Usage: ftpmirror.php <outputfile>\n\n";
		exit(1);
	}

	$_SERVER["SERVER_NAME"] = ""; // To get rid of a warning from settings.php
	require_once 'system/global/settings.php';

	$dirlinks = array();
	$root = rtrim($_SETTINGS['ftp_root'],'/');

	scan_directory($root);

	ksort($dirlinks);

	$f = fopen($argv[1],'w');
	fwrite($f,"Options FollowSymLinks\n");
	fwrite($f,"RewriteEngine On\n");

	foreach ($dirlinks as $src=>$tgt) {
		fwrite($f,"# $src -> $tgt\n");
		if (strpos($src,'/') == FALSE) {
			// No slash -> top level dir -> no change
			$tgtref = $tgt;
		} else {
			$tgtref = substr($src, 0, strrpos($src,'/')) . '/' . $tgt;
		}
		fwrite($f,'RewriteRule ^' . $src .'/?$'. ' /ftp/' .$tgtref . ' [R,L]' . "\n");
	}

	fclose($f);


	function scan_directory($dirname) {
		global $dirlinks,$root;
		$d = opendir($dirname) or die ('Could not open ' . $dirname);
		while ($f = readdir($d)) {
			if ($f=='.' || $f=='..')
				continue;
			if (is_link($dirname . '/' . $f)) {
				$tgt = readlink($dirname . '/' . $f);
				if (is_dir($dirname . '/' . $tgt)) {
					$src=($dirname==$root)?$f:substr($dirname,strlen($root)+1) . '/' . $f;
					$dirlinks[$src] = $tgt;
				}
				continue;
			}
			if (is_dir($dirname . '/' . $f)) {
				// Recurse down subdirectories only if they are not links!
				scan_directory($dirname . '/' . $f);
				continue;
			}
		}
		closedir($d);
	}
?>
