#!/usr/local/bin/bash

SERVER=pgsql74.hub.org
DB=186_www
USER=186_pgsql

if [ "$1" == "" -o "$2" == "" ]; then
   echo Usage: $0 majorversion minorversion
   echo Example: $0 8.3 8.3.3
   exit 1
fi

MAJOR=$1
MINOR=$2

rm -rf $MAJOR
rm -rf tmp

TARBALL="ftp/source/v$MINOR/postgresql-$MINOR.tar.gz"
if [ ! -f $TARBALL ]; then
   echo "Could not find $TARBALL"
   exit 1
fi

mkdir tmp
cd tmp
echo Uncompressing main tarball...
tar xfz ../$TARBALL
cd ..

X=$(find tmp -name postgres.tar.gz |wc -l|sed -e 's/ //g')
if [ "$X" == "0" ]; then
   echo New style tarball with nothing embedded, loading docs
   DIR=$(find tmp -type d -name html)
   ./store $DIR $MAJOR $SERVER $DB $USER
else
   echo Old style tarball with embedded documentation tarball

   mkdir $MAJOR
   cd $MAJOR
   echo Uncompressing embedded tarball...
   find ../tmp -name postgres.tar.gz |xargs tar xfz
   cd ..

   if [ ! -f $MAJOR/index.html ]; then
      echo Could not find index.html in uncompressed tarball
      exit 1
   fi

   echo Loading into database...
   ./store $MAJOR $MAJOR $SERVER $DB $USER
fi

echo Cleaing up
rm -rf $MAJOR
rm -rf tmp
echo Done
