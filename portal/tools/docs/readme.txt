Here's how to do the docs...

1) Checkout portal/tools/docs from the pgweb subversion repository (https://pgweb.postgresql.org/svn/).

2) Build store.cxx using the Makefile. It's a hack, so might need minor 
   tweaks to get the include/lib paths right.

3) Make sure there is a symbolic link "ftp" pointing to a ftp archive 
   somewhere.

4) For each major version to load docs, run
 
  ./load_docs.sh 8.3 8.3.3
 
  (or similar for other versions)

5) Drink beer, wait for it to finish!
