#! /usr/local/bin/php -c /usr/local/etc/php.ini
<?php
   require_once '../system/global/settings.php';

   $file = isset($_SERVER['argv'][1])?$_SERVER['argv'][1]:'versions.xml';

   // Create the XML doc
   $xw = new xmlWriter();
   $xw->openMemory();
   $xw->startDocument('1.0','UTF-8');

   $xw->startElement ('versions');
   // Weird split of $ and Revision so cvs won't expand it
   preg_match('/\$' . 'Revision: (.*) \$/', $_SETTINGS['settingsver'], $m) or die ('Could not determine revision');
   $xw->writeAttribute('revision', $m[1]);
   foreach ($_SETTINGS['versions'] as $major=>$verinfo) 
   {
       $vv = split('\.', $verinfo['version']);
       $xw->startElement('version');
       $xw->writeAttribute('major', $major);
       $xw->writeAttribute('minor', $vv[2]);
       $xw->writeAttribute('version', $verinfo['version']);
       $xw->startElement('numericversion');
       $xw->writeAttribute('v1', $vv[0]);
       $xw->writeAttribute('v2', $vv[1]);
       $xw->writeAttribute('v3', $vv[2]);
       $xw->endElement();
       $xw->writeElement('releasedate', $verinfo['reldate']);
       $xw->writeElement('relnoteurl', 'http://www.postgresql.org/docs/' . $major . '/static/' . $verinfo['relnotes']);
       $xw->endElement();
   }

   $xw->endElement(); // versions
   $xw->endDtd();

   $fp = fopen($file, "w");
   if (!$fp) {
       echo "Couldn't open the output file: " . $file . ".\n";
       exit;
   }

   if (!fwrite($fp, $xw->outputMemory(true)))
   {
       echo "Couldn't write to output file: " . $file . ".\n";
       fclose($fp);
       exit;
   }

   fclose($fp);
?>

