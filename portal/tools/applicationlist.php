#! /usr/local/bin/php -c /usr/local/etc/php.ini
<?php

   $file = isset($_SERVER['argv'][1])?$_SERVER['argv'][1]:'applications-v2.xml';

   $DB = "host=pgsql74.hub.org port=5432 dbname=186_www user=186_pgsql"; 

   // connect to database
   $db = @pg_pconnect($DB);
   if (isset($php_errormsg) && $php_errormsg != "") {
      echo "Error: Couldn't connect to the database server - " . $php_errormsg . "\n";
      exit;
   }

   // Set the encoding
   pg_exec($db, "SET client_encoding = 'utf-8';");

   // Create the XML docs
   $xw = new xmlWriter();
   $xw->openMemory();
   $xw->startDocument('1.0','UTF-8');
   $xw->startElement ('applications');

   // Get the applications, and loop round them, adding each one to the doc.
   echo "Querying applications...\n";
   $sql = "SELECT * FROM applications WHERE active = TRUE;";
   $rs = @pg_exec($db, $sql);
   if (pg_result_status($rs) != PGSQL_TUPLES_OK && pg_result_status($rs) != PGSQL_COMMAND_OK) {
      echo "Error: Could not query the applications table - " . $php_errormsg . "\n";
      exit;
   }
  
   $rows = pg_numrows($rs);
  
   for ($x = 0; $x < $rows; $x++) {
      WriteApplication($xw, $rs, $x);
   }

   pg_close($db);

   // Close tags
   $xw->endElement(); // applications
   $xw->endDtd();

   $fp = fopen($file, "w");
   if (!$fp) {
       echo "Couldn't open the output file: " . $file . ".\n";
       exit;
   }

   if (!fwrite($fp, $xw->outputMemory(true)))
   {
       echo "Couldn't write to output file: " . $file . ".\n";
       fclose($fp);
       exit;
   }

   fclose($fp);
   echo "Processed " . $x . " application records for " . $file . ".\n";

   function WriteApplication($xw, $rs, $x) {

      $xw->startElement('application');

      if (pg_result($rs, $x, "id") != "")
	  $xw->writeElement('id', htmlentities(pg_result($rs, $x, "id"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "platform") != "")
	  $xw->writeElement('platform', htmlentities(pg_result($rs, $x, "platform"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "secondaryplatform") != "")
          $xw->writeElement('secondaryplatform', htmlentities(pg_result($rs, $x, "secondaryplatform"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "version") != "")
          $xw->writeElement('version', htmlentities(pg_result($rs, $x, "version"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "name") != "")
          $xw->writeElement('name', htmlentities(pg_result($rs, $x, "name"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "description") != "")
          $xw->writeElement('description', htmlentities(pg_result($rs, $x, "description"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "category") != "")
          $xw->writeElement('category', htmlentities(pg_result($rs, $x, "category"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "pgversion") != "")
          $xw->writeElement('pgversion', htmlentities(pg_result($rs, $x, "pgversion"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "edbversion") != "")
          $xw->writeElement('edbversion', htmlentities(pg_result($rs, $x, "edbversion"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "format") != "")
          $xw->writeElement('format', htmlentities(pg_result($rs, $x, "format"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "installoptions") != "")
          $xw->writeElement('installoptions', htmlentities(pg_result($rs, $x, "installoptions"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "upgradeoptions") != "")
          $xw->writeElement('upgradeoptions', htmlentities(pg_result($rs, $x, "upgradeoptions"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "checksum") != "")
          $xw->writeElement('checksum', htmlentities(pg_result($rs, $x, "checksum"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "mirrorpath") != "")
          $xw->writeElement('mirrorpath', htmlentities(pg_result($rs, $x, "mirrorpath"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "alturl") != "")
          $xw->writeElement('alturl', htmlentities(pg_result($rs, $x, "alturl"), ENT_QUOTES, 'UTF-8'));
      if (pg_result($rs, $x, "versionkey") != "")
          $xw->writeElement('versionkey', htmlentities(pg_result($rs, $x, "versionkey"), ENT_QUOTES, 'UTF-8'));

      $dependencies = trim(pg_result($rs, $x, "dependencies"), "{}");
      $deparray = split(",", $dependencies);

      foreach($deparray as $dependency) {
          if ($dependency != "")
               $xw->writeElement('dependency', htmlentities(trim($dependency, "' "), ENT_QUOTES, 'UTF-8'));
      }


      $xw->endElement(); // application
  }
?>

