#! /usr/local/bin/php -c /usr/local/etc/php.ini
<?php

require_once '../system/global/settings.php';

if (file_exists('../system/global/settings.local.php'))
    require_once '../system/global/settings.local.php';

   if (isset($_SERVER['argv'][1]))
       $EMAIL = $_SERVER['argv'][1];
   else
       $EMAIL = "PostgreSQL WWW List <pgsql-www@postgresql.org>";

   $DB = $_SETTINGS['db_portal'];
   $SEND_MAIL = 0;

   $dt = getdate();
   $today = $dt['year'] . "-" . $dt['mon'] . "-" . $dt['mday'];
   $LOG = "PostgreSQL.org moderation report - " . $today . " " . $dt['hours'] . ":" . $dt['minutes'] . ":" . $dt['seconds'] . ".\n\n";

   // connect to database
   $db = @pg_connect($DB);
   if ($php_errormsg != "") {
      $LOG .= "Couldn't connect to the database server: " . $DB . "\n\n" . $php_errormsg;
      mail($EMAIL, "PostgreSQL moderation report: " . $today, $LOG, "From: webmaster@postgresql.org\nReply-To: webmaster@postgresql.org"); 
      exit;
   }

   //
   // Applications
   //

   $sql = "SELECT count(*) FROM comments WHERE processed = FALSE;";
   $rs = @pg_exec($db, $sql);
   if (pg_result_status($rs) != PGSQL_TUPLES_OK && pg_result_status($rs) != PGSQL_COMMAND_OK) {
      $LOG .= "Error: Could not query the comments table!\n\n" . $php_errormsg;
      mail($EMAIL, "PostgreSQL moderation report: " . $today, $LOG, "From: webmaster@postgresql.org\nReply-To: webmaster@postgresql.org"); 
      exit;
   }

   $records = pg_result($rs, 0, 0);
 
   if ($records > 0)
   {
      $LOG .= "There are $records documentation comment(s) requiring moderation.\n";
      $SEND_MAIL = 1;
   }

   //
   // Events
   //

   $sql = "SELECT count(*) FROM events WHERE approved = FALSE;";
   $rs = @pg_exec($db, $sql);
   if (pg_result_status($rs) != PGSQL_TUPLES_OK && pg_result_status($rs) != PGSQL_COMMAND_OK) {
      $LOG .= "Error: Could not query the events table!\n\n" . $php_errormsg;
      mail($EMAIL, "PostgreSQL moderation report: " . $today, $LOG, "From: webmaster@postgresql.org\nReply-To: webmaster@postgresql.org");
      exit;
   }

   $records = pg_result($rs, 0, 0);

   if ($records > 0)
   {
      $LOG .= "There are $records event(s) requiring moderation.\n";
      $SEND_MAIL = 1;
   }

   //
   // News
   //

   $sql = "SELECT count(*) FROM news WHERE approved = FALSE;";
   $rs = @pg_exec($db, $sql);
   if (pg_result_status($rs) != PGSQL_TUPLES_OK && pg_result_status($rs) != PGSQL_COMMAND_OK) {
      $LOG .= "Error: Could not query the news table!\n\n" . $php_errormsg;
      mail($EMAIL, "PostgreSQL moderation report: " . $today, $LOG, "From: webmaster@postgresql.org\nReply-To: webmaster@postgresql.org");
      exit;
   }

   $records = pg_result($rs, 0, 0);

   if ($records > 0)
   {
      $LOG .= "There are $records news item(s) requiring moderation.\n";
      $SEND_MAIL = 1;
   }

   //
   // Organisations
   //

   $sql = "SELECT count(*) FROM organisations WHERE approved = FALSE;";
   $rs = @pg_exec($db, $sql);
   if (pg_result_status($rs) != PGSQL_TUPLES_OK && pg_result_status($rs) != PGSQL_COMMAND_OK) {
      $LOG .= "Error: Could not query the organisations table!\n\n" . $php_errormsg;
      mail($EMAIL, "PostgreSQL moderation report: " . $today, $LOG, "From: webmaster@postgresql.org\nReply-To: webmaster@postgresql.org");
      exit;
   }

   $records = pg_result($rs, 0, 0);

   if ($records > 0)
   {
      $LOG .= "There are $records organisation(s) requiring moderation.\n";
      $SEND_MAIL = 1;
   }

   //
   // Products
   // 
   
   $sql = "SELECT count(*) FROM products WHERE approved = FALSE;";
   $rs = @pg_exec($db, $sql);
   if (pg_result_status($rs) != PGSQL_TUPLES_OK && pg_result_status($rs) != PGSQL_COMMAND_OK) {
      $LOG .= "Error: Could not query the products table!\n\n" . $php_errormsg;
      mail($EMAIL, "PostgreSQL moderation report: " . $today, $LOG, "From: webmaster@postgresql.org\nReply-To: webmaster@postgresql.org");
      exit;
   }

   $records = pg_result($rs, 0, 0);

   if ($records > 0)
   {
      $LOG .= "There are $records product(s) requiring moderation.\n";
      $SEND_MAIL = 1;
   }

   //
   // Professional services
   // 
   
   $sql = "SELECT count(*) FROM profserv WHERE approved = FALSE;";
   $rs = @pg_exec($db, $sql);
   if (pg_result_status($rs) != PGSQL_TUPLES_OK && pg_result_status($rs) != PGSQL_COMMAND_OK) {
      $LOG .= "Error: Could not query the profserv table!\n\n" . $php_errormsg;
      mail($EMAIL, "PostgreSQL moderation report: " . $today, $LOG, "From: webmaster@postgresql.org\nReply-To: webmaster@postgresql.org");
      exit;
   }

   $records = pg_result($rs, 0, 0);

   if ($records > 0)
   {
      $LOG .= "There are $records professional service(s) requiring moderation.\n";
      $SEND_MAIL = 1;
   }

   // 
   // Send the email
   //
   if ($SEND_MAIL)
   {
      $LOG .= "\nModerators; please check and moderate these items as soon as possible!";
      mail($EMAIL, "PostgreSQL moderation report: " . $today, $LOG, "From: webmaster@postgresql.org\nReply-To: webmaster@postgresql.org"); 
   }

   pg_close($db);

?> 
