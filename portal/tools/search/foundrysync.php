#!/usr/bin/php
<?php
   function __autoload($class) { require_once("classes/$class.class.php"); }

   $url = 'http://pgfoundry.org/export/rss_sfprojects.php';
   
   $str = file_get_contents($url);
   if (!$str) {
      print "FAILED to download pgFoundry project list!\n";
      exit(1);
   }
   if (!preg_match_all('#<title>([^<]+)</title>[^<]*<link>http://pgfoundry.org/projects/([a-z0-9_-]+)/</link>#si', $str, $matches)) {
      print "FAILED to match regexp for pgFoundry project list!\n";
      exit(1);
   }
   for ($i = 0; $i < count($matches[1]); $i++) {
      $sites[$matches[2][$i]] = $matches[1][$i];
   }

   foreach (SearchDB::QuerySingleColumn("SELECT baseurl FROM sites WHERE baseurl LIKE 'http://%.projects.postgresql.org'") as $site) {
      if (!$sites[$site]) {
         print "Deleting site $site\n";
         SearchDB::ExecuteStatement("DELETE FROM sites WHERE baseurl='" . $site . "'");
      }
   }
   
   foreach ($matches[2] as $site) {
      if (SearchDB::QueryScalarValue("SELECT count(*) FROM sites WHERE baseurl='http://" . $site . ".projects.postgresql.org/'") == 0) {
         print "Add site $site\n";
         SearchDB::ExecuteStatement("INSERT INTO sites (baseurl,description,allowquestionmark,pagecount) VALUES ('http://" . $site . ".projects.postgresql.org/','" . pg_escape_string($sites[$site]) . "',0,0)");
      }
   }

   SearchDB::Commit();
   print "Done synchronizing pgFoundry sites.\n";
?>