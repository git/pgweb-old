#!/usr/bin/php
<?php
   function __autoload($class) { require_once("classes/$class.class.php"); }
   ini_set("memory_limit", "32M");

   if (strlen($argv[1]) < 1) {
      print "Usage: mainsite.php <sitemapurl>\n";
      exit(1);
   }

   $id = SearchDB::QueryScalarValue("SELECT id FROM sites WHERE baseurl='http://www.postgresql.org'");

   $psi = new SitemapSiteIndexer($id, $argv[1]);
   $psi->IndexSitemapSite();

   print "Finished indexing " . $psi->count() . " pages on static site.\n";
   if ($psi->count() > 0) {
      SearchDB::ExecuteStatement("UPDATE sites SET pagecount=(SELECT count(*) FROM webpages WHERE site=$id) WHERE id=$id");
   }
   SearchDB::Commit();
?>
