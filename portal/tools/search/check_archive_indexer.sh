#!/bin/bash

INTERVAL="2 hours"

OUT=$(/usr/local/pgsql/bin/psql -q -At -U search -d search -c "SELECT CASE WHEN CURRENT_TIMESTAMP-lastcrawl > '$INTERVAL'::interval THEN 'ERROR:' ELSE 'OK:' END || ' updated ' || (CURRENT_TIMESTAMP-lastcrawl)::text || ' ago.' FROM lastcrawl")

echo $OUT

if [[ $OUT == OK* ]]; then
   exit 0
fi
exit 2
