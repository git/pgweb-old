<?php
class PgSiteIndexer {
   private $siteid = -1;
   private $indexer = 0;
   private $basedir = 0;
   private $count = 0;
   private $found = Array();

   function __construct($siteid) {
      $this->siteid = $siteid;
      $this->indexer = new WebPageIndexer($siteid);
   }

   function IndexStaticSite($basedir) {
      $this->basedir = rtrim($basedir,'/') . '/';
      $this->baselen = strlen($this->basedir);
      $this->ScanDirectory($this->basedir);
      foreach (SearchDB::QuerySingleColumn("SELECT suburl FROM webpages WHERE site=" . $this->siteid) as $suburl) {
         if (!$this->found[$suburl]) {
            print "Page removed: $suburl\n";
            SearchDB::ExecuteStatement("DELETE FROM webpages WHERE site=" . $this->siteid . " AND suburl='" . $suburl . "'");
            SearchDB::ExecuteStatement("DELETE FROM webpages_scantime WHERE site=" . $this->siteid . " AND suburl='" . $suburl . "'");
            $this->count++;
         }
      }
   }

   function count() {
      return $this->count;
   }

   private function ScanDirectory($dir) {
      $dh = opendir($dir);
      if (!$dh) die("Could not open directory $dir\n");
      while ($d = readdir($dh)) {
         if ($d == '.' || $d == '..')
            continue;
         $fd = $dir . '/' . $d;
         if (is_dir($fd)) {
            $this->ScanDirectory($fd);
            continue;
         }
         $suburl = substr($fd, $this->baselen);
         $this->found[$suburl] = 1;
         $mtime = filemtime($fd);
         if ($this->indexer->WantIndexFile($suburl, $mtime)) {
            $str = file_get_contents($fd);
            if ($this->indexer->IndexSinglePage($str, 0, $suburl, $mtime)) {
               $this->count++;
               if (($this->count % 100) == 0) {
                  print "Indexed " . $this->count . " pages.\n";
               }
            }
         }
      }
      closedir($dh);
   }
}
?>
