<?php
class SitemapSiteIndexer {
   private $siteid = -1;
   private $indexer = 0;
   private $basedir = 0;
   private $count = 0;
   private $found = Array();

   function __construct($siteid, $baseurl) {
      $this->siteid = $siteid;
      $this->indexer = new WebPageIndexer($siteid);

      $this->baseurl = $baseurl;
      $urlinfo = parse_url($baseurl);
      $this->host = $urlinfo['host'];
      if (isset($urlinfo['port'])) $this->port = $urlinfo['port'];
      $this->http = new HttpRequest($this->host, $this->port);
   }

   function IndexSitemapSite() {
    
      $xml = simplexml_load_file($this->baseurl);
      if ($xml->getName() != "urlset") {
         print "Root element is not urlset!\n";
         exit(1);
      }

      // Fetch existing list
      $lastdates = SearchDB::QueryAssociative("SELECT suburl,extract(epoch from lastscanned) FROM webpages_scantime WHERE site=" . $this->siteid);

      foreach ($xml->children() as $child) {
         if ($child->getName() != "url") {
            print "Child element is not url!\n";
            exit(1);
         }

         $urlpiece = substr($child->loc, 25); // Skip past http://www.postgresql.org <-- FIXME: should not be hardcoded

         $lastscan = array_key_exists($urlpiece, $lastdates) ? $lastdates[$urlpiece] : 0;
         if ($this->http->RequestURL($urlpiece, $lastscan)) {
            if ($this->http->status == 304) {
               print "Page $urlpiece not changed.\n";
            }
            else if ($this->http->status == 404) {
               print "Page $urlpiece in sitemap, but does not exist!\n";
               SearchDB::ExecuteStatement("DELETE FROM webpages_scantime WHERE site=" . $this->siteid . " AND suburl='" . $urlpiece . "'");
               SearchDB::ExecuteStatement("DELETE FROM webpages WHERE site=" . $this->siteid . " AND suburl='" . $urlpiece . "'");
               continue;
            }
            else if ($this->http->status != 200) {
               print "Page $urlpiece returned invalid status " . $this->http->status . "!\n";
               exit(1);
            }
            else {
               if ($this->http->getHeader("Last-Modified")) {
                  $lastmod = strtotime($this->http->getHeader("Last-Modified"));
               } else {
                  $lastmod = time();
               }
               $contenttype = $this->http->getHeader("Content-type");
               $contenttype = preg_replace('/,.*$/', '', $contenttype);
               $contenttype = preg_replace('/;.*$/', '', $contenttype);
               if (!$this->indexer->WantIndexFile($urlpiece,$lastmod,$contenttype)) {
                  print "Don't want to index $urlpiece\n";
                  continue;
               }
               if (!$this->indexer->IndexSinglePage($this->http->responsetext, 1, $urlpiece, $lastmod, $contenttype)) {
                  print "Failed to index $urlpiece!\n";
                  continue;
               }
               $this->count++;
            } // http 200
         }
         else {
            print "Strange error for $urlpiece\n";
            exit(1);
         }
         
         // Add it to the list of indexed URLs, so we can figure out
         // if it should be removed.
         $this->found[$urlpiece] = 1;
      }

      foreach (SearchDB::QuerySingleColumn("SELECT suburl FROM webpages WHERE site=" . $this->siteid) as $suburl) {
         if (!$this->found[$suburl]) {
            print "Page removed: $suburl\n";
            SearchDB::ExecuteStatement("DELETE FROM webpages WHERE site=" . $this->siteid . " AND suburl='" . $suburl . "'");
            SearchDB::ExecuteStatement("DELETE FROM webpages_scantime WHERE site=" . $this->siteid . " AND suburl='" . $suburl . "'");
            $this->count++;
         }
      }
   }

   function count() {
      return $this->count;
   }

}
?>
