<?php
class HttpRequest {
   public $status = 0;
   public $responsetext = NULL;
   public $encoding = '';
   public $headers = Array();
   private $host='';
   private $port;
   
   function __construct($host, $port=80) {
      $this->host = $host;
      $this->port = $port;
   }

   function RequestURL($url,$ifmodified=0,$redirects=0) {
      $this->status = 0;
      $this->responsetext = '';
      $this->encoding = '';
      $this->headers = Array();
      if (substr($url,0,1) != '/' && substr($url,0,7) != 'http://') {
         $url = '/' . $url;
      }

		$retries = 5;
		while ($retries > 0) {
         $fp = @fsockopen($this->host, $this->port, $errno, $errstr, 10);
         if (!$fp) {
            if ($errno == 110) {
               // Connection timed out, so try again
               $retries--;
               continue;
            }
   			print "Failed to connect to " . $this->host . ": " . $errno . " (" . $errstr . ")\n";
   			$this->status = 599;
   			return 0;
   		}
   		else {
            break;
         }
      }
      if (!$fp) {
			print "Failed to connect to " . $this->host . ": " . $errno . " (" . $errstr . "), still after 5 retries\n";
			$this->status = 598;
			return 0;
      }

		socket_set_timeout($fp, 10);
		$lmhdr='';
		if ($ifmodified > 0) {
         $lmhdr = 'If-Modified-Since: ' . gmstrftime ("%a, %d %b %Y %T %Z", $ifmodified) . "\n";
      }
		fwrite($fp,"GET $url HTTP/1.0\nHost: " . $this->host . "\nConnection: close\nUser-Agent: pgIndexer/0.1\nAccept: text/xml,application/xhtml+xml,text/html,text/plain,*/*\n$lmhdr\n");
		$first = 1;
		$headers = 1;
		while (!feof($fp)) {
         $line = fgets($fp, 4096);
		   if ($first) {
            if (strlen($line) == 0) {
               continue;
            }
            $first = 0;
            if (!preg_match('/HTTP\/(\d\.\d)\s*(\d+)/', $line, $matches)) {
               $this->status = 597;
               print "Got invalid status for $url: '$line'\n";
               fclose($fp);
               return 0;
            }
            if ($matches[1] != '1.1' && $matches[1] != '1.0') {
               $this->status = 596;
               print "Got invalid http version: $matches[1]\n";
               fclose($fp);
               return 0;
            }
				$this->status = $matches[2];
				continue;
		   }
		   if ($headers) {
            if (trim($line) == '') {
               // End of headers
               $headers = 0;
               continue;
            }
            if (preg_match('/([^:]+):\s*(.*)\s*/', $line, $matches)) {
               // Proper header
               $this->headers[strtolower($matches[1])] = trim($matches[2]);
            }
            continue;
         }
         $this->responsetext .= $line;
       }
       fclose($fp);
       if ($this->status == 301) {
         // Redirect
         $redirects++;
         if ($redirects < 2 && $this->headers['location']) {
            return $this->RequestURL($this->headers['location'],$ifmodified,$redirects);
         }
      }
       if (isset($this->headers['content-type'])) {
          if (preg_match('/.*charset=(.*)/', $this->headers['content-type'], $matches)) {
             $this->encoding = $matches[1];
          }
       }
       return 1;
   }

   function getHeader($header) {
      if (array_key_exists(strtolower($header), $this->headers)) {
         return $this->headers[strtolower($header)];
      } else {
         return NULL;
      }
   }
}
?>
