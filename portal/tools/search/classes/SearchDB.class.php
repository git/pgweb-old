<?php
require_once '../../system/global/settings.php';

class SearchDB
{
   private static $dbhandle = 0;
   private static $in_transaction = 0;

   static function GetHandle() {
      global $_SETTINGS;

      if (self::$dbhandle == 0) {
         self::$dbhandle = pg_connect($_SETTINGS['db_search']);
         if (!self::$dbhandle) self::mydie ("Failed to connect to database.");
      }
      return self::$dbhandle;
   }

   static function QueryScalarValue($query) {
      self::VerifyTransaction();
      $res = pg_query(self::GetHandle(), $query);
      if (!$res) self::mydie ("Query failed: $query\n");
      return pg_fetch_result($res, 0, 0);
   }

   static function QueryArray($query) {
      self::VerifyTransaction();
      $res = pg_query(self::GetHandle(), $query);
      if (!$res) self::mydie ("Query failed: $query\n");
		return pg_fetch_all($res);
   }

   static function QueryAssociative($query) {
      self::VerifyTransaction();
      $res = pg_query(self::GetHandle(), $query);
      if (!$res) self::mydie ("Query failed: $query\n");
      $ret = Array();
      while ($row = pg_fetch_row($res)) {
         $ret[$row[0]] = $row[1];
      }
      return $ret;
   }

   static function QuerySingleColumn($query) {
      self::VerifyTransaction();
      $res = pg_query(self::GetHandle(), $query);
      if (!$res) self::mydie ("Query failed: $query\n");
      return pg_fetch_all_columns($res);
   }

   static function PrepareStatement($query) {
      $name = microtime();
      self::VerifyTransaction();
      if (!pg_prepare(self::GetHandle(), $name, $query)) self::mydie("Prepare failed: $query\n");
      return $name;
   }

   static function ExecutePrepared($name, $params) {
      self::VerifyTransaction();
      $res = pg_execute(self::GetHandle(), $name, $params);
      if (!$res) self::mydie("Query failed: $name\n");
   }

   static function ExecuteStatement($query) {
      self::VerifyTransaction();
      $res = pg_query(self::GetHandle(), $query);
      if (!$res) self::mydie ("Query failed: $query\n");
   }

   static function VerifyTransaction() {
      if (!self::$in_transaction) {
         self::BeginTransaction();
      }
   }

   static function BeginTransaction() {
      if (self::$in_transaction) self::mydie("Attempt to begin transaction when already in one!");
      $res = pg_query(self::GetHandle(), "BEGIN");
      if (!$res) self::mydie ("BEGIN failed\n");
      self::$in_transaction=1;
   }

   static function Commit() {
      $res = pg_query(self::GetHandle(), "COMMIT");
      if (!$res) self::mydie ("COMMIT failed\n");
      self::$in_transaction=0;
   }

   static function Rollback() {
      $res = pg_query(self::GetHandle(), "ROLLBACK");
      if (!$res) self::mydie ("ROLLBACK failed\n");
      self::$in_transaction=0;
   }

   static function mydie($msg) {
      debug_print_backtrace();
      die($msg . "\n");
   }
}
?>
