<?php
class GenericSiteIndexer {
   private $siteid;
   private $indexer;
   private $baseurl;
   private $baselen;
   private $host;
   private $port=80;
   private $http;
   private $count;
   private $allowqm;

   function __construct($siteid, $baseurl, $allowqm=0) {
      $this->siteid = $siteid;
      $this->baseurl = rtrim($baseurl,'/');
      $this->baselen = strlen($baseurl);
      $this->allowqm = $allowqm;

      $urlinfo = parse_url($baseurl);
      $this->host = $urlinfo['host'];
      if (isset($urlinfo['port'])) $this->port = $urlinfo['port'];
      $this->http = new HttpRequest($this->host, $this->port);

      $this->count = 0;
   }

   function IndexSite($full) {
      if ($full) {
         SearchDB::ExecuteStatement("DELETE FROM webpages_scantime WHERE site=" . $this->siteid);
         SearchDB::ExecuteStatement("DELETE FROM webpages WHERE site=" . $this->siteid);
         $pendingurls[$this->baseurl] = 0;
      }
      else {
         // Inc scan. So load up the oldest 20 URLs to scan
         $pendingurls = SearchDB::QueryAssociative("SELECT '" . $this->baseurl . "'||suburl,EXTRACT (epoch FROM lastscanned) FROM webpages_scantime WHERE site=" . $this->siteid . " ORDER BY lastscanned LIMIT 20");

         // Now claim we've already scanned everything, so we only pick up *new* URLs
         // when scanning.
         $doneurls = SearchDB::QueryAssociative("SELECT '" . $this->baseurl . "'||suburl,1 FROM webpages WHERE site=" . $this->siteid);

         // Prepare our update statement
         $updscandate = SearchDB::PrepareStatement("UPDATE webpages_scantime SET lastscanned=CURRENT_TIMESTAMP where site=" . $this->siteid . " AND suburl=\$1");
      }
      $this->indexer = new WebPageIndexer($this->siteid);

      while (count($pendingurls)) {
         $url = array_shift(array_keys($pendingurls));
         $lastscan = $pendingurls[$url];
         unset($pendingurls[$url]);
         $url = rtrim($url,'/');
         if (substr($url, $this->baselen) == '') $url .= '/';
         $doneurls[$url] = 1;

         print "Indexing $url\n";
         if ($this->http->RequestURL($url, $lastscan)) {
            $suburl = substr($url, $this->baselen);
            if (substr($suburl, 0,1) != '/') $suburl = '/' . $suburl;
            if ($this->http->status == 304) {
//               print "Page $suburl not changed\n";
               SearchDB::ExecutePrepared($updscandate, Array($suburl));
               continue;
            }
            if ($this->http->status == 404) {
               print "Page $suburl does not exist.\n";
               SearchDB::ExecuteStatement("DELETE FROM webpages_scantime WHERE site=" . $this->siteid . " AND suburl='" . $suburl . "'");
               SearchDB::ExecuteStatement("DELETE FROM webpages WHERE site=" . $this->siteid . " AND suburl='" . $suburl . "'");
               continue;
            }
            if ($this->http->status != 200) {
               print "Failed to get $url: " . $this->http->status . "\n";
               continue;
            }
            if ($this->http->getHeader("Last-Modified")) {
               $lastmod = strtotime($this->http->getHeader("Last-Modified"));
            } else {
               $lastmod = time();
            }
            $contenttype = $this->http->getHeader("Content-type");
            if (!$contenttype) {
               print "No content-type for $suburl\n";
               continue;
            }
            $contenttype = preg_replace('/,.*$/', '', $contenttype);
            $contenttype = preg_replace('/;.*$/', '', $contenttype);

            if (!$this->indexer->WantIndexFile($suburl,$lastmod,$contenttype)) {
               continue;
            }

            if (!$this->indexer->IndexSinglePage($this->http->responsetext, 1, $suburl, $lastmod, $contenttype)) {
               print "Failed to index $suburl!\n";
               continue;
            }
            $this->count++;

            foreach ($this->indexer->getLinks() as $link) {
               if (substr($link,0,1) == '#') {
                  // Page-local link
                  continue;
               }
               $link = preg_replace('/#.*/','',$link); // Remove page-local links
               $link = rtrim($link,'/');
               if ($this->allowqm==0 && strstr($link, '?')) {
                  // Ignore links with ? in them, because they're normally cgis. Unless
                  // specifically allowed in the site record, that is.
                  continue;
               }
               if (strstr($link, '/cgi-bin/')) {
                  // Exclude specific CGI directories
                  continue;
               }
               if (!strstr($link, ':')) {
                  // Relative URL
                  if (substr($link,0,1) == '/') {
                     $link = rtrim($this->baseurl,'/') . $link;
                  }
                  else {
                     $here = preg_replace('#/[^/]+\.(html|php)?$#i', '', $url);
                     $here = preg_replace('/\?.*/','',$here);
                     if (substr($link,0,1) == '?') {
                        $link = $here . $link;
                     }
                     else {
                        $link = $here . '/' .  $link;
                     }
                  }
               }
               if (substr($link,0,7) != 'http://') {
                  // Not a web link
                  continue;
               }
               if (strstr($link, $this->baseurl) != $link) {
                  // Not in our base
                  continue;
               }
               if (preg_match('#\.\.#', $link) || preg_match('#/\.#', $link)) {
                  $pathparts = split('/', substr($link,7)); // Skip http://
                  $pathnew = Array();
                  while ($part = array_shift($pathparts)) {
                     if ($part == '.') {
                        continue;
                     }
                     elseif ($part == '..') {
                        array_pop($pathnew);
                     }
                     else {
                        array_push($pathnew, $part);
                     }
                  }
                  $link = 'http://' . join('/', $pathnew);
               }
               $link = preg_replace('#([^:])//#','$1/',$link);

               if (isset($doneurls[$link]) || isset($doneurls[$link .'/']) || isset($doneurls[rtrim($link,'/')]) || isset($pendingurls[$link]) || isset($pendingurls[$link . '/']) || isset($pendingurls[rtrim($link,'/')])) {
                  // Already did this one
                  continue;
               }
               if (!$this->indexer->WantIndexFile(substr($link, $this->baselen), 1, 'text/html')) {
                  // Don't want to index this file even if it's new and HTML
                  continue;
               }
               $pendingurls[$link] = 0;
            }
         }
      }
      SearchDB::ExecuteStatement("UPDATE sites SET pagecount=(SELECT count(*) FROM webpages WHERE site=" . $this->siteid . ") WHERE id=" . $this->siteid);
      SearchDB::Commit();
      print "Finished indexing " . $this->count . " pages on generic site.\n";
   }
}
?>
