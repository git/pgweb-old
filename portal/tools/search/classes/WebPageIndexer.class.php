<?php
class WebPageIndexer {
   private $siteid = -1;
   private $stage = 0;
   private $text = '';
   private $pgpage = 0;
   private $divstack = 0;
   private $links;
   private $urlinfo;
   private $excludes;
   private $ins1,$ins2,$upd1,$upd2;
   private $excludeextensions = array('.jpg','.gif','.png','.js','.zip','.tar','.tar.gz','.bz2','.gz','.jar','.po','.pot');
   private $allowedcontenttypes = array('text/html','application/xhtml+xml');

   function __construct($siteid) {
      $this->siteid = $siteid;
      $this->urlinfo = SearchDB::QueryAssociative("SELECT suburl,EXTRACT(epoch FROM lastscanned) AS ago FROM webpages_scantime WHERE site=$siteid");
      $this->excludes = SearchDB::QuerySingleColumn("SELECT suburlre FROM site_excludes WHERE site=$siteid");

      $this->ins1=SearchDB::PrepareStatement("INSERT INTO webpages (site,suburl,title,txt,fti) VALUES ($siteid,\$1,\$2,\$3,setweight(to_tsvector(\$4),'A')||to_tsvector(\$5))");
      $this->ins2=SearchDB::PrepareStatement("INSERT INTO webpages_scantime (site,suburl,lastscanned) VALUES ($siteid,\$1,to_timestamp(\$2))");
      $this->upd1=SearchDB::PrepareStatement("UPDATE webpages SET title=\$1,txt=\$2,fti=setweight(to_tsvector(\$3),'A')||to_tsvector(\$4) WHERE site=$siteid AND suburl=\$5");
      $this->upd2=SearchDB::PrepareStatement("UPDATE webpages_scantime SET lastscanned=to_timestamp(\$1) WHERE site=$siteid AND suburl=\$2");
   }

   function IndexSinglePage($pagecontents, $collectlinks, $suburl, $indextime, $contenttype=0, $encoding=null) {
      $title = "No title";
      $this->links = Array();
      if ($suburl == '') $suburl = '/';

      if ($contenttype = 0) {
         $contenttype = $this->GetContentType($suburl);
         if (isnull($contenttype)) {
            print "Not indexing contenttype $contenttype for $suburl\n";
            return false;
         }
      }
      if (is_null($encoding) && preg_match('/<meta.*charset=([^"]+)/is', $pagecontents, $matches) > 0) {
         $encoding = $matches[1];
      }
      if (is_null($encoding))
         $encoding = "iso-8859-1"; // Default
      $str = iconv($encoding, 'utf-8//TRANSLIT', $pagecontents);
      $before = memory_get_usage();

      if (preg_match('#<title[^>]*>([^<]+)</title[^>]*>#si', $str, $matches)) {
         $title = $matches[1];
         $title = preg_replace('#\s+#',' ',$title);
         $title = substr($title, 0, 128);
      }
      if (preg_match('#<div[^>*]id="pgContentWrap"[^>]*>(.*)<div[^>*]id="pgFooter"#si', $str, $matches)) {
         $txt = $matches[1];
      }
      else {
         /* Standard page, so just extract the body */
         if (preg_match('#<body[^>]*>(.*)</body[^>]*>#si', $str, $matches)) {
            $txt = $matches[1];
         }
         else {
//            print "Body not found for $suburl\n";
            return false;
         }
      }
      $txt = strip_tags($txt);
      $txt = preg_replace('#\s+#',' ',$txt);

      $title = html_entity_decode($title,ENT_COMPAT,'utf-8');
      $txt = html_entity_decode($txt,ENT_COMPAT,'utf-8');

      if (array_key_exists($suburl, $this->urlinfo)) {
         SearchDB::ExecutePrepared($this->upd1,Array($title,$txt,$title,$txt,$suburl));
         SearchDB::ExecutePrepared($this->upd2,Array($indextime,$suburl));
      }
      else {
         SearchDB::ExecutePrepared($this->ins1,Array($suburl,$title,$txt,$title,$txt));
         SearchDB::ExecutePrepared($this->ins2,Array($suburl,$indextime));
      }
      
      if ($collectlinks) {
         preg_match_all('#<a[^>]+href="([^"]+)"[^>]*>#i', $str, $matches);
         $this->links = $matches[1];
      }

      return true;
   }

   function getLinks() {
      return $this->links;
   }

   function WantIndexFile($suburl, $filetime, $contenttype=NULL) {
      if ($suburl == '') $suburl = '/';
      foreach ($this->excludes as $x) {
         if (preg_match('#' . $x . '#', $suburl)) {
            return false;
         }
      }
      foreach ($this->excludeextensions as $x) {
         if (substr_compare($suburl, $x, strlen($suburl)-strlen($x)) == 0) {
            return false;
         }
      }
      if (is_null($contenttype)) {
         $contenttype = $this->GetContentType($suburl);
      }
      if (!in_array($contenttype, $this->allowedcontenttypes)) {
         return false;
      }
      // Now finally check the date
      if (!array_key_exists($suburl, $this->urlinfo)) {
         // New file, always index
         return true;
      }
      if ($this->urlinfo[$suburl] < $filetime) {
         // Newer
         return true;
      }
      return false;
   }

   private function GetContentType($suburl) {
      if (preg_match('/\.html$/', $suburl))
         return "text/html";
      if (preg_match('/\.txt$/', $suburl))
         return "text/plain";
      if (preg_match('/\.html\.\w{2}$/',$suburl) && !preg_match('/\.html\.en$/',$suburl))
         return "text/html";
      return NULL;
   }
}
?>