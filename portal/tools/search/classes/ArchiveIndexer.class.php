<?php
class ArchiveIndexer {
   private $count = 0;
   private $http;
   private $statement;

   function __construct() {
      $this->http = new HttpRequest('archives.postgresql.org',80);
   }

   function Index($full, $list, $year, $month) {
      $dat = getdate();

      $restrict = ($list=="")?"active=1":"name='$list'";
      $lists = SearchDB::QueryArray("SELECT id,name FROM lists WHERE $restrict ORDER BY name");
		foreach ($lists as $row) {
         print "Indexing list " . $row['id'] . " (" . $row['name'] . ")\n";
         if ($year != -1 && $month != -1) {
				// Crawl specific month
				$this->IndexMonth($row['id'], $row['name'], $year, $month);
         }
         elseif (!$full) {
            // Incremental crawl. First last month
            if ($dat['mon'] == 1) {
				   // January, so index last september
				   $this->IndexMonth($row['id'], $row['name'], $dat['year']-1, 12);
            }
            else {
               // Other month, so index last month
				   $this->IndexMonth($row['id'], $row['name'], $dat['year'], $dat['mon']-1);
            }
            // Now index current month
			   $this->IndexMonth($row['id'], $row['name'], $dat['year'], $dat['mon']);
         }
			else {
            // Full crawl
            for ($y = 1997; $y <= $dat['year']; $y++) {
               $maxmonth = ($y==$dat['year'])?$dat['mon']:12;
               for ($m = 1; $m <= $maxmonth ; $m++) {
						 $this->IndexMonth($row['id'], $row['name'], $y, $m);
               }
            }
			}
      }
      if ($this->count > 0) {
         SearchDB::ExecuteStatement("UPDATE lists SET pagecount=(SELECT count(*) FROM messages WHERE messages.list=lists.id)");
         SearchDB::ExecuteStatement("UPDATE lastcrawl SET lastcrawl=CURRENT_TIMESTAMP");
         SearchDB::Commit();
      }
      print "Completed indexing " . $this->count . " messages.\n";
   }

   private function IndexMonth($listid, $list, $year, $month) {
      $failcount = 0;
      $firstfail = 0;

      $this->statement = SearchDB::PrepareStatement("INSERT INTO messages (list, year, month, msgnum, date, subject, author, txt, fti) VALUES ($listid, $year, $month, \$1, to_timestamp(\$2), \$3, \$4, \$5, setweight(to_tsvector(\$6),'A')||to_tsvector(\$7))");
      $ids = SearchDB::QueryAssociative("SELECT msgnum,1 FROM messages WHERE list=$listid AND year=$year AND month=$month");

      for ($i = 0; $i < 100000; $i++) {
         if (isset($ids[$i]) && $ids[$i] == "1") continue;

         $r = $this->IndexSinglePage($listid, $list, $year, $month, $i);
         if ($r == 0) {
            $failcount++;
            if ($firstfail == 0) {
               $firstfail = $i;
            }
         }
         else {
            $this->count++;
            if (($this->count % 500) == 0) {
               SearchDB::Commit();
               print "Indexed " . $this->count . " messages.\n";
            }
         }
         if ($failcount > 10) {
            return $firstfail;
         }
      }
      return $firstfail;
   }

   private function IndexSinglePage($listid, $list, $year, $month, $msgnum) {
      $url = '/' . $list . '/' . sprintf("%04d",$year) . '-' . sprintf("%02d",$month) . '/msg' . sprintf("%05d",$msgnum) . '.php';

		if (!$this->http->RequestURL($url)) {
         die("Could not fetch $url\n");
      }
		if ($this->http->status == 404) {
         return 0;
      }
      elseif ($this->http->status != 200) {
         die("Got bad resultcode for url $url: " . $this->http->status);
      }

		if (!preg_match("#<!--X-Subject: ([^\n]*) -->.*<!--X-From-R13: ([^\n]*) -->.*<!--X-Date: ([^\n]*) -->.*<!--X-Body-of-Message-->(.*)<!--X-Body-of-Message-End-->#s", $this->http->responsetext, $matches)) {
         print "Message $url not properly formatted.\n";
         return 0;
      }
      $subject = iconv($this->http->encoding, 'utf-8//TRANSLIT', substr(html_entity_decode(strip_tags(iconv($this->http->encoding, 'utf-8//TRANSLIT', $matches[1])),ENT_COMPAT,'utf-8'),0,128));
      $from = iconv($this->http->encoding, 'utf-8//TRANSLIT', substr(html_entity_decode($this->rot13_decrypt(iconv($this->http->encoding, 'utf-8//TRANSLIT', $matches[2])),ENT_COMPAT,'utf-8'),0,128));
      $date = html_entity_decode(strip_tags(iconv($this->http->encoding, 'utf-8//TRANSLIT', $matches[3])),ENT_COMPAT,'utf-8');
      $body = iconv($this->http->encoding, 'utf-8//TRANSLIT', html_entity_decode(strip_tags(iconv($this->http->encoding, 'utf-8//TRANSLIT', $matches[4])),ENT_COMPAT,'utf-8'));

      if (preg_match('/(.*)\(envelope.*/', $date, $matches)) {
         $date = $matches[1];
      }

		$time = strtotime($date);
		if (!$time) {
         $time = strtotime('1960-01-02 00:00:00 GMT');
      }
      
      SearchDB::ExecutePrepared($this->statement, Array($msgnum, $time, $subject, $from, $body, $subject, $body));
      return 1;
   }

   private function rot13_decrypt($str) {
      // Semi-hacked rot13, because the one used by mhonarc is broken. So we copy the brokenness
      // This code is from /usr/local/share/MHonArc/ewhutil.pl, mrot13()
      $in = '@ABCDEFGHIJKLMNOPQRSTUVWXYZ[abcdefghijklmnopqrstuvwxyz';
      $ou = 'NOPQRSTUVWXYZ[@ABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm';
      return strtr($str,$in,$ou);
   }
}
?>
