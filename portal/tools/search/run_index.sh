#!/bin/bash

#
# This script runs continuously to deal with index updates. It will run
# the separate scripts at regular intervals as needed
#

echo $$ > /home/search/run_index.pid
BASE=/home/search
cd $BASE/portal/tools/search

while true; do
   echo Downloading list of pgFoundry projects...
   $BASE/portal/tools/search/foundrysync.php
   echo Doing incremental crawl of generic websites...
   $BASE/portal/tools/search/generic.php -all
   echo Syncing main website...
   rsync -vazH --delete rsync://wwwmaster.postgresql.org/pgsql-wwwmaster $BASE/static
   echo Syncing list of lists...
   $BASE/portal/tools/search/synclists.sh
   echo Indexing main website...
   $BASE/portal/tools/search/mainsite.php $BASE/static

# Do the archives 10 times in a loop before we move on
   for FOO in 1 2 3 4 5 6 7 8 9 10 ; do
      echo "Incrementally crawling archives (run $FOO)..."
      $BASE/portal/tools/search/archives.php
      echo Going to sleep...
      sleep 1800
   done
done
