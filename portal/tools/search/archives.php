#!/usr/bin/php
<?php
   function __autoload($class) { require_once("classes/$class.class.php"); }

	$full=0;
   $list="";
   $year=-1;
   $month=-1;

	array_shift($argv);
   while ($a = array_shift($argv)) {
      switch ($a) {
         case "-h": Usage();break;
         case "-?": Usage();break;
         case "-l": $list = array_shift($argv);if (!$list) Usage("-l requires list to be specified");break;
         case "-f": $full = 1;break;
         case "-y": $year = array_shift($argv);if (!$year) Usage("-y requires year to be specified");break;
         case "-m": $month = array_shift($argv);if (!$month) Usage("-m requires month to be specified");break;
         default: Usage("Unknown argument $a");
      }
   }

   if (($year != -1 && $month == -1) || ($year == -1 && $month != -1)) {
      Usage("Must specify both year and month");
   }

	$ai = new ArchiveIndexer();
	$ai->Index($full, $list, $year, $month);
	exit(0);

	function Usage($err = "") {
      if ($err != "") {
         print $err . "\n\n";
      }
      print "Usage:\n";
      print "  archives.php [-l list] [-f | [-y year -m month]]\n";
      print " -l               = specify list. If not set, all lists are indexed.\n";
      print " -f               = perform full index.\n";
      print " -y year -m month = index specified year and month\n";
      print "   If neither -f or -y/-m is specified, incremental indexing will be done.\n";

		exit(1);
    }
?>