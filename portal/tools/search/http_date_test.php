#!/usr/bin/php
<?
   require_once('classes/HttpRequest.class.php');
   
   $url = $argv[1];
   if (!$url || $url == '') die("No url specified\n");
   $url = parse_url($url);
   if ($url['scheme'] != 'http') die("Invalid scheme: " . $url['scheme'] . "\n");
   $http = new HttpRequest($url['host'], $url['port']>0?$url['port']:80);
   $http->RequestURL($url['path']);
   print "Status: $http->status\n";
   print "Last modified: " . $http->getHeader('Last-Modified') . "\n";
   
   $t = strtotime($http->getHeader('Last-Modified'));
   print "Last modified (int): $t\n";
   print "-- redo same time (shuld give 304) --\n";
   $http->RequestURL($url['path'], $t);
   print "Status: $http->status\n";
   print "-- inc time (should give 304) --\n";
   $t += 10;
   $http->RequestURL($url['path'], $t);
   print "Status: $http->status\n";
   print "-- dec time (should give 200) --\n";
   $t -= 20;
   $http->RequestURL($url['path'], $t);
   print "Status: $http->status\n";
   
?>
