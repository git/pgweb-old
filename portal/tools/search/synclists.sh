#!/bin/bash

# Make sure that the 'search' user for wwwmaster has it's password
# in ~/.pgpass!

MAILTO=pgsql-slavestothewww@postgresql.org
MAILFROM=search@postgresql.org

PSQL=/usr/local/pgsql/bin/psql

# Note! Need to copy unnecessary fields over, since v8.1 that runs on
# wwwmaster doesn't support copying only some fields.

(
   cat <<EOF
BEGIN TRANSACTION;
CREATE TEMP TABLE listgroups_load(id int, name varchar(64), sortkey int);
CREATE TEMP TABLE lists_load(id int, name varchar(64), active int, grp int, description text);
COPY listgroups_load FROM stdin;
EOF
   $PSQL -h wwwmaster.postgresql.org 186_www search -c "COPY listgroups(id,name,sortkey) TO stdout"
   cat <<EOF
\\.
COPY lists_load FROM stdin;
EOF
   $PSQL -h wwwmaster.postgresql.org 186_www search -c "COPY lists(id,name,active,grp,description) TO stdout"
   cat <<EOF
\\.

SELECT 'Adding listgroup: ', name FROM listgroups_load WHERE id NOT IN (
 SELECT id FROM listgroups
);
INSERT INTO listgroups (id,name,sortkey)
 SELECT id,name,sortkey FROM listgroups_load WHERE id NOT IN (
  SELECT id FROM listgroups
);
SELECT 'Updating listgroup: ', listgroups.name FROM listgroups_load
 INNER JOIN listgroups ON listgroups_load.id=listgroups.id
 WHERE NOT (
  listgroups_load.name=listgroups.name AND
  listgroups_load.sortkey=listgroups.sortkey
 );
UPDATE listgroups SET
  name=listgroups_load.name,
  sortkey=listgroups_load.sortkey
 FROM listgroups_load WHERE listgroups_load.id=listgroups.id AND NOT (
  listgroups_load.name=listgroups.name AND
  listgroups_load.sortkey=listgroups.sortkey
 );

SELECT 'Adding list: ', name FROM lists_load WHERE id NOT IN (
 SELECT id FROM lists
);
INSERT INTO lists (id,name,active,grp,pagecount)
 SELECT id,name,active,grp,0 FROM lists_load WHERE id NOT IN (
  SELECT id FROM lists
);

SELECT 'Updating list: ', lists.name FROM lists_load
 INNER JOIN lists ON lists_load.id=lists.id
 WHERE NOT (
  lists_load.name=lists.name AND
  lists_load.active=lists.active AND
  lists_load.grp=lists.grp
 );
UPDATE lists SET 
  name=lists_load.name, 
  active=lists_load.active,
  grp=lists_load.grp
 FROM lists_load WHERE lists_load.id=lists.id AND NOT (
  lists_load.name=lists.name AND
  lists_load.active=lists.active AND
  lists_load.grp=lists.grp
 );

SELECT 'NEED TO DELETE LIST (DO MANUALLY): ', lists.name FROM lists
 WHERE id NOT IN (SELECT id FROM lists_load);

COMMIT;
EOF
) | $PSQL -Atq -F' ' search search > /tmp/listsync.$$ 2>&1

if [ -s  /tmp/listsync.$$ ]; then
   cat /tmp/listsync.$$ |mail -s "Updated list of lists" $MAILTO -- -f $MAILFROM
   cat /tmp/listsync.$$
fi

rm -f /tmp/listsync.$$
