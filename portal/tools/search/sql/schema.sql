CREATE TABLE listgroups (
   id int NOT NULL PRIMARY KEY,
   name varchar(64) NOT NULL
);
CREATE TABLE lists (
   id SERIAL NOT NULL PRIMARY KEY,
   name varchar(64) NOT NULL,
   active int NOT NULL,
   grp int NOT NULL REFERENCES listgroups(id),
   pagecount int NOT NULL
);

CREATE TABLE messages (
   list int NOT NULL REFERENCES lists(id) ON DELETE CASCADE,
   year int NOT NULL,
   month int NOT NULL,
   msgnum int NOT NULL,
   date timestamptz NOT NULL,
   subject varchar(128) NOT NULL,
   author varchar(128) NOT NULL,
   txt text NOT NULL,
   fti tsvector NOT NULL
);
ALTER TABLE messages ADD CONSTRAINT pk_messages PRIMARY KEY (list,year,month,msgnum);


CREATE TABLE sites (
   id SERIAL NOT NULL PRIMARY KEY,
   baseurl varchar(256) NOT NULL UNIQUE,
   description varchar(256) NOT NULL,
   allowquestionmark int NOT NULL,
   pagecount int NOT NULL
);

CREATE TABLE webpages (
   site int NOT NULL REFERENCES sites(id) ON DELETE CASCADE,
   suburl varchar(512) NOT NULL,
   title varchar(128) NOT NULL,
   txt text NOT NULL,
   fti tsvector NOT NULL
);
ALTER TABLE webpages ADD CONSTRAINT pk_webpages PRIMARY KEY (site, suburl);

CREATE TABLE webpages_scantime (
   site int NOT NULL REFERENCES sites(id) ON DELETE CASCADE,
   suburl varchar(512) NOT NULL,
   lastscanned timestamptz NOT NULL
);
ALTER TABLE webpages_scantime ADD CONSTRAINT pk_webpages_scantime PRIMARY KEY (site, suburl);

CREATE TABLE site_excludes (
   site int NOT NULL REFERENCES sites(id) ON DELETE CASCADE,
   suburlre varchar(512) NOT NULL
);
ALTER TABLE site_excludes ADD CONSTRAINT pk_site_excludes PRIMARY KEY (site,suburlre);

CREATE TABLE search_stats (
   t timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
   what char(1) NOT NULL,
   hits int NOT NULL,
   query tsquery NOT NULL
);

CREATE TABLE lastcrawl(lastcrawl timestamp with time zone);
