INSERT INTO sites(baseurl,description,allowquestionmark,pagecount)
 VALUES ('http://www.postgresql.org','Main PostgreSQL Website', 0, 0);

INSERT INTO sites(baseurl,description,allowquestionmark,pagecount)
 VALUES ('http://www.pgadmin.org','pgAdmin III', 0, 0);

INSERT INTO sites(baseurl,description,allowquestionmark,pagecount)
 VALUES ('http://jdbc.postgresql.org','JDBC driver', 0, 0);


INSERT INTO site_excludes VALUES (1,'^/files/documentation');
INSERT INTO site_excludes VALUES (2,'^/archives');
INSERT INTO site_excludes VALUES (2,'^/docs/dev');
INSERT INTO site_excludes VALUES (2,'^/docs/1.4');
INSERT INTO site_excludes VALUES (2,'^/docs/[^/]+/pg');
INSERT INTO site_excludes VALUES (2,'^/snapshots');
INSERT INTO site_excludes VALUES (3,'^/development');

COPY listgroups (id, name) FROM stdin;
1	Developer lists
2	User lists
3	Community lists
4	Regional lists
\.

SELECT pg_catalog.setval('lists_id_seq', 33, true);

COPY lists (id, name, active, grp, pagecount) FROM stdin;
2	pgsql-general	1	2	0
4	pgsql-sql	1	2	0
5	pgsql-admin	1	2	0
6	pgsql-advocacy	1	2	0
7	pgsql-announce	1	2	0
8	pgsql-bugs	1	2	0
10	pgsql-docs	1	2	0
11	pgsql-interfaces	1	2	0
12	pgsql-novice	1	2	0
13	pgsql-performance	1	2	0
14	pgsql-benchmarks	1	2	0
15	pgsql-chat	1	2	0
17	pgsql-cygwin	1	2	0
19	pgsql-jdbc	1	2	0
20	pgsql-jobs	1	2	0
21	pgsql-odbc	1	2	0
22	pgsql-php	1	2	0
23	pgsql-ports	1	2	0
24	pgsql-www	1	2	0
1	pgsql-hackers	1	1	0
3	pgsql-patches	1	1	0
16	pgsql-committers	1	1	0
18	pgsql-hackers-win32	1	1	0
25	pgadmin-hackers	1	3	0
26	pgadmin-support	1	3	0
32	sfpug	1	3	0
33	sydpug	1	3	0
27	pgsql-fr-generale	1	4	0
28	pgsql-de-allgemein	1	4	0
29	pgsql-es-ayuda	1	4	0
30	pgsql-ru-general	1	4	0
31	pgsql-tr-genel	1	4	0
\.
