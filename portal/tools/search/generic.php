#!/usr/bin/php
<?php
   function __autoload($class) { require_once("classes/$class.class.php"); }
   ini_set("memory_limit", "32M");

   $full = 0;
   $site = '';

	array_shift($argv);
   while ($a = array_shift($argv)) {
      switch ($a) {
         case '-h': Usage();
         case '-?': Usage();
         case '-site': $site = array_shift($argv);if (!$site) Usage("-site requires site name to be specified");break;
         case '-empty': $site = '%';break;
         case '-all': $site = '*';break;
         case '-full': $full = 1; break;
         default: Usage("Unknown argument $a");
      }
   }

   if ($site == '') Usage("Must specify site, -empty or -all");

   if ($site == '*' || $site == '%') {
      if ($site == '*') {
         $sites = SearchDB::QueryArray("SELECT id,baseurl,allowquestionmark,pagecount FROM sites WHERE NOT id=1 ORDER BY id");
      } else {
         $sites = SearchDB::QueryArray("SELECT id,baseurl,allowquestionmark,pagecount FROM sites WHERE NOT id=1 AND pagecount=0 ORDER BY id");
      }
      foreach ($sites as $siteinfo) {
         print "Starting site " . $siteinfo['baseurl'] . " [" . $siteinfo['id'] . "]\n";
         $gsi = new GenericSiteIndexer($siteinfo['id'], $siteinfo['baseurl'], $siteinfo['allowquestionmark']);
         if (!$full && $siteinfo['pagecount'] == 0) {
            print "No pages yet, doing full scan\n";
            $gsi->IndexSite(1);
         }
         else {
            $gsi->IndexSite($full);
         }
      }
   }
   else {
      $siteid = SearchDB::QueryArray("SELECT id,allowquestionmark FROM sites WHERE baseurl='" . $site . "'");
      if (!$siteid || count($siteid)!=1) {
         die("Could not find site $site\n");
      }
      $siteinfo = $siteid[0];
      $gsi = new GenericSiteIndexer($siteinfo['id'], $site, $siteinfo['allowquestionmark']);
      $gsi->IndexSite($full);
   }

   function Usage($err='') {
      if ($err != "") {
         print $err . "\n\n";
      }
      print "Usage:\n";
      print "  generic.php (-site <site> | -all | -empty) [-full]\n\n";
      exit(1);
   }
?>
