  iab PP <ESC>kJhxr/0cwIn <ESC>0D?^- <CR>wPa, <ESC>l~:.,/^\s*Modified Files:$/-2j<CR>Q}d}
  autocmd BufNewFile,BufRead */weekly_news/* set autoindent
  autocmd BufNewFile *.pwn set autoindent
  autocmd BufNewFile *.pwn
              \ 0r ~/vim/skeleton.pwn |
              \ 1s/DATE/\=strftime("%B %d %Y") |
  autocmd BufNewFile *.pwn %s/MONTH/\=strftime("%B")/
  autocmd BufNewFile *.pwn %s/JOB_URL/\=strftime("%Y-%m")/
  autocmd BufNewFile *.pwn /PostgreSQL .* Feature of the Week/+1r
              \ !./2pick 2new 2used
  autocmd BufNewFile *.pwn /PostgreSQL Tip/+1r
              \ !./2pick 2tip 2oldtip
  autocmd BufNewFile *.pwn 2r !ls |egrep -v '^(2|patches|local|reviews)' |xargs cat
  autocmd BufNewFile *.pwn
              \ if filereadable("local") |
              \   %s/Everything this week was global./\=readfile("local")/ |
              \ endif |
              \ if filereadable("reviews") |
              \   $r reviews |
              \ endif |
  autocmd BufNewFile *.pwn $r !./2patch_swapur.pl
  autocmd BufNewFile,BufRead */weekly_news/{patches,reviews}
          \ ab AHP Alvaro Herrera pushed:<CR><CR>-|
          \ ab ADP Andrew Dunstan pushed:<CR><CR>-|
          \ ab BMP Bruce Momjian pushed:<CR><CR>-|
          \ ab DCP D'Arcy J.M. Cain pushed:<CR><CR>-|
          \ ab HLP Heikki Linnakangas pushed:<CR><CR>-|
          \ ab JWP Jan Wieck pushed:<CR><CR>-|
          \ ab JCP Joe Conway pushed:<CR><CR>-|
          \ ab MHP Magnus Hagander pushed:<CR><CR>-|
          \ ab MFP Marc Fournier pushed:<CR><CR>-|
          \ ab MMP Michael Meskes pushed:<CR><CR>-|
          \ ab NCP Neil Conway pushed:<CR><CR>-|
          \ ab PEP Peter Eisentraut pushed:<CR><CR>-|
          \ ab TIP Tatsuo Ishii pushed:<CR><CR>-|
          \ ab TSP Teodor Sigaev pushed:<CR><CR>-|
          \ ab TLP Tom Lane pushed:<CR><CR>-|
          \ ab AH Alvaro Herrera|
          \ ab AD Andrew Dunstan|
          \ ab BM Bruce Momjian|
          \ ab DC D'Arcy J.M. Cain|
          \ ab HL Heikki Linnakangas|
          \ ab JW Jan Wieck|
          \ ab JC Joe Conway|
          \ ab MH Magnus Hagander|
          \ ab MF Marc Fournier|
          \ ab MM Michael Meskes|
          \ ab NC Neil Conway|
          \ ab PE Peter Eisentraut|
          \ ab TI Tatsuo Ishii|
          \ ab TS Teodor Sigaev|
          \ ab TL Tom Lane|
          \ ab KK KaiGai Kohei|
          \ ab SR Simon Riggs|
          \ ab SRP Simon Riggs pushed:<CR><CR>-|
          \ ab JD Jeff Davis|
          \ ab BH Bernd Helmle|
          \ ab FM Fujii Masao|
          \ ab RH Robert Haas|
          \ ab RHP Robert Haas pushed:<CR><CR>-|
          \ ab KS Koichi Suzuki|
          \ ab ADS Andreas 'ads' Scherbaum|
          \ ab SF Stephen Frost|
          \ ab HI Hiroshi Inoue|
          \ ab BJ Brendan Jurd|
          \ ab BN Bryce Nesbitt|
          \ ab BC Bryce Cutt|
          \ ab ZK Zdenek Kotala|
          \ ab RL Ramon Lawrence|
          \ ab IT ITAGAKI Takahiro|
          \ ab ITP ITAGAKI Takahiro pushed:<CR><CR>-|
          \ ab MK Mark Kirkwood|
          \ ab KM Kenneth Marshall|
          \ ab MP Martin Pihlak|
          \ ab FI Fernando Ike de Oliveira|
          \ ab ET Euler Taveira de Oliveira|
          \ ab HS Hiroshi Saito|
          \ ab DW David Wheeler|
          \ ab MB Matteo Beccati|
          \ ab AC Andrew Chernow|
          \ ab DP Dave Page|
          \ ab SD Selena Deckelmann|
          \ ab JT Josh Tolley|
          \ ab GS Gregory Stark|
          \ ab GSP Gregory Stark pushed:<CR><CR>-|
          \ ab GM Greg Sabino Mullane|
          \ ab AG Andrew (RhodiumToad) Gierth|
          \ ab HH Hitoshi Harada|
          \ ab PS Pavel Stehule|
          \ ab AMS Abhijit Menon-Sen|
          \ ab DG Dickson S. Guedes|
          \ ab DF David Fetter|
          \ ab PJ Petr (PJMODOS) Jelinek|
          \ ab ZB Zoltan Boszormenyi|
          \ ab HJS Hans-Juergen Schoenig|
          \ ab DIM Dimitri Fontaine|
          \ ab MT Marko (johto) Tiikkaja|
          \ ab OB Oleg Bartunov|
          \ ab KG Kevin Grittner|
          \ ab AF Andres Freund|
          \ ab SK Sergey V. Karpov|
          \ ab PC Pierre Frederic Caillaud|
          \ ab DR Dean Rasheed|
          \ ab GJ Grzegorz Jaskiewicz|
          \ ab EC Emmanuel Cecchet|
          \ ab JJ Jeff Janes|
          \ ab SW Stef Walter|
          \ ab MPQ Michael Paquier|
          \ ab RLL Roger Leigh|
          \ ab LA Laurenz Albe|
          \ ab TB Tim Bunce|
          \ ab TY Tsutomu Yamada|
          \ ab JCA Jaime Casanova|
          \ ab GSM Greg Smith|
          \ ab JWI Joachim Wieland|
          \ ab KH Kurt Harriman|
          \ ab MKK Marko Kreen|
          \ ab HY Hiroyuki Yamada|
          \ ab MCA Mark Cave-Ayland|
          \ ab GSI Gurjeet Singh|
          \ ab JN John Naylor|
          \ ab GL Guillaume Lelarge|
          \ ab AHU Alex Hunsaker|
          \ ab JU Jan Urbanski|
          \ ab LF Leonardo Francalanci|
          \ ab GR Gabrielle Roth|
          \ ab DCH David Christensen|
          \ ab MW Mark Wong|
          \ ab MWA Marcus Wanner|
          \ ab YH Yeb Havinga|
          \ ab MC Marc Cousin|
          \ ab SN Satoshi Nagayasu|
          \ ab TBR Thom Brown|
          \ ab AK Alexander Korotkov|
          \ ab JA Joseph Adams|
          \ ab MFO Mike Fowler|
          \ ab BZ Boxuan Zhai|
          \ ab FP Florian Pflug|
          \ ab ER Erik Rijkers|
          \ ab KJ Kris Jurka|
          \ ab JJA Joel Jacobson|
          \ ab SS Sushant Sinha|
          \ ab SM SAKAMOTO Masahiko|
          \ ab JK Jesper Krogh|
          \ ab GV Ganesh Venkitachalam|
          \ ab PG Peter Geoghegan|
          \ ab CR Craig Ringer|
          \ ab JKU Josh Kupershmidt|
          \ ab MR Marti Raudsepp|
          \ ab SH Shigeru HANADA|
