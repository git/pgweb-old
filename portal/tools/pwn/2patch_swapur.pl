#!/usr/bin/env perl
use strict;
use warnings;

my $file = $ARGV[0] || "patches";
my @patches = ();

$/ = "\n\n";
open my $f, "<", $file
    or die "Can't open $file for reading: $!";
# Print first section to avoid special-casing it in the loop.
print scalar <$f>;
my $functor;
$functor->{'-'} = sub {unshift @patches, $_}; # It's a committed patch.
$functor->{'='} = sub {print @patches, $_, <$f>}; # End of committed patches.
my $indicator;
while (<$f>) {
    $indicator = substr($_,0,1);
    if (exists $functor->{$indicator}) {
        $functor->{$indicator}->();
    }
    else { # It's a committer.
        print @patches, $_;
        @patches = ();
    }

}
close $f;

