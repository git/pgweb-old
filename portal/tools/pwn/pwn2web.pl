#!/usr/bin/env perl
use strict;
use warnings;

########################################################
#                                                      #
# Set the input record separator to pairs of newlines. #
#                                                      #
########################################################
$/="\n\n";

if (scalar @ARGV == 0 || $ARGV[0] eq '-h' || $ARGV[0] eq '--help' ) {
    print <<USAGE;

    Usage: $0 file_1 [file_2 ... file_n]

where the file names are of the form YYYYMMDD.pwn and are generated
via vimrc.pwn and skeleton.pwn in this directory.  The result will be
output files named like pwnYYYYMMDD.html

$0 does NOT handle CR/LF line endings, so make sure you've
got \\n the way G-d intended.

USAGE
    exit;
}

my ($in_fh, $out_fh, $file);
foreach $file (@ARGV) {
    open $in_fh, "<", $file
        or die "Can't open $file for reading: $!";
    my $out_file = $file;
    if ($out_file =~ m|/|) {
        $out_file = (split('/', $out_file))[-1];
    }
    $out_file =~ s/(.*).pwn$/pwn$1.html/;
    open $out_fh, ">", $out_file
        or die "Can't open $out_file for writing: $!";
    chomp(my @blocks = @{[<$in_fh>]});
    close $in_fh;
    my $title = shift @blocks;
    $title =~ s/^=+\s+(.*)\s+=+/$1/;
    chomp($title);
    (my $short_title = $title) =~ s/^PostgreSQL //i;
    print $out_fh <<TITLE;
<!-- BEGIN page_title_block -->
$short_title
<!-- END page_title_block -->

<h1>$title</h1>

TITLE
    foreach my $block (@blocks) {
        if ($block =~ /^=+\s+(.*)\s+=+$/) {
            print $out_fh <<HEADER;
<h2>$1</h2>
HEADER
        } else {
            $block =~ s(http\S+)(<a href="$&">$&</a>)g;
            print $out_fh <<PARAGRAPH;
<p>
$block
</p>

PARAGRAPH
        }
    }
    close $out_fh;
}
