#!/usr/bin/perl
use strict;
use warnings;

# use Cwd;
use Date::Manip;
use XML::RSS;

my $rss = new XML::RSS (version => '2.0');
$rss->channel(
    title    => "PostgreSQL Weekly News",
    language => 'en',
    link => 'http://www.postgresql.org/community/weeklynews/',
);
my $dir = cwd();
$dir =~ s|portal.*|portal/files|;
print STDERR $dir;
my $file = '../weeklynews.html';
open my $out_fh, '>', $file
    or die "Couldn't open $file for overwrite: $!";
print $out_fh <<'EOT';
<!-- BEGIN page_title_block -->
Weekly News
<!-- END page_title_block -->

<h1>Weekly News</h1>

<p>The PostgreSQL Weekly News is a community newsletter, summarising the news and events of the past week, focussing primarily on the development of PostgreSQL.</p>

<p>To receive the Weekly News in your inbox, please subscribe to the <a href="/community/lists/subscribe">pgsql-announce@postgresql.org</a> mailing list.</p>

<p>Here is the <a href="http://www.postgresql.org/files/weeklynews.xml">RSS feed</a>.</p>

<ul>
EOT
my $i=0;
foreach my $pwn (sort {$b cmp $a} <pwn2*>) {
    ++$i;
    my $url="/community/weeklynews/$pwn";
    $url =~ s/\.html$//;
    my $raw_date=$pwn;
    $raw_date =~ s/pwn(.*)\.html$/$1/;
    my $date=ParseDate($raw_date);
    my $pretty_date=UnixDate($date,"%B %E %Y");
    print $out_fh <<EOT;
    <li><a href="$url">$pretty_date</a></li>
EOT
    next if $i > 10;
    $rss->add_item(
        title       => UnixDate($date, "PostgreSQL Weekly News %B %E %Y"),
        permaLink   => "http://www.postgresql.org$url",
        description => get_description($pwn),
        pubDate     => UnixDate($date, "%a, %d %b %Y 00:00:00 PST"),
    );
}
print $out_fh "</ul>\n";
close $out_fh;
$rss->save("../../../../files/weeklynews.xml");
# $rss->save(*STDOUT);

sub get_description {
    local $/="\n\n";
    my $file = shift;
    my $first_para = undef;
    open my $fh, '<', $file
        or die "In get_description, can't open $file for reading: $!";
    while(<$fh>) {
        # print STDERR "In $file, $_";
        next unless /^<p>/;
        $first_para = $_;
        $first_para =~ s(</?p>)()msg;
        # print STDERR "In $file, $first_para";
        last;
    }
    close $fh;
    return $first_para;
}
