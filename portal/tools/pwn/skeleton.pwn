== PostgreSQL Weekly News - DATE ==

== PostgreSQL Product News ==

== PostgreSQL 9.0 Feature of the Week ==

== PostgreSQL Tip of the Week ==

== PostgreSQL Jobs for MONTH ==

http://archives.postgresql.org/pgsql-jobs/JOB_URL/threads.php

== PostgreSQL Local ==

Everything this week was global.

== PostgreSQL in the News ==

Planet PostgreSQL: http://planet.postgresql.org/

PostgreSQL Weekly News is brought to you this week by David Fetter

Submit news and announcements by Sunday at 3:00pm Pacific time.
Please send English language ones to david@fetter.org, German language
to pwn@pgug.de, Italian language to pwn@itpug.org.  Spanish language
to pwn@arpug.com.ar.
