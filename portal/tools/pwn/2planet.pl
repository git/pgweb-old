#!/usr/bin/perl
use strict;
use warnings;

chomp(my $file = $ARGV[0] || `date +'%Y%m%d.pwn'`);

open my $fh, '<', $file
    or die "Can't open $file for reading: $!";
while (<$fh>) {
    s{(^|\s)([A-Z]\S*[a-z]+[A-Z])}{$1!$2}g;
    s{== (.*) ==}{**$1**}g;
    print;
}
