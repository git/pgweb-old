# LaTeX2HTML 98.1p1 release (March 2nd, 1998)
# Associate labels original text with physical files.


$key = q/column_label_section/;
$external_labels{$key} = "$URL/" . q|node38.html|; 
$noresave{$key} = "$nosave";

$key = q/creating_a_database/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/data_types_section/;
$external_labels{$key} = "$URL/" . q|node34.html|; 
$noresave{$key} = "$nosave";

$key = q/vacuum_section/;
$external_labels{$key} = "$URL/" . q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_10/;
$external_labels{$key} = "$URL/" . q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_11/;
$external_labels{$key} = "$URL/" . q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/issuing_database_commands/;
$external_labels{$key} = "$URL/" . q|node16.html|; 
$noresave{$key} = "$nosave";

$key = q/table_and_column_references/;
$external_labels{$key} = "$URL/" . q|node56.html|; 
$noresave{$key} = "$nosave";

$key = q/insert_section/;
$external_labels{$key} = "$URL/" . q|node25.html|; 
$noresave{$key} = "$nosave";

$key = q/functions_and_operators/;
$external_labels{$key} = "$URL/" . q|node46.html|; 
$noresave{$key} = "$nosave";

$key = q/constraint_chapter/;
$external_labels{$key} = "$URL/" . q|node127.html|; 
$noresave{$key} = "$nosave";

$key = q/backup_and_restore/;
$external_labels{$key} = "$URL/" . q|node179.html|; 
$noresave{$key} = "$nosave";

$key = q/abort_section/;
$external_labels{$key} = "$URL/" . q|node100.html|; 
$noresave{$key} = "$nosave";

$key = q/non-standard_features_chapter/;
$external_labels{$key} = "$URL/" . q|node199.html|; 
$noresave{$key} = "$nosave";

$key = q/manual_pages/;
$external_labels{$key} = "$URL/" . q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/joining_tables_chapter/;
$external_labels{$key} = "$URL/" . q|node55.html|; 
$noresave{$key} = "$nosave";

$key = q/oid_section/;
$external_labels{$key} = "$URL/" . q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/numbering_rows_chapter/;
$external_labels{$key} = "$URL/" . q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/primary_and_foreign_keys/;
$external_labels{$key} = "$URL/" . q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/group_by_section/;
$external_labels{$key} = "$URL/" . q|node51.html|; 
$noresave{$key} = "$nosave";

$key = q/type_conversion_section/;
$external_labels{$key} = "$URL/" . q|node91.html|; 
$noresave{$key} = "$nosave";

$key = q/alter_table/;
$external_labels{$key} = "$URL/" . q|node120.html|; 
$noresave{$key} = "$nosave";

$key = q/rules_section/;
$external_labels{$key} = "$URL/" . q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/set_section/;
$external_labels{$key} = "$URL/" . q|node47.html|; 
$noresave{$key} = "$nosave";

$key = q/exotic_types/;
$external_labels{$key} = "$URL/" . q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/processing_select_results_section/;
$external_labels{$key} = "$URL/" . q|node152.html|; 
$noresave{$key} = "$nosave";

$key = q/character_types/;
$external_labels{$key} = "$URL/" . q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/using_null_values_section/;
$external_labels{$key} = "$URL/" . q|node36.html|; 
$noresave{$key} = "$nosave";

$key = q/Interfaces/;
$external_labels{$key} = "$URL/" . q|node146.html|; 
$noresave{$key} = "$nosave";

$key = q/performance_section/;
$external_labels{$key} = "$URL/" . q|node182.html|; 
$noresave{$key} = "$nosave";

$key = q/trigger_section/;
$external_labels{$key} = "$URL/" . q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/access_configuration/;
$external_labels{$key} = "$URL/" . q|node178.html|; 
$noresave{$key} = "$nosave";

$key = q/administration_chapter/;
$external_labels{$key} = "$URL/" . q|node174.html|; 
$noresave{$key} = "$nosave";

$key = q/extending_chapter/;
$external_labels{$key} = "$URL/" . q|node168.html|; 
$noresave{$key} = "$nosave";

$key = q/query_tools_chapter/;
$external_labels{$key} = "$URL/" . q|node142.html|; 
$noresave{$key} = "$nosave";

$key = q/aggregate_chapter/;
$external_labels{$key} = "$URL/" . q|node49.html|; 
$noresave{$key} = "$nosave";

$key = q/functions_section/;
$external_labels{$key} = "$URL/" . q|node92.html|; 
$noresave{$key} = "$nosave";

$key = q/documentation_section/;
$external_labels{$key} = "$URL/" . q|node189.html|; 
$noresave{$key} = "$nosave";

$key = q/server-side_programming_chapter/;
$external_labels{$key} = "$URL/" . q|node162.html|; 
$noresave{$key} = "$nosave";

$key = q/outer_join_section/;
$external_labels{$key} = "$URL/" . q|node82.html|; 
$noresave{$key} = "$nosave";

$key = q/Installation/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/when_to_use_a_database/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/regular_expression_section/;
$external_labels{$key} = "$URL/" . q|node43.html|; 
$noresave{$key} = "$nosave";

$key = q/advanced_psql/;
$external_labels{$key} = "$URL/" . q|node143.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_1/;
$external_labels{$key} = "$URL/" . q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/indexing/;
$external_labels{$key} = "$URL/" . q|node107.html|; 
$noresave{$key} = "$nosave";

$key = q/check_constraint_section/;
$external_labels{$key} = "$URL/" . q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_2/;
$external_labels{$key} = "$URL/" . q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/transaction_isolation/;
$external_labels{$key} = "$URL/" . q|node101.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_3/;
$external_labels{$key} = "$URL/" . q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/vacuum_analyze_section/;
$external_labels{$key} = "$URL/" . q|node111.html|; 
$noresave{$key} = "$nosave";

$key = q/performance_chapter/;
$external_labels{$key} = "$URL/" . q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_4/;
$external_labels{$key} = "$URL/" . q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/Default_section/;
$external_labels{$key} = "$URL/" . q|node37.html|; 
$noresave{$key} = "$nosave";

$key = q/additional_resources_chapter/;
$external_labels{$key} = "$URL/" . q|node187.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_5/;
$external_labels{$key} = "$URL/" . q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/initdb/;
$external_labels{$key} = "$URL/" . q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_6/;
$external_labels{$key} = "$URL/" . q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/self-join_section/;
$external_labels{$key} = "$URL/" . q|node65.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_7/;
$external_labels{$key} = "$URL/" . q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_8/;
$external_labels{$key} = "$URL/" . q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_9/;
$external_labels{$key} = "$URL/" . q|node287.html|; 
$noresave{$key} = "$nosave";

1;

