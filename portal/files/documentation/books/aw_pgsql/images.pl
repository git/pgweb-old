# LaTeX2HTML 98.1p1 release (March 2nd, 1998)
# Associate images original text with physical files.


$key = q/{inline}mboxbullet{inline}MSF=1.6;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="img61.gif"
 ALT="$\mbox{$\bullet$}$">|; 

$key = q/{inline}(pm){inline}MSF=1.6;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img22.gif"
 ALT="\( \pm \)">|; 

$key = q/{}resizebox*!0.2textheightincludegraphicspostmaster.eps{}AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="431" HEIGHT="178" ALIGN="BOTTOM" BORDER="0"
 SRC="img39.gif"
 ALT="\resizebox*{!}{0.2\textheight}{\includegraphics{postmaster.eps}}">|; 

$key = q/{}resizebox*!0.3textheightincludegraphicsjoins.eps{}AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="609" HEIGHT="267" ALIGN="BOTTOM" BORDER="0"
 SRC="img9.gif"
 ALT="\resizebox*{!}{0.3\textheight}{\includegraphics{joins.eps}}">|; 

$key = q/{inline}(times){inline}MSF=1.6;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img24.gif"
 ALT="\( \times \)">|; 

$key = q/{}resizebox*!0.3textheightincludegraphicsdatabases.eps{}AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="438" HEIGHT="269" ALIGN="BOTTOM" BORDER="0"
 SRC="img6.gif"
 ALT="\resizebox*{!}{0.3\textheight}{\includegraphics{databases.eps}}">|; 

$key = q/{}resizebox*!0.3textheightincludegraphicslibpq.eps{}AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="262" HEIGHT="269" ALIGN="BOTTOM" BORDER="0"
 SRC="img28.gif"
 ALT="\resizebox*{!}{0.3\textheight}{\includegraphics{libpq.eps}}">|; 

$key = q/{}resizebox*!0.4textheightincludegraphicspgaccess.eps{}AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="294" HEIGHT="359" ALIGN="BOTTOM" BORDER="0"
 SRC="img26.gif"
 ALT="\resizebox*{!}{0.4\textheight}{\includegraphics{pgaccess.eps}}">|; 

$key = q/{}resizebox*!0.4textheightincludegraphicspgaccess2.eps{}AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="551" HEIGHT="359" ALIGN="BOTTOM" BORDER="0"
 SRC="img27.gif"
 ALT="\resizebox*{!}{0.4\textheight}{\includegraphics{pgaccess2.eps}}">|; 

$key = q/{inline}(pm4times10^18){inline}MSF=1.6;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img23.gif"
 ALT="\( \pm 4\times 10^{18} \)">|; 

1;

