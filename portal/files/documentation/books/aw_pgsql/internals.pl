# LaTeX2HTML 98.1p1 release (March 2nd, 1998)
# Associate internals original text with physical files.


$key = q/column_label_section/;
$ref_files{$key} = "$dir".q|node38.html|; 
$noresave{$key} = "$nosave";

$key = q/creating_a_database/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/data_types_section/;
$ref_files{$key} = "$dir".q|node34.html|; 
$noresave{$key} = "$nosave";

$key = q/vacuum_section/;
$ref_files{$key} = "$dir".q|node110.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_10/;
$ref_files{$key} = "$dir".q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_11/;
$ref_files{$key} = "$dir".q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/issuing_database_commands/;
$ref_files{$key} = "$dir".q|node16.html|; 
$noresave{$key} = "$nosave";

$key = q/table_and_column_references/;
$ref_files{$key} = "$dir".q|node56.html|; 
$noresave{$key} = "$nosave";

$key = q/insert_section/;
$ref_files{$key} = "$dir".q|node25.html|; 
$noresave{$key} = "$nosave";

$key = q/functions_and_operators/;
$ref_files{$key} = "$dir".q|node46.html|; 
$noresave{$key} = "$nosave";

$key = q/constraint_chapter/;
$ref_files{$key} = "$dir".q|node127.html|; 
$noresave{$key} = "$nosave";

$key = q/backup_and_restore/;
$ref_files{$key} = "$dir".q|node179.html|; 
$noresave{$key} = "$nosave";

$key = q/abort_section/;
$ref_files{$key} = "$dir".q|node100.html|; 
$noresave{$key} = "$nosave";

$key = q/non-standard_features_chapter/;
$ref_files{$key} = "$dir".q|node199.html|; 
$noresave{$key} = "$nosave";

$key = q/manual_pages/;
$ref_files{$key} = "$dir".q|node200.html|; 
$noresave{$key} = "$nosave";

$key = q/joining_tables_chapter/;
$ref_files{$key} = "$dir".q|node55.html|; 
$noresave{$key} = "$nosave";

$key = q/oid_section/;
$ref_files{$key} = "$dir".q|node71.html|; 
$noresave{$key} = "$nosave";

$key = q/numbering_rows_chapter/;
$ref_files{$key} = "$dir".q|node70.html|; 
$noresave{$key} = "$nosave";

$key = q/primary_and_foreign_keys/;
$ref_files{$key} = "$dir".q|node68.html|; 
$noresave{$key} = "$nosave";

$key = q/group_by_section/;
$ref_files{$key} = "$dir".q|node51.html|; 
$noresave{$key} = "$nosave";

$key = q/type_conversion_section/;
$ref_files{$key} = "$dir".q|node91.html|; 
$noresave{$key} = "$nosave";

$key = q/alter_table/;
$ref_files{$key} = "$dir".q|node120.html|; 
$noresave{$key} = "$nosave";

$key = q/rules_section/;
$ref_files{$key} = "$dir".q|node124.html|; 
$noresave{$key} = "$nosave";

$key = q/set_section/;
$ref_files{$key} = "$dir".q|node47.html|; 
$noresave{$key} = "$nosave";

$key = q/exotic_types/;
$ref_files{$key} = "$dir".q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/processing_select_results_section/;
$ref_files{$key} = "$dir".q|node152.html|; 
$noresave{$key} = "$nosave";

$key = q/character_types/;
$ref_files{$key} = "$dir".q|node90.html|; 
$noresave{$key} = "$nosave";

$key = q/using_null_values_section/;
$ref_files{$key} = "$dir".q|node36.html|; 
$noresave{$key} = "$nosave";

$key = q/Interfaces/;
$ref_files{$key} = "$dir".q|node146.html|; 
$noresave{$key} = "$nosave";

$key = q/performance_section/;
$ref_files{$key} = "$dir".q|node182.html|; 
$noresave{$key} = "$nosave";

$key = q/trigger_section/;
$ref_files{$key} = "$dir".q|node166.html|; 
$noresave{$key} = "$nosave";

$key = q/access_configuration/;
$ref_files{$key} = "$dir".q|node178.html|; 
$noresave{$key} = "$nosave";

$key = q/administration_chapter/;
$ref_files{$key} = "$dir".q|node174.html|; 
$noresave{$key} = "$nosave";

$key = q/extending_chapter/;
$ref_files{$key} = "$dir".q|node168.html|; 
$noresave{$key} = "$nosave";

$key = q/query_tools_chapter/;
$ref_files{$key} = "$dir".q|node142.html|; 
$noresave{$key} = "$nosave";

$key = q/aggregate_chapter/;
$ref_files{$key} = "$dir".q|node49.html|; 
$noresave{$key} = "$nosave";

$key = q/functions_section/;
$ref_files{$key} = "$dir".q|node92.html|; 
$noresave{$key} = "$nosave";

$key = q/documentation_section/;
$ref_files{$key} = "$dir".q|node189.html|; 
$noresave{$key} = "$nosave";

$key = q/server-side_programming_chapter/;
$ref_files{$key} = "$dir".q|node162.html|; 
$noresave{$key} = "$nosave";

$key = q/outer_join_section/;
$ref_files{$key} = "$dir".q|node82.html|; 
$noresave{$key} = "$nosave";

$key = q/Installation/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/when_to_use_a_database/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/regular_expression_section/;
$ref_files{$key} = "$dir".q|node43.html|; 
$noresave{$key} = "$nosave";

$key = q/advanced_psql/;
$ref_files{$key} = "$dir".q|node143.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_1/;
$ref_files{$key} = "$dir".q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/indexing/;
$ref_files{$key} = "$dir".q|node107.html|; 
$noresave{$key} = "$nosave";

$key = q/check_constraint_section/;
$ref_files{$key} = "$dir".q|node132.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_2/;
$ref_files{$key} = "$dir".q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/transaction_isolation/;
$ref_files{$key} = "$dir".q|node101.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_3/;
$ref_files{$key} = "$dir".q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/vacuum_analyze_section/;
$ref_files{$key} = "$dir".q|node111.html|; 
$noresave{$key} = "$nosave";

$key = q/performance_chapter/;
$ref_files{$key} = "$dir".q|node106.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_4/;
$ref_files{$key} = "$dir".q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/Default_section/;
$ref_files{$key} = "$dir".q|node37.html|; 
$noresave{$key} = "$nosave";

$key = q/additional_resources_chapter/;
$ref_files{$key} = "$dir".q|node187.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_5/;
$ref_files{$key} = "$dir".q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/initdb/;
$ref_files{$key} = "$dir".q|node198.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_6/;
$ref_files{$key} = "$dir".q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/self-join_section/;
$ref_files{$key} = "$dir".q|node65.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_7/;
$ref_files{$key} = "$dir".q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_8/;
$ref_files{$key} = "$dir".q|node287.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_9/;
$ref_files{$key} = "$dir".q|node287.html|; 
$noresave{$key} = "$nosave";

1;

