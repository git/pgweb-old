/* Tinymce integration code */
tinyMCE.init({
mode: "textareas",
theme: "advanced",
theme_advanced_buttons1 : "bold,italic,underline,separator,justifyleft,justifycenter,justfyright,bullist,numlist,undo,redo,link,unlink,image,code,formatselect",
theme_advanced_blockformats : "p,div,h2,h3,blockquote,dt,dd,code",
theme_advanced_buttons2 : "",
theme_advanced_buttons3 : "",
theme_advanced_toolbar_location: "top",
theme_advanced_toolbar_align: "left",
extended_valid_elements : "a[name|href|title],img[src|border=0|alt|title|hspace|vspace|width|height|align],hr[width|size],span[align],code",
dialog_type : "modal",
doctype: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
theme_advanced_path: true,
remove_linebreaks: false,
relative_urls: false,
remove_script_host: true
});
