<?php

//
// List of news items
//
// $Id: news.php,v 1.8 2007-03-12 14:51:43 mha Exp $
//
class Admin_News extends Admin_BasePage {
    function __construct() {
        $this->content_template = 'admin/news.html';
    }
    function Render() {
        global $_SETTINGS;
        global $_LANGUAGES;

        if (isset($_GET['action']) && $_GET['action'] == 'delete') {
            $this->pg_query_params("DELETE FROM news WHERE id=$1", array($_GET['id']));
            header('Location: /admin/news.php');
            exit(0);
        }

        $rs = $this->pg_query(
            "SELECT n.id, n.posted, n.posted_by, n.approved, n.active, nt.headline, nt.language,\n" .
            "       EXTRACT(EPOCH FROM nt.modified) AS modified\n" .
            "FROM news n, news_text nt\n" .
            "WHERE n.id = nt.newsid\n" .
            "ORDER BY n.approved, n.posted DESC, n.id DESC" .
            ((isset($_GET['all']) && $_GET['all']=='1')?'':' LIMIT 20')
        );

        $news      = array();
        $newsLangs = array();
        $newsId    = -1;

        for ($i = 0, $rows = pg_num_rows($rs); $i < $rows; $i++) {
            $ary = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            if ($newsId != $ary['id']) {
                $news[] = array(
                    'id'        => $ary['id'],
                    'posted'    => $ary['posted'],
                    'posted_by' => $ary['posted_by'],
                    'approved'  => $ary['approved'],
                    'active'    => $ary['active']
                );
            }
            $newsId = $ary['id'];
            if ($ary['language'] == $_SETTINGS['defaultlanguage']) {
                $news[count($news) - 1]['headline'] = $ary['headline'];
            }
            $newsLangs[$newsId][$ary['language']] = $ary['modified'];
        }

        foreach ($news as $item) {
            $langs = $newsLangs[$item['id']];
            foreach ($_LANGUAGES as $handler => $name) {
                if ($handler != $_SETTINGS['defaultlanguage']) {
                    $this->tpl->setVariable(array(
                        'lang_name'     => $name,
                        'lang_handler'  => $handler,
                        'lang_exists'   => !empty($langs[$handler])? 't': 'f',
                        'lang_up2date'  => (empty($langs[$handler]) || $langs[$handler] >= $langs[$_SETTINGS['defaultlanguage']])? 't': 'f'
                    ));
                    $this->tpl->parse('news_translations_loop');
                }
            }
            $this->tpl->setVariable($item);
            $this->tpl->parse('news_loop');
        }
    }
}

?>
