<?php
//
// List of professional services 
//
// $Id: prof.php,v 1.3 2007-03-12 14:51:43 mha Exp $
//
class Admin_Prof extends Admin_BasePage {
    function __construct() {
        $this->content_template = 'admin/prof.html';
    }

    function Render() {

        $rs = $this->pg_query("SELECT id,name,CASE WHEN approved THEN 'Yes' ELSE 'No' END AS approved,lastconfirmed,case when provides_support then 'Support' ELSE '' END || ' ' || case when provides_hosting then 'Hosting' ELSE '' END AS provides FROM profserv ORDER BY approved,name,id");

        for ($i = 0, $rows = pg_num_rows($rs); $i < $rows; $i++) {
            $cons = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            $this->tpl->setVariable($cons);
            $this->tpl->parse('cons_loop');
        }
    }
}
?>
