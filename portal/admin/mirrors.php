<?php

//
// List of mirrors
//
// $Id: mirrors.php,v 1.4 2007-03-12 14:51:43 mha Exp $
//
class Admin_Mirrors extends Admin_BasePage {
    function __construct() {
        $this->content_template = 'admin/mirrors.html';
    }

    function Render() {
        if (isset($_GET['action']) && $_GET['action'] == 'delete') {
            $this->pg_query_params("DELETE FROM mirrors WHERE id=$1", array($_GET['id']));
            header('Location: /admin/mirrors.php');
            exit(0);
        }
        $rs = $this->pg_query(
            "SELECT id, country_name, mirror_type, mirror_active, mirror_dns, mirror_private, host_sponsor, mirror_last_rsync,\n" .
            "       mirror_type || CASE WHEN mirror_index = 0 THEN ''::text ELSE mirror_index::text END || '.' || country_code || '.postgresql.org' AS mirror_hostname\n" .
            "FROM mirrors\n" .
            "ORDER BY country_code, mirror_type, mirror_index"
        );

        for ($i = 0, $rows = pg_num_rows($rs); $i < $rows; $i++) {
            $mirror = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            $this->tpl->setVariable($mirror);
            $this->tpl->parse('mirrors_loop');
        }
    }
}
?>
