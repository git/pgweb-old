<?php

//
// Edit the comment
//
// $Id: commentedit.php,v 1.2 2007-03-16 11:25:16 mha Exp $
//
class Admin_CommentEdit extends PgForm {
    function __construct() {
        $this->navsection = 'admin';
    }
    function SetupForm() {
        if (!empty($_GET['id'])) {
            $id = intval($_GET['id']);
            $rs = $this->pg_query(
                "SELECT id, posted_by, comment, approved, processed\n" .
                "FROM comments\n" .
                "WHERE id = " . $id
            );
            if (0 == pg_num_rows($rs)) {
                throw new Exception('Cannot find comment #' . $id . '. It was probably deleted already.');
            } else {
                $defaults = array_map('html_entity_decode', pg_fetch_array($rs, 0, PGSQL_ASSOC));
                $defaults['posted_by'] = str_replace(' AT ', '@', $defaults['posted_by']);
                $defaults['state'] = ('f'==$defaults['processed']?0:('f'==$defaults['approved']?1:2));
                $this->form->setDefaults($defaults);
            }
        }

        $this->form->addElement('hidden',     'id');
        $this->form->addElement('select',		'state', 'State', array(0=>'Pending',1=>'Saved',2=>'Approved'));
        $this->form->addElement('text',       'posted_by', 'Posted by:', array('size' => 50, 'maxlength' => 255));
        $this->form->addElement('textarea',   'comment', 'Comment text:', array('rows' => 8, 'cols' => 66, 'wrap' => 'soft'));

        $this->form->applyFilter(array('posted_by', 'comment'), 'trim');
        $this->form->applyFilter('id', 'intval');

        $this->form->addRule('posted_by', 'The poster info is required', 'required', null, 'client');
        $this->form->addRule('comment', 'The comment is required', 'required', null, 'client');
        $this->form->addRule('comment', 'The comment should be at least 10 characters long.', 'minlength', 10, 'client');
    }
    function ProcessForm($f) {
        global $_SETTINGS;

        $f  = array_map('pg_escape_string', $f);

        $rs = $this->pg_query( "SELECT approved,processed,version,file FROM comments WHERE id=" . $f['id']);
        if (0 == pg_num_rows($rs)) {
            throw new Exception('Cannot find comment #' . $id . '.');
        }
        $oldstate = ('f'==pg_fetch_result($rs,0,1)?0:('f'==pg_fetch_result($rs,0,0)?1:2));

        $approvedvals = array(0=>'f', 1=>'f', 2=>'t');
        $processedvals = array(0=>'f', 1=>'t', 2=>'t');
        $this->pg_query(
            "UPDATE comments SET posted_by = '" . htmlspecialchars($f['posted_by'], ENT_NOQUOTES) . "',\n" .
            "    \"comment\" = '" . htmlspecialchars($f['comment'], ENT_NOQUOTES) . "',\n" .
            "    \"approved\" = '" . $approvedvals[$f['state']] . "', \"processed\"='" . $processedvals[$f['state']] . "' " .
            "WHERE id = " . $f['id']
        );

        if ($oldstate != $f['state']) {
            // State changed, send mail
            switch ($f['state']) {
            case 0: $action_past_tense = 're-pendinged';break;
            case 1: $action_past_tense = 'saved';break;
            case 2: $action_past_tense = 'approved';break;
            default: $action_past_tense = 'unknown action';break;
            }
            $mailsubj = 'Comment #' . $id . ' was ' . $action_past_tense . ' by ' . $_SERVER['PHP_AUTH_USER'];
            $mailtext = "Author: " . $f['posted_by'] . "\n" .
                "Page:   " . pg_fetch_result($rs,0,2) . '/' . pg_fetch_result($rs,0,3) . "\n----\n" .
                $f['comment'];
            @mail($_SETTINGS['notifymail'], $mailsubj, $mailtext);
        }
        $this->redirect_relative = '/admin/comments.php';
    }
    function RenderThanks() {
    }
}

?>
