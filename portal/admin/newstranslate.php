<?php

//
// Translating a news item
//
// $Id: newstranslate.php,v 1.1 2007-03-12 14:51:43 mha Exp $
//
class Admin_NewsTranslate extends PgForm {
    function __construct() {
        $this->navsection = 'admin';
    }

    function SetupForm() {
        $defaults = null;
        if (!empty($_GET['id']) && !empty($_GET['lang']) && 'en' != $_GET['lang']) {
            $id   = intval($_GET['id']);
            $lang = pg_escape_string($_GET['lang']);
            $rs   = $this->pg_query(
                "SELECT ne.newsid AS id, '{$lang}' AS lang, nt.headline, nt.summary, nt.story,\n" .
                "       ne.headline_en, ne.summary_en, ne.story_en\n" .
                "FROM (\n" .
                "    SELECT newsid, headline AS headline_en, summary AS summary_en, story AS story_en\n" .
                "    FROM news_text\n" .
                "    WHERE newsid = {$id} AND\n" .
                "          language = 'en'\n" .
                ") AS ne LEFT JOIN (\n" .
                "    SELECT newsid, headline, summary, story\n" .
                "    FROM news_text\n" .
                "    WHERE newsid = {$id} AND\n" .
                "          language = '{$lang}'\n" .
                ") AS nt ON (ne.newsid = nt.newsid)"
            );
            if (pg_num_rows($rs)) {
                $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
            }
        }
        if (!empty($defaults)) {
            $this->form->setDefaults($defaults);
        }

        $en = array();
        $this->form->addElement('hidden',     'id');
        $this->form->addElement('hidden',     'lang');
        $en[] =& $this->form->addElement('text',  'headline_en', 'English headline:');
        $this->form->addElement('text',       'headline', 'Headline:', array('size' => 50, 'maxlength' => 100));
        $en[] =& $this->form->addElement('textarea',   'summary_en', 'English summary:');
        $this->form->addElement('textarea',   'summary', 'Summary:', array('rows' => 5, 'cols' => 50));
        $en[] =& $this->form->addElement('textarea',   'story_en', 'English story:');
        $this->form->addElement('textarea',   'story', 'Story:', array('rows' => 15, 'cols' => 50));
        $buttons = array();
        $buttons[] =& $this->form->createElement('button',     null, 'Preview...', array('onclick' => "doPreview(this.form['headline'].value, this.form['summary'].value, this.form['story'].value);"));
        $this->form->addGroup($buttons, null, null, '&nbsp;', false);

        foreach (array_keys($en) as $key) {
            $en[$key]->freeze();
        }

        $this->form->applyFilter('__ALL__', 'trim');
        $this->form->applyFilter('id', 'intval');

        $this->form->addRule('headline', 'The headline is required.', 'required', null, 'client');
        $this->form->addRule('summary', 'The summary is required.', 'required', null, 'client');
        $this->form->addRule('story', 'The story is required.', 'required', null, 'client');

        $this->form->addRule('headline', 'The headline must be between 3 and 100 characters long.', 'rangelength', array(3, 100), 'client');
        $this->form->addRule('summary', 'The summary must be between 3 and 500 characters long.', 'rangelength', array(3, 500), 'client');
        $this->form->addRule('story', 'The story must be at least 3 characters long.', 'minlength', 3, 'client');
    }

    function ProcessForm($f) {
        $f  = array_map('pg_escape_string', $f);
        $rs = $this->pg_query(
            "UPDATE news_text SET headline = '{$f['headline']}', summary = '{$f['summary']}', story = '{$f['story']}'\n" .
            "WHERE newsid = {$f['id']} AND language = '{$f['lang']}'"
        );
        if (0 == pg_affected_rows($rs)) {
            $this->pg_query(
                "INSERT INTO news_text\n" .
                "    (newsid, headline, summary, story, language)\n" .
                "VALUES\n" .
                "    ({$f['id']}, '{$f['headline']}', '{$f['summary']}', '{$f['story']}', '{$f['lang']}')"
            );
        }
        $this->redirect_relative='/admin/news.php';
    }

    function RenderThanks() {
    }
}
?>
