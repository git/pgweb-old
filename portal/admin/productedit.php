<?php

//
// Adding / editing a product
//
// $Id: $
//
class Admin_ProductEdit extends PgForm {
    function __construct() {
        $this->navsection = 'admin';
    }
    function SetupForm() {
        if ('POST' != $_SERVER['REQUEST_METHOD']) {
            if (isset($_GET['action']) && $_GET['action'] == 'delete') {
                if (empty($_GET['id'])) {
                    throw new Exception('Id not specified');
                }
                $this->pg_query('DELETE FROM products WHERE id=' . intval($_GET['id']));
                header('Location: products.php');
                exit(0);
            }

            if (isset($_GET['action']) && $_GET['action'] == 'approve') {
                if (empty($_GET['id'])) {
                    throw new Exception('Id not specified');
                }
                $this->pg_query('UPDATE products SET approved = true WHERE id=' . intval($_GET['id']));

                // Approve the publishing organisation if it's also specifed
                if (isset($_GET['publisher']) && intval($_GET['publisher']) > 0) {
                    $this->pg_query('UPDATE organisations SET approved = true WHERE id=' . intval($_GET['publisher']));
                }

                header('Location: products.php');
                exit(0);
            }

            $defaults = array('lastconfirmed'=>date('Y-m-d H:i'));
            if (!empty($_GET['id'])) {
                $rs = $this->pg_query(
                    "SELECT id,publisher,name,url,category,description,price,licence,contact,NULLIF(approved,false) AS approved,lastconfirmed FROM products WHERE id=" . intval($_GET['id']));
                if (pg_num_rows($rs)) {
                    $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
                }
            }
            $this->form->setDefaults($defaults);
        }

        $txtf = array('size' => 52);
        $txta = array('cols' => 40, 'rows'=>8);
        $this->form->addElement('hidden','id');
        $this->form->addElement('header',null,'Approval');
        $this->form->addElement('checkbox','approved',null,'Is approved (<b>Always</b> verify all HTML content before approving!)');
        $this->form->addElement('header',null,'Basic data');
	$this->form->addElement('select','publisher','Publisher',$this->fetch_publisher_list());
        $this->form->addElement('text','name','Name', $txtf);
        $this->form->addElement('text','url','URL',$txtf);
	$this->form->addElement('select','category','Category',$this->fetch_category_list());
	$this->form->addElement('select','licence','Licence type',$this->fetch_licence_list());
        $this->form->addElement('textarea','description','Description', $txta);
        $this->form->addElement('text','price','Price', $txtf);
	$this->form->addElement('select','contact','Contact user ID (private)',$this->fetch_contact_list());
        $this->form->addElement('text','lastconfirmed','Last confirmed', $txtf);

        $this->form->applyFilter('__ALL__', 'trim');
        $this->form->addRule('publisher','Publisher required','required',null,'client');
        $this->form->addRule('name','Name required','required',null,'client');
        $this->form->addRule('url','URL required','required',null,'client');
        $this->form->addRule('url','URL must be to a webpage','regex','/^https?:\/\//','client');
        $this->form->addRule('category','Category required','required',null,'client');
        $this->form->addRule('licence','Licence type required','required',null,'client');
        $this->form->addRule('description','Description required','required',null,'client');
        $this->form->addRule('lastconfirmed','Last confirmed required','required',null,'client');
    }

    function ProcessForm($f) {
        global $_SETTINGS;

        $id = $f['id'];
        $f['approved'] = empty($f['approved'])?'f':'t';
        unset($f['id']);

        $eqns = $keys = $vals = array();
        foreach ($f as $key => $val) {
            if ($key == 'submit') continue;
            $keys[] = $key;
            if (empty($val) || $val=='') {
                $vals[] = 'NULL';
                $eqns[] = $key . ' = NULL';
            } else {
                $vals[] = "'" . pg_escape_string($val) . "'";
                $eqns[] = $key . ' = \'' . pg_escape_string($val) . '\'';
            }
        }

        if (empty($id)) {
            $r = $this->pg_query("SELECT nextval('products_id_seq')");
            list($id) = pg_fetch_row($r,0);

            $this->pg_query('INSERT INTO products (id,' . implode(', ', $keys) . ") VALUES ({$id}," . implode(', ',$vals) . ')');
        } else {
            $r = $this->pg_query("SELECT approved FROM products WHERE id={$id}");
            list($oldapproved) = pg_fetch_row($r,0);

            $this->pg_query("UPDATE products SET " . implode(', ', $eqns) . " WHERE id={$id}");

            if ($oldapproved=='f' && $f['approved']=='t') {
                $mailtext = 'Edit: ' . $_SETTINGS['masterserver'] . '/admin/product-edit.php?id=' . $id;
                mail($_SETTINGS['notifymail'], 'Product ' . $f['name'] . ' was approved by ' . $_SERVER['PHP_AUTH_USER'], $mailtext);
            }
        }
        $this->redirect_relative = '/admin/products.php';
    }

    function RenderThanks() {
    }

    function fetch_publisher_list() {
        $rs = $this->pg_query('SELECT id,name FROM organisations ORDER BY name');
        $c = array();
        for ($i = 0; $i < pg_num_rows($rs); $i++) {
            $row = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            $c[$row['id']] = $row['name'];
        }
        return $c;
    }

    function fetch_category_list() {
        $rs = $this->pg_query('SELECT id,name FROM product_categories ORDER BY name');
        $c = array();
        for ($i = 0; $i < pg_num_rows($rs); $i++) {
            $row = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            $c[$row['id']] = $row['name'];
        }
        return $c;
    }

    function fetch_licence_list() {
        $c = array();

        $c['c'] = 'Commercial';
        $c['f'] = 'Freeware';
        $c['m'] = 'Multiple';
        $c['o'] = 'Open source';
 
        return $c;
    }

    function fetch_contact_list() {
        $rs = $this->pg_query('SELECT userid,CASE WHEN trim(fullname) = \'\' THEN userid ELSE coalesce(fullname,userid) END AS fullname FROM users ORDER BY fullname');
        $c = array();
        for ($i = 0; $i < pg_num_rows($rs); $i++) {
            $row = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            $c[$row['userid']] = $row['fullname'];
        }
        return $c;
    }


}

?>
