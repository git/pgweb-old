<?php

//
// Adding / editing a mailinglist
//
// $Id$
//
class Admin_ListEdit extends PgForm {
    function __construct() {
        $this->navsection = 'admin';
    }
    function SetupForm() {
        if ('POST' != $_SERVER['REQUEST_METHOD']) {
            if (isset($_GET['action']) && $_GET['action'] == 'del') {
                if (empty($_GET['id'])) {
                    throw new Exception('Id not specified');
                }
                $this->pg_query('DELETE FROM lists WHERE id=' . intval($_GET['id']));
                header('Location: lists.php');
                exit(0);
            }

            if (!empty($_GET['id'])) {
                $rs = $this->pg_query(
                    "SELECT id,name,active,grp,description,shortdesc FROM lists WHERE id=" . intval($_GET['id']));
                if (pg_num_rows($rs)) {
                    $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
                }
            }
            $this->form->setDefaults($defaults);
        }

        $txtf = array('size' => 52);
        $txta = array('cols' => 40, 'rows'=>8);
        $txtd = array('cols' => 30, 'rows'=>2);
        $this->form->addElement('hidden','id');
        $this->form->addElement('text','name','Name', $txtf);
        $this->form->addElement('select','grp','Group', $this->fetch_group_list());
        $this->form->addElement('checkbox','active',null,'Active');
        $this->form->addElement('textarea','description','Description', $txta);
        $this->form->addElement('textarea','shortdesc','Short Description', $txtd);

        $this->form->applyFilter('__ALL__', 'trim');
        $this->form->addRule('name','Name required','required',null,'client');
        $this->form->addRule('description','Description required','required',null,'client');
    }

    function ProcessForm($f) {
        if (empty($f['id'])) {
            $this->pg_query_params('INSERT INTO lists (id,name,grp,active,description,shortdesc) SELECT max(id)+1,$1,$2,$3,$4,$5 FROM lists',
                array($f['name'], $f['grp'], empty($f['active'])?0:1, $f['description'], $f['shortdesc']));
        } else {
            $this->pg_query_params('UPDATE lists SET name=$1, grp=$2, active=$3, description=$4, shortdesc=$5 WHERE id=$6',
                array($f['name'], $f['grp'], empty($f['active'])?0:1, $f['description'], $f['shortdesc'], $f['id']));
        }
        $this->redirect_relative = '/admin/lists.php';
    }
    function RenderThanks() {
    }

    function fetch_group_list() {
        $rs = $this->pg_query('SELECT id,name FROM listgroups ORDER BY id');
        $g = array();
        while ($r = pg_fetch_row($rs)) {
            $g[$r[0]] = $r[1];
        }
        return $g;
    }
}

?>
