<?php

//
// List of surveys
//
// $Id: surveys.php,v 1.4 2007-03-12 14:51:43 mha Exp $
//
class Admin_Surveys extends Admin_BasePage {
    function __construct() {
        $this->content_template = 'admin/surveys.html';
    }

    function Render() {
        global $_SETTINGS;
        global $_LANGUAGES;

        if (isset($_GET['action']) && $_GET['action'] == 'delete') {
            $this->pg_query_params("DELETE FROM surveys WHERE id=$1", array($_GET['id']));
            header('Location: /admin/surveys.php');
            exit(0);
        }

        $rs = $this->pg_query(
            "SELECT s.id, s.respondants, s.current, q.question, q.language,\n" .
            "       EXTRACT(EPOCH FROM q.modified) AS modified\n" .
            "FROM surveys s, survey_questions q\n" .
            "WHERE s.id = q.surveyid\n" .
            "ORDER BY s.id DESC"
        );

        $surveys     = array();
        $surveyLangs = array();
        $surveyId    = -1;

        for ($i = 0, $rows = pg_num_rows($rs); $i < $rows; $i++) {
            $ary = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            if ($surveyId != $ary['id']) {
                $surveys[] = array(
                    'id'          => $ary['id'],
                    'respondents' => $ary['respondants'],
                    'current'     => $ary['current']
                );
            }
            $surveyId = $ary['id'];
            if ($ary['language'] == $_SETTINGS['defaultlanguage']) {
                $surveys[count($surveys) - 1]['question'] = $ary['question'];
            }
            $surveyLangs[$surveyId][$ary['language']] = $ary['modified'];
        }

        foreach ($surveys as $item) {
            $langs = $surveyLangs[$item['id']];
            foreach ($_LANGUAGES as $handler => $name) {
                if ($handler != $_SETTINGS['defaultlanguage']) {
                    $this->tpl->setVariable(array(
                        'lang_name'     => $name,
                        'lang_handler'  => $handler,
                        'lang_exists'   => !empty($surveyLangs[$item['id']][$handler])? 't': 'f',
                        'lang_up2date'  => (empty($langs[$handler]) || $langs[$handler] >= $langs[$_SETTINGS['defaultlanguage']])? 't': 'f'
                    ));
                    $this->tpl->parse('surveys_translations_loop');
                }
            }
            $this->tpl->setVariable($item);
            $this->tpl->parse('surveys_loop');
        }
    }
}

?>
