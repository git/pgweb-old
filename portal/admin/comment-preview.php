<?php

//
// Preview a comment together with the documentation page 
//
// $Id: comment-preview.php,v 1.2 2005-12-14 09:39:32 dpage Exp $
//

require_once './common.php';
require_once 'HTML/Template/Sigma.php';


if (empty($_GET['id'])) {
	header('Location: comments.php');
	exit();
}
$id = intval($_GET['id']);
$db = database_connect('portal') or die('No database connection');

$tpl = template_create_from_file('common-docs.html','','en','ltr');

$rs = @pg_query($db,"SELECT file,version FROM comments WHERE id=" . $id) or die ('Database error: ' . pg_last_error($db));
$version = pg_fetch_result($rs,0,1);

if (!$docs = fetch_docs_page($db, $version, pg_fetch_result($rs,0,0))) {
   die('Failed to fetch docs!');
}

$content = preg_replace('/href="(?<!http|ftp|mailto)([^\.]+).html/i', "href=\"\$1" . '.html', $docs['content']);
$content = str_replace('IMG SRC="', "IMG SRC=\"/images/documentation/$version/", $content);
$tpl->setVariable(array(
	'page_title'    => "PostgreSQL ${version}: " . $docs['title'],
	'page_content'  => $content,
	'doc_nav_version' => $version
));

$tpl->addBlockfile('comments', 'comments_block', 'docs-comments.html');

if (!$comments = fetch_comments_list($db, $version, pg_fetch_result($rs,0,0), true)) {
	$tpl->touchBlock('comment_empty');
} else {
	foreach ($comments as $comment) {

                // Cleanup the comment
                $comment['comment'] = str_replace("  ", "&nbsp;&nbsp;",  $comment['comment']);
                $comment['comment'] = str_replace("<", "&lt;",  $comment['comment']);
                $comment['comment'] = str_replace(">", "&gt;",  $comment['comment']);
                $comment['comment'] = nl2br($comment['comment']);

		$tpl->setVariable(array(
			'poster'  => $comment['posted_by'],
			'date'    => date('d M Y G:i:s', $comment['timestamp']),
			'comment' => $comment['comment']
		));
		$tpl->parse('comment_loop');
	}
}

$tpl->show();
?>
