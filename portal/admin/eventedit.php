<?php

//
// Adding / editing an event
//
// $Id: eventedit.php,v 1.2 2007-11-03 22:04:20 dpage Exp $
//
class Admin_EventEdit extends PgForm {
    function __construct() {
        $this->navsection = 'admin';
    }

    function SetupForm() {
        $defaults = array(
            'approved' => 't'
        );
        if (!empty($_GET['id'])) {
            $rs = $this->pg_query(
                "SELECT e.id, e.posted_by, e.organisation, NULLIF(e.approved, false) AS approved,\n" .
                "       e.start_date, e.end_date,\n" .
                "       et.event, et.summary, et.details,\n" .
                "       NULLIF(e.training, false) AS training,\n" .
                "       el.city, el.state, el.country\n" .
                "FROM events e, events_text et, events_location el\n" .
                "WHERE e.id = et.eventid AND\n" .
                "      e.id = el.eventid AND\n" .
                "      et.language = 'en' AND\n" .
                "      e.id = " . intval($_GET['id'])
            );
            if (pg_num_rows($rs)) {
                $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
                list($defaults['start']['Y'], $defaults['start']['M'], $defaults['start']['d']) = explode('-', $defaults['start_date']);
                list($defaults['end']['Y'], $defaults['end']['M'], $defaults['end']['d']) = explode('-', $defaults['end_date']);
            }
        }
        $this->form->setDefaults($defaults);

        $this->form->addElement('hidden',     'id');
        $this->form->addElement('text',       'organisation', 'Organisation/company:', array('size' => 30, 'maxlength' => 100));
        $this->form->addElement('text',       'posted_by', 'Posted by:', array('size' => 30, 'maxlength' => 100));
        $this->form->addElement('checkbox',   'approved', null, 'Approved');
        $this->form->addElement('text',       'event', 'Event:', array('size' => 50, 'maxlength' => 100));
        $this->form->addElement('text',       'city', 'City:', array('size' => 50, 'maxlength' => 100));
        $this->form->addElement('text',       'state', 'State:', array('size' => 50, 'maxlength' => 100));
        $this->form->addElement('select',       'country', 'Country:', $this->fetch_countries_list());
        $this->form->addElement('checkbox',   'training', null, 'is training event');
        $this->form->addElement('date',       'start', 'Start Date:', array(
            'format'         => 'd M Y',
            'minYear'        => date('Y') - 2,
            'maxYear'        => date('Y') + 2,
            'addEmptyOption' => true
        ));
        $this->form->addElement('date',       'end', 'End Date:', array(
            'format'         => 'd M Y',
            'minYear'        => date('Y') - 2,
            'maxYear'        => date('Y') + 2,
            'addEmptyOption' => true
        ));
        $this->form->addElement('textarea',   'summary', 'Summary:', array('rows' => 5, 'cols' => 50));
        $this->form->addElement('textarea',   'details', 'Details:', array('rows' => 15, 'cols' => 50));
        $buttons = array();
        $buttons[] =& $this->form->createElement('button',     null, 'Preview...', array('onclick' => "doPreview(this.form['event'].value, this.form['summary'].value, this.form['details'].value);"));
        $this->form->addGroup($buttons, null, null, '&nbsp;', false);

        $this->form->applyFilter('__ALL__', 'trim');
        $this->form->applyFilter(array('id', 'start', 'end'), 'intval');

        // Make all fields required
        $this->form->addRule('organisation', 'The organisation/company is required.', 'required', null, 'client');
        $this->form->addRule('posted_by', 'The poster\'s email is required.', 'required', null, 'client');
        $this->form->addRule('event', 'The event title is required.', 'required', null, 'client');
        $this->form->addRule('city', 'The city is required.', 'required', null, 'client');
        $this->form->addRule('country', 'The country is required.', 'required', null, 'client');
        $this->form->addRule('summary', 'The summary is required.', 'required', null, 'client');
        $this->form->addRule('details', 'The details are required.', 'required', null, 'client');
        $this->form->addGroupRule('start', 'The start date is required.', 'required', null, 0, 'client');
        $this->form->addGroupRule('end', 'The end date is required.', 'required', null, 0, 'client');

        // Field-specific rules
        $this->form->addRule('posted_by', 'The email address you entered does not appear to be valid.', 'email', true, 'client');
        $this->form->addRule('event', 'The event title must be between 3 and 100 characters long.', 'rangelength', array(3, 100), 'client');
        $this->form->addRule('city', 'The city must be between 3 and 100 characters long.', 'rangelength', array(3, 100), 'client');
        $this->form->addRule('summary', 'The summary must be between 3 and 500 characters long.', 'rangelength', array(3, 500), 'client');
        $this->form->addRule('details', 'The event details must be at least 3 characters long.', 'minlength', 3, 'client');

    }

    function ProcessForm($f) {
        $startDate = sprintf('%04d-%02d-%02d', $f['start']['Y'], $f['start']['M'], $f['start']['d']);
        $endDate   = sprintf('%04d-%02d-%02d', $f['end']['Y'], $f['end']['M'], $f['end']['d']);
        $f['approved'] = empty($f['approved'])? 'f': 't';
        $f['training'] = empty($f['training'])? 'f': 't';
        $error = null;
        $this->pg_query('BEGIN TRANSACTION');
        $this->pg_query_params(
            "UPDATE events SET posted_by = $1, approved=$2, start_date=$3, end_date=$4, training=$5, organisation=$6 WHERE id=$7",
            array($f['posted_by'], $f['approved'], $startDate, $endDate, $f['training'], $f['organisation'], $f['id']));
        $this->pg_query_params(
            "UPDATE events_location SET city = $1, state=$2, country=$3 WHERE eventid=$4",
            array($f['city'], $f['state'], $f['country'], $f['id']));
        $this->pg_query_params(
            "UPDATE events_text SET event = $1, summary=$2, details=$3 WHERE eventid=$4 AND language='en'",
            array($f['event'], $f['summary'], $f['details'], $f['id']));
        $this->pg_query('COMMIT');
        $this->redirect_relative = '/admin/events.php';
    }

    function RenderThanks() {
    }

    function fetch_countries_list() {
        $rs = $this->pg_query('SELECT id,name FROM countries ORDER BY id');
        $c = array();
        for ($i = 0; $i < pg_num_rows($rs); $i++) {
            $countries = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            $c[$countries['id']] = $countries['name'];
        }
        return $c;
    }

}


?>
