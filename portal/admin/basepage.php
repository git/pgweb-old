<?php
abstract class Admin_BasePage extends PgPage {
    function PreRender() {
        $this->navsection = 'admin';
    }
    function PrepareTemplate() {
        $this->tpl->setCallbackFunction('iif', 'admin_sigma_iif');
    }
}

function admin_sigma_iif($trigger, $trueOption, $falseOption = '')
{
    if ('t' === $trigger || true === $trigger) {
        return $trueOption;
    } else {
        return $falseOption;
    }
}

?>
