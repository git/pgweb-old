<?php

//
// Frontend mirror management
//
// $Id$
//
class Admin_Frontends extends Admin_BasePage {
    function __construct() {
        $this->content_template = 'admin/frontends.html';
    }

    function Render() {
        if ($_POST['op'] == 'syncreq') 
            return $this->NewSyncRequest();

        $this->tpl->setVariable('now', date('c', time()));
        $st = @stat('/tmp/.website_update.lck');
        if (!$st) {
            $this->tpl->setVariable('updstatus', 'No update currently running');
        }
        else {
            $this->tpl->setVariable('updstatus', 'Update has been running since ' . date('c',$st['mtime']) . ' (' . $this->formattime(time()-$st['mtime']) . ').');
        }
        $rs = $this->pg_query("SELECT t,docs,ftp,requestby,completed,completed-t FROM sync_request ORDER BY t DESC LIMIT 15");
        for ($i = 0, $rows = pg_num_rows($rs); $i < $rows; $i++) {
            $this->tpl->setVariable('request_time', pg_fetch_result($rs, $i, 0));
            $this->tpl->setVariable('request_by', pg_fetch_result($rs, $i, 3));
            $this->tpl->setVariable('request_complete', pg_fetch_result($rs ,$i,4));
            $this->tpl->setVariable('request_elapsed', pg_fetch_result($rs, $i, 5));
            $p = '';
            if (pg_fetch_result($rs, $i, 1) == 1) $p .= 'Docs included<br>';
            if (pg_fetch_result($rs, $i, 2) == 1) $p .= 'Ftp included<br>';
            $this->tpl->setVariable('request_params', $p);
            $this->tpl->parse('requests_loop');
        }

        $rs = $this->pg_query("SELECT t AS log_t,op AS log_op,node AS log_node,start AS log_start,t-start AS log_elapsed FROM sync_log ORDER BY t DESC LIMIT 50");
        for ($i = 0, $rows = pg_num_rows($rs); $i < $rows; $i++) {
            $this->tpl->setVariable(pg_fetch_assoc($rs, $i));
            $this->tpl->parse('log_loop');
        }
    }

    function NewSyncRequest() {
        $ftp = isset($_POST['ftp'])?1:0;
        $docs = isset($_POST['docs'])?1:0;

        $rs = $this->pg_query_params('INSERT INTO sync_request (t,docs,ftp,requestby) VALUES ($1, $2, $3, $4)', array(
            $_POST['when'],
            $docs,
            $ftp,
            $_SERVER['PHP_AUTH_USER']
        ));
        if (!$rs) 
            throw new Exception("Database update failed");

        header('Location: frontends.php');
        exit(0);
    }

    function formattime($t) {
        $s = '';
        if ($t > 3600) {
            $s = intval($t/3600) . ' hours ';
            $t = intval($t%3600);
        }
        if ($t > 60) {
            $s .= intval($t/60) . ' minutes ';
            $t = intval($t%60);
        }
        $s .= $t . ' seconds';
        return $s;
    }
}
?>
