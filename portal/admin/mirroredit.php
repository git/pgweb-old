<?php

//
// Adding / editing a mirror
//
// $Id: mirroredit.php,v 1.3 2007-03-21 22:27:06 dpage Exp $
//
class Admin_MirrorEdit extends PgForm {
    function __construct() {
        $this->navsection = 'admin';
    }
    function SetupForm() {
        $defaults = array(
            'mirror_active' => 't',
            'mirror_type'   => 'ftp'
        );
        if (!empty($_GET['id'])) {
            $rs = $this->pg_query(
                "SELECT id, country_name, country_code, mirror_type, host_addr, host_path, host_port,\n" .
                "       host_sponsor, host_contact, host_email, host_notes, rsync_host1, rsync_host2,\n" .
                "       NULLIF(mirror_active, false) AS mirror_active, NULLIF(mirror_private, false) AS mirror_private,\n" .
                "       NULLIF(host_use_cname, false) AS host_use_cname, host_cname_host\n" .
                "FROM mirrors\n" .
                "WHERE id = " . intval($_GET['id'])
            );
            if (pg_num_rows($rs)) {
                $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
            }
        }
        $this->form->setDefaults($defaults);

        $this->form->addElement('hidden',     'id');

        $this->form->addElement('header',     null, 'General mirror data');
        $country[] =& $this->form->createElement('text', 'country_code', null, array('size' => 2, 'maxlength' => 2));
        $country[] =& $this->form->createElement('text', 'country_name', null, array('size' => 20, 'maxlength' => 50));
        $this->form->addGroup($country, 'country', 'Country (code, name):', '&nbsp;', false);
        $types[] =& $this->form->createElement('radio', null, null, 'arc', 'arc');
        $types[] =& $this->form->createElement('radio', null, null, 'ftp', 'ftp');
        $types[] =& $this->form->createElement('radio', null, null, 'www', 'www');
        $this->form->addGroup($types, 'mirror_type', 'Mirror type:', '&nbsp;');
        $this->form->addElement('checkbox',   'mirror_active', null, 'Active');
        $this->form->addElement('checkbox',   'mirror_private', null, 'Private (testing purposes only)');

        $this->form->addElement('header',     null, 'Host data');
        $this->form->addElement('text',       'host_addr', 'Host address:', array('size' => 32));
        $this->form->addElement('text',       'host_port', array('Host port:', '(should generally be empty)'), array('size' => 5, 'maxlength' => 5));
        $this->form->addElement('text',       'host_path', 'Path on host:', array('size' => 32, 'maxlength' => 100));
        $this->form->addElement('text',       'rsync_host1', 'Rsync host 1:', array('size' => 32, 'maxlength' => 100));
        $this->form->addElement('text',       'rsync_host2', 'Rsync host 2:', array('size' => 32, 'maxlength' => 100));
        $this->form->addElement('checkbox',   'host_use_cname', null, 'Use CName');
        $this->form->addElement('text',       'host_cname_host', 'CName Host:', array('size' => 32, 'maxlength' => 100));

        $this->form->addElement('header',     null, 'Contacts');
        $this->form->addElement('text',       'host_sponsor', 'Sponsor:', array('size' => 32, 'maxlength' => 100));
        $this->form->addElement('text',       'host_contact', 'Contact:', array('size' => 32, 'maxlength' => 100));
        $this->form->addElement('text',       'host_email', 'Email:', array('size' => 32, 'maxlength' => 100));
        $this->form->addElement('textarea',   'host_notes', 'Notes:', array('rows' => 5, 'cols' => 25));

        $this->form->applyFilter('__ALL__', 'trim');
        $this->form->applyFilter(array('id', 'host_port'), 'intval');

        $this->form->addGroupRule('country', 'Country information is required', 'required', null, 2, 'client');
        $this->form->addRule('host_addr', 'Host address is required', 'required', null, 'client');
        $this->form->addRule('rsync_host1', 'Rsync host is required', 'required', null, 'client');
    }

    function ProcessForm($f) {
        $id = $f['id'];
        $active  = empty($f['mirror_active'])? "'f'": "'t'";
        $private = empty($f['mirror_private'])? "'f'": "'t'";
        $cname   = empty($f['host_use_cname'])? "'f'": "'t'";
        unset($f['id'], $f['mirror_active'], $f['mirror_private'], $f['host_use_cname']);
        $eqns = $keys = $vals = array();
        if (empty($id)) {
            $rs = $this->pg_query(
                "SELECT nextval('mirrors_id_seq'), COALESCE(max(mirror_index) + 1, 0)\n" .
                "FROM mirrors \n" .
                "WHERE mirror_type = '" . pg_escape_string($f['mirror_type']) . "' AND\n" .
                "      country_code = '" . pg_escape_string($f['country_code']) . "'"
            );
            list($id, $idx) = pg_fetch_row($rs, 0);
            foreach ($f as $key => $val) {
                if (!empty($val)) {
                    if ($key == 'submit') continue;
                    $keys[] = $key;
                    $vals[] = "'" . pg_escape_string($val) . "'";
                }
            }
            $this->pg_query(
                "INSERT INTO mirrors\n" .
                "    (id, mirror_index, mirror_active, mirror_private, host_use_cname, " . implode(', ', $keys) . ")\n" .
                "VALUES\n" .
                "    ({$id}, {$idx}, {$active}, {$private}, {$cname}, " . implode(', ', $vals) . ")"
            );
        } else {
            foreach ($f as $key => $val) {
                if ($key == 'submit') continue;
                if (!empty($val)) {
                    $eqns[] = $key . ' = \'' . pg_escape_string($val) . '\'';
                }
            }
            $this->pg_query(
                "UPDATE mirrors SET mirror_active = {$active}, mirror_private = {$private}, host_use_cname = {$cname},\n" .
                implode(', ', $eqns) .
                "WHERE id = {$id}"
            );
        }
        $this->redirect_relative = '/admin/mirrors.php';
    }

    function RenderThanks() {
    }
}

?>
