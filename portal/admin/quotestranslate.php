<?php

//
// Translating a quotes item
//
// $Id: quotestranslate.php,v 1.1 2007-03-12 14:51:43 mha Exp $
//
class Admin_QuotesTranslate extends pgForm {
    function __construct() {
        $this->navsection = 'admin';
    }

    function SetupForm() {
        $defaults = null;
        if (!empty($_GET['id']) && !empty($_GET['lang']) && 'en' != $_GET['lang']) {
            $id   = intval($_GET['id']);
            $lang = pg_escape_string($_GET['lang']);
            $rs   = $this->pg_query(
                "SELECT qe.quoteid AS id, '{$lang}' AS lang, qt.quote, qt.tagline,\n" .
                "       qe.quote_en, qe.tagline_en\n" .
                "FROM (\n" .
                "    SELECT quoteid, quote AS quote_en, tagline AS tagline_en\n" .
                "    FROM quotes_text\n" .
                "    WHERE quoteid = {$id} AND\n" .
                "          language = 'en'\n" .
                ") AS qe LEFT JOIN (\n" .
                "    SELECT quoteid, quote, tagline\n" .
                "    FROM quotes_text\n" .
                "    WHERE quoteid = {$id} AND\n" .
                "          language = '{$lang}'\n" .
                ") AS qt ON (qe.quoteid = qt.quoteid)"
            );
            if (pg_num_rows($rs)) {
                $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
            }
        }
        if (!empty($defaults)) {
            $this->form->setDefaults($defaults);
        }

        $en = array();
        $this->form->addElement('hidden',     'id');
        $this->form->addElement('hidden',     'lang');
        $en[] =& $this->form->addElement('textarea',  'quote_en', 'English quote:');
        $this->form->addElement('textarea',       'quote', 'Quote:', array('rows' => 15, 'cols' => 50));
        $en[] =& $this->form->addElement('textarea',   'tagline_en', 'English tag line:');
        $this->form->addElement('textarea',   'tagline', 'Tag line:', array('rows' => 15, 'cols' => 50));
        $buttons = array();
        $buttons[] =& $this->form->createElement('button',     null, 'Preview...', array('onclick' => "doPreview(this.form['quote'].value, this.form['tagline'].value);"));
        $this->form->addGroup($buttons, null, null, '&nbsp;', false);

        foreach (array_keys($en) as $key) {
            $en[$key]->freeze();
        }

        $this->form->applyFilter('__ALL__', 'trim');
        $this->form->applyFilter('id', 'intval');

        $this->form->addRule('quote', 'The quote is required.', 'required', null, 'client');
        $this->form->addRule('tagline', 'The tag line is required.', 'required', null, 'client');

        $this->form->addRule('quote', 'The quote must be between 3 and 500 characters long.', 'rangelength', array(3, 500), 'client');
        $this->form->addRule('tagline', 'The tag line must be between 3 and 500 characters long.', 'rangelength', array(3, 500), 'client');
    }

    function ProcessForm($f) {
        $f  = array_map('pg_escape_string', $f);
        $rs = $this->pg_query(
            "UPDATE quotes_text SET quote = '{$f['headline']}', tagline = '{$f['tagline']}'\n" .
            "WHERE quoteid = {$f['id']} AND language = '{$f['lang']}'"
        );
        if (0 == pg_affected_rows($rs)) {
            $this->pg_query(
                "INSERT INTO quotes_text\n" .
                "    (quoteid, quote, tagline, language)\n" .
                "VALUES\n" .
                "    ({$f['id']}, '{$f['quote']}', '{$f['tagline']}', '{$f['lang']}')"
            );
        }
        $this->redirect_relative = '/admin/quotes.php';
    }

    function RenderThanks() {
    }
}
?>
