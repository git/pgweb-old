<?php

//
// Translating a survey
//
// $Id: survey-translate.php,v 1.1 2004-05-11 11:19:46 avb Exp $
//

require_once './common.php';
require_once 'HTML/QuickForm.php';

function create_optfields($base, $as = '')
{
    $fields = array();
    for ($i = 1; $i <= MAX_OPTIONS; $i++) {
        $fields[] = $base . $i . (empty($as)? '': ' AS ' . $as . $i);
    }
    return implode(', ', $fields);
}

$form =& new HTML_QuickForm('newstrans');

if ('POST' != $_SERVER['REQUEST_METHOD']) {
    $defaults = null;
    if (!empty($_GET['id']) && !empty($_GET['lang']) && 'en' != $_GET['lang']) {
        $id   = intval($_GET['id']);
        $lang = pg_escape_string($_GET['lang']);
        $db   = database_connect('portal') or die('No database connection');
        $rs   = pg_query($db, 
            "SELECT qe.surveyid AS id, '{$lang}' AS lang, qt.question, " . create_optfields('qt.opt') . ",\n".
            "       qe.question_en, " . create_optfields('qe.opt_en') . "\n" .
            "FROM (\n" .
            "    SELECT surveyid, question AS question_en, " . create_optfields('opt', 'opt_en') . "\n" .
            "    FROM survey_questions\n" .
            "    WHERE surveyid = {$id} AND\n" .
            "          language = 'en'\n" .
            ") AS qe LEFT JOIN (\n" .
            "    SELECT surveyid, question, " . create_optfields('opt') . "\n" .
            "    FROM survey_questions\n" .
            "    WHERE surveyid = {$id} AND\n" .
            "          language = '{$lang}'\n" .
            ") AS qt ON (qe.surveyid = qt.surveyid)"
        );
        if (pg_num_rows($rs)) {
            $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
            for ($i = 1; $i <= MAX_OPTIONS && !empty($defaults['opt' . $i]); $i++) {
                $defaults['opt'][$i] = $defaults['opt' . $i];
            }
            for ($i = 1; $i <= MAX_OPTIONS && !empty($defaults['opt_en' . $i]); $i++) {
                $defaults['opt_en'][$i] = $defaults['opt_en' . $i];
            }
        }
    }
    if (!empty($defaults)) {
        $form->setDefaults($defaults);
    } else {
        header('Location: surveys.php');
        exit();
    }
}

$en = $opts = $opts_en = array();
$form->addElement('hidden',     'id');
$form->addElement('hidden',     'lang');
$en[] =& $form->addElement('text',   'question_en', 'English question:');
$form->addElement('text',   'question', 'Question:', array('size' => 50));

$optCount = 'POST' == $_SERVER['REQUEST_METHOD']? count($_POST['opt_en']): count($defaults['opt_en']);
for ($i = 1; $i <= $optCount; $i++) {
    $opts[] =& $form->createElement('static', 'label' . $i, null, $i . '.');
    $opts[] =& $form->createElement('text', $i, null, array('size' => 50));
    $opts_en[] =& $form->createElement('static', 'label' . $i, null, $i . '.');
    $opts_en[] =& $form->createElement('text', $i);
}
$en[] =& $form->addGroup($opts_en, 'opt_en', 'English options:', array('&nbsp;', '<br />'));
$form->addGroup($opts, 'opt', 'Options:', array('&nbsp;', '<br />'));
$form->addElement('submit', null, 'Send');

foreach (array_keys($en) as $key) {
    $en[$key]->freeze();
}

$form->applyFilter('__ALL__', 'trim');
$form->applyFilter('id', 'intval');

$form->addRule('question', 'The question is required', 'required', null, 'client');
$form->addGroupRule('opt', 'The options are required', 'required', null, $optCount, 'client');

if ($form->validate()) {
    $db = database_connect('portal') or die('No database connection');
    $f  = $form->exportValues();

    $keys = array('question');
    $vals = array("'" . pg_escape_string($f['question']) . "'");
    $eqns = array('question = \'' . pg_escape_string($f['question']) . "'");

    for ($i = 1; $i <= $optCount; $i++) {
        $keys[] = 'opt' . $i;
        $vals[] = "'" . pg_escape_string($f['opt'][$i]) . "'";
        $eqns[] = 'opt' . $i . ' = \'' . pg_escape_string($f['opt'][$i]) . "'";
    }
    for ($i = $optCount + 1; $i <= MAX_OPTIONS; $i++) {
        $keys[] = 'opt' . $i;
        $vals[] = 'NULL';
        $eqns[] = 'opt' . $i . ' = NULL';
    }

    $rs = @pg_query($db,
        "UPDATE survey_questions SET " . implode(', ', $eqns) . "\n" .
        "WHERE surveyid = {$f['id']} AND language = '" . pg_escape_string($f['lang']) . "'"
    ) or die('Database error: ' . pg_last_error($db));
    if (0 == pg_affected_rows($rs)) {
        @pg_query($db,
            "INSERT INTO survey_questions\n" .
            "    (surveyid, language, " . implode(', ', $keys) . ")\n" .
            "VALUES\n" .
            "    ({$f['id']}, '" . pg_escape_string($f['lang']) . "', " . implode(', ', $vals) . ")"
        ) or die('Database error: ' . pg_last_error($db));
    }

    header('Location: surveys.php');
    exit();
}

admin_display_form($form, 'Translate survey');
?>
