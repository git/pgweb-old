<?php

//
// Adding / editing a news item
//
// $Id: newsedit.php,v 1.1 2007-03-12 14:51:43 mha Exp $
//
class Admin_NewsEdit extends PgForm {
    function __construct() {
        $this->navsection = 'admin';
    }
    function SetupForm() {
        $defaults = array(
            'active'   => 't',
            'approved' => 't'
        );
        if (!empty($_GET['id'])) {
            $rs = $this->pg_query(
                "SELECT n.id, n.posted_by, NULLIF(n.approved, false) AS approved, NULLIF(n.active, false) AS active,\n" .
                "       nt.headline, nt.summary, nt.story\n" .
                "FROM news n, news_text nt\n" .
                "WHERE n.id = nt.newsid AND\n" .
                "      nt.language = 'en' AND\n" .
                "      n.id = " . intval($_GET['id'])
            );
            if (pg_num_rows($rs)) {
                $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
            }
        }
        $this->form->setDefaults($defaults);

        $this->form->addElement('hidden',     'id');
        $this->form->addElement('text',       'posted_by', 'Posted by:', array('size' => 30, 'maxlength' => 100));
        $this->form->addElement('checkbox',   'approved', null, 'Approved');
        $this->form->addElement('checkbox',   'active', null, 'Active');
        $this->form->addElement('text',       'headline', 'Headline:', array('size' => 50, 'maxlength' => 100));
        $this->form->addElement('textarea',   'summary', 'Summary:', array('rows' => 5, 'cols' => 50));
        $this->form->addElement('textarea',   'story', 'Story:', array('rows' => 15, 'cols' => 50));
        $buttons = array();
        $buttons[] =& $this->form->createElement('button',     null, 'Preview...', array('onclick' => "doPreview(this.form['headline'].value, this.form['summary'].value, this.form['story'].value);"));
        $this->form->addGroup($buttons, null, null, '&nbsp;', false);

        $this->form->applyFilter('__ALL__', 'trim');
        $this->form->applyFilter('id', 'intval');

        // Make all fields required
        $this->form->addRule('posted_by', 'The poster\'s email is required.', 'required', null, 'client');
        $this->form->addRule('headline', 'The headline is required.', 'required', null, 'client');
        $this->form->addRule('summary', 'The summary is required.', 'required', null, 'client');
        $this->form->addRule('story', 'The story is required.', 'required', null, 'client');

        // Apply field-specific rules
        $this->form->addRule('posted_by', 'The email address you entered does not appear to be valid.', 'email', true, 'client');
        $this->form->addRule('headline', 'The headline must be between 3 and 100 characters long.', 'rangelength', array(3, 100), 'client');
        $this->form->addRule('summary', 'The summary must be between 3 and 300 characters long.', 'rangelength', array(3, 300), 'client');
        $this->form->addRule('story', 'The story must be at least 3 characters long.', 'minlength', 3, 'client');
    }


    function ProcessForm($f) {
        $f['approved'] = empty($f['approved'])? 'f': 't';
        $f['active']   = empty($f['active'])? 'f': 't';

        $this->pg_query('BEGIN');

        $r = $this->pg_query_params("SELECT approved FROM news WHERE id=$1", array($f['id']));
        list($oldapproved) = pg_fetch_row($r,0); 
        if ($oldapproved=='f' && $f['approved']=='t') {
            $mailtext = "View: " . $_SETTINGS['masterserver'] . '/about/news.' .
                $f['id'] .
                "\nEdit: " . $_SETTINGS['masterserver'] . '/admin/news-edit.php?id=' . $f['id'];
            @mail($_SETTINGS['notifymail'], 'News entry ' . $f[id] . ' was approved by ' . $_SERVER['PHP_AUTH_USER'], $mailtext);
        }

        $this->pg_query_params(
            "UPDATE news SET posted_by = $1, approved=$2, active=$3 WHERE id=$4",
            array($f['posted_by'], $f['approved'], $f['active'], $f['id']));
        $this->pg_query_params(
            "UPDATE news_text SET headline = $1, summary=$2, story=$3 WHERE newsid=$4 AND language='en'",
            array($f['headline'], $f['summary'], $f['story'], $f['id']));


        $this->pg_query('COMMIT');
        $this->redirect_relative = '/admin/news.php';
    }

    function RenderThanks() {
    }
}
?>
