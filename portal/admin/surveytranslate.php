<?php

//
// Translating a survey
//
// $Id: surveytranslate.php,v 1.1 2007-03-12 14:51:43 mha Exp $
//
class Admin_SurveyTranslate extends pgForm {
    function __construct() {
        $this->navsection = 'admin';
    }
    
    function create_optfields($base, $as = '')
    {
        $fields = array();
        for ($i = 1; $i <= MAX_OPTIONS; $i++) {
            $fields[] = $base . $i . (empty($as)? '': ' AS ' . $as . $i);
        }
        return implode(', ', $fields);
    }

    function SetupForm() {
        $defaults = null;
        if (!empty($_REQUEST['id']) && !empty($_REQUEST['lang']) && 'en' != $_REQUEST['lang']) {
            $id   = intval($_REQUEST['id']);
            $lang = pg_escape_string($_REQUEST['lang']);
            $rs   = $this->pg_query(
                "SELECT qe.surveyid AS id, '{$lang}' AS lang, qt.question, " . $this->create_optfields('qt.opt') . ",\n".
                "       qe.question_en, " . $this->create_optfields('qe.opt_en') . "\n" .
                "FROM (\n" .
                "    SELECT surveyid, question AS question_en, " . $this->create_optfields('opt', 'opt_en') . "\n" .
                "    FROM survey_questions\n" .
                "    WHERE surveyid = {$id} AND\n" .
                "          language = 'en'\n" .
                ") AS qe LEFT JOIN (\n" .
                "    SELECT surveyid, question, " . $this->create_optfields('opt') . "\n" .
                "    FROM survey_questions\n" .
                "    WHERE surveyid = {$id} AND\n" .
                "          language = '{$lang}'\n" .
                ") AS qt ON (qe.surveyid = qt.surveyid)"
            );
            if (pg_num_rows($rs)) {
                $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
            }
        }
        if (!empty($defaults)) {
            $this->form->setDefaults($defaults);
        }

        $en = $opts = $opts_en = array();
        $this->form->addElement('hidden',     'id');
        $this->form->addElement('hidden',     'lang');
        $en[] =& $this->form->addElement('text',   'question_en', 'English question:');
        $this->form->addElement('text',   'question', 'Question:', array('size' => 50));

        $optCount = 0;
        for ($i = 1; $i <= 8; $i++) {
            if ($defaults['opt_en' . $i] != '')
                $optCount = $i;
        }

        for ($i = 1; $i <= $optCount; $i++) {
            $en[] =& $this->form->addElement('text', 'opt_en' . $i, null, $i . '.');
            $this->form->addElement('text', 'opt' . $i, null, array('size' => 50));
            $this->form->addRule('opt' . $i,' The options are required', 'required', null, 'client');
        }

        foreach (array_keys($en) as $key) {
            $en[$key]->freeze();
        }

        $this->form->applyFilter('__ALL__', 'trim');
        $this->form->applyFilter('id', 'intval');

        $this->form->addRule('question', 'The question is required', 'required', null, 'client');
    }

    function ProcessForm($f) {
        for ($i = 1; $i <= 8; $i++)
            if (!isset($f['opt'.$i]))
                $f['opt'.$i]='';
        $rs = $this->pg_query_params("UPDATE survey_questions SET question=$1, opt1=$2, opt2=$3, opt3=$4, opt4=$5, opt5=$6, opt6=$7, opt7=$8, opt8=$9 WHERE surveyid=$10 AND language=$11",
            array($f['question'], $f['opt1'], $f['opt2'], $f['opt3'], $f['opt4'], $f['opt5'], $f['opt6'], $f['opt7'], $f['opt8'], $f['id'], $f['lang']));

        if (0 == pg_affected_rows($rs)) {
            $this->pg_query_params(
                "INSERT INTO survey_questions (question, opt1, opt2, opt3, opt4, opt5, opt6, opt7, opt8, surveyid, language) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)",
            array($f['question'], $f['opt1'], $f['opt2'], $f['opt3'], $f['opt4'], $f['opt5'], $f['opt6'], $f['opt7'], $f['opt8'], $f['id'], $f['lang']));

        }

        $this->redirect_relative='/admin/surveys.php';
    }

    function RenderThanks() {
    }
}
?>
