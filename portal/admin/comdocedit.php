<?php
class Admin_ComDocEdit extends PgForm {
    private $pageid = -1;
    function __construct() {
        $this->submitbutton = false;
        $this->navsection = 'admin';
    }

    function SetupForm() {
        if (isset($_GET['id']))
            $this->pageid = $_GET['id'];
        else
            $this->pageid = $_POST['pageid'];

        $res = $this->pg_query("SELECT pageid, author, title, shorttitle, contents FROM communitypages_work WHERE pageid=" . $this->pageid);
        if (pg_num_rows($res) != 1)
            throw new Exception('Selected page not found');

        $liveres = $this->pg_query("SELECT contents FROM communitypages WHERE pageid=" . $this->pageid);

        $all = pg_fetch_assoc($res);

        $this->form->addElement('hidden', 'pageid');
        $this->form->addElement('checkbox', 'approved', null, 'Approved');
        $this->form->addElement('static', 'author', 'Author');
        $this->form->addElement('text', 'title', 'Title', array('size'=>52, 'maxlength'=>128));
        $this->form->addElement('text', 'shorttitle', 'Short title', array('size'=>52, 'maxlength'=>128));
        $this->form->addElement('textarea', 'contents', 'Contents', array('rows'=>20, 'cols'=>50));
        $this->block_elements = array('author' => 'qf_element');

        $buttons = array();
        $buttons[] =& $this->form->createElement('submit',     null, 'Save');
        $buttons[] =& $this->form->createElement('button',     null, 'Preview...', array('onclick' => "doPreview(this.form['title'].value,'', this.form['contents'].value);"));
        if (pg_num_rows($liveres) == 1)
        {
            // Previous version was live
            $this->form->addElement('hidden', 'diffprev');
            $buttons[] =& $this->form->createElement('button', null, 'Diff...', array('onclick' => "doPreview(this.form['title'].value,'', this.form['diffprev'].value);"));
            if (function_exists('xdiff_string_diff'))
                //$all[diffprev] = '<pre>' . xdiff_string_diff(htmlentities(pg_fetch_result($liveres,0,0)) . "\n",htmlentities($all['contents']) . "\n") . '</pre>';
                $all[diffprev] = '<pre>' . htmlentities(xdiff_string_diff(pg_fetch_result($liveres,0,0), pg_fetch_result($res, 0, 4))) . '</pre>';
            else
                $all[diffprev] = 'xdiff not supported by this PHP';
        }
        $this->form->addGroup($buttons, null, null, '&nbsp;', false);

        $this->form->setDefaults($all);

        $this->form->applyFilter('__ALL__', 'trim');
        $this->form->applyFilter('id', 'intval');

        // Make all fields required
        $this->form->addRule('shorttitle', 'The short title is required.', 'required', null, 'client');
        $this->form->addRule('title', 'The title is required.', 'required', null, 'client');
    }

    function ProcessForm($f) {
        $this->pg_query('BEGIN');

        $f  = array_map('pg_escape_string', $f);

        $this->pg_query("UPDATE communitypages_work SET title='{$f['title']}',shorttitle='{$f['shorttitle']}',contents='{$f['contents']}' WHERE pageid={$this->pageid}");

        if ($f['approved'] == 1)
        {
            /* Move this one off pending */
            $this->pg_query("SELECT communitypages_approve({$this->pageid})");
        }

        $this->pg_query("COMMIT");

        $this->redirect_relative = '/admin/comdocs.php';
    }
    function RenderThanks() {
    }
}
?>
