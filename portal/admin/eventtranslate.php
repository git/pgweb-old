<?php

//
// Translating an event
//
// $Id: eventtranslate.php,v 1.1 2007-03-12 14:51:43 mha Exp $
//
class Admin_EventTranslate extends pgForm {
    function __construct() {
        $this->navsection = 'admin';
    }

    function SetupForm() {
        $defaults = null;
        if (!empty($_GET['id']) && !empty($_GET['lang']) && 'en' != $_GET['lang']) {
            $id   = intval($_GET['id']);
            $lang = pg_escape_string($_GET['lang']);
            $rs   = $this->pg_query(
                "SELECT ee.eventid AS id, '{$lang}' AS lang, et.event, et.summary, et.details,\n" .
                "       ee.event_en, ee.summary_en, ee.details_en\n" .
                "FROM (\n" .
                "    SELECT eventid, event AS event_en, summary AS summary_en, details AS details_en\n" .
                "    FROM events_text\n" .
                "    WHERE eventid = {$id} AND\n" .
                "          language = 'en'\n" .
                ") AS ee LEFT JOIN (\n" .
                "    SELECT eventid, event, summary, details\n" .
                "    FROM events_text\n" .
                "    WHERE eventid = {$id} AND\n" .
                "          language = '{$lang}'\n" .
                ") AS et USING (eventid)"
            );
            if (pg_num_rows($rs)) {
                $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
            }
        }
        if (!empty($defaults)) {
            $this->form->setDefaults($defaults);
        }

        $en = array();
        $this->form->addElement('hidden',     'id');
        $this->form->addElement('hidden',     'lang');
        $en[] =& $this->form->addElement('text', 'event_en', 'Event in English:');
        $this->form->addElement('text',       'event', 'Event:', array('size' => 50, 'maxlength' => 100));
        $en[] =& $this->form->addElement('textarea', 'summary_en', 'English summary:');
        $this->form->addElement('textarea',   'summary', 'Summary:', array('rows' => 5, 'cols' => 50));
        $en[] =& $this->form->addElement('textarea', 'details_en', 'English details:');
        $this->form->addElement('textarea',   'details', 'Details:', array('rows' => 15, 'cols' => 50));
        $buttons = array();
        $buttons[] =& $this->form->createElement('button',     null, 'Preview...', array('onclick' => "doPreview(this.form['event'].value, this.form['summary'].value, this.form['details'].value);"));
        $this->form->addGroup($buttons, null, null, '&nbsp;', false);

        foreach (array_keys($en) as $key) {
            $en[$key]->freeze();
        }

        $this->form->applyFilter('__ALL__', 'trim');
        $this->form->applyFilter('id', 'intval');

        $this->form->addRule('event', 'The event title is required.', 'required', null, 'client');
        $this->form->addRule('summary', 'The summary is required.', 'required', null, 'client');
        $this->form->addRule('details', 'The details are required.', 'required', null, 'client');

        $this->form->addRule('event', 'The event title must be between 3 and 100 characters long.', 'rangelength', array(3, 100), 'client');
        $this->form->addRule('summary', 'The summary must be between 3 and 500 characters long.', 'rangelength', array(3, 500), 'client');
        $this->form->addRule('details', 'The event details must be at least 3 characters long.', 'minlength', 3, 'client');
    }

    function ProcessForm($f) {
        $f  = array_map('pg_escape_string', $f);
        $rs = $this->pg_query(
            "UPDATE events_text SET event = '{$f['event']}', summary = '{$f['summary']}', details = '{$f['details']}'\n" .
            "WHERE eventid = {$f['id']} AND language = '{$f['lang']}'"
        );
        if (0 == pg_affected_rows($rs)) {
            $this->pg_query(
                "INSERT INTO events_text\n" .
                "    (eventid, event, summary, details, language)\n" .
                "VALUES\n" .
                "    ({$f['id']}, '{$f['event']}', '{$f['summary']}', '{$f['details']}', '{$f['lang']}')"
            );
        }

        $this->redirect_relative = '/admin/events.php';
    }

    function RenderThanks() {
    }
}

?>
