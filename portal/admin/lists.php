<?php
//
// List of mailinglists
//
// $Id$
//
class Admin_Lists extends Admin_BasePage {
    function __construct() {
        $this->content_template = 'admin/lists.html';
    }

    function Render() {
        $rs = $this->pg_query("SELECT lists.id,lists.name,listgroups.name as group,CASE WHEN lists.active=1 THEN 'Yes' ELSE 'No' END AS active FROM lists INNER JOIN listgroups ON lists.grp=listgroups.id ORDER BY lists.active DESC, lists.name");

        for ($i = 0, $rows = pg_num_rows($rs); $i < $rows; $i++) {
            $list = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            $this->tpl->setVariable($list);
            $this->tpl->parse('list_loop');
        }
    }
}
?>
