<?php

//
// Adding / editing a stackbuild application
//
// $Id: applicationedit.php,v 1.2 2007-09-12 19:45:39 mha Exp $
//
class Admin_ApplicationEdit extends PgForm {
    function __construct() {
        $this->navsection = 'admin';
    }
    function SetupForm() {

	$DoCopy = false;

        if ('POST' != $_SERVER['REQUEST_METHOD']) {
            if (isset($_GET['action']) && $_GET['action'] == 'del') {
                if (empty($_GET['id']) || empty($_GET['version']) || empty($_GET['platform'])) {
                    throw new Exception('Id, version or platform not specified');
                }
                $this->pg_query("DELETE FROM applications WHERE id='" . pg_escape_string($_GET['id']) . "'  AND version='" . pg_escape_string($_GET['version']) . "'  AND platform='" . pg_escape_string($_GET['platform']) . "'");
                header('Location: applications.php');
                exit(0);
	    } else if (isset($_GET['action']) && $_GET['action'] == 'cp') {
		$DoCopy = true;
	    }


            if (!empty($_GET['id']) && !empty($_GET['version']) && !empty($_GET['platform'])) {
                $rs = $this->pg_query(
                    "SELECT id AS origid,version AS origversion,platform AS origplatform,id,version,platform,secondaryplatform,name,NULLIF(active,false) AS active,description,category,pgversion,edbversion,format,installoptions,upgradeoptions,checksum,mirrorpath,alturl,dependencies,versionkey FROM applications WHERE id='" . pg_escape_string($_GET['id']) . "' AND version='" . pg_escape_string($_GET['version']) . "' AND platform='" . pg_escape_string($_GET['platform']) . "'");
                if (pg_num_rows($rs)) {
                    $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
                }
            }
            $this->form->setDefaults($defaults);
        }


        $txtf = array('size' => 52);
	$txta = array('cols' => 40, 'rows'=>8);
	if ($DoCopy == false) {
            $this->form->addElement('hidden','origid');
            $this->form->addElement('hidden','origversion');
       	    $this->form->addElement('hidden','origplatform');
	}
        $this->form->addElement('text','id','ID', $txtf);
        $this->form->addElement('text','version','Version', $txtf);
	$this->form->addElement('select','platform','Platform',$this->fetch_platform_list());
	$this->form->addElement('select','secondaryplatform','Secondary Platform',$this->fetch_secondaryplatform_list());
        $this->form->addElement('text','name','Name', $txtf);
        $this->form->addElement('checkbox','active',null,'Is active? (<b>Always</b> verify all data before activating!)');
        $this->form->addElement('textarea','description','Description',$txta);
        $this->form->addElement('text','category','Category', $txtf);
        $this->form->addElement('text','pgversion','PostgreSQL version', $txtf);
        $this->form->addElement('text','edbversion','EnterpriseDB version', $txtf);
	$this->form->addElement('select','format','Format',$this->fetch_format_list());
        $this->form->addElement('text','installoptions','Install options', $txtf);
        $this->form->addElement('text','upgradeoptions', 'Upgrade options', $txtf);
  	$this->form->addElement('text','checksum','Checksum', $txtf);
        $this->form->addElement('text','mirrorpath','Mirror path', $txtf);
        $this->form->addElement('text','alturl','Alternate URL', $txtf);
        $this->form->addElement('text','dependencies','Dependencies (varchar[])', $txtf);
        $this->form->addElement('text','versionkey','Version registry key', $txtf);
    }

    function ProcessForm($f) {
        $origid = pg_escape_string($f['origid']);
        $origversion = pg_escape_string($f['origversion']);
        $origplatform = pg_escape_string($f['origplatform']);
        $id = pg_escape_string($f['id']);
        $version = pg_escape_string($f['version']);
        $platform = pg_escape_string($f['platform']);
        $secondaryplatform = pg_escape_string($f['secondaryplatform']);
        $name = pg_escape_string($f['name']);
        $active = pg_escape_string($f['active']);
	if (trim($active) == '')
            $active=0;
        $description = pg_escape_string($f['description']);
        $category = pg_escape_string($f['category']);
        $pgversion = pg_escape_string($f['pgversion']);
        $edbversion = pg_escape_string($f['edbversion']);
        $format = pg_escape_string($f['format']);
        $installoptions = pg_escape_string($f['installoptions']);
        $upgradeoptions = pg_escape_string($f['upgradeoptions']);
        $checksum = pg_escape_string($f['checksum']);
        $mirrorpath = pg_escape_string($f['mirrorpath']);
        $alturl = pg_escape_string($f['alturl']);
        $dependencies = pg_escape_string($f['dependencies']);
        if ($dependencies == '')
            $dependencies = '{}';
        $versionkey = pg_escape_string($f['versionkey']);

	if (empty($origid)) {
            $this->pg_query("INSERT INTO applications (id, version, platform, secondaryplatform, name, active, description, category, pgversion, edbversion, format, installoptions, upgradeoptions, checksum, mirrorpath, alturl, dependencies, versionkey) VALUES " .
                            "('" . $id . "','" . $version . "','" . $platform . "','" . $secondaryplatform . "','" . $name . "'," . $active . "::bool,'" . $description . "','" . $category . "','" . $pgversion . "','" . $edbversion . "','" . $format . "','" . $installoptions . "','" . $upgradeoptions . "','" . $checksum . "','" . $mirrorpath . "','" . $alturl . "','" . $dependencies . "','" . $versionkey . "');");
        } else {
            $this->pg_query("UPDATE applications SET id='" . $id . "', version='" . $version . "', platform='" . $platform . "', secondaryplatform='" . $secondaryplatform . "', name='" . $name . "', active=" . $active . "::bool, description='" . $description . "', category='" . $category . "', pgversion='" . $pgversion . "', edbversion='" . $edbversion . "', format='" . $format . "', installoptions='" . $installoptions . "', upgradeoptions='" . $upgradeoptions . "', checksum='" . $checksum . "', mirrorpath='" . $mirrorpath . "', alturl='" . $alturl . "', dependencies='" . $dependencies . "', versionkey='" . $versionkey . "' WHERE id='" . $origid . "' AND version='" . $origversion . "' AND platform='" . $origplatform . "'");
        }
        $this->redirect_relative = '/admin/applications.php';
    }
    function RenderThanks() {
    }

    function fetch_platform_list() {
        $c = array();

        $c['linux'] = 'Linux (32bit)';
        $c['linux-x64'] = 'Linux (64bit)';
        $c['osx'] = 'Mac OS X';
        $c['windows'] = 'Windows (32bit)';
        $c['windows-x64'] = 'Windows (64bit)';
  
        return $c;
    }

    function fetch_secondaryplatform_list() {
        $c = array();

        $c[''] = 'None';
        $c['linux'] = 'Linux (32bit)';
        $c['linux-x64'] = 'Linux (64bit)';
        $c['osx'] = 'Mac OS X';
        $c['windows'] = 'Windows (32bit)';
        $c['windows-x64'] = 'Windows (64bit)';
 
        return $c;
    }

    function fetch_format_list() {
        $c = array();

        $c['bin'] = 'Linux .bin';
        $c['app'] = 'Mac .app';
        $c['pkg'] = 'Mac .pkg';
        $c['mpkg'] = 'Mac .mpkg';
        $c['exe'] = 'Windows .exe';
        $c['msi'] = 'Windows .msi';

        return $c;
    }
}

?>
