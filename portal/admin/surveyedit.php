<?php

//
// Adding / editing a survey
//
// $Id: surveyedit.php,v 1.2 2007-08-08 08:17:24 mha Exp $
//
class Admin_SurveyEdit extends PgForm {
    function __construct() {
        $this->navsection = 'admin';
    }
    function SetupForm() {
        $defaults = array();
        if (!empty($_GET['id'])) {
            $rs = $this->pg_query(
                "SELECT s.id, NULLIF(s.current, false) AS current, q.*\n" .
                "FROM surveys s, survey_questions q\n" .
                "WHERE s.id = q.surveyid AND\n" .
                "      q.language = 'en' AND\n" .
                "      s.id = " . intval($_GET['id'])
            );
            if (pg_num_rows($rs)) {
                $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
                for ($i = 1; $i <= MAX_OPTIONS && !empty($defaults['opt' . $i]); $i++) {
                    $defaults['opt'][$i - 1] = $defaults['opt' . $i];
                }
            }
        }
        $this->form->setDefaults($defaults);

        $this->form->addElement('hidden',     'id');
        $this->form->addElement('text',       'question', 'Survey question:', array('size' => 50));
        $options = array();
        for ($i = 0; $i < MAX_OPTIONS; $i++) {
            $options[] =& $this->form->createElement('text',  $i, null, array('size' => 50));
        }
        $this->form->addGroup($options, 'opt', 'Options:', '<br />');
        $this->form->addElement('checkbox',   'current', null, 'Make survey current');

        $this->form->applyFilter('__ALL__', 'trim');
        $this->form->applyFilter('id', 'intval');

        $this->form->addRule('question', 'The question is required', 'required', null, 'client');
        $this->form->addGroupRule('opt', 'At least 2 options are required', 'required', null, 2, 'client');
    }

    function ProcessForm($f) {
        $current = empty($f['current'])? 'false': 'true';
        $id      = $f['id'];
        $opt     = array_values(array_filter($f['opt'], 'strlen'));
        $error   = null;

        unset($f['id'], $f['current'], $f['opt']);

        for ($i = 0, $optCount = count($opt); $i < $optCount; $i++) {
            $f['opt' . ($i + 1)] = $opt[$i];
        }
        for ($i = $optCount + 1; $i <= MAX_OPTIONS; $i++) {
            $f['opt' . $i] = null;
        }

        $this->pg_query('BEGIN TRANSACTION');

        // clear 'current' status of a previous survey
        if ('true' == $current) {
            $this->pg_query(
                "UPDATE surveys SET current = false\n" .
                "WHERE current"
            );
        }

        $eqns = $keys = $vals = array();
        if (!empty($id)) {
            foreach ($f as $key => $val) {
                if ($key == 'submit')
                    continue;
                $eqns[] = $key . ' = ' . (is_null($val)? 'NULL': "'" . pg_escape_string($val) . "'");
            }
            $this->pg_query(
                "UPDATE surveys SET current = {$current}\n" .
                "WHERE id = " . $id
            );
            $this->pg_query(
                "UPDATE survey_questions SET " . implode(', ', $eqns) . "\n" .
                "WHERE language = 'en' AND surveyid = " . $id
            );
        } else {
            // Add a survey
            foreach ($f as $key => $val) {
                if ($key == 'submit')
                    continue;
                $keys[] = $key;
                $vals[] = is_null($val)? 'NULL': "'" . pg_escape_string($val) . "'";
            }
            $rs = $this->pg_query(
                "SELECT nextval('survey_id_seq')"
            );
            $surveyId = pg_fetch_result($rs, 0, 0);
            $this->pg_query(
                "INSERT INTO surveys\n" .
                "    (id, current)\n" .
                "VALUES\n" .
                "    ({$surveyId}, {$current})"
            );
            $this->pg_query(
                "INSERT INTO survey_questions\n" .
                "    (surveyid, language, " . implode(', ', $keys) . ")\n" .
                "VALUES\n" .
                "    ({$surveyId}, 'en', " . implode(', ', $vals) . ")"
            );
        }

        $this->pg_query('COMMIT');
        $this->redirect_relative = '/admin/surveys.php';
    }

    function RenderThanks() {
    }
}
?>
