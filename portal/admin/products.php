<?php
//
// List of products
//
// $Id$
//
class Admin_Products extends Admin_BasePage {
    function __construct() {
        $this->content_template = 'admin/products.html';
    }

    function Render() {

        $rs = $this->pg_query("SELECT prod.id,prod.name,org.name AS publisher,CASE WHEN prod.approved THEN 'Yes' ELSE 'No' END AS approved,prod.lastconfirmed FROM products prod, organisations org WHERE prod.publisher = org.id ORDER BY prod.approved,prod.name,org.name,id");

        for ($i = 0, $rows = pg_num_rows($rs); $i < $rows; $i++) {
            $cons = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            $this->tpl->setVariable($cons);
            $this->tpl->parse('cons_loop');
        }
    }
}
?>
