<?php
//
// List of organisations
//
// $Id$
//
class Admin_Orgs extends Admin_BasePage {
    function __construct() {
        $this->content_template = 'admin/orgs.html';
    }

    function Render() {

        $rs = $this->pg_query("SELECT id,name,CASE WHEN approved THEN 'Yes' ELSE 'No' END AS approved,lastconfirmed FROM organisations ORDER BY approved,name,id");

        for ($i = 0, $rows = pg_num_rows($rs); $i < $rows; $i++) {
            $cons = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            $this->tpl->setVariable($cons);
            $this->tpl->parse('cons_loop');
        }
    }
}
?>
