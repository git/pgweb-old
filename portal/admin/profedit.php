<?php

//
// Adding / editing a professional service provider 
//
// $Id: profedit.php,v 1.3 2007-03-16 11:27:39 mha Exp $
//
class Admin_ProfEdit extends PgForm {
    function __construct() {
        $this->navsection = 'admin';
    }
    function SetupForm() {
        if ('POST' != $_SERVER['REQUEST_METHOD']) {
            if (isset($_GET['action']) && $_GET['action'] == 'del') {
                if (empty($_GET['id'])) {
                    throw new Exception('Id not specified');
                }
                $this->pg_query('DELETE FROM profserv WHERE id=' . intval($_GET['id']));
                header('Location: prof.php');
                exit(0);
            }

            $defaults = array('lastconfirmed'=>date('Y-m-d H:i'));
            if (!empty($_GET['id'])) {
                $rs = $this->pg_query(
                    "SELECT id,email,lastconfirmed,name,url,description,employees,locations,NULLIF(provides_support,false) AS provides_support,NULLIF(provides_hosting,false) AS provides_hosting,NULLIF(region_africa,false) AS region_africa,NULLIF(region_asia,false) AS region_asia,NULLIF(region_europe,false) AS region_europe,NULLIF(region_northamerica,false) AS region_northamerica,NULLIF(region_oceania,false) AS region_oceania,NULLIF(region_southamerica,false) AS region_southamerica,hours,languages,customerexample,experience,contact,interfaces,NULLIF(approved,false) AS approved FROM profserv WHERE id=" . intval($_GET['id']));
                if (pg_num_rows($rs)) {
                    $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
                }
            }
            $this->form->setDefaults($defaults);
        }

        $txtf = array('size' => 52);
        $txta = array('cols' => 40, 'rows'=>8);
        $this->form->addElement('hidden','id');
        $this->form->addElement('header',null,'Approval');
        $this->form->addElement('checkbox','approved',null,'Is approved (<b>Always</b> verify all HTML content before approving!)');
        $this->form->addElement('header',null,'Basic data');
        $this->form->addElement('text','name','Name', $txtf);
        $this->form->addElement('text','url','URL',$txtf);
        $this->form->addElement('text','email','Manager email', $txtf);
        $this->form->addElement('text','lastconfirmed','Last confirmed', $txtf);
        $this->form->addElement('textarea','description','Description', $txta);
        $this->form->addElement('text','locations','Locations', $txtf);
        $regions[] =& $this->form->createElement('checkbox','region_africa',null,'Africa');
        $regions[] =& $this->form->createElement('checkbox','region_asia',null,'Asia');
        $regions[] =& $this->form->createElement('checkbox','region_europe',null,'Europe');
        $regions[] =& $this->form->createElement('checkbox','region_northamerica',null,'North America');
        $regions[] =& $this->form->createElement('checkbox','region_oceania',null,'Oceania');
        $regions[] =& $this->form->createElement('checkbox','region_southamerica',null,'South America');
        $this->form->addGroup($regions, 'regions', 'Regions', '<br/>', false);
        $this->form->addElement('textarea','contact','Contact Info',$txta);
        $this->form->addElement('header',null,'Support and services');
        $this->form->addElement('checkbox','provides_support',null,'Provides Support Services');
        $this->form->addElement('text','employees','Number of employees', $txtf);
        $this->form->addElement('text','hours','Hours',$txtf);
        $this->form->addElement('text','languages','Languages',$txtf);
        $this->form->addElement('textarea','customerexample','Customer example',$txta);
        $this->form->addElement('textarea','experience','Experience',$txta);
        $this->form->addElement('header',null,'Hosting');
        $this->form->addElement('checkbox','provides_hosting',null,'Provides Hosting');
        $this->form->addElement('textarea','interfaces','Supported Interfaces',$txta);

        $this->form->applyFilter('__ALL__', 'trim');
        $this->form->addRule('name','Name required','required',null,'client');
        $this->form->addRule('email','Manager email required','required',null,'client');
        $this->form->addRule('lastconfirmed','Last confirmed required','required',null,'client');
        $this->form->addRule('description','Description required','required',null,'client');
        $this->form->addRule('url','URL must be to a webpage','regex','/^https?:\/\//','client');
    }

    function ProcessForm($f) {
        $id = $f['id'];
        $f['region_africa'] = empty($f['region_africa'])?'f':'t';
        $f['region_asia'] = empty($f['region_asia'])?'f':'t';
        $f['region_europe'] = empty($f['region_europe'])?'f':'t';
        $f['region_northamerica'] = empty($f['region_northamerica'])?'f':'t';
        $f['region_oceania'] = empty($f['region_oceania'])?'f':'t';
        $f['region_southamerica'] = empty($f['region_southamerica'])?'f':'t';
        $f['provides_support'] = empty($f['provides_support'])?'f':'t';
        $f['provides_hosting'] = empty($f['provides_hosting'])?'f':'t';
        $f['approved'] = empty($f['approved'])?'f':'t';
        unset($f['id']);

        $eqns = $keys = $vals = array();
        foreach ($f as $key => $val) {
            if ($key == 'submit') continue;
            $keys[] = $key;
            if (empty($val) || $val=='') {
                $vals[] = 'NULL';
                $eqns[] = $key . ' = NULL';
            } else {
                $vals[] = "'" . pg_escape_string($val) . "'";
                $eqns[] = $key . ' = \'' . pg_escape_string($val) . '\'';
            }
        }

        if (empty($id)) {
            $r = $this->pg_query("SELECT nextval('profserv_id_seq')");
            list($id) = pg_fetch_row($r,0);

            $this->pg_query('INSERT INTO profserv (id,' . implode(', ', $keys) . ") VALUES ({$id}," . implode(', ',$vals) . ')');
        } else {
            $r = $this->pg_query("SELECT approved FROM profserv WHERE id={$id}");
            list($oldapproved) = pg_fetch_row($r,0);

            $this->pg_query("UPDATE profserv SET " . implode(', ', $eqns) . " WHERE id={$id}");

            if ($oldapproved=='f' && $f['approved']=='t') {
                $mailtext = 'Edit: ' . $_SETTINGS['masterserver'] . '/admin/prof-edit.php?id=' . $id;
                mail($_SETTINGS['notifymail'], 'Professional service ' . $f['name'] . ' was approved by ' . $_SERVER['PHP_AUTH_USER'], $mailtext);
            }
        }
        $this->redirect_relative = '/admin/prof.php';
    }
    function RenderThanks() {
    }
}

?>
