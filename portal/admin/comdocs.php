<?php

//
// Pending community documentation 
//
// $Id: comdocs.php,v 1.5 2007-06-29 08:39:46 mha Exp $
//
class Admin_ComDocs extends Admin_BasePage {
    function __construct() {
        $this->content_template = 'admin/comdocs.html';
    }

    function Render() {
        if (!empty($_GET['doapprove']) && $_GET['doapprove'] == '1') {
            $pageid = intval($_GET['id']);
            $this->pg_query("SELECT communitypages_approve({$pageid})");
            header('Location: comdocs.php');
            exit(0);
        }

        if (!empty($_GET['dorelease']) && $_GET['dorelease'] == '1') {
            $pageid = intval($_GET['id']);
            $this->pg_query("BEGIN TRANSACTION");
            $this->pg_query("DELETE FROM communitypages_work_files WHERE pageid={$pageid}");
            $this->pg_query("DELETE FROM communitypages_work WHERE pageid={$pageid}");
            $this->pg_query("COMMIT");
            header('Location: comdocs.php');
            exit(0);
        }

        $q = '';
        $rdy = ' WHERE ready=1';
        if (isset($_GET['all']) && $_GET['all'] == '1') {
            $q = "SELECT pageid, parent, title, author, savedate::timestamp(0) as savedate, 'Published' AS status, 3 AS intstatus FROM communitypages WHERE pageid NOT IN (SELECT pageid FROM communitypages_work) UNION ALL";
            $rdy = '';
        }
        $q .= " SELECT pageid, parent, title, author, savedate::timestamp(0) as savedate, CASE WHEN ready=1 THEN 'READY' ELSE 'WORKING' END AS status, ready as intstatus FROM communitypages_work $rdy ORDER BY parent, pageid";
        $res = $this->pg_query($q);

        for ($i = 0; $i < pg_num_rows($res); $i++)
        {
            $r = pg_fetch_assoc($res);
            $this->tpl->setVariable($r);
            $pid = intval($r['pageid']);
            $r2 = $this->pg_query("SELECT * FROM communitypages_breadcrumbs(" . $pid . ")");
            $bc = '';
            if (pg_num_rows($r2) > 1) {
                for ($j=pg_num_rows($r2)-1; $j >=0; $j--) {
                    if ($bc != '')
                        $bc .= ' &gt; ';
                    $bc .= pg_fetch_result($r2, $j, 1);
                }
            }
            $this->tpl->setVariable('crumbs',$bc);
            $this->tpl->parse('pending_loop');
        }

        // If there is nothing to show, need a touch
        $this->tpl->touchBlock('content_block');

    }
}
?>
