<?php

//
// Adding / editing a developer
//
// $Id: developeredit.php,v 1.2 2007-09-12 21:08:15 mha Exp $
//
class Admin_DeveloperEdit extends PgForm {
    function __construct() {
        $this->navsection = 'admin';
    }

    function SetupForm() {
        if ('POST' != $_SERVER['REQUEST_METHOD']) {
            if (isset($_GET['action']) && $_GET['action'] == 'del') {
                if (empty($_GET['id'])) {
                    throw new Exception('Id not specified');
                }
                $this->pg_query_params("DELETE FROM developers WHERE id=$1", array($_GET['id']));
                header('Location: developers.php');
                exit(0);
            }
        }

        if (!empty($_GET['id'])) {
            $rs = $this->pg_query_params(
                "SELECT id,type,lastname,firstname,email,company,companyurl,location,contribution FROM developers WHERE id=$1", array($_GET['id']));

            if (pg_num_rows($rs)) {
                $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
            }
            $this->form->setDefaults($defaults);
        }


        $txtf = array('size' => 52);
        $txta = array('cols' => 40, 'rows'=>8);
        $this->form->addElement('hidden','id');
        $this->form->addElement('select','type','Type', $this->get_types());
        $this->form->addElement('text','lastname','Lastname', $txtf);
        $this->form->addElement('text','firstname','Firstname', $txtf);
        $this->form->addElement('text','email','Email', $txtf);
        $this->form->addElement('text','company','Company', $txtf);
        $this->form->addElement('text','companyurl','Company URL', $txtf);
        $this->form->addElement('text','location','Location',$txtf);
        $this->form->addElement('textarea','contribution','Contribution',$txta);
    }

    function ProcessForm($f) {
        if (empty($f['id'])) {
            $this->pg_query_params("INSERT INTO developers (type,lastname,firstname,email,company,companyurl,location,contribution) VALUES ($1,$2,$3,$4,$5,$6,$7,$8)",
                array($f['type'],$f['lastname'],$f['firstname'],$f['email'],$f['company'],$f['companyurl'],$f['location'],$f['contribution']));
        } 
        else {
            $this->pg_query_params("UPDATE developers SET type=$1,lastname=$2,firstname=$3,email=$4,company=$5,companyurl=$6,location=$7,contribution=$8 WHERE id=$9",
                array($f['type'],$f['lastname'],$f['firstname'],$f['email'],$f['company'],$f['companyurl'],$f['location'],$f['contribution'],$f['id']));
        }
        $this->redirect_relative = '/admin/developers.php';
    }

    function RenderThanks() {
    }

    function get_types() {
        $rs = $this->pg_query('SELECT type,typename FROM developers_types ORDER BY sortorder');
        $c = array();
        for ($i = 0; $i < pg_num_rows($rs); $i++) {
            $r = pg_fetch_array($rs,$i,PGSQL_ASSOC);
            $c[$r['type']]=$r['typename'];
        }
        return $c;
    }
}

?>
