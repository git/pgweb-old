<?php
//
// List of developers 
//
// $Id: developers.php,v 1.2 2007-09-12 21:08:15 mha Exp $
//
class Admin_Developers extends Admin_BasePage {
    function __construct() {
        $this->content_template = 'admin/developers.html';
    }

    function Render() {
        $rs = $this->pg_query("SELECT id,typename,firstname||' '||lastname AS name,email FROM developers NATURAL JOIN developers_types ORDER BY developers_types.sortorder,lastname,firstname");

        for ($i = 0, $rows = pg_num_rows($rs); $i < $rows; $i++) {
            $cons = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            $this->tpl->setVariable($cons);
            $this->tpl->parse('cons_loop');
        }
    }
}
?>
