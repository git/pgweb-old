<?php

//
// List of user quotes
//
// $Id: quotes.php,v 1.2 2007-03-12 14:51:43 mha Exp $
//
class Admin_Quotes extends Admin_BasePage {
    function __construct() {
        $this->content_template = 'admin/quotes.html';
    }
    function Render() {
        global $_SETTINGS;
        global $_LANGUAGES;

        if (!empty($_GET['action']) && !empty($_GET['id'])) {
            $id = intval($_GET['id']);
            switch ($_GET['action']) {
            case 'delete':
                $this->pg_query(
                    "DELETE FROM quotes\n" .
                    "WHERE id = {$id}"
                );
                break;
            case 'approve': 
                $this->pg_query(
                    "UPDATE quotes\n" .
                    "SET approved = true, active = true\n" .
                    "WHERE id = {$id}"
                );
                $mailtext = "View: " . $_SETTINGS['masterserver'] . '/about/quotes.' . $id .
                    "\nEdit: " . $_SETTINGS['masterserver'] . '/admin/quotes-edit.php?id=' . $id;
                @mail($_SETTINGS['notifymail'], 'Quotes entry ' . $id . ' was approved by ' . $_SERVER['PHP_AUTH_USER'], $mailtext);
                break;
            } // switch
            header('Location: quotes.php');
            exit();
        }

        $rs = $this->pg_query(
            "SELECT q.id, q.approved, q.active, qt.quote, qt.tagline, qt.language,\n" .
            "       EXTRACT(EPOCH FROM qt.modified) AS modified\n" .
            "FROM quotes q, quotes_text qt\n" .
            "WHERE q.id = qt.quoteid\n" .
            "ORDER BY q.id DESC"
        );

        $quotes      = array();
        $quotesLang = array();
        $quotesId    = -1;

        for ($i = 0, $rows = pg_num_rows($rs); $i < $rows; $i++) {
            $ary = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            if ($quotesId != $ary['id']) {
                $quotes[] = array(
                    'id'        => $ary['id'],
                    'quote'    => $ary['quote'],
                    'tagline' => $ary['tagline'],
                    'approved'  => $ary['approved'],
                    'active'    => $ary['active']
                );
            }
            $quotesId = $ary['id'];
            if ($ary['language'] == $_SETTINGS['defaultlanguage']) {
                $quotes[count($quotes) - 1]['quote'] = $ary['quote'];
            }
            $quotesLangs[$quotesId][$ary['language']] = $ary['modified'];
        }

        foreach ($quotes as $item) {
            $langs = $quotesLangs[$item['id']];
            foreach ($_LANGUAGES as $handler => $name) {
                if ($handler != $_SETTINGS['defaultlanguage']) {
                    $this->tpl->setVariable(array(
                        'lang_name'     => $name,
                        'lang_handler'  => $handler,
                        'lang_exists'   => !empty($langs[$handler])? 't': 'f',
                        'lang_up2date'  => (empty($langs[$handler]) || $langs[$handler] >= $langs[$_SETTINGS['defaultlanguage']])? 't': 'f'
                    ));
                    $this->tpl->parse('quotes_translations_loop');
                }
            }
            $this->tpl->setVariable($item);
            $this->tpl->parse('quotes_loop');
        }
    }
}
?>
