<?php

//
// List of events
//
// $Id: events.php,v 1.9 2007-11-03 02:44:46 xzilla Exp $
//
class Admin_Events extends Admin_BasePage {
    function __construct() {
        $this->content_template = 'admin/events.html';
    }

    function Render() {
        global $_SETTINGS;
        global $_LANGUAGES;

        if (isset($_GET['action']) && $_GET['action'] == 'delete') {
            $this->pg_query_params("DELETE FROM events WHERE id=$1", array($_GET['id']));
            header('Location: /admin/events.php');
            exit(0);
        }

        $rs = $this->pg_query(
            "SELECT e.id, e.posted, e.posted_by, e.approved, e.start_date, e.end_date,\n" .
            "       et.event, et.language, EXTRACT(EPOCH FROM et.modified) AS modified,\n" .
            "       COALESCE(e.training, false) AS training, el.city, el.state, c.name AS country\n" .
            "FROM events e, events_text et, events_location el, countries c\n" .
            "WHERE e.id = et.eventid\n" .
            "AND   e.id = el.eventid\n" .
            "AND   c.id = el.country\n" .
            "ORDER BY e.approved, e.start_date DESC, e.posted" .
            ((isset($_GET['all']) && $_GET['all']=='1')?'':' LIMIT 20')
        );

        $events     = array();
        $eventLangs = array();
        $eventId    = -1;

        for ($i = 0, $rows = pg_num_rows($rs); $i < $rows; $i++) {
            $ary = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            if ($eventId != $ary['id']) {
                $events[] = array(
                    'id'         => $ary['id'],
                    'posted'     => $ary['posted'],
                    'posted_by'  => $ary['posted_by'],
                    'approved'   => $ary['approved'],
                    'start_date' => $ary['start_date'],
                    'end_date'   => $ary['end_date'],
                    'training'   => $ary['training'],
                    'city'	 => $ary['city'],
                    'state'	 => $ary['state'],
                    'country'	 => $ary['country'],
                );
            }
            $eventId = $ary['id'];
            if ($ary['language'] == $_SETTINGS['defaultlanguage']) {
                $events[count($events) - 1]['event'] = $ary['event'];
            }
            $eventLangs[$eventId][$ary['language']] = $ary['modified'];
        }

        foreach ($events as $item) {
            $langs = $eventLangs[$item['id']];
            foreach ($_LANGUAGES as $handler => $name) {
                if ($handler != $_SETTINGS['defaultlanguage']) {
                    $this->tpl->setVariable(array(
                        'lang_name'     => $name,
                        'lang_handler'  => $handler,
                        'lang_exists'   => !empty($langs[$handler])? 't': 'f',
                        'lang_up2date'  => (empty($langs[$handler]) || $langs[$handler] >= $langs[$_SETTINGS['defaultlanguage']])? 't': 'f'
                    ));
                    $this->tpl->parse('events_translations_loop');
                }
            }
            $this->tpl->setVariable($item);
            $this->tpl->parse('events_loop');
        }
    }
}
?>
