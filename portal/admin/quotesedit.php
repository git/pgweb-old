<?php

//
// Adding / editing a quotes item
//
// $Id: quotesedit.php,v 1.2 2007-04-08 15:59:14 mha Exp $
//
class Admin_QuotesEdit extends PgForm {
    function __construct() {
        $this->navsection = 'admin';
    }

    function SetupForm() {
        if ('POST' != $_SERVER['REQUEST_METHOD']) {
            $defaults = array(
                'active'   => 't',
                'approved' => 't'
            );
            if (!empty($_GET['id'])) {
                $rs = $this->pg_query(
                    "SELECT q.id, NULLIF(q.approved, false) AS approved, NULLIF(q.active, false) AS active,\n" .
                    "       qt.quote, qt.tagline\n" .
                    "FROM quotes q, quotes_text qt\n" .
                    "WHERE q.id = qt.quoteid AND\n" .
                    "      qt.language = 'en' AND\n" .
                    "      q.id = " . intval($_GET['id'])
                );

                if (pg_num_rows($rs)) {
                    $defaults = pg_fetch_array($rs, 0, PGSQL_ASSOC);
                }
            }
            $this->form->setDefaults($defaults);
        }

        $this->form->addElement('hidden',     'id');
        $this->form->addElement('checkbox',   'approved', null, 'Approved');
        $this->form->addElement('checkbox',   'active', null, 'Active');
        $this->form->addElement('textarea',   'quote', 'Quote:', array('rows' => 5, 'cols' => 50));
        $this->form->addElement('textarea',   'tagline', 'Tag line:', array('rows' => 2, 'cols' => 50));
        $buttons = array();
        $buttons[] =& $this->form->createElement('button',     null, 'Preview...', array('onclick' => "doPreview(this.form['quote'].value, this.form['tagline'].value);"));
        $this->form->addGroup($buttons, null, null, '&nbsp;', false);

        $this->form->applyFilter('__ALL__', 'trim');
        $this->form->applyFilter('id', 'intval');

        // Make all fields required
        $this->form->addRule('quote', 'The quote is required.', 'required', null, 'client');
        $this->form->addRule('tagline', 'The tag line is required.', 'required', null, 'client');

        // Apply field-specific rules
        $this->form->addRule('quote', 'The quote must be between 3 and 500 characters long.', 'rangelength', array(3, 500), 'client');
        $this->form->addRule('tagline', 'The tag line must be between 3 and 500 characters long.', 'rangelength', array(3, 500), 'client');
    }

    function ProcessForm($f) {
        $f  = array_map('pg_escape_string', $f);
        $f['approved'] = empty($f['approved'])? 'f': 't';
        $f['active']   = empty($f['active'])? 'f': 't';
        $error         = null;

        $this->pg_query('BEGIN');
        if (empty($f['id'])) {
            $rs     = $this->pg_query("SELECT nextval('quotes_id_seq')");
            $quotesId = pg_fetch_result($rs, 0, 0);
            $this->pg_query(
                "INSERT INTO quotes \n" .
                "    (id, approved, active) \n" .
                "VALUES \n" .
                "    ({$quotesId}, '{$f['approved']}', '{$f['active']}')"
            );
            $this->pg_query(
                "INSERT INTO quotes_text\n" .
                "    (quoteid, quote, tagline, language)\n" .
                "VALUES\n" .
                "    ({$quotesId}, '{$f['quote']}', '{$f['tagline']}', 'en')"
            );
        } else {
            $r = $this->pg_query("SELECT approved FROM quotes WHERE id={$f['id']}");
            list($oldapproved) = pg_fetch_row($r,0); 
            if ($oldapproved=='f' && $f['approved']=='t') {
                $mailtext = "View: " . $_SETTINGS['masterserver'] . '/about/quotes.' .
                    $f['id'] .
                    "\nEdit: " . $_SETTINGS['masterserver'] . '/admin/quotes-edit.php?id=' . $f['id'];
                @mail($_SETTINGS['notifymail'], 'Quotes entry ' . $f[id] . ' was approved by ' . $_SERVER['PHP_AUTH_USER'], $mailtext);
            }

            $this->pg_query(
                "UPDATE quotes SET approved = '{$f['approved']}', active = '{$f['active']}'\n" .
                "WHERE id = {$f['id']}"
            );
            $this->pg_query(
                "UPDATE quotes_text SET quote = '{$f['quote']}', tagline = '{$f['tagline']}'\n" .
                "WHERE quoteid = {$f['id']} AND language = 'en'"
            );
        }

        $this->pg_query('COMMIT');
        $this->redirect_relative = '/admin/quotes.php';
    }
    function RenderThanks() {
    }
}

?>
