<?php

//
// Search for comments, approve / delete comments
//
// $Id: comments.php,v 1.8 2007-03-12 14:51:43 mha Exp $
//
class Admin_Comments extends Admin_BasePage {
    function __construct() {
        $this->content_template = 'admin/comments.html';
    }
    function Render() {
        global $_SETTINGS;

        $clause = '';
        $filtertxt = '';
        $qsfilter='action=search';
        $MAX_PER_PAGE = 20;
        $offset = 0;

        if (!empty($_GET['action'])) {
            $id = !empty($_GET['id'])? intval($_GET['id']): null;
            switch ($_GET['action']) {
            case 'approve':
            case 'save':
            case 'delete':
            case 'reject':
                if (empty($id)) {
                    header('Location: comments.php');
                    exit();
                }
                $rs = $this->pg_query(
                    "SELECT id,posted_by,file,version,comment,CASE WHEN posted_at > '2005-05-01' THEN 1 ELSE 0 END AS sendmail\n" .
                    "FROM comments\n" .
                    "WHERE id = " . $id
                );
                if (0 == pg_num_rows($rs)) {
                    throw new Exception('Cannot find comment #' . $id . '. It was probably deleted already.');
                } else {
                    $comment = array_map('html_entity_decode', pg_fetch_array($rs, 0, PGSQL_ASSOC));
                    $comment['posted_by'] = str_replace(' AT ', '@', $comment['posted_by']);
                }
                if ('delete' == $_GET['action'] || 'reject' == $_GET['action']) {
                    $this->pg_query(
                        "DELETE FROM comments\n" .
                        "WHERE id = " . $id
                    );
                    if ('reject' == $_GET['action']) {
                        $this->pg_query(
                            "UPDATE comments SET approved = false\n" .
                            "WHERE id = " . $id
                        );
                        if ($comment['sendmail']==1) {
                            $mailsubj = 'Your comment was rejected from PostgreSQL interactive documentation';
                            $mailtext = "Comments are normally rejected for the following reasons:\n" .
                                "Your comment was actually a support request and should be sent to the appropriate mailing list.\n" .
                                "Your comment contained erroneous information or could not be understood.\n" .
                                "You posted spam or some other nonsense.\n" .
                                "The original comment follows\n----\n" .
                                $comment['comment'];
                            @mail($comment['posted_by'], $mailsubj, $mailtext);
                        }
                        $action_past_tense = 'rejected';
                    }
                    else {
                        $action_past_tense = 'deleted';
                    }
                } // delete or reject
                else if ('approve' == $_GET['action']) {
                    $this->pg_query(
                        "UPDATE comments SET approved=true, processed=true WHERE id=" . $id);
                    $action_past_tense = 'approved';
                }
                else { // save
                    $this->pg_query(
                        "UPDATE comments SET approved=false, processed=true WHERE id=" . $id);
                    if ($comment['sendmail']==1) {
                        $mailsubj = 'Your comment on the PostgreSQL interactive documentation has been saved';
                        $mailtext = "Your comment on the PostgreSQL interactive documentation has been saved for review by a\n" .
                            "documentation editor. This means it will not be displayed on the website\n" .
                            "but we will attempt to incoporate suggested/required changes in the next\n" .
                            "version of the main documentation.\n" .
                            "The original comment follows\n----\n" .
                            $comment['comment'];
                        @mail($comment['posted_by'], $mailsubj, $mailtext);
                    }                    
                    $action_past_tense = 'saved';
                }

                if ($comment['sendmail']==1) {
                    $mailsubj = 'Comment #' . $id . ' was ' . $action_past_tense . ' by ' . $_SERVER['PHP_AUTH_USER'];
                    $mailtext = "Author: " . $comment['posted_by'] . "\n" .
                        "Page:   " . $comment['version'] . '/' . $comment['file'] . "\n----\n" .
                        $comment['comment'];
                    @mail($_SETTINGS['notifymail'], $mailsubj, $mailtext);
                }
                header('Location: comments.php');
                exit();
                break;

            case 'search':
                if (!empty($_GET['keyword'])) {
                    $keyword = "'%" . pg_escape_string($_GET['keyword']) . "%'";
                    $clause  = " AND (file LIKE {$keyword} OR \"comment\" LIKE {$keyword}" .
                        (is_numeric($_GET['keyword'])? ' OR id = ' . intval($_GET['keyword']): '') .
                        ')';
                    $filtertxt .= 'Keyword search for "' . htmlspecialchars($_GET['keyword']) . '", ';
                    $qsfilter .= '&keyword=' . urlencode($_GET['keyword']); 
                }
                if (!empty($_GET['version']) && is_numeric($_GET['version'])) {
                    $clause .= ' AND version=' . $_GET['version'];
                    $filtertxt .= 'Version=' . $_GET['version']  . ', ';
                    $qsfilter .= '&version=' . $_GET['version'];
                }
                if (!empty($_GET['state']) || $_GET['state']==0) {
                    switch ($_GET['state']) {
                    case -1: // No state filter
                        break;
                    case 0: // Pending
                        $clause .= ' AND NOT processed';
                        $filtertxt .= 'State=pending, ';
                        break;
                    case 1: // Saved for later
                        $clause .= ' AND processed AND NOT approved';
                        $filtertxt .= 'State=saved for later, ';
                        break;
                    case 2: // Approved
                        $clause .= ' AND processed AND approved';
                        $filtertxt .= 'State=approved, ';
                        break;
                    default:
                        throw new Exception('Invalid state specified');
                    }
                    $qsfilter .= '&state=' . $_GET['state'];
                }
                if (!empty($_GET['offset']) && is_numeric($_GET['offset'])) {
                    $offset = intval($_GET['offset']);
                }

                // Remove initial AND 
                if ($clause != '') {
                    $clause = substr($clause, 4);
                }

                $filtertxt = rtrim($filtertxt,' ,');

                break;
            default:
                throw new Exception('Invalid action.');
            }
        }

        if ($clause=='') {
            // default filter
            $clause = 'NOT processed';
            $filtertxt = 'Pending';
        }

        $rs = $this->pg_query("SELECT * FROM comments WHERE ${clause} ORDER BY processed,approved,id DESC LIMIT " . ($MAX_PER_PAGE+1) . " OFFSET ${offset}");

        $this->tpl->setCallbackFunction('getstate','admin_comments_getstate');
        $this->tpl->setCallbackFunction('cansave','admin_comments_cansave');
        $this->tpl->setVariable('filter',$filtertxt);

        $hasmore = 0;

        if (!empty($rs)) {
            $rows = pg_num_rows($rs);
            if ($rows > $MAX_PER_PAGE) {
                $hasmore = 1;
                $rows = $MAX_PER_PAGE;
            }
            for ($i = 0; $i < $rows; $i++) {
                $ary = pg_fetch_array($rs, $i, PGSQL_ASSOC);

                // Cleanup the comment
                $ary['comment'] = str_replace("  ", "&nbsp;&nbsp;",  $ary['comment']);
                $ary['comment'] = str_replace("<", "&lt;",  $ary['comment']);
                $ary['comment'] = str_replace(">", "&gt;",  $ary['comment']);
                $ary['comment'] = nl2br($ary['comment']);

                $this->tpl->setVariable($ary);
                $this->tpl->parse('comments_loop');
            }
            if (0 == $rows) {
                $this->tpl->touchBlock('comments_empty');
            }
        }

        if ($offset>0 || $hasmore) {
            $this->tpl->touchBlock('comments_backnext');
        }
        if ($offset>0) {
            // Contain back button
            $backofs = ($offset > $MAX_PER_PAGE)?$offset-$MAX_PER_PAGE:0;
            $this->tpl->setVariable('backofs',$backofs);
            $this->tpl->setVariable('backqs',$qsfilter);
        }
        if ($hasmore) {
            // Contain next button
            $this->tpl->setVariable('nextofs',$offset + $MAX_PER_PAGE);
            $this->tpl->setVariable('nextqs',$qsfilter);
        }


        require_once 'HTML/QuickForm.php';
        require_once 'HTML/QuickForm/Renderer/ITDynamic.php';

        $rsv = $this->pg_query(
            "SELECT DISTINCT version\n" .
            "FROM docs\n" .
            "ORDER BY version DESC"
        );
        $versions = array('' => '&nbsp;');
        for ($i = 0, $rows = pg_num_rows($rsv); $i < $rows; $i++) {
            $version = pg_fetch_result($rsv, $i, 0);
            $versions[$version] = $version;
        }

/*$form =& new HTML_QuickForm('commentsearch', 'get');
$form->setConstants(array(
    'action' => 'search'
));
$form->addElement('hidden',     'action');
$form->addElement('select',     'state', 'State', array(-1=>'',0=>'Pending',1=>'Saved',2=>'Approved'));
$form->addElement('select',     'version', 'PostgreSQL version:', $versions);
$form->addElement('text',       'keyword', 'Keyword or ID:', array('size' => 50, 'maxlength' => 100));
$form->addElement('submit',     null, 'Send');

$renderer =& new HTML_QuickForm_Renderer_ITDynamic($this->tpl);
$form->removeAttribute('name');
$form->accept($renderer);
$this->tpl->show();
 */

    }
}


function admin_comments_getstate($processed, $approved) {
	if ('f'==$processed) {
		return 'Pending';
	} else if ('t'==$approved) {
		return 'Approved';
	} else {
		return 'Saved';
	}
}

function admin_comments_cansave($processed, $approved, $txt) {
	return ('f'==$processed || 't'==$approved)?$txt:'';
}
?>
