<?php
//
// List of StackBuilder applications
//
// $Id: applications.php,v 1.1 2007-04-26 13:23:08 dpage Exp $
//
class Admin_Applications extends Admin_BasePage {
    function __construct() {
        $this->content_template = 'admin/applications.html';
    }

    function Render() {
        $rs = $this->pg_query("SELECT id, platform, CASE WHEN active = TRUE THEN 'Yes' ELSE 'No' END AS active, version, name FROM applications ORDER BY id, version, name;");

        for ($i = 0, $rows = pg_num_rows($rs); $i < $rows; $i++) {
            $cons = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            $this->tpl->setVariable($cons);
            $this->tpl->parse('cons_loop');
        }
    }
}
?>
