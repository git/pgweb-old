<?php
   require_once './global/settings.php';

   // Second version of redirector that doesn't redirect to arbitrary URLS!
   
   $mirror = intval($_GET['mirror']);
   $type = $_GET['type'];
   $file = $_GET['file'];
   
   if ($mirror <= 0) {
      echo "Invalid mirror specification.";
      exit(1);
   }
   if ($type != 'f' && $type != 'h') {
      echo "Invalid mirror type.";
      exit(1);
   }
   if ($file != '' && !preg_match('#^[a-zA-Z0-9/\._-]+$#', $file)) {
      echo "Invalid character in filename (" . htmlentities($file) . ").";
      exit(1);
   }
   
   // Set a cookie with the last mirror used
   setcookie('dlmirror', $mirror, time()+60*60*24*300,'/');
   setcookie('dlmirrortype', $type, time()+60*60*24*300,'/');

   $db = @pg_pconnect($_SETTINGS['db_portal']);
   if (!$db) {
      echo "Unable to connect to the database.";
      exit(1);
   }
   
   $res = pg_query_params($db, "SELECT 'ftp'||CASE WHEN mirror_index = 0 THEN ''::text ELSE mirror_index::text END ||" .
" '.' || country_code || '.postgresql.org' AS hostname, host_port AS port, host_path AS path, " .
"alternate_at_root FROM mirrors WHERE id=$1 " . ($type == 'h'?"AND alternate_protocol ":"") . 
" AND mirror_type='ftp' AND mirror_active AND NOT mirror_private AND mirror_dns AND mirror_last_rsync > (now() - '48 hrs'::interval)",
   				array($mirror));
   if (pg_num_rows($res) != 1) {
      echo "Mirror not found.";
      exit(1);
   }
   
   $rs = pg_fetch_row($res, 0);
   $host = $rs[0];
   $port = $rs[1];
   $path = $rs[2];
   $altroot = $rs[3];
   
   if ($file[0] != '/') {
      $file = '/' . $file;
   }
   if ($path[0] != '/') {
      $path = '/' . $path;
   }
   if ($path[strlen($path)-1] == '/') {
      $path = substr($path, 0, strlen($path)-1);
   }
   
   if ($type == 'h') {
   	// Construct http url
   	if ($altroot == 't') {
   		$url = "http://" . $host . $file;
   	}
   	else {
   		$url = "http://" . $host . $path . $file;
   	}
   }
   else {
   	// Construct ftp url
   	if ($rs[1] > 0 && $rs[1] != 21) {
   		echo "Port changes not currently supported";
   	}
   	else {
		$url = "ftp://" . $host . $path . $file;
	}
   }

   $ip = $_SERVER['REMOTE_ADDR'];
   $ip = preg_split('/[.]+/', $ip);
   $ipnumber = (double) ($ip[0] * 16777216) + ($ip[1] * 65536) + ($ip[2] * 256) + ($ip[3]);
   
   @pg_query_params($db, "INSERT INTO clickthrus (scheme, host, path, country) VALUES ($1,$2, $3, (SELECT countrycode FROM iptocountry WHERE $4 BETWEEN startip AND endip LIMIT 1))",
   	array(
   		$type=='h'?'http':'ftp',
   		$host,
   		($type=='h'&&$altroot=='t')?$file:($path . $file),
   		$ipnumber
   	));

   header("Location: " . $url);
   exit;
?>
