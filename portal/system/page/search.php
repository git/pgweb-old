<?php
// $Id: search.php,v 1.11 2007-03-12 20:45:30 mha Exp $

class Page_Search extends PgPage {
    private $searchbase = 'http://search.postgresql.org/search';
    private $maxdocbots = 3;
    public $hitsperpage = 20;
    private $pagenum = 1;
    public $query = '';
    private $searcher;

    function __construct() {
        $this->content_template = 'search.html';
        $this->navsection = 'search';
    }

    function PreRender() {
        // Parse shared parameters
        if (isset($_GET['p']) && intval($_GET['p']) > 0)
            $this->pagenum = intval($_GET['p']);

        if (isset($_GET['m']) && $_GET['m'] == '1') {
            require_once 'search_archives.php';
            $this->searcher = new ArchivesSearcher($this);
            $this->title = 'Archives Search';
        }
        else {
            require_once 'search_website.php';
            $this->searcher = new WebsiteSearcher($this);
            $this->title = 'Website Search';
        }

        if (isset($_GET['q'])) {
            $this->query = $_GET['q'];
            if ($this->query == 'Search') $this->query = '';
        }
    }

    function Render() {
        $this->searcher->DoPrepareForm($this->tpl);

        $this->firsthit = ($this->pagenum-1)*$this->hitsperpage+1;

        $this->tpl->setVariable('searchurl', $this->searchbase);
        $this->tpl->setVariable('searchfor', htmlentities($this->query,ENT_COMPAT,'utf-8'));

        if (strlen($this->query)<=0)
            // $this->tpl->touchBlock('no_hits');
            return;

        $start = microtime(true);
        $num = $this->searcher->ExecuteQuery();
        $end = microtime(true);

        $this->search_docbot();

        if ($num == 1) {
            // One row means just the rowcount
            $this->tpl->touchBlock('no_hits');
            return;
        }

        $this->tpl->setVariable('firsthit', $this->firsthit);
        $this->tpl->setVariable('lasthit', min($this->searcher->totalhits, $this->pagenum*$this->hitsperpage));
        if ($this->searcher->totalhits < 1000) 
            $this->tpl->setVariable('totalhits', $this->searcher->totalhits);
        else 
            $this->tpl->setVariable('totalhits', 'more than 1000');
        $this->tpl->setVariable('searchpages', number_format($this->searcher->search_base_count));
        $this->tpl->setVariable('searchtime', round($end-$start,5));
        $searchstr = '?q=' . urlencode($this->query) . $this->searcher->BuildQueryString();

        if ($this->searcher->totalhits > $this->hitsperpage) {
            $pl = '';
            if ($this->pagenum > 1) {
                $pl .= '<a href="' . htmlentities($this->searchbase . $searchstr . '&p=' . ($this->pagenum-1),ENT_COMPAT,'utf-8') . '">Prev</a>&nbsp;';
            }
            $start = 0;
            for ($i = 1; $i < min(21, ($this->searcher->totalhits/$this->hitsperpage)+1); $i++) {
                if ($this->pagenum > 10) {
                    $start = $this->pagenum-10;
                }
                $x = $i + $start;
                if ($this->pagenum == $x) {
                    $pl .= $x . '&nbsp;';
                    $lastselected=1;
                }
                else {
                    $pl .= '<a href="' . htmlentities($this->searchbase . $searchstr . '&p=' . $x,ENT_COMPAT,'utf-8') . '">' . $x . '</a>&nbsp;';
                    $lastselected=0;
                }
                if (($x*$this->hitsperpage) > $this->searcher->totalhits)
                    break;
            }
            if ($lastselected==0) {
                $pl .= '<a href="' . htmlentities($this->searchbase . $searchstr . '&p=' . ($this->pagenum+1),ENT_COMPAT,'utf-8') . '">Next</a>';
            }
            $this->tpl->setVariable('page_links',$pl);
            $this->tpl->touchBlock('page_links1');
            $this->tpl->touchBlock('page_links2');
        }

        $this->searcher->DisplayResults();
    }

    function search_docbot() {
        if ($this->pagenum > 1) return; // only show docbot urls on first page 
        $qry = "SELECT url FROM docbot_keylist WHERE \"key\" = '". pg_escape_string($this->query) ."'ORDER BY url ~ 'postgresql.org' desc, url LIMIT ". $this->maxdocbots;
        $rs = $this->pg_query($qry,'search');
        for ($i = 0; $i < pg_num_rows($rs); $i++) {
            $r = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            $this->tpl->setVariable('docbot_url', htmlentities($r['url'],ENT_COMPAT,'utf-8'));
            $this->tpl->parse('docbot_search_loop');
        }
    }
}

abstract class SearchBase {
    protected $page;
    protected $tpl;
    protected $rs;
    public $totalhits;
    public $search_base_count;

    function __construct($page) {
        $this->page = $page;
    }
    function DoPrepareForm() {
        $this->tpl = $this->page->tpl;
        $this->PrepareForm();
    }
    abstract function PrepareForm();
    abstract function ExecuteQuery();
    abstract function BuildQueryString();
    abstract function RenderRow($i,$r);

    function DisplayResults() {
        for ($i=0; $i < pg_num_rows($this->rs)-1; $i++) {
            $this->RenderRow($i, pg_fetch_assoc($this->rs, $i));
        }
    }
}
?>
