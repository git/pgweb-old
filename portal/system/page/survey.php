<?php
// $Id: survey.php,v 1.7 2007-10-23 20:06:25 mastermind Exp $
//
// Poll results
//
class Page_Survey extends PgPage {
   private $id;
   function __construct($id) {
      $this->id = intval($id);
      $this->navsection = 'community';
      $this->content_template = 'community/survey.html';
   }

   function Render() {
      $rs = $this->pg_query_params("SELECT tot1,tot2,tot3,tot4,tot5,tot6,tot7,tot8,respondants,q.* FROM surveys s, survey_questions q WHERE s.id={$this->id} AND s.id=q.surveyid AND q.language=$1",array($this->language));
      if (0 == pg_num_rows($rs))
          $rs = $this->pg_query("SELECT tot1,tot2,tot3,tot4,tot5,tot6,tot7,tot8,respondants,q.* FROM surveys s, survey_questions q WHERE s.id={$this->id} AND s.id=q.surveyid");
      if (pg_num_rows($rs) != 1) {
         throw new Exception('Survey results not found');
      }
      $this->tpl->setVariable(array(
         'page_title' => pg_fetch_result($rs, 0, 'question'),
         'result_title' => pg_fetch_result($rs, 0, 'question'),
         'result_total' => pg_fetch_result($rs, 0, 'respondants')
      ));
      for ($i=1; $i <= 8; $i++) {
         if (pg_field_is_null($rs, 0, 'opt'.$i) || pg_fetch_result($rs,0,'opt'.$i)=='')
             break;
         $this->tpl->setVariable(array(
            'result_option' => htmlentities(pg_fetch_result($rs, 0, 'opt' . $i),ENT_COMPAT,'utf-8'),
            'result_votes' => pg_fetch_result($rs, 0, 'tot' . $i),
            'result_percent' => $this->getpct(pg_fetch_result($rs, 0, 'tot' . $i), pg_fetch_result($rs, 0, 'respondants'))
         ));
         $this->tpl->parse('result_option_loop');
      }

      // other polls
      $rs = $this->pg_query_params("SELECT id, COALESCE(q1.question, q2.question) AS question FROM surveys s INNER JOIN survey_questions q2 ON s.id=q2.surveyid LEFT JOIN (SELECT surveyid, question FROM survey_questions WHERE language=$1) q1 ON q1.surveyid=s.id WHERE q2.language='en' AND s.respondants > 0 ORDER BY s.id DESC", array($this->language));
      for ($i=0; $i < pg_num_rows($rs); $i++) {
         if (pg_fetch_result($rs, $i, 'id') != $this->id) {
            $this->tpl->setVariable(array(
               'other_id' => pg_fetch_result($rs, $i, 'id'),
               'other_question' => htmlentities(pg_fetch_result($rs, $i, 'question'),ENT_COMPAT,'utf-8')
            ));
            $this->tpl->parse('other_surveys_loop');
         }
      }
   }

   function getpct($val, $tot)
   {
       if ($val == 0) {
           $res = 0;
       } else {
           $res = (100 / ($tot / $val));
       }
       return number_format($res, 3);
   }
}
?>
