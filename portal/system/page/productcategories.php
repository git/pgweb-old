<?php
// $Id: $
// Product categories
class Page_ProductCategories extends PgPage {
   function __construct() {
      $this->navsection = 'download';
      $this->content_template = 'download/productcategories.html';
   }

   function Render() {
       $rs = $this->pg_query('SELECT id,name FROM product_categories ORDER BY name');
       while ($row = pg_fetch_row($rs)) {
           $this->tpl->setVariable('id', $row[0]);
           $this->tpl->setVariable('name', $row[1]);

           $this->tpl->parse('productcategory-loop');
       }
   }
}
?>
