<?php
// $Id: search_website.php,v 1.3 2007-03-21 09:39:41 mha Exp $
class WebsiteSearcher extends SearchBase {
    private $allsites = false;
    function PrepareForm() {
        if (isset($_GET['a']) && $_GET['a'] == '1') {
            $this->allsites = true;
            $this->tpl->setVariable('comsites','CHECKED');
        }

        $this->tpl->touchBlock('main_form');
    }

    function ExecuteQuery() {
        $suburl=null;
        if (isset($_GET['u'])) {
            $suburl = $_GET['u'];
            $this->tpl->setVariable('suburl',htmlentities($_GET['u'],ENT_COMPAT,'utf-8'));
        }
        $this->rs = $this->page->pg_query_params("SELECT * FROM site_search($1,$2,$3,$4,$5)",
           array($this->page->query,$this->page->firsthit-1,$this->page->hitsperpage,$this->allsites?1:0,$suburl),
           'search');
        $this->totalhits = pg_fetch_result($this->rs, pg_num_rows($this->rs)-1, 5);
        $this->search_base_count = pg_fetch_result($this->rs, pg_num_rows($this->rs)-1, 0);
        return pg_num_rows($this->rs);
    }

    function BuildQueryString() {
        $s = '';
        if ($this->allsites)
            $s .= '&a=1';
        if (isset($_GET['u']))
            $s .= '&u=' . urlencode($_GET['u']);
        return $s;
    }

    function RenderRow($i,$r) {
        $this->tpl->setVariable('hit_num', $this->page->firsthit+$i);
        $this->tpl->setVariable('hit_url', $this->trailslash($r['baseurl']) . ltrim($r['suburl'],'/'));
        $this->tpl->setVariable('hit_title', htmlentities($r['title'],ENT_COMPAT,'utf-8'));
        $this->tpl->setVariable('hit_rank', $r['rank']);
        $this->tpl->setVariable('hit_abstract', str_replace(array('[[[[[[',']]]]]]'),array('<b>','</b>'),htmlentities($r['headline'],ENT_COMPAT,'utf-8')));
        $this->tpl->parse('main_search_loop');
    }

    private function trailslash($str) {
        if (substr($str, -1, 1) == '/')
            return $str;
        else
            return $str . '/';
    }
}
?>
