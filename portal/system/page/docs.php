<?php

//
// Documentation
//
require_once 'HTML/QuickForm.php';
require_once 'HTML/QuickForm/Renderer/ITDynamic.php';

class Page_Docs extends PgPage {
    private $version;
    private $type;
    private $chapter;
    private $extension = '.html';
    private $indexname = 'index.html';
    private $preview = false;

    function __construct($docinfo) {
        global $_SETTINGS;

		if ($docinfo[1]=='current')
			$this->version=$_SETTINGS['current_docs_version'];
		elseif ($docinfo[1]=='beta')
			$this->version=$_SETTINGS['beta_docs_version'];
		else
			$this->version=$docinfo[1];

        if ($docinfo[2]=='interactive')
            $this->type = 'interactive';
        elseif ($docinfo[2]=='static')
            $this->type = 'static';
        else
            throw new NotFoundException('Invalid type of docs');

		if (isset($docinfo[3])) {
            // ? after l so we can deal with both html and htm
            $this->chapter = preg_replace('/(\\.html?)?$/', '', $docinfo[3]);
            $this->chapter = preg_replace('!^/!', '', $this->chapter);
        }
        else
            $this->chapter = 'index';

        if ($this->version < 7.1) $this->extension = '.htm';
        if ($this->version < 7.2) $this->indexname = 'postgres.htm';
        if ($this->version < 6.4) $this->indexname = 'book01.htm';

        $this->base_template = 'common-docs.html';
        $this->inner_template = '';

        if (isset($_GET['preview']) && $_GET['preview'] == '1') {
            // Preview is only allowed when called from /admin interface.
            // Just to prevent stupidities, we know referer check isn't
            // actually secure, but it's good enough.
            if ( preg_replace("/^https/","http",$_SERVER['HTTP_REFERER']) !=
                $_SETTINGS['masterserver'] . '/admin/comments.php')
                throw new Exception('Invalid referer');
            $this->preview=1;
        }
    }

    function Render() {
        global $_SETTINGS;

        $this->tpl->setOption('preserve_data', true);
        $this->tpl->setVariable('docs_type', $this->type);
        $this->tpl->setVariable('docs_index_filename',$this->indexname);
        $rs = $this->pg_query_params('SELECT title,content FROM docs WHERE version=$1::numeric AND file=$2', array($this->version, $this->chapter . $this->extension));
        if (pg_num_rows($rs) != 1)
            throw new NotFoundException('Docs page not found: ' . htmlentities($this->chapter . $this->extension));
        $docs = pg_fetch_assoc($rs);

        $content = preg_replace('/href="(?<!http|ftp|mailto)([^\.]+).html/i', "href=\"\$1" . $this->extension, $docs['content']);
        $content = str_replace('IMG SRC="', "IMG SRC=\"/images/documentation/$this->version/", $content);

        $this->tpl->setVariable(array(
            'page_title'    => 'PostgreSQL ' . $this->version . ': ' . $docs['title'],
            'page_content'  => $content,
            'doc_nav_version' => $this->version
        ));

        if ($this->type == 'interactive') {
            $this->tpl->addBlockFile('comments', 'comments_block', 'docs-comments.html');
            $app = $this->preview?'':'approved AND';
            $rs = $this->pg_query_params("SELECT id,comment,date_part('epoch', posted_at) AS timestamp, posted_by FROM comments WHERE $app version=$1::numeric AND file=$2 ORDER BY posted_at", array($this->version, $this->chapter . $this->extension));
            if (pg_num_rows($rs) == 0) {
                $this->tpl->touchBlock('comment_empty');
            }
            while ($comment = pg_fetch_assoc($rs)) {
                // Cleanups
                $comment['comment'] = str_replace("  ", "&nbsp;&nbsp;",  $comment['comment']);
                $comment['comment'] = str_replace("<", "&lt;",  $comment['comment']);
                $comment['comment'] = str_replace(">", "&gt;",  $comment['comment']);
                $comment['comment'] = nl2br($comment['comment']);

                $this->tpl->setVariable(array(
                    'poster' => $comment['posted_by'],
                    'date' => date('d M Y G:i:s', $comment['timestamp']),
                    'comment' => $comment['comment']
                ));
                $this->tpl->parse('comment_loop');
            }

            if ($this->version == $_SETTINGS['current_docs_version'] || $this->version == $_SETTINGS['beta_docs_version'])
            {
                // Only show the form for the latest version docs, for various versions of latest ;-) 
                $this->tpl->setVariable('form_title', gettext('Add Comment'));
                $form = new HTML_QuickForm('commentdoc', 'post', $_SETTINGS['masterserver'] . '/commentdoc');
                $form->applyFilter('__ALL__', 'trim');
                $form->setDefaults(array(
                    'doc'     => $this->chapter . $this->extension,
                    'version' => $this->version,
                ));
                $form->addElement('hidden', 'lang');
                $form->addElement('hidden', 'doc');
                $form->addElement('hidden', 'version');
                $form->addElement('static',     null, null, gettext("Please use this form to add your own comments regarding your experience with particular features of PostgreSQL, clarifications of the documentation, or hints for other users. Please note, this is <strong>not</strong> a support forum, and your IP address will be logged. If you have a question or need help, please see the <a href=\"http://www.postgresql.org/docs/faq/\">faq</a>, try a <a href=\"http://www.postgresql.org/community/lists/\">mailing list</a>, or join us on <a href=\"http://www.postgresql.org/community/irc\">IRC</a>. Note that submissions containing URLs or other keywords commonly found in 'spam' comments may be silently discarded. Please contact the <a href=\"mailto:webmaster@postgresql.org\">webmaster</a> if you think this is happening to you in error."));
                $form->addElement('static',     null, null, gettext('In order to submit a comment, you must have a <a href="/community/signup">community account</a>.'));
                $form->addElement('textarea',   'comment', gettext("Comment"), array('rows' => 8, 'cols' => 66, 'wrap' => 'soft'));
                $form->addRule('comment', gettext("The comment is required."), 'required', null, 'client');
                $form->addRule('comment', gettext("Your comment should be at least 10 characters long."), 'minlength', 10, 'client');
                $form->addElement('submit', 'submit', gettext("Submit"));
                $form->setRequiredNote(gettext('<span class="txtRequiredField">*</span> denotes required field'));
                $form->setJsWarnings(gettext('Invalid information entered.'), gettext('Please correct these fields.'));
                $form->removeAttribute('name');
                $renderer =& new HTML_QuickForm_Renderer_ITDynamic($this->tpl);
                $form->accept($renderer);
            }
            else {
                $this->tpl->touchBlock('no_form');
            }
        }
    }
}
?>
