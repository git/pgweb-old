<?php

//
// Ftp browser
//
class Page_FTP extends PgPage {
    private $fpath;
    private $parentpath;
    function __construct($fpath) {
        $this->fpath = $fpath;
        $this->fpath = preg_replace('/(index(\.html)?)$/', '', $this->fpath);
        if ($this->fpath == '' || $this->fpath[0] != '/') $this->fpath = '/' . $this->fpath;
        if ($this->fpath[strlen($this->fpath)-1] != '/') $this->fpath .= '/';
        $this->navsection = 'download';
        $this->content_template = 'ftp.html';
    }

    function Render() {
        global $_SETTINGS;
        $this->GenerateBreadcrumbs();

        // Read contents of directory
        $dircont = @scandir($_SETTINGS['ftp_root'] . $this->fpath);
        if (!$dircont)
            throw new NotFoundException('Directory not found');

        // Link to parent directory
        if ($this->fpath!= '') {
            $this->tpl->setVariable(array(
                'link' => '[Parent Directory]',
                'url' => '/ftp/' . $this->parentpath
            ));
            $this->tpl->parse('directory_loop');
        }

        // Directory contents
        foreach ($dircont as $item) {
            if ($item[0] == '.' || $item == 'sync_timestamp' || $item == 'web_sync_timestamp')
                continue;

            $fullname = $_SETTINGS['ftp_root'] . $this->fpath . '/' . $item;

            // Special files
            if ($item == '.message') {
                $this->tpl->setVariable('message', file_get_contents($fullname));
                continue;
            }
            if ($item == 'README') {
                $this->tpl->setVariable('readme', file_get_contents($fullname));
                continue;
            }
            if ($item == 'CURRENT_MAINTAINER') {
                $this->tpl->setVariable('maintainer', file_get_contents($fullname));
                continue;
            }

            if (is_dir($fullname)) {
                if (is_link($fullname)) {
                    $rl = readlink($fullname);
                    $this->tpl->setVariable('url', '/ftp' . $this->fpath . $rl . '/');
                }
                else {
                    $this->tpl->setVariable('url', '/ftp' . $this->fpath . $item . '/');
                }
                $this->tpl->setVariable('link', $item);
                $this->tpl->parse('directory_loop');
            }
            else {
                // Is a file
                $this->tpl->setVariable(array(
                    'link'  => $item,
                    'url'   => '/download/mirrors-ftp' . htmlentities($this->fpath . $item),
                    'mtime' => date('Y-m-d H:i:s', filemtime($fullname)),
                    'size'  => $this->size_pretty($fullname)
                ));
                $this->tpl->parse('file_loop');
            }
        }
    }

    function size_pretty($fn) {
        $s = filesize($fn);
        if ($s >= 1048576)
            return round($s / 1024 / 1024, 1) . ' MB';
        if ($s >= 1024)
            return round($s / 1024, 1) . ' KB';
        return $s . ' bytes';
    }

    function GenerateBreadcrumbs() {
        if ($this->fpath == '')
            $ftp_location = 'Top ';
        else
            $ftp_location = '<a href="/ftp/">Top</a> ';
        $pieces = explode('/', $this->fpath);
        $this->parentpath = '';
        if ($pieces[count($pieces)-1] == '') array_pop($pieces);
        for ($i = 0; $i < count($pieces)-1; $i++) {
            $step = $pieces[$i];
            if ($step == '') continue;
            $this->parentpath .= $step . '/';
            $ftp_location .= ' &rarr; <a href="/ftp/' . $this->parentpath.'">' . $step . '</a>';
        }
        if ($pieces[count($pieces)-1] != '')
            $ftp_location .= ' &rarr; ' . $pieces[count($pieces)-1];
        $this->tpl->setVariable('ftp_location', $ftp_location);
    }
}
?>
