<?php
// $Id: eventarchive.php,v 1.2 2007-05-11 16:32:24 dpage Exp $
// Events archive
class Page_EventArchive extends PgPage {
   function __construct() {
      $this->navsection = 'about';
      $this->content_template = 'about/eventarchive.html';
   }

   function Render() {
      $this->DoEvents('event');
      $this->DoEvents('training');
   }

   function DoEvents($what) {
      $train = ($what == 'training')?'TRUE':'NOT TRUE';
      $rs = $this->pg_query_params("SELECT e.id, posted, posted_by, start_date, end_date, COALESCE(training,false) AS training, COALESCE(et1.event, et2.event) AS event, COALESCE(et1.summary, et2.summary) AS summary, COALESCE(et1.details, et2.details) AS details, city, state, c.name AS country FROM events e INNER JOIN events_location el ON el.eventid=e.id INNER JOIN countries c ON c.id=el.country INNER JOIN events_text et2 ON et2.eventid=e.id LEFT JOIN (SELECT eventid,event,summary,details FROM events_text WHERE language=$1) et1 ON et1.eventid=e.id WHERE e.approved AND et2.language='en' AND e.end_date > CURRENT_DATE AND training IS $train ORDER BY end_date, start_date, posted", array($this->language));
      for ($i = 0; $i < pg_num_rows($rs); $i++) {
         $r = pg_fetch_assoc($rs, $i);
         if (isset($r['end_date']) && $r['end_date'] != $r['start_date']) {
            // both start and end date
            $this->tpl->setVariable(array(
               $what . '_date_start' => $r['start_date'],
               $what . '_date_end' => $r['end_date']
            ));
         }
         else {
            $this->tpl->setVariable($what . '_date', $r['start_date']);
         }

         $this->tpl->setVariable(array(
            $what . '_id' => $r['id'],
            $what . '_event' => $r['event'],
            $what . '_posted_by' => $r['posted_by'],
            $what . '_summary' => $r['summary'],
            $what . '_location' => $this->format_location($r)
         ));
         $this->tpl->parse($what . '_loop');
      }
   }
}
?>
