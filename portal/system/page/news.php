<?php
// $Id: news.php,v 1.1 2007-03-12 14:51:43 mha Exp $
// News details
class Page_News extends PgPage {
   private $id;
   function __construct($id) {
      $this->id = intval($id);
      $this->navsection = 'about';
      $this->content_template = 'about/news.html';
   }
   function Render() {
      $rs = $this->pg_query_params("SELECT posted, posted_by, COALESCE(nt1.headline, nt2.headline) AS headline, COALESCE(nt1.summary,nt2.summary) AS summary, COALESCE(nt1.story,nt2.story) AS story FROM news n INNER JOIN news_text nt2 ON n.id=nt2.newsid LEFT JOIN (SELECT newsid,headline,summary,story FROM news_text WHERE language=$1) nt1 ON n.id=nt1.newsid WHERE n.approved AND n.id=$2 AND nt2.language='en'", array($this->language, $this->id));
      if (pg_num_rows($rs) != 1) {
         throw new Exception('News item not found');
      }
      $r = pg_fetch_assoc($rs, 0);
      $r['story'] = nl2br(empty($r['story'])?$r['summary']:$r['story']);
      $this->tpl->setVariable($r);
      $this->tpl->setVariable('title_headline', $r['headline']);
   }
}
?>
