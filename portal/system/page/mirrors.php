<?php

//
// The page for mirror selection
//
class Page_Mirrors extends PgPage {
    function __construct($path) {
        $this->navsection = 'download';
        $this->content_template = 'download.html';
        $this->path = $path;
    }

    function Render() {
        if ($this->path) {
           $file = $this->path;
        }
        else {
           $file = isset($_GET['file'])?$_GET['file']:'';
        }
        if ($file[0] == '/') {
           $file = substr($file,1);
        }

        if (isset($_COOKIE['dlmirror']) && isset($_COOKIE['dlmirrortype'])) {
            $lastmirrid = intval($_COOKIE['dlmirror']);
            $lastmirrtype = $_COOKIE['dlmirrortype'];

	    $this->tpl->setVariable(array(
	    	'last_mirror' => $lastmirrid,
	    	'last_protocol' => $lastmirrtype,
	    	'last_file' => $file
	    ));
	    $master = $_SETTINGS['masterserver'];
	    $this->tpl->setVariable('extra_header_data',
	    	"<meta http-equiv=\"refresh\" content=\"5;$master/redir/$lastmirrid/$lastmirrtype/$file\" />");
        }
        else
            $lastmirrid = -1;

        $this->tpl->touchBlock('ftp_mirror_greeting');

        if ($file != '/' && $file != '') {
            $this->tpl->setVariable('filename', htmlentities($file));
            $this->tpl->touchBlock('download_file');
        }

        $rs = $this->pg_query("SELECT CASE WHEN mirror_index = 0 THEN ''::text ELSE mirror_index::text END || '.' || country_code || '.postgresql.org' AS hostname, host_port AS port, host_path AS path, country_name AS country, country_code, alternate_protocol, id, alternate_at_root FROM mirrors WHERE mirror_type='ftp' AND mirror_active AND NOT mirror_private AND mirror_dns AND mirror_last_rsync > (now() - '48 hrs'::interval) ORDER BY country_name, alternate_protocol DESC, mirror_index");
        if (pg_num_rows($rs) == 0)
            throw new Exception('No mirrors were found');

        $userCountry = $this->GetUserCountry();
        $stdCount = 0;
        $nearCount = 0;
	while ($mirror = pg_fetch_assoc($rs)) {
            $this->tpl->setVariable(array(
                'img'     => str_replace(' ', '', $mirror['country']) . '.gif',
                'country' => ucwords($mirror['country']),
                'mirid'  => $mirror['id'],
                'file' => htmlentities($file)
            ));
            if ($mirror['id'] == $lastmirrid) {
                $this->tpl->setVariable(array(
                   'last_img' => str_replace(' ', '', $mirror['country']) . '.gif',
                   'last_country' => ucwords($mirror['country'])
                ));
            }
            if ($userCountry && $userCountry == $mirror['country_code']) {
                // near mirror
	        if ($mirror['alternate_protocol'] == 't') {
	            $this->tpl->touchBlock('mirror_http_link');
	        }
                $this->tpl->parse('near_mirror_cell_loop');
                if (0 == (++$nearCount % MIRROR_CELLS)) {
                    $this->tpl->parse('near_mirror_row_loop');
                }
            } else {
                // other mirror
	        if ($mirror['alternate_protocol'] == 't') {
	            $this->tpl->touchBlock('mirror_http_link_std');
	        }
                $this->tpl->parse('std_mirror_cell_loop');
                if (0 == (++$stdCount % MIRROR_CELLS)) {
                    $this->tpl->parse('std_mirror_row_loop');
                }
            }
        }

        // Add some empty cells if needed
        if (0 != ($stdCount % MIRROR_CELLS)) {
            while (0 != ($stdCount++ % MIRROR_CELLS)) {
                $this->tpl->touchBlock('std_mirror_cell_empty');
                $this->tpl->parse('std_mirror_cell_empty');
            } // while
            $this->tpl->parse('std_mirror_row_loop');
        }
        if (0 != ($nearCount % MIRROR_CELLS)) {
            while (0 != ($nearCount++ % MIRROR_CELLS)) {
                $this->tpl->touchBlock('near_mirror_cell_empty');
                $this->tpl->parse('near_mirror_cell_empty');
            } // while
            $this->tpl->parse('near_mirror_row_loop');
        }
    }

    function GetUserCountry() {
        global $_SERVER;

        $ip = preg_split('/[.]+/', $_SERVER['REMOTE_ADDR']);
        $ipnumber = (double) ($ip[0] * 16777216) + ($ip[1] * 65536) + ($ip[2] * 256) + ($ip[3]);
        $rs = $this->pg_query_params("SELECT countrycode FROM iptocountry WHERE $1 BETWEEN startip AND endip LIMIT 1", array($ipnumber));
        if (pg_num_rows($rs) == 1) {
            $countrycode = trim(strtolower(pg_fetch_result($rs, 0, 0)));
            if ($countrycode == 'gb') $countrycode = 'uk';
            return $countrycode;
        }
        return false;
    }
}
?>
