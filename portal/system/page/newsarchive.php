<?php
// $Id: newsarchive.php,v 1.1 2007-03-12 14:51:43 mha Exp $
// News archive
class Page_NewsArchive extends PgPage {
   function __construct() {
      $this->navsection = 'about';
      $this->content_template = 'about/newsarchive.html';
   }
   
   function Render() {
       $rs = $this->pg_query_params("SELECT n.id, COALESCE(nt1.headline, nt2.headline) AS headline, posted, posted_by, COALESCE(nt1.summary, nt2.summary) AS summary FROM news n INNER JOIN news_text nt2 ON n.id=nt2.newsid LEFT JOIN (SELECT newsid,headline,summary FROM news_text WHERE language=$1) nt1 ON n.id=nt1.newsid WHERE n.approved AND nt2.language='en' ORDER BY n.posted DESC", array($this->language));
      for ($i = 0; $i < pg_num_rows($rs); $i++) {
         $this->tpl->setVariable(pg_fetch_assoc($rs, $i));
         $this->tpl->parse('news_loop');
      }
   }
}
?>
