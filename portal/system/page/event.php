<?php
// $Id: event.php,v 1.2 2007-05-11 16:32:24 dpage Exp $
// Event details
class Page_Event extends PgPage {
   private $id;
   function __construct($id) {
      $this->id = intval($id);
      $this->navsection = 'about';
      $this->content_template = 'about/event.html';
   }
   function Render() {
      $rs = $this->pg_query_params("SELECT posted, posted_by, start_date, end_date, COALESCE(training,false) AS training, COALESCE(et1.event, et2.event) AS event, COALESCE(et1.summary,et2.summary) AS summary, COALESCE(et1.details, et2.details) AS details, city, state, c.name AS country FROM events e INNER JOIN events_location el ON e.id=el.eventid INNER JOIN countries c ON el.country=c.id INNER JOIN events_text et2 ON e.id=et2.eventid LEFT JOIN (SELECT eventid,event,summary,details FROM events_text WHERE language=$1) et1 ON et1.eventid=e.id WHERE e.approved AND e.id=$2 AND et2.language='en'", array($this->language, $this->id));
      if (pg_num_rows($rs) != 1) {
         throw new NotFoundException('Event item not found');
      }
      $r = pg_fetch_assoc($rs, 0);
      if (isset($r['end_date']) && $r['end_date'] != $r['start_date']) {
         // both start and end date
         $this->tpl->setVariable(array(
            'date_start' => $r['start_date'],
            'date_end' => $r['end_date']
         ));
      }
      else {
         $this->tpl->setVariable('date', $r['start_date']);
      }

      $r['details'] = nl2br(empty($r['details'])?$r['summary']:$r['details']);
      $this->tpl->setVariable($r);
      $this->tpl->setVariable('location', $this->format_location($r));
      $this->tpl->setVariable('title_location', $this->format_location($r));
      $this->tpl->setVariable('title_event', $r['event']);
      if ($r['training'] == 't') {
         $this->tpl->touchBlock('event_is_training');
      }
   }
}
?>
