<?php

//
// Static page. First try a page in current language, fall back to English
//
class Page_Static extends PgPage {
   private $page;
   function __construct($page) {
      $this->page = $page;
   }
   function PreRender() {
      $page = explode('/', $this->page);
      $items = count($page);
      if (1 < $items && 'index.' == substr($page[$items - 1],0,6)) {
          unset($page[$items - 1]);
      }
      if ($page[count($page)-1] == 'index') {
         unset($page[count($page)-1]);
      }
      $path = implode('/', $page);

      $this->navsection = $page[0];
      if (file_exists('../template/' . $this->language . '/' . $path . '.html')) {
         $this->content_template = $this->language . '/' . $path . '.html';
      }
      elseif (file_exists('../template/en/' . $path . '.html')) {
         $this->content_template = 'en/' . $path . '.html';
      }
      elseif (file_exists('../template/' . $path . '/index.html')) {
          $this->content_template = $path . '/index.html';
      }
      else {
         throw new NotFoundException('Static page not found: ' . htmlentities($this->page));
      }
   }
   function Render() {
      $this->tpl->touchBlock('page_content');
      $this->tpl->touchBlock('content_block');
   }
}
?>
