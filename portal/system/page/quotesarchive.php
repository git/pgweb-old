<?php

//
// Archived quotes
//
class Page_QuotesArchive extends PgPage {
    function __construct() {
        $this->navsection = 'about';
        $this->content_template = 'about/quotesarchive.html';
    }

    function Render() {
        $rs = $this->pg_query_params("SELECT q.id, COALESCE(qt1.quote,qt2.quote) AS quote, COALESCE(qt1.tagline,qt2.tagline) AS tagline FROM quotes q INNER JOIN quotes_text qt2 ON q.id=qt2.quoteid LEFT JOIN (SELECT quoteid,quote,tagline FROM quotes_text WHERE language=$1) qt1 ON qt1.quoteid=q.id WHERE q.approved AND qt2.language='en' ORDER BY q.id DESC", array($this->language));
        for ($i = 0; $i < pg_num_rows($rs); $i++) {
            $item = pg_fetch_assoc($rs, $i);

            $this->tpl->setVariable($item);
            $this->tpl->parse('quotes_loop');
        }
    }
}
?>
