<?php
// $Id: community.php,v 1.2 2007-10-21 10:51:55 mastermind Exp $
// community index page
class Page_Community extends PgPage {
   function __construct() {
      $this->navsection = 'community';
      $this->content_template = 'en/community.html';
   }
   
   function Render() {
       // Current survey
      $rs = $this->pg_query_params("SELECT q.*, s.id FROM surveys s, survey_questions q WHERE s.current AND s.id=q.surveyid AND q.language=$1",array($this->language));
      if (0 == pg_num_rows($rs))
          $rs = $this->pg_query("SELECT q.*, s.id FROM surveys s, survey_questions q WHERE s.current AND s.id=q.surveyid AND q.language='en'");
      $this->tpl->setVariable(array(
         'survey_id' => pg_fetch_result($rs, 0, 'id'),
         'survey_question' => htmlentities(pg_fetch_result($rs, 0, 'question'),ENT_COMPAT,'utf-8')
      ));
      for ($i = 1; $i <= 8; $i++) {
         if (!pg_field_is_null($rs, 0, 'opt' . $i) && pg_fetch_result($rs,0,'opt'.$i) != '') {
            $this->tpl->setVariable(array(
               'option_num' => $i,
               'option_text' => htmlentities(pg_fetch_result($rs, 0, 'opt' . $i),ENT_COMPAT,'utf-8')
            ));
            $this->tpl->parse('survey_option_loop');
         }
      }
      
      // RSS feed from planet
      $rssparser = new RSSParser('../../dl/planetpostgresql.rss');
      if ($rssparser->success()) {
         $this->tpl->touchBlock('planet_block');
         $n=0;
         foreach ($rssparser->getItems() as $i) {
            $this->tpl->setVariable($i);
            $this->tpl->parse('planet_loop');
            if ($n++ > 5) break;
         }
      }
   }
}
