<?php
// $Id: developers.php,v 1.1 2007-09-13 12:00:47 mha Exp $
// Developers
class Page_Developers extends PgPage {
   function __construct() {
      $this->navsection = 'community';
      $this->content_template = 'developer/bios.html';
   }

   function Render() {
       $rs = $this->pg_query('SELECT type,typename,extrainfo,detailed FROM developers_types');
       while ($row = pg_fetch_row($rs)) {
           $sectname[$row[0]] = $row[1];
           $sectinfo[$row[0]] = $row[2];
           if ($row[3] == 0) {
               $nodetails[$row[0]] = 1;
           }
       }

       $rs = $this->pg_query("SELECT type,firstname||' '||lastname AS name,email,company,companyurl,location,contribution FROM developers NATURAL JOIN developers_types ORDER BY sortorder,lastname,firstname");
       $lasttype=-1;
       for ($rownum=0; $rownum < pg_num_rows($rs); $rownum++) {
           $row = pg_fetch_array($rs, $rownum, PGSQL_NUM);
           if ($lasttype != $row[0]) {
               // New type
               if ($lasttype != -1) {
                   $this->tpl->parse('outer_loop');
               }
               $this->tpl->setVariable('section_name', $sectname[$row[0]]);
               $this->tpl->setVariable('section_description', $sectinfo[$row[0]]);
               if (!isset($nodetails[$row[0]])) {
                   $this->tpl->touchBlock('header_major');
                   $odd = 0;
               }
               else {
                   $odd = 1;
               }
               $lasttype = $row[0];
           }
           if ($rownum+1 == pg_num_rows($rs) ||
               $row[0] != pg_fetch_result($rs, $rownum+1, 0)) {
               // Next row is in a new section
               $this->tpl->setVariable('rowmod', 'class="lastrow"');
           }
           if (!isset($nodetails[$row[0]])) {
               // Normal view
               $comp = '';
               if ($row[3] != '') {
                   if ($row[4] != '') {
                       $comp = '<a href="' . $row[4] . '">' . htmlentities($row[3],ENT_COMPAT,'utf-8') . '</a><br/>';
                   }
                   else {
                       $comp = htmlentities($row[3],ENT_COMPAT,'utf-8') . '<br/>';
                   }
               }
               $this->tpl->setVariable(array(
                   'name' => htmlentities($row[1],ENT_COMPAT,'utf-8'),
                   'email' => $this->cleanemail($row[2],ENT_COMPAT,'utf-8'),
                   'company' => $comp,
                   'location' => htmlentities($row[5],ENT_COMPAT,'utf-8'),
                   'contribution' => htmlentities($row[6],ENT_COMPAT,'utf-8')
               ));
               $this->tpl->parse('inner_loop');
           }
           else {
               // No details view
               // Somewhat ugly, since we need to do two-column here
               $this->tpl->setVariable(array(
                   'name' . $odd => htmlentities($row[1],ENT_COMPAT,'utf-8'),
                   'email' . $odd => $this->cleanemail($row[2])
               ));
               if ($odd == 1) {
                   // Just did first column, just move to next
                   $odd = 2;
               }
               else {
                   // Did second column, spit out before moving on
                   $this->tpl->parse('inner_loop_small');
                   $odd = 1;
               }
           }
       }
       // Dual column table
       if ($odd == 2) {
           // Did a single column in a dual-column table
           $this->tpl->setVariable(array('name2'=>'','email2'=>''));
           $this->tpl->parse('inner_loop_small');
       }
   }

   function cleanemail($email) {
       return '(' . htmlentities(str_replace('@',' at ', $email),ENT_COMPAT,'utf-8') . ')';
   }
}
?>
