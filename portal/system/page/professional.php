<?php
//
// Professional services list
//
// $Id: professional.php,v 1.6 2007-03-12 14:51:43 mha Exp $
//
class Page_Professional extends PgPage {
    private $path;
    private $region;
    private $regionname;
    private $type;
    private $regions = array('africa'=>'Africa','asia'=>'Asia','europe'=>'Europe','northamerica'=>'North America','oceania'=>'Oceania','southamerica'=>'South America');
    function __construct($path) {
        $this->path = $path;
        $this->navsection = 'support';
    }
    function PreRender() {

        $pp = explode('_',$this->path);

        $this->type = $pp[1];
        if (count($pp) == 3) {
            $this->region = $pp[2];
            if (!isset($this->regions[$this->region])) {
                throw new Exception('Region not found');
            }
            $this->regionname = $this->regions[$this->region];
        }
        if ($this->type != 'hosting' && $this->type != 'support') {
            throw new NotFoundException('Invalid type of service');
        }
        $this->content_template = empty($this->regionname)?'support/profregion.html':'support/professional.html';
    }
    function Render() {
        if (empty($this->regionname)) {
            $this->tpl->touchBlock('do_' . $this->type);
            $this->tpl->setVariable('page_title',($this->type=='hosting')?'Hosting Solutions':'Professional Services');
            foreach ($this->regions as $region=>$regionname) {
                $rs = $this->pg_query('SELECT id FROM profserv WHERE provides_' . $this->type . ' AND region_' . $region . ' AND approved LIMIT 1');
                if (pg_num_rows($rs) == 1) {
                    $this->tpl->setVariable('what',$this->type);
                    $this->tpl->setVariable('region',$region);
                    $this->tpl->setVariable('regionname',$regionname);
                    $this->tpl->parse('region_loop');
                }
            }
        }
        else {
            $this->tpl->setVariable(array('region'=>$this->region,'regionname'=>$this->regionname));
            if ($this->type=='hosting') {
                $this->tpl->touchBlock('do_hosting');
                $this->tpl->setVariable('page_title','Hosting Solutions');
                $this->tpl->setVariable('what','hosting providers');
                $rs = $this->pg_query('SELECT id,email,lastconfirmed,name,url,description,NULLIF(region_africa,false) AS region_africa,NULLIF(region_asia,false) AS region_asia,NULLIF(region_europe,false) AS region_europe,NULLIF(region_northamerica,false) AS region_northamerica,NULLIF(region_oceania,false) AS region_oceania,NULLIF(region_southamerica,false) AS region_southamerica,contact,interfaces,provides_support,provides_hosting FROM profserv WHERE region_' . $this->region . ' AND provides_hosting AND approved ORDER BY (name)');
            } else {
                $this->tpl->touchBlock('do_support');
                $this->tpl->setVariable('page_title','Professional Services');
                $this->tpl->setVariable('what','support providers');
                $rs = $this->pg_query('SELECT id,email,lastconfirmed,name,url,description,employees,locations,NULLIF(region_africa,false) AS region_africa,NULLIF(region_asia,false) AS region_asia,NULLIF(region_europe,false) AS region_europe,NULLIF(region_northamerica,false) AS region_northamerica,NULLIF(region_oceania,false) AS region_oceania,NULLIF(region_southamerica,false) AS region_southamerica,hours,languages,customerexample,experience,contact,provides_support,provides_hosting FROM profserv WHERE region_' . $this->region . ' AND provides_support AND approved ORDER BY lower(name)');
            }
            if ($rs && pg_num_rows($rs) > 0) {
                for ($i = 0, $rows = pg_num_rows($rs); $i < $rows; $i++) {
                    $vals = pg_fetch_array($rs, $i, PGSQL_ASSOC);
                    if ($vals['provides_support']=='t' && $vals['provides_hosting']=='t') {
                        $this->tpl->setVariable(array('provides_multi'=>'Provides both support and hosting'));
                    }
                    $this->tpl->setVariable($vals);
                    $this->tpl->parse('entries_loop');
                }
            }
        }
    }
}
