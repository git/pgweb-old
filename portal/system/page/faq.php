<?php
// $Id: faq.php,v 1.7 2007-07-18 07:37:36 mha Exp $
//
// FAQ pages
//   or actually, any pages from external cvs, but renaming
//   a file in cvs is too much of a pain ;-)
//
class Page_FAQ extends PgPage {
   private $faq;
   function __construct($faq, $navsect) {
      $this->faq = $faq;
      $this->navsection = $navsect;
   }

   function Render() {
      $base = '../../ext/';
      $dirs = array('faqs' => 'html',
                    'faqs_text' => 'text',
                    'pginstaller' => 'html',
                    'backend' => 'html');
      $imgbase = array('backend' => '/files/developer/backend/');
      $href = array('backend' => 'http://developer.postgresql.org/cvsweb.cgi/pgsql/src/tools/backend/');
      $hreflocaltranslate = array('backend' => 1);


      $found = false;
      foreach ($dirs as $subdir=>$type) {
         $ext = ($type=='html')?'.html':'';
         $fn = $base . $subdir . '/' . $this->faq . $ext;
         $f = @fopen($fn,'r');
         if (!$f && $subdir==$this->faq) { // Not found, try subdir match
            $fn = $base . $subdir . '/index.html';
            $f = @fopen($fn,'r');
         }
         if ($f) {
            $content=fread($f, @filesize($fn));
            fclose($f);

            if ($type=='html') {
              // Convert character set
              if (preg_match('!<meta.*content-type.*content=.*charset="*([^"]*)"!is', $content, $matches)) {
                      $charset=$matches[1];
              } else {
                      $charset='';
              }

         	  if (!preg_match('!<body[^>]*>(.*)</body>!is', $content, $matches)) {
         		  $content = 'Malformatted FAQ';
         	  }
         	  else {
         		  $content = $matches[1];
         	  }

              if ($charset != '') {
            		if ($charset == 'x-euc-jp') {
            			$charset = 'euc-jp';
            		}
            		$content = iconv($charset,'utf-8',$content);
            		if ($content == '') {
            			$content = 'Character set conversion failed.';
            		}
              }

         	  // Let's see if we can find a title
         	  if (preg_match('!<h1>([^<]+)</h1>!is', $content, $matches)) {
                 $this->title = $matches[1];
         	  }

         	  // Lowercase all tags
         	  $content = preg_replace("/(<\/?)(\w+)([^>]*>)/e","'\\1'.strtolower('\\2').'\\3'", $content);
         	  $content = str_replace('\"','"',$content);

         	  // Make some common tags XHTML compatiable
         	  $content = str_replace(array('<br>','<hr>','<h2 align="center">','HREF="','NAME="'),array('<br/>','<hr/>','<h2>','href="','name="'),$content);
         	  $content = preg_replace('/(<img[^\>]+)>/','$1 />', $content);

         	  // Possibly change references to images
         	  if (!empty($imgbase[$subdir])) {
         		  $content = preg_replace('/(<img[^\<]+src=")([^"]+)"/','$1' . $imgbase[$subdir] . '$2"',$content);
         	  }

         	  // Possibly change the links to a new base
         	  if (!empty($href[$subdir])) {
         		  if ($hreflocaltranslate) {
         			  // Translate local HREFs to ourselves
                      $content = preg_replace('/(<a[^\<]+href=")([^"\/:]+)"/','$1ext.$2"',$content);

         			  // Now translate all others
         			  $content = preg_replace('/(<a[^\<]+href=")([^"\/]+\/[^"]+)"/','$1' . $href[$subdir] . '$2"',$content);
         		  } else {
         			  $content = preg_replace('/(<a[^\<]+href=")([^"]+)"/','$1' . $href[$subdir] . '$2"',$content);
         		  }
         	  }
            } // HTML
            else { // plaintext for all else
            	$content = '<pre>' . htmlentities($content) . '</pre>';
            }
            $this->tpl->setVariable('nav_page_content',$content);
            $found=true;
            break;
         } // if ($f)
      } //foreach
      if (!$found) {
         throw new NotFoundException('FAQ not found');
      }
   }
}
?>
