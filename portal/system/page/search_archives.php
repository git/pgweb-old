<?php
// $Id: search_archives.php,v 1.2 2007-03-12 20:28:26 mha Exp $
class ArchivesSearcher extends SearchBase {
    private $archage = 365;
    private $sort = 'r';
    private $list = NULL;
    function PrepareForm(){
        if (isset($_GET['s']) && $_GET['s'] == 'd')
            $this->sort = 'd';
        if (isset($_GET['d']) && (intval($_GET['d']) > 0 || intval($_GET['d']) == -1))
            $this->archage = intval($_GET['d']);
        if (isset($_GET['l']) && intval($_GET['l']) >= 0)
            $this->list = intval($_GET['l']);


        $this->tpl->touchBlock('archives_form');
        // Generate list of dates
        foreach (array(-1=>'anytime', 1=>'within last day', 7=>'within last week', 31=>'within last month', 186=>'within last 6 months', 365=>'within last year') as $val=>$txt) {
            $this->tpl->setVariable('archives_dates_val', $val);
            $this->tpl->setVariable('archives_dates_txt', $txt);
            $this->tpl->setVariable('archives_dates_sel', ($val == $this->archage)?' SELECTED':'');
            $this->tpl->parse('archives_dates');
        }
        // Generate list of lists
        $rsl = $this->page->pg_query("SELECT lists.id AS listid,lists.name AS listname,lists.grp AS listgroup,1 as tmp,listgroups.sortkey FROM lists INNER JOIN listgroups ON lists.grp=listgroups.id UNION ALL  SELECT -id,'--' || listgroups.name, listgroups.id, 0, sortkey FROM listgroups UNION ALL SELECT NULL,'-- All lists', -99999, 0, -1 ORDER BY 5,3,4,2;",
                'search');
        for ($i = 0; $i < pg_num_rows($rsl); $i++) {
            $r = pg_fetch_array($rsl, $i);
            $this->tpl->setVariable($r);
            if ($r['listid'] == $this->list) {
                $this->tpl->setVariable('listselect', ' SELECTED');
            }
            $this->tpl->parse('archives_lists');
        }
        if ($this->list == NULL) 
            $this->list = 'NULL';

        $sortstr = '<option value="r"';
        if ($this->sort == 'r') $sortstr .= ' SELECTED';
        $sortstr .= '>Rank</option><option value="d"';
        if ($this->sort == 'd') $sortstr .= ' SELECTED';
        $sortstr .= '>Date</option>';
        $this->tpl->setVariable('archives_sort_str', $sortstr);
    }

    function ExecuteQuery() {
        $datestr = ($this->archage==-1)?"NULL":("CURRENT_TIMESTAMP-'" . $this->archage . " days'::interval");
        $this->rs = $this->page->pg_query_params("SELECT * FROM archives_search($1,$this->list,$datestr,NULL,$2,$3,$4)",
           array($this->page->query,$this->page->firsthit-1,$this->page->hitsperpage,$this->sort),
           'search');
        $this->totalhits = pg_fetch_result($this->rs, pg_num_rows($this->rs)-1, 1);
        $this->search_base_count = pg_fetch_result($this->rs, pg_num_rows($this->rs)-1, 2);
        return pg_num_rows($this->rs);
    }

    function BuildQueryString() {
        return '&m=1&l=' . $this->list . '&d=' . $this->archage . '&s=' . $this->sort;
    }

    function RenderRow($i,$r) {
        $this->tpl->setVariable('arch_num', $this->page->firsthit+$i);
        $this->tpl->setVariable('arch_list', $r['listname']);
        $this->tpl->setVariable('arch_year', $r['year']);
        $this->tpl->setVariable('arch_month', sprintf("%02d",$r['month']));
        $this->tpl->setVariable('arch_msgnum', sprintf("%05d",$r['msgnum']));
        $this->tpl->setVariable('arch_date', $r['date']);
        $this->tpl->setVariable('arch_subject', htmlentities($r['subject'],ENT_COMPAT,'utf-8'));
        $this->tpl->setVariable('arch_author', htmlentities($r['author'],ENT_COMPAT,'utf-8'));
        $this->tpl->setVariable('arch_abstract', str_replace(array('[[[[[[',']]]]]]'),array('<b>','</b>'),htmlentities($r['headline'],ENT_COMPAT,'utf-8')));
        $this->tpl->setVariable('arch_rank', $r['rank']);
        $this->tpl->parse('archives_search_loop');
    }
}
?>
