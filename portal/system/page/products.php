<?php
// $Id: $
// Product categories
class Page_Products extends PgPage {
   function __construct() {
      $this->navsection = 'download';
      $this->content_template = 'download/products.html';
   }

   function Render() {

       if (empty($_GET['id'])) {
           throw new Exception(gettext('Category ID not specified'));
       }

       $rs = $this->pg_query_params('SELECT name AS category, blurb FROM product_categories WHERE id=$1', array(intval($_GET['id'])));
       if (pg_num_rows($rs) == 0) {
           header('HTTP/1.0 404 Not found');
           throw new Exception (gettext('The specified category does not exist'));
       } 

       $category = pg_fetch_result($rs, 0, 0); 
       $blurb = pg_fetch_result($rs, 0, 1); 
       $this->tpl->setVariable('category_title', $category);
       $this->tpl->setVariable('category_header', $category);
       $this->tpl->setVariable('category_blurb', $blurb);

       // Products
       $rs = $this->pg_query_params(
           'SELECT prod.name AS product_name, prod.url AS product_url, prod.description AS product_description, prod.price AS product_price, prod.licence, pub.name AS publisher_name, pub.url AS publisher_name FROM products prod, organisations pub WHERE prod.approved = true AND prod.publisher = pub.id AND prod.category=$1 ORDER BY prod.name, pub.name',
           array(intval($_GET['id'])));

       $this->tpl->setVariable('product_count', pg_num_rows($rs));

       while ($row = pg_fetch_row($rs)) {
           $this->tpl->setVariable('product_name', $row[0]);
           $this->tpl->setVariable('product_url', $row[1]);
           $this->tpl->setVariable('product_description', nl2br($row[2]));

           if (trim($row[3]) == '') {
               $this->tpl->hideBlock('price_block');
           } else {
               $this->tpl->setVariable('product_price', $row[3]);
           }

           switch ($row[4]) {
               case 'c':
                   $licence = 'Commercial';
                   break;
               case 'f':
                   $licence = 'Freeware';
                   break;
               case 'm':
                   $licence = 'Multiple';
                   break;
               case 'o':
                   $licence = 'Open Source';
                   break;
           }

           $this->tpl->setVariable('product_licence', $licence);
           $this->tpl->setVariable('publisher_name', $row[5]);
           $this->tpl->setVariable('publisher_url', $row[6]);

           $this->tpl->parse('product-loop');
       }
   }
}
?>
