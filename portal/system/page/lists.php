<?php
// $Id$
// Lists
class Page_Lists extends PgPage {
   function __construct() {
      $this->navsection = 'community';
      $this->content_template = 'community/lists.html';
   }

   function Render() {
       $rs = $this->pg_query('SELECT id,name FROM listgroups ORDER BY sortkey,name');
       while ($row = pg_fetch_row($rs)) {
           $this->tpl->setVariable('group_name', $row[1]);

           $lrs = $this->pg_query_params('SELECT name,coalesce(shortdesc, name), description FROM lists WHERE grp=$1 ORDER BY name', 
               array($row[0]));
           while ($lrow = pg_fetch_row($lrs)) {
               $this->tpl->setVariable('list_name', $lrow[0]);
               $this->tpl->setVariable('list_shortdesc', $lrow[1]);
               $this->tpl->setVariable('list_description', $lrow[2]);
               $this->tpl->parse('list_loop');
           }

           $this->tpl->parse('group_loop');
       }
   }
}
?>
