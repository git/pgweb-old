<?php

//
// Single quote
//
class Page_Quotes extends PgPage {
    private $id;
    function __construct($id) {
        $this->id = $id;
        $this->navsection = 'about';
        $this->content_template = 'about/quotes.html';
    }

    function Render() {
        $rs = $this->pg_query_params("SELECT COALESCE(qt1.quote,qt2.quote) AS quote, COALESCE(qt1.tagline,qt2.tagline) AS tagline FROM quotes q INNER JOIN quotes_text qt2 ON q.id=qt2.quoteid LEFT JOIN (SELECT quoteid,quote,tagline FROM quotes_text WHERE language=$1) qt1 ON qt1.quoteid=q.id WHERE q.approved AND q.id=$2 AND qt2.language='en'", array($this->language, $this->id));
        if (pg_num_rows($rs) != 1)
            throw new NotFoundException('Quote not found');
        $r = pg_fetch_assoc($rs);
        $this->tpl->setVariable($r);
        $this->tpl->setVariable('title_headline', $r['tagline']);
    }
}
?>
