<?php
// $Id: communitydocs.php,v 1.5 2007-03-12 14:51:43 mha Exp $
// Community-contributed documentation
class Page_CommunityDocs extends PgPage {
   private $root;
   private $id;
   private $basepath;
   function __construct($navsection,$basepath,$root,$pageid) {
      $this->root = $root;
      $this->id = isset($pageid)?intval($pageid):$root;
      $this->content_template = 'community-docs.html';
      $this->navsection = $navsection;
      $this->basepath = $basepath;
   }
   function Render() {
      if (!empty($_GET['preview']) && $_GET['preview']==1) {
         // Show a preview, from the working table
         $res = $this->pg_query("SELECT p.parent,p.shorttitle,p.title,to_char(p.savedate,'YYYY-MM-DD HH24:MI') as savedate,p.syspage,'preview' as userid,'Preview User' as fullname,'Preview User Blurb' AS authorblurb,p.contents FROM communitypages_work p WHERE p.pageid=" . pg_escape_string($this->id));
      }
      if (!isset($res) || pg_num_rows($res) == 0) {
         // Standard page view if we didn't pick preview, or if preview was picked but not found
         $res = $this->pg_query("SELECT p.parent,p.shorttitle,p.title,to_char(p.savedate,'YYYY-MM-DD HH24:MI') AS savedate,p.syspage,u.userid,u.fullname,u.authorblurb,p.contents FROM communitypages p INNER JOIN users u ON p.origauthor=u.userid WHERE p.pageid=" . pg_escape_string($this->id));
      }
      if (pg_num_rows($res) != 1) {
         throw new NotFoundexception('Requested page does not exist');
      }
      else {
        $all = pg_fetch_assoc($res);
        $this->tpl->setVariable('page_title', $all['title']);
        $this->tpl->setVariable('page_content', $all['contents']);
        $this->tpl->setVariable('author_info', $all['authorblurb']);
        $this->tpl->setVariable('author_name', $all['fullname']);
        $this->tpl->setVariable('page_id', $this->id);
        $this->tpl->setVariable('savedate', $all['savedate']);

        if ($all['syspage'] != 1)
            $this->tpl->touchBlock('community_editlinks');
        else
            $this->tpl->hideBlock('community_editlinks');

        // Build the version history
        $res = $this->pg_query("SELECT	max(savedate::date),users.fullname FROM communitypages_history inner join users on (communitypages_history.author=users.userid) WHERE pageid={$this->id} GROUP BY	savedate::date,users.fullname ORDER BY max DESC");
        for ($i = 0; $i < pg_num_rows($res); $i++) {
            $this->tpl->setVariable(array(
                        'verhist_date'=>pg_fetch_result($res, $i, 0),
                        'verhist_name'=>pg_fetch_result($res, $i, 1)));
            $this->tpl->parse('communitydocs_versionhistory');
        }

        // Build the lefthand menu
        $parent = $all['parent'];
        $shorttitle = $all['shorttitle'];
        
        // Add ourselves to the menu, but only if we're not the root element
        if ($parent != $this->id) {
           $this->tpl->setVariable('menu_page_id_self', $this->id);
           $this->tpl->setVariable('menu_page_title_self', $shorttitle);
           $this->tpl->touchBlock('community_docs_menuself');
           $this->tpl->touchBlock('community_docs_menuself2');
        }

        // Build submenu
        $res = pg_query('SELECT p.pageid, p.shorttitle FROM communitypages p WHERE p.parent=' . pg_escape_string($this->id) . ' AND NOT p.pageid=p.parent ORDER BY p.shorttitle');
        for ($i = 0; $i < pg_num_rows($res); $i++)
        {
           $this->tpl->setVariable('menu_page_id', pg_fetch_result($res, $i, 0));
           $this->tpl->setVariable('menu_page_title', pg_fetch_result($res, $i, 1));
           $this->tpl->parse('community_docs_menu');
        }

        // Build breadcrumbs
        $res = $this->pg_query('SELECT * FROM communitypages_breadcrumbs(' . $this->id . ')');
        $crumbs = '';
        if (pg_num_rows($res) > 1) { // Don't show breadcrumbs for root page
            for ($i = pg_num_rows($res)-1; $i >= 0; $i--)
            {
                if ($crumbs != '')
                    $crumbs .= '&nbsp;&gt;&nbsp;';
                $crumbs .= '<a href="/' . $this->basepath . '.' . pg_fetch_result($res,$i,0) . '">' . pg_fetch_result($res,$i,1) . '</a>';
            }
        }
        $this->tpl->setVariable('breadcrumbs', $crumbs);
      }
   }
}
?>
