<?php
// $Id: featuredetail.php,v 1.1 2007-11-02 12:41:35 mha Exp $
// Feature details
class Page_FeatureDetail extends PgPage {
   private $featureid;

   function __construct($suffix) {
      $this->navsection = 'about';
      $this->content_template = 'about/featuredetails.html';
      $this->featureid = intval($suffix);
   }

   function Render() {
       $rs = $this->pg_query_params('SELECT featurename,featuredescription FROM features_features WHERE featureid=$1',
           array($this->featureid));
       $this->tpl->setVariable(pg_fetch_assoc($rs));
       $this->title = 'Feature description: ' . pg_fetch_result($rs, 0, 0);
   }
}
?>
