<?php
class ErrorPage extends PgPage {
    private $errcode;
    private $errmsg;
    private $errdetail;

    function __construct($errcode, $errmsg, $errdetail, $url) {
        $this->errcode = $errcode;
        $this->errmsg = $errmsg;
        $this->errdetail = $errdetail;

        $this->content_template = 'error.html';
        if (isset($url) && strpos($url,'/')) {
            $pieces = explode('/', $url);
            $this->navsection = $pieces[0];
        }
    }

    function ShowError() {
        header('HTTP/1.0 ' . $this->errcode . ' ' . $this->errmsg);
        $this->DoRender();
        $this->Show();
        exit(0);
    }

    function Render() {
        $this->tpl->setVariable('errmsg', $this->errmsg);
        $this->tpl->setVariable('errdetail', $this->errdetail);
    }
}
?>
