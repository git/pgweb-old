<?php
// $Id: dispatcher.php,v 1.9 2007-11-02 12:41:35 mha Exp $
// Dispatcher for normal pages
class Dispatcher {
    static function DoDispatch($page) {
        $pargs = explode('.', $page);
        switch ($pargs[0]) {
        case '':
        case 'index': return new Page_Index();
        case 'about/casestudies/submitcontact': return new Form_SubmitContact();
        case 'about/event': return new Page_Event($pargs[1]);
        case 'about/eventarchive': return new Page_EventArchive();
        case 'about/featuredetail/feature': return new Page_FeatureDetail($pargs[1]);
        case 'about/featurematrix': return new Page_FeatureMatrix(1,$pargs[1]);
        case 'about/news': return new Page_News($pargs[1]);
        case 'about/newsarchive': return new Page_NewsArchive();
        case 'about/quotes': return new Page_Quotes($pargs[1]);
        case 'about/quotesarchive': return new Page_QuotesArchive();
        case 'about/submitevent': return new Form_SubmitEvent();
        case 'about/submitnews': return new Form_SubmitNews();
        case 'admin/applications': return new Admin_Applications();
        case 'admin/application-edit': return new Admin_ApplicationEdit();
        case 'admin/comdoc-edit': return new Admin_ComDocEdit();
        case 'admin/comdocs': return new Admin_ComDocs();
        case 'admin/comments': return new Admin_Comments();
        case 'admin/comment-edit': return new Admin_CommentEdit();
        case 'admin/developers': return new Admin_Developers();
        case 'admin/developer-edit': return new Admin_DeveloperEdit();
        case 'admin/events': return new Admin_Events();
        case 'admin/event-edit': return new Admin_EventEdit();
        case 'admin/event-translate': return new Admin_EventTranslate();
        case 'admin/frontends': return new Admin_Frontends();
        case 'admin/lists': return new Admin_Lists();
        case 'admin/list-edit': return new Admin_ListEdit();
        case 'admin/mirror-edit': return new Admin_MirrorEdit();
        case 'admin/mirrors': return new Admin_Mirrors();
        case 'admin/news': return new Admin_News();
        case 'admin/news-edit': return new Admin_NewsEdit();
        case 'admin/news-translate': return new Admin_NewsTranslate();
        case 'admin/orgs': return new Admin_Orgs();
        case 'admin/org-edit': return new Admin_OrgEdit();
        case 'admin/products': return new Admin_Products();
        case 'admin/product-edit': return new Admin_ProductEdit();
        case 'admin/prof': return new Admin_Prof();
        case 'admin/prof-edit': return new Admin_ProfEdit();
        case 'admin/quotes': return new Admin_Quotes();
        case 'admin/quotes-edit': return new Admin_QuotesEdit();
        case 'admin/quotes-translate': return new Admin_QuotesTranslate();
        case 'admin/survey-edit': return new Admin_SurveyEdit();
        case 'admin/survey-translate': return new Admin_SurveyTranslate();
        case 'admin/surveys': return new Admin_Surveys();
        case 'commentdoc': return new Form_CommentDoc();
        case 'community': return new Page_Community();
        case 'community/contributors': return new Page_Developers();
        case 'community/docadd': return new Form_CommunityDocAdd(isset($pargs[1])?$pargs[1]:-1);
        case 'community/docedit': return new Form_CommunityDocEdit(isset($pargs[1])?$pargs[1]:-1);
        case 'community/lists': return new Page_Lists();
        case 'community/lists/subscribe': return new Form_Subscribe();
        case 'community/lostpwd': return new Form_CommunityLostPwd();
        case 'community/profile': return new Form_CommunityProfile();
        case 'community/resetpwd': return new Form_CommunityResetPwd();
        case 'community/signup': return new Form_CommunitySignup();
        case 'community/survey': return new Page_Survey($pargs[1]);
        case 'docs/faqs': return new Page_FAQ($pargs[1],'docs');
        case 'docs/techdocs': return new Page_CommunityDocs('docs',$pargs[0],0,count($pargs)>1?$pargs[1]:null);
        case 'download/mirrors-ftp': return new Page_Mirrors('');
        case (substr($pargs[0],0,21) == 'download/mirrors-ftp/'):
            return new Page_Mirrors(substr($page, 21));
        case 'download/product-categories': return new Page_ProductCategories();
        case 'download/products': return new Page_Products();
        case 'download/submitproduct': return new Form_SubmitProduct();
        case 'developer/ext': return new Page_FAQ($pargs[1],'developer');
        case 'login': return new Form_Login();
        case 'search': return new Page_Search();
        case 'support/newprof': return new Form_NewProfService();
        case 'support/submitbug': return new Form_SubmitBug();
        case 'vote': return new Form_Vote();
            // Special multi-match rules below
        case 'ftp': // fallthrough!
        case (substr($pargs[0],0,4) == 'ftp/'): 
            return new Page_FTP(substr($page,4));
        case (substr($pargs[0],0,21) == 'support/professional_'): 
            return new Page_Professional($pargs[0]);
        case (substr($pargs[0],0,5) == 'docs/'):
            if (preg_match('!^docs/(current|[0-9\.]*)/([a-z]*)(/[^/]*)?$!', $page, $docinfo)) 
                return new Page_Docs($docinfo);
            break;
        }
        // If there is no special handler, load a static page
        return new Page_Static($page);
    }

    static function Dispatch($page) {
        if (substr($page, -5, 5) == '.html') {
            $page = substr($page, 0, -5);
        }
        if (substr($page, -6, 6) == '/index') {
            $page = substr($page, 0, -5);
        }
        if (substr($page, -1, 1) == '/') {
            $page = substr($page, 0, -1);
        }

        $p = self::DoDispatch($page);
        $p->url = $page;
        return $p;
    }
}
?>
