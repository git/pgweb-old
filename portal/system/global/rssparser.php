<?php

///
/// Simple RSS parser that returns titles and links only
///

class RSSParser {
   var $_insideitem = false;
   var $_tag = "";
   var $_title = "";
   var $_link = "";

   var $_items = array();
   var $_filter;

   var $_url = '';
   var $_xml_parser;

   var $_success = false;

   function RSSParser($url, $filter=array()) {
      $this->_url = $url;
      $this->_filter = $filter;
	  $this->_xml_parser = xml_parser_create();
	  xml_set_object($this->_xml_parser, $this);
	  xml_set_element_handler($this->_xml_parser, "startElement", "endElement");
	  xml_set_character_data_handler($this->_xml_parser, "characterData");
	  $fp = @fopen($url,'r');
	  if (!$fp) return; 
	  while ($data = fread($fp, 4096)) {
	  	if (!xml_parse($this->_xml_parser, $data, feof($fp))) return;
	  }
	  fclose($fp);
	  xml_parser_free($this->_xml_parser);
	  $this->_success = true;
   }

   function success() {
      return $this->_success;
   }

   function getItems() {
      if (!$this->_success) return null;
	  return $this->_items;
   }

   function startElement($parser, $tagName, $attrs) {
       if ($this->_insideitem) {
           $this->_tag = $tagName;
       } elseif ($tagName == "ITEM") {
           $this->_insideitem = true;
       }
   }

   function endElement($parser, $tagName) {
       if ($tagName == "ITEM") {
           // Filter
           $include = 1;
           foreach ($this->_filter as $filt) {
               if (preg_match($filt, $this->_link)) {
                   $include = 0;
                   break;
               }
           }
           if ($include == 1) {
               $this->_items[] = array('link'=>htmlspecialchars(trim($this->_link)), 'title'=>htmlspecialchars(trim($this->_title)),'date'=>$this->formatDate($this->_date));
           }
		   $this->_title = "";
           $this->_link = "";
           $this->_date = "";
           $this->_insideitem = false;
       }
   }

   function formatDate($d) {
       return strftime("%Y-%m-%d", strtotime($d));
   }

   function characterData($parser, $data) {
       if ($this->_insideitem) {
           switch ($this->_tag) {
               case "TITLE":
				   $this->_title .= $data;
				   break;
               case "LINK":
				   $this->_link .= $data;
				   break;
               case "PUBDATE":
                   $this->_date .= $data;
                   break;
           }
       }
   }
}
?>
