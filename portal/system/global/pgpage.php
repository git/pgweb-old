<?php
// $Id: pgpage.php,v 1.6 2007-10-21 17:20:07 mastermind Exp $
// Base-class for all pages
//

function getimage($arg) {
  global $pgpage;
  if (file_exists('../layout/images/' . $pgpage->getLanguage() . '/'.$arg)) {
     return '"/layout/images/' . $pgpage->getLanguage() . '/'.$arg.'"';
  }
  else {
     return '"/layout/images/en/' . $arg . '"';
  }
}

abstract class PgPage {
   protected $language;
   protected $language_direction;
   public $tpl;
   protected $base_template = 'common.html';
   protected $inner_template = 'common-standard.html';
   protected $content_template = '';
   protected $navsection = '';
   protected $title = null;
   public $requirelogin = false;
   protected $userinfo = null;
   public $url;

   // MUST override
   abstract function Render();

   // overridable
   function PreRender() {
   }
   function PrepareTemplate() {
   }

   // Utilities
   function pg_query($query, $dbname = 'portal') {
      $db = $this->database($dbname);
      $rs = @pg_query($db, $query);
      if (!$rs) {
         throw new Exception('Error communicating with database: ' . pg_last_error($db));
      }
      return $rs;
   }
   function pg_query_params($query, $params, $dbname = 'portal') {
      $db = $this->database($dbname);
      $rs = @pg_query_params($db, $query, $params);
      if (!$rs) {
         throw new Exception('Error communicating with database: ' . pg_last_error($db));
      }
      return $rs;
   }
   function database($dbname = 'portal') {
      global $_SETTINGS;
      $db = @pg_pconnect($_SETTINGS['db_' . $dbname]);
      if (!$db) {
         throw new Exception('Error connecting to database');
      }
      return $db;
   }

   function format_location($data) {
      if (empty($data['state'])) {
         return $data['city'] . ', ' . $data['country'];
      }
      else {
         return $data['city'] . ', ' . $data['state'] . ', ' . $data['country'];
      }
   }

   // Implementation
   function setLanguage($override) {
       global $_LANGUAGES;
       global $_LANGUAGE_ALIASES;
       global $_LANGUAGE_DIRECTION;

       if ($override) {
           if (isset($_LANGUAGES[$override]))
               $this->language = $override;
           else
               $this->language = 'en';
       }
       else {
           // Determine language from accept language params
           if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
               $this->language = $this->getLanguageFromHeaders();
           else
               $this->language = 'en';
       }
       if ($this->language == 'uk') $this->language = 'en';

       // Set language for gettext and friends. At this point we know
       // that $this->language contains a valid language. Which makes
       // this part much easier. A language either matches the locale
       // or is aliased, simple as that.
       $locale = $this->language;
       if (isset($_LANGUAGE_ALIASES[$this->language]))
           $locale = $_LANGUAGE_ALIASES[$this->language];
       $locale .= '.UTF-8';

       putenv('LANG=' . $locale);
       putenv('LANGUAGE=' . $locale);
       setlocale(LC_ALL, $locale);

       bindtextdomain('portal', '../intl');
       bind_textdomain_codeset('portal', 'UTF-8');
       textdomain('portal');

       if (isset($_LANGUAGE_DIRECTION[$this->language]))
           $this->language_direction = $_LANGUAGE_DIRECTION[$this->language];
       else
           $this->language_direction = 'ltr';

       header('Content-Language: ' . $this->language);
   }

   public function getLanguage() {
     return $this->language;
   }

   private function getLanguageFromHeaders() {
       global $_LANGUAGES;

       $accepts_langs = explode(';', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
       $accepts_langs = explode(',', $accepts_langs[0]);
       foreach ($accepts_langs as $accepts_lang) {
           if (isset($_LANGUAGES[$accepts_lang])) 
               return $accepts_lang;
       }
       return 'en';
   }

   function DoRender() {
      global $_SETTINGS;

      // Prepare the template
      $this->tpl =& new HTML_Template_Sigma('../template');
      $this->tpl->setCallbackFunction('lang', 'gettext', true);
      $this->tpl->setCallbackFunction('image', 'getimage', true);
      $this->tpl->loadTemplateFile($this->base_template, true, true);
      $this->tpl->setGlobalVariable(array(
        'site_title'        => $_SETTINGS['site_title'],
        'current_page'      => '/' == substr($this->url, -1)? $this->url . 'index': $this->url,
        'current_lang'      => $this->language,
        'current_lang_dir'  => $this->language_direction,
        'master_server'     => $_SETTINGS['masterserver'],
        'mirror_master_server'     => $_SETTINGS['masterserver']
    ));
      if ($this->navsection != 'admin')
          $this->tpl->addBlockfile('top_nav', 'top_nav_block', 'navigation/top-pgsql.html');
      else
          $this->tpl->addBlockfile('top_nav', 'top_nav_block', 'navigation/top-admin.html');
      $this->tpl->touchBlock('top_nav_block');
      $this->tpl->parse('top_nav');

      if ($this->inner_template != '') {
         $this->tpl->addBlockfile('page_inner', 'inner_block', $this->inner_template);
      }
      if ($this->navsection != '' && $this->navsection != 'admin') {
         $this->tpl->addBlockFile('page_content', 'content_block', 'navigation/common.html');
         if ($this->content_template != '') {
            $this->tpl->addBlockFile('nav_page_content', 'nav_page_content', $this->content_template);
         }
         $this->tpl->addBlockFile('nav_section', 'nav_section', 'navigation/' . $this->navsection . '.html');
         $this->tpl->touchBlock('nav_section');
         $this->tpl->touchBlock('nav_page_content');
      }
      else {
         if ($this->content_template != '') {
            $this->tpl->addBlockfile('page_content', 'content_block', $this->content_template);
         }
      }

      $this->PrepareTemplate();

      $this->Render();

      // Fix page title
      if ($this->tpl->blockExists('page_title_block')) {
         if ($this->title) {
            $this->tpl->setVariable('page_title', $this->title);
         }
         else {
            $this->tpl->touchBlock('page_title_block');
            $this->tpl->parse('page_title_block');
            $this->tpl->setVariable('page_title', $this->tpl->get('page_title_block', true));
         }
      }
      else if ($this->title) {
          $this->tpl->setVariable('page_title', $this->title);
      }

      $this->tpl->setVariable('current_year', date('Y'));
      $this->tpl->setVariable('site_type', $_SETTINGS['site_type']);
      $this->tpl->setVariable('site_hosted_by', $_SETTINGS['hosted_by']);
      $this->tpl->setVariable('site_hosted_url', $_SETTINGS['hosted_url']);


      if (!isset($_SERVER['ISMASTER']) || $_SERVER['ISMASTER'] != '1') {
          if ($this->navsection != 'admin')
             $this->tpl->setVariable('base_href', 'http://www.postgresql.org/' . $this->url);
      }
   }

   function Show() {
      $this->tpl->show();
   }

   function ValidateLogin() {
       session_start();
       if ($this->check_login()) {
           return;
       }
       // User not authenticated, redirect to login form
       // If we are the recipient of a form, we need to store away the form data somehow
       if ($_SERVER['REQUEST_METHOD'] == 'POST') {
           $_SESSION['saved_form_url'] = $this->url;
           $_SESSION['saved_form_data'] = $_POST;
       }
       header('Location: /login?p=' . $this->url);
       exit(0);
   }

   function check_login() {
       if (isset($_SESSION['userid'])) {
           if ($_SESSION['ip'] != $_SERVER['REMOTE_ADDR']) {
               // Different IP, so require a login again
               return false;
           }

           // Copy the whole session - easier that way
           $this->userinfo = $_SESSION;
           return true; // Authentication succeeded
       }

       // No session = not logged in
       return false;
   }
}

class NotFoundException extends Exception {
}

   // Pull in language definitions
   require_once './global/languages.php';
?>
