<?php
require_once 'HTML/QuickForm.php';
require_once 'HTML/QuickForm/Renderer/ITDynamic.php';

abstract class PgForm extends PgPage  {
   protected abstract function SetupForm();
   protected abstract function ProcessForm($f);
   protected abstract function RenderThanks();

   protected $form;
   protected $submitbutton = true;
   protected $redir_param = '';
   protected $redirect_relative = '';
   protected $block_elements = '';
   protected $include_tinymce = false;

   public $thanks = false;

   function PreRender() {
       global $_SETTINGS;
       if ($this->thanks) {
           $this->content_template = 'en/submitthanks.html';
           return;
       }
       if ($this->navsection != 'admin')
           $this->content_template = 'common-form.html';
       else
           $this->content_template = 'admin/common-form.html';

       // If we have stored away form data that we need to process, do so here
       if (isset($_SESSION['saved_form_url']) && isset($_SESSION['saved_form_data'])) {
           // Match the URL of the saved data
           if ($_SESSION['saved_form_url'] == $this->url) {
               // Only get the data if the URLs match
               $_POST = $_SESSION['saved_form_data'];
               $_SERVER['REQUEST_METHOD'] = 'POST';
               unset($_SESSION['saved_form_data']);
               unset($_SESSION['saved_form_url']);
           }
       }

       $this->CreateForm();

       if ($_SERVER['REQUEST_METHOD'] == 'POST') {
           if ($this->form->validate()) {
               // Form validates, process results
               $this->ProcessForm($this->form->exportValues());

               if ($this->redirect_relative != '') {
                   header('Location: ' . $_SETTINGS['masterserver'] . $this->redirect_relative);
               }
               else {
                   header('Location: ' . $_SETTINGS['masterserver'] .
                       '/system/handler.php?page=submitthanks' .
                       '&lang=' . $this->language .
                       '&action=' . $this->url .
                       $this->redir_param);
               }
               return;
           }
           // If validation failed, just re-render the form here. It will automatically contain
           // information about what was wrong.
       }
   }

   function Render() {
      if ($this->thanks) {
         $this->RenderThanks();
         return;
      }

      if ($this->include_tinymce)
          $this->tpl->touchBlock('include_tinymce_scripts');

      $renderer = new HTML_QuickForm_Renderer_ITDynamic($this->tpl);
      if ($this->block_elements != '')
          $renderer->setElementBlock($this->block_elements);

      $this->form->accept($renderer);
   }

   private $_created = false;
   private function CreateForm() {
      if ($this->_created) return;
      $this->_created = true;

      global $_SETTINGS;

      if ($this->title && $this->tpl) {
         $this->tpl->setVariable('form_title', $this->title);
         $this->tpl->setVariable('page_title', $this->title);
      }

      if ($this->navsection == 'admin') {
         // In the admin section, use a relative URL
         $this->form = new HTML_QuickForm('pgform', 'post', '/' . $this->url);
      } else {
         // For the main site, use absolute URL so forms always end up on wwwmaster
         $this->form = new HTML_QuickForm('pgform', 'post', $_SETTINGS['masterserver'] . '/' . $this->url);
      }
      $this->form->applyFilter('__ALL__', 'trim');

      $this->SetupForm();

      if ($this->submitbutton)
         $this->form->addElement('submit', 'submit', gettext("Submit"));
      $this->form->setRequiredNote(gettext('<span class="txtRequiredField">*</span> denotes required field'));
      $this->form->setJsWarnings(gettext('Invalid information entered.'), gettext('Please correct these fields.'));
      // XHTML compliance
      $this->form->removeAttribute('name');
   }
}
?>
