<?php
//
// Languages
//
// Currently available languages
$GLOBALS['_LANGUAGES'] = array(
    'ar' => '&#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;',
    'br' => 'Portugu&ecirc;s',
    'bg' => '&#1041;&#1098;&#1083;&#1075;&#1072;&#1088;&#1089;&#1082;&#1080;',
    'cz' => '&#268;e&#353;tina',
    'de' => 'Deutsch',	
    'el' => '&#917;&#955;&#955;&#951;&#957;&#953;&#954;&#940;',
    'en' => 'English',
    'es' => 'Español',
    'fi' => 'Suomeksi',
    'fr' => 'Français',
    'he' => '&#1506;&#1489;&#1512;&#1497;&#1514;',
    'hu' => 'Magyar',
    'it' => 'Italiano',
    'ja' => '&#26085;&#26412;&#35486;',
    'nl' => 'Nederlands',
    'pl' => 'Polski',
    'ro' => 'Rom&#x00e2;n&#x0103;',
    'pt' => 'Portugu\xc3\xaas',
    'ru' => '&#1056;&#1091;&#1089;&#1089;&#1082;&#1080;&#1081;',
    'sv' => 'Svenska',
    'sl' => 'Slovensko',
    'tr' => 'Türkçe',
    'zh_cn' => '&#31616;&#20307;&#20013;&#25991;'
);

// Aliases for languages with different browser and gettext codes
// Codes like en-gb are translated to gettext ones (en_GB) automatically
$GLOBALS['_LANGUAGE_ALIASES'] = array(
    'br' => 'pt_BR',
    'en' => 'en_US',
    'ru' => 'ru_RU',
    'tr' => 'tr_TR'
);

// Language-direction only needs to be set for languages that are
// right-to-left, we will default to left-to-right...
$GLOBALS['_LANGUAGE_DIRECTION'] = array(
    'ar' => 'rtl',
    'he' => 'rtl'
);
?>
