<?php

// clear the $_SETTINGS array
$_SETTINGS = array();

$_SETTINGS['settingsver'] = '$Revision: 1.34 $';

// some default settings
$_SETTINGS['site_title'] = 'PostgreSQL';
$_SETTINGS['site_type'] = 'Project';
$_SETTINGS['hosted_by'] = 'hub.org';
$_SETTINGS['hosted_url'] = 'http://www.postgresql.org/about/servers';
$_SETTINGS['masterserver'] = "http://wwwmaster.postgresql.org";
$_SETTINGS['defaultlanguage'] = "en";
$_SETTINGS['notifymail'] = "pgsql-slavestothewww@postgresql.org";
$_SETTINGS['bugsmail'] = "pgsql-bugs@postgresql.org";
$_SETTINGS['current_docs_version'] = '9.1';
$_SETTINGS['beta_docs_version'] = 9.2;
$_SETTINGS['ftp_root'] = "/home/ftp/";

// database settings
$_SETTINGS['usepersistent'] = true;
$_SETTINGS['db_portal'] = "dbname=186_www host=pgsql74.hub.org user=186_pgsql sslmode=disable";
$_SETTINGS['db_search'] = "dbname=search user=search port=5433 host=/tmp";

// Max poll options
define ('MAX_OPTIONS',  8);

// How many mirror links to have in one row
define ('MIRROR_CELLS', 5);

// Whether to display debug information (e.g. profiling)
define ('DEBUG', false);

// Version information
$_SETTINGS['versions'] = array(
    '9.1' => array (
        'version' => '9.1.1',
        'reldate' => '2011-09-26',
        'relnotes' => 'release-9-1-1.html',
        'frontpage' => 1,
    ),
    '9.0' => array (
        'version' => '9.0.5',
        'reldate' => '2011-09-26',
        'relnotes' => 'release-9-0-5.html',
        'frontpage' => 1,
    ),
    '8.4' => array (
        'version' => '8.4.9',
        'reldate' => '2011-09-26',
        'relnotes' => 'release-8-4-9.html',
        'frontpage' => 1,
    ),
    '8.3' => array (
        'version' => '8.3.16',
        'reldate' => '2011-09-26',
        'relnotes' => 'release-8-3-16.html',
        'frontpage' => 1,
    ),
    '8.2' => array (
        'version' => '8.2.22',
        'reldate' => '2011-09-26',
        'relnotes' => 'release-8-2-22.html',
        'frontpage' => 1,
    ),
    '8.1' => array (
        'version' => '8.1.23',
        'reldate' => '2010-12-16',
        'relnotes' => 'release.html#RELEASE-8-1-23',
        'frontpage' => 0,
    ),
    '8.0' => array (
        'version' => '8.0.26',
        'reldate' => '2010-10-04',
        'relnotes' => 'release.html#RELEASE-8-0-26',
        'frontpage' => 0,
    ),
    '7.4' => array (
        'version' => '7.4.30',
        'reldate' => '2010-10-04',
        'relnotes' => 'release.html#RELEASE-7-4-30',
        'frontpage' => 0,
    )
);

// Load local settings
if (file_exists('./global/settings.local.php'))
    require_once './global/settings.local.php';
?>
