<?php
   require_once './global/settings.php';

   // Get the URL and hostname
   if (isset($_GET['url'])) {
      $url = $_GET['url'];
      if (isset($_GET['setmir']) && isset($_GET['typ'])) {
         if ($_GET['typ'] == 'h' || $_GET['typ'] == 'f') {
            setcookie('dlmirror', intval($_GET['setmir']), time()+60*60*24*300);
            setcookie('dlmirrortype', $_GET['typ'], time()+60*60*24*300);
         }
      }
   }
   else 
      $url = urldecode($_SERVER['QUERY_STRING']);

   // Insert a clickthru record. If this fails in any way, just keep going...
   $db = @pg_pconnect($_SETTINGS['db_portal']);
   if ($db) {

     $bits = parse_url($url);
     $scheme = $bits['scheme'];
     $host = $bits['host'];
     $path = $bits['path'];

     if (get_magic_quotes_gpc()) {
       $url = pg_escape_string(stripslashes($url));
       $scheme = pg_escape_string(stripslashes($scheme));
       $host = pg_escape_string(stripslashes($host));
       $path = pg_escape_string(stripslashes($path));
     } else {
       $url = pg_escape_string($url);
       $scheme = pg_escape_string($scheme);
       $host = pg_escape_string($host);
       $path = pg_escape_string($path);
     }

      $ip = $_SERVER['REMOTE_ADDR'];
      $ip = preg_split('/[.]+/', $ip);
      $ipnumber = (double) ($ip[0] * 16777216) + ($ip[1] * 65536) + ($ip[2] * 256) + ($ip[3]);

      // This codepath is only used by stackbuilder, so don't log if it's an arbitrary redirect.
      // Keeps us from logging all those security attackers
      if (isset($_GET['sb'])) {
         @pg_exec($db, "INSERT INTO clickthrus (scheme, host, path, country) VALUES ('" . $scheme . "', '" . $host . "', '" . $path . "', (SELECT countrycode FROM iptocountry WHERE " . $ipnumber . " BETWEEN startip AND endip LIMIT 1));");
      }
   }

   // Now do the redirect if this didn't come from StackBuilder
   if (!isset($_GET['sb']))
 	echo "Old-style redirection not supported for security reasons. Please <a href=\"http://www.postgresql.org/ftp/\">return</a> to the file browser and try again.";

   exit;
?>
