<?php

//
// RSS feed generator
//
// $Id: rss.php,v 1.9 2007-04-10 11:49:59 mha Exp $
//

require_once './global/settings.php';

// PEAR class
require_once 'XML/Serializer.php';

if (!empty($_GET['lang'])) {
    $language = addslashes($_GET['lang']);
} else {
    $language = $_SETTINGS['defaultlanguage'];
}

if (!empty($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 'news';
}

$db = @pg_pconnect($_SETTINGS['db_portal']);
if (!$db)
    die('No database connection');

$options = array(
    'indent'         => '    ',
    'linebreak'      => "\n",
    'typeHints'      => false,
    'addDecl'        => false,
    'encoding'       => 'UTF-8',
    'rootName'       => 'rss',
    'rootAttributes' => array(
        'version'   => '2.0', 
        'xmlns:rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
    ),
    'defaultTagName' => 'item'
);
switch ($page) {
    case 'events': 
        $rs = @pg_query("select to_char(max(modified),'Dy, DD Mon YYYY HH24:MI:SS GMT') AS last_modified from events_text");
	$ev = pg_fetch_assoc($rs);
        $rdf = array(
            'channel' => array(
                'title' => gettext("PostgreSQL Events"),
                'link'  => 'http://www.postgresql.org/',
                'description' => gettext("PostgreSQL Events"),
                'webMaster' => 'webmaster@postgresql.org (PostgreSQL webmasters)',
                'lastBuildDate' => $ev['last_modified'],
            )
        );
        $rs = @pg_query("SELECT e.id, to_char(posted, 'Dy, DD Mon YYYY HH24:MI:SS GMT') AS posted_ts,e.posted_by, e.start_date, e.end_date, et.event, el.city, el.state, c.name AS country, et.summary FROM events e, events_text et, events_location el, countries c WHERE e.approved AND e.id=et.eventid AND e.id=el.eventid AND el.country=c.id AND e.start_date IS NOT NULL AND e.end_date >= CURRENT_DATE AND et.language IN ('{$_SETTINGS['defaultlanguage']}') AND NOT e.training ORDER BY e.end_date,e.start_date LIMIT 5");
        while ($ev = pg_fetch_assoc($rs)) {
            $rdf['channel'][] = array(
                'title'       => $ev['event'],
                'link'        => 'http://www.postgresql.org/about/event.' . $ev['id'],
                'guid'        => 'http://www.postgresql.org/about/event.' . $ev['id'],
                'description' => $ev['summary'],
                'author'      => $ev['posted_by'],
                'pubDate'        => $ev['posted_ts']
            );
        }
        break;

    case 'news': 
        $rs = @pg_query("select to_char(max(modified),'Dy, DD Mon YYYY HH24:MI:SS GMT') AS last_modified from news_text");
	$ev = pg_fetch_assoc($rs);
        $rdf = array(
            'channel' => array(
                'title' => gettext("PostgreSQL News"),
                'link'  => 'http://www.postgresql.org/',
                'description' => gettext("PostgreSQL News"),
                'webMaster' => 'webmaster@postgresql.org (PostgreSQL webmasters)',
                'lastBuildDate' => $ev['last_modified']
            )
        );
        $rs = @pg_query("SELECT n.id, n.posted, n.posted_by, nt.headline, nt.summary, to_char(posted, 'Dy, DD Mon YYYY HH24:MI:SS GMT') AS posted_ts FROM news n, news_text nt WHERE n.approved AND n.id=nt.newsid AND nt.language='en' ORDER BY n.posted DESC LIMIT 6");
        while ($news = pg_fetch_assoc($rs)) {
            $rdf['channel'][] = array(
                'title'       => $news['headline'],
                'link'        => 'http://www.postgresql.org/about/news.' . $news['id'],
                'guid'        => 'http://www.postgresql.org/about/news.' . $news['id'],
                'description' => $news['summary'],
                'author'      => $news['posted_by'],
                'pubDate'        => $news['posted_ts']
            );
        }
        break;

    case 'versions': 
        $rdf = array(
            'channel' => array(
                'title' => gettext('PostgreSQL latest versions'),
                'link'  => 'http://www.postgresql.org/',
                'description' => gettext('PostgreSQL latest versions'),
                'webMaster' => 'webmaster@postgresql.org (PostgreSQL webmasters)'
            )
        );
	date_default_timezone_set('UTC');
        foreach ($_SETTINGS['versions'] as $major => $verinfo) {
            $rdf['channel'][] = array(
                'title' => $verinfo['version'],
                'link'  => 'http://www.postgresql.org/docs/' . $major . '/static/' . $verinfo['relnotes'],
                'guid'  => 'http://www.postgresql.org/docs/' . $major . '/static/' . $verinfo['relnotes'],
                'description' => $verinfo['version'] . ' is the latest release in the ' . $major . ' series',
                'author'      => 'webmaster@postgresql.org (PostgreSQL webmasters)',
                'pubDate'     => date(DATE_RSS,strtotime($verinfo['reldate']))
		);
        }
        break;

    default: // Nothing specified, send 404
        header('HTTP/1.0 404 Not found');
        header('Content-type: text/plain');
        echo 'File not found';
        exit(0);
} // switch

$serializer =& new XML_Serializer($options);
if (true === $serializer->serialize($rdf)) {
    header('Content-Type: application/rss+xml');
    echo $serializer->getSerializedData();
}
