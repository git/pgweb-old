<?php

//
// Community file download
//
// $Id: communityfiles.php,v 1.4 2007-04-11 20:55:08 mha Exp $
//

require_once './global/settings.php';

if (empty($_GET['page']))
    die('Invalid usage');
$page = explode('.',$_GET['page']);

$db = @pg_pconnect($_SETTINGS['db_portal']);
if (!$db)
    die('No database connection');

$fid = $page[0];
if (!is_numeric($fid))
    die('Invalid URL format');

$res = pg_query_params($db, "SELECT mimetype,image FROM communitypages_files WHERE fileid=$1", array($fid));
if (pg_num_rows($res) == 0) {
    // Second try
    $res = pg_query_params($db, "SELECT mimetype,image FROM communitypages_work_files WHERE fileid=$1", array($fid));
    if (pg_num_rows($res) != 1) {
        header('HTTP/1.0 404 Not found');
        die('File does not exist');
    }
}

header('Content-type: ' . pg_fetch_result($res, 0, 0));
print pg_unescape_bytea(pg_fetch_result($res, 0, 1));
?>
