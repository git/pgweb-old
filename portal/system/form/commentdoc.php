<?php

//
// Form for doc comments
//
// $Id: commentdoc.php,v 1.6 2007-03-12 14:51:43 mha Exp $
//
class Form_CommentDoc extends PgForm {
    function __construct() {
        $this->title = 'Doc comment';
        $this->navsection = 'docs';
        $this->requirelogin = true;
    }

    function SetupForm() {
        $this->form->addElement('hidden',     'doc');
        $this->form->addElement('hidden',     'version');

        $this->form->addElement('static',     null, null, gettext("Please use this form to add your own comments regarding your experience with particular features of PostgreSQL, clarifications of the documentation, or hints for other users. Please note, this is <strong>not</strong> a support forum, and your IP address will be logged. If you have a question or need help, please see the <a href=\"http://www.postgresql.org/docs/faq/\">faq</a>, try a <a href=\"http://www.postgresql.org/community/lists/\">mailing list</a>, or join us on <a href=\"http://www.postgresql.org/community/irc\">IRC</a>. Note that submissions containing URLs or other keywords commonly found in 'spam' comments may be silently discarded. Please contact the <a href=\"mailto:webmaster@postgresql.org\">webmaster</a> if you think this is happening to you in error."));
        $this->form->addElement('static',     null, null, gettext('In order to submit a comment, you must have a <a href="/community/signup">community account</a>.'));

        $this->form->addElement('textarea',   'comment', gettext("Comment"), array('rows' => 8, 'cols' => 66, 'wrap' => 'soft'));

        $this->form->addRule('comment', gettext("The comment is required."), 'required', null, 'client');

        $this->form->addRule('comment', gettext("Your comment should be at least 10 characters long."), 'minlength', 10, 'client');
    }

    function ProcessForm($f) {
        global $_SETTINGS;

        $this->redir_param = '&ver=' . $f['version'] . '&doc=' . $f['doc'];

        $rs = $this->pg_query('SELECT re FROM comment_rejects');
        for ($i = 0; $i < pg_num_rows($rs); $i++) {
            if (preg_match('/' . pg_fetch_result($rs, $i, 0) . '/i', $f['comment']))
                return; // Banned word matched, so just don't do anything
        }

        $rs = $this->pg_query("SELECT nextval('comments_id_seq')");
        $commentid = pg_fetch_result($rs, 0, 0);
        $mailtext  = "Author: " . $this->userinfo['fullname'] . ' <' . $this->userinfo['email'] . '>' .
            "\n----\n" . $f['comment'] . "\n----\n" .
            "Preview:     " . $_SETTINGS['masterserver'] . '/admin/comment-preview.php?id=' . $commentid . "  (preview comment on the documentation page)\n" .
            "Approve:     " . $_SETTINGS['masterserver'] . '/admin/comments.php?action=approve&id=' . $commentid . " (approve for display on the website)\n" .
            "Save:        " . $_SETTINGS['masterserver'] . '/admin/comments.php?action=save&id=' . $commentid . " (save comment for later review, don't show on website, send mail to author about saving)\n" .
            "Reject:      " . $_SETTINGS['masterserver'] . '/admin/comments.php?action=reject&id=' . $commentid . " (delete comment, automatically send mail to author about rejection)\n" .
            "Delete:      " . $_SETTINGS['masterserver'] . '/admin/comments.php?action=delete&id=' . $commentid . " (delete comment without notifying author)\n" .
            "Edit:        " . $_SETTINGS['masterserver'] . '/admin/comment-edit.php?id=' . $commentid . " (edit comment text)\n";
        $email = str_replace('@', ' AT ', $this->userinfo['email']);
        $this->pg_query_params('INSERT INTO comments (id,version,file,comment,posted_by,posted_ip) VALUES ($1,$2::numeric,$3,$4,$5,$6)', array($commentid,$f['version'],$f['doc'],$f['comment'],$this->userinfo['fullname'] . ' <' . $email . '>',$_SERVER['REMOTE_ADDR']));
        @mail($_SETTINGS['notifymail'], 'Comment ' . $commentid . ' added to page ' . $f['doc'] . ' of version ' . $f['version'], $mailtext);
    }

    function RenderThanks() {
        $this->tpl->touchBlock('commentdoc_block');
        $this->tpl->setVariable('referer', '/docs/' . $_GET['ver'] . '/interactive/' . $_GET['doc']);
    }
}
?>
