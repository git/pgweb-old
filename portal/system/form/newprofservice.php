<?php
//
// Form for submitting a professional services company 
//
// $Id: newprofservice.php,v 1.1 2007-03-12 14:51:43 mha Exp $
//
class Form_NewProfService extends PgForm {
   function __construct() {
      $this->title = 'Register a company';
      $this->navsection = 'support';
   }

   function SetupForm() {
       $txtf = array('size' => 52);
       $txta = array('cols' => 40, 'rows'=>8);

       $this->form->addElement('static',null,null,gettext("Please enter information about your company. Note that the company will not be listed until the information has been approved by a web editor based on the project <a href=\"http://wiki.postgresql.org/wiki/Policies\">policies</a>."));
       $this->form->addElement('header',null,'Basic data');
       $this->form->addElement('static','info1',null,gettext('Always fill in the fields in this section.'));
       $this->form->addElement('text','name','Name of company', $txtf);
       $this->form->addElement('text','url','URL',$txtf);
       $this->form->addElement('text','email','Manager email', $txtf);
       $this->form->addElement('textarea','description','Description', $txta);
       $this->form->addElement('text','locations','Office locations', $txtf);
       $regions[] =& $this->form->createElement('checkbox','region_africa',null,'Africa',array('id' => 'region_africa'));
       $regions[] =& $this->form->createElement('checkbox','region_asia',null,'Asia',array('id' => 'region_asia'));
       $regions[] =& $this->form->createElement('checkbox','region_europe',null,'Europe',array('id' => 'region_europe'));
       $regions[] =& $this->form->createElement('checkbox','region_northamerica',null,'North America',array('id' => 'region_northamerica'));
       $regions[] =& $this->form->createElement('checkbox','region_oceania',null,'Oceania',array('id' => 'region_oceania'));
       $regions[] =& $this->form->createElement('checkbox','region_southamerica',null,'South America',array('id' => 'region_southamerica'));
       $this->form->addGroup($regions, 'regions', 'Regions', '<br/>', false);
       $this->form->addElement('textarea','contact','Contact Info',$txta);
       $this->form->addElement('header',null,'Support and services');

       $this->form->addElement('static','info1',null,gettext('Check the box and fill in all the fields below if your company provides software support or offers additional services like custom installations, on-site training or custom software development for PostgreSQL. Do not check it if you are a hosting company and only provide support for hosting customers.'));

       $this->form->addElement('checkbox','provides_support',null,'Provides support and services',array('id' => 'provides_support'));
       $this->form->addElement('text','employees','Number of employees', $txtf);
       $this->form->addElement('text','hours','Hours',$txtf);
       $this->form->addElement('text','languages','Languages',$txtf);
       $this->form->addElement('textarea','customerexample','Customer example',$txta);
       $this->form->addElement('textarea','experience','Experience',$txta);
       $this->form->addElement('header',null,'Hosting');
       $this->form->addElement('static','info3',null,'Check the box and fill in all the fields below if your company provides hosting for PostgreSQL based applications and database.');
       $this->form->addElement('checkbox','provides_hosting',null,'Provides hosting of PostgreSQL databases',array('id' => 'provides_hosting'));
       $this->form->addElement('textarea','interfaces','Supported Interfaces',$txta);

       $this->form->addRule('name','Name required','required',null,'client');
       $this->form->addRule('email','Manager email required','required',null,'client');
       $this->form->addRule('lastconfirmed','Last confirmed required','required',null,'client');
       $this->form->addRule('description','Description required','required',null,'client');
       $this->form->addRule('email', gettext("The email address you entered does not appear to be valid."), 'email', true, 'client');
       $this->form->addRule('url','URL must start with http or https','regex','/^https?:\/\//','client');

       $this->block_elements = array('info1' => 'qf_element', 'info2' => 'qf_element', 'info3' => 'qf_element');
   }

   function ProcessForm($f) {
       global $_SETTINGS;

       $rs = $this->pg_query("SELECT nextval('profserv_id_seq')");
       $profid = pg_fetch_result($rs, 0, 0);
       $africa = empty($f['region_africa'])?0:1;
       $asia = empty($f['region_asia'])?0:1;
       $europe = empty($f['region_europe'])?0:1;
       $northamerica = empty($f['region_northamerica'])?0:1;
       $oceania = empty($f['region_oceania'])?0:1;
       $southamerica = empty($f['region_southamerica'])?0:1;
       $support = empty($f['provides_support'])?0:1;
       $hosting = empty($f['provides_hosting'])?0:1;

       $this->pg_query_params('INSERT INTO profserv (id,email,name,description,employees,locations,region_africa,region_asia,region_europe,region_northamerica,region_oceania,region_southamerica,hours,languages,customerexample,experience,contact,url,provides_support,provides_hosting,interfaces) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21)',
           array($profid, $f['email'], $f['name'], $f['description'], $f['employees'], $f['locations'], $africa, $asia, $europe, $northamerica, $oceania, $southamerica, $f['hours'], $f['languages'], $f['customerexample'], $f['experience'], $f['contact'], $f['url'], $support, $hosting, $f['interfaces']));

       $mailtext = "A new professional services registration has been submitted:\n\n" .
           'Edit/approve: ' . $_SETTINGS['masterserver'] . '/admin/prof-edit.php?id=' . $profid . "\n\n" .
           wordwrap('Name:         ' . $f['name'], 76, "\n", true) . "\n" .
           wordwrap('URL:          ' . $f['url'], 76, "\n", true) . "\n" .
           wordwrap('Email:        ' . $f['email'], 76, "\n", true) . "\n" .
           wordwrap('Description:  ' . $f['description'], 76, "\n", true) . "\n" .
           wordwrap('Locations:    ' . $f['locations'], 76, "\n", true) . "\n" .
           wordwrap('Africa:       ' . $f['region_africa'], 76, "\n", true) . "\n" .
           wordwrap('Asia:         ' . $f['region_asia'], 76, "\n", true) . "\n" .
           wordwrap('Europe:       ' . $f['region_europe'], 76, "\n", true) . "\n" .
           wordwrap('North America:' . $f['region_northamerica'], 76, "\n", true) . "\n" .
           wordwrap('Oceania:      ' . $f['region_oceania'], 76, "\n", true) . "\n" .
           wordwrap('South America:' . $f['region_southamerica'], 76, "\n", true) . "\n" .
           wordwrap('Contact      :' . $f['contact'], 76, "\n", true) . "\n" .
           wordwrap('Prov. support:' . $f['provides_support'], 76, "\n", true) . "\n" .
           wordwrap('Employees    :' . $f['employees'], 76, "\n", true) . "\n" .
           wordwrap('Hours        :' . $f['hours'], 76, "\n", true) . "\n" .
           wordwrap('Languages    :' . $f['languages'], 76, "\n", true) . "\n" .
           wordwrap('Example      :' . $f['customerexample'], 76, "\n", true) . "\n" .
           wordwrap('Experience   :' . $f['experience'], 76, "\n", true) . "\n" .
           wordwrap('Prov. hosting:' . $f['provides_hosting'], 76, "\n", true) . "\n" .
           wordwrap('Interfaces   :' . $f['interfaces'], 76, "\n", true) . "\n" .
           "\n\n";

       @mail($_SETTINGS['notifymail'], 'New professional service', $mailtext);
   }
   
   function RenderThanks() {
       $this->tpl->touchBlock('support_newprof_block');
       $this->tpl->setVariable('referer', '/support/newprof');
   }
}
?>
