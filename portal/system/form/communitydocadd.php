<?php
class Form_CommunityDocAdd extends PgForm {
    private $pageid;
    function __construct($pageid) {
        $this->title = 'Add community documentation';
        $this->navsection = 'community';
        $this->requirelogin = true;
        if ($pageid == -1)
            throw new NotFoundException('Page to edit not specified');
        else
            $this->pageid = $pageid;
    }

    function SetupForm() {
        $res = $this->pg_query_params('SELECT pageid FROM communitypages WHERE pageid=$1', array($this->pageid));
        if (pg_num_rows($res) != 1)
            throw new NotFoundException('Specified parent page does not exist');

        $this->form->addElement('static', null, null, 'In this form you can create a new documentation page. The page will be checkedout for editing by you, and you will be sent directly to the editor.');
        $this->form->addElement('text', 'title', 'Page title', array('size'=>52, 'maxlength'=>128));
        $this->form->addElement('text', 'shorttitle', 'Short title', array('size'=>52, 'maxlength'=>128));

        $this->form->addRule('title', gettext('The title is required'), 'required', null, 'client');
        $this->form->addRule('shorttitle', gettext('the short title is required'), 'required', null, 'client');

    }

    function ProcessForm($f) {
        $this->pg_query('BEGIN TRANSACTION');
        $res = $this->pg_query("SELECT nextval('communitypages_id_seq')");
        $newpid = pg_fetch_result($res,0,0);
        $this->pg_query_params('INSERT INTO communitypages_work (pageid, parent, author, title, shorttitle, contents) VALUES ($1, $2, $3, $4, $5, $6)', array($newpid, $this->pageid, $this->userinfo['userid'], $f['title'], $f['shorttitle'], '<p></p>'));
        $this->pg_query('COMMIT');
        $this->redirect_relative = '/community/docedit.' . $newpid;
    }

    function RenderThanks() {
    }
}
?>
