<?php
class Form_SubmitContact extends PgForm {
   function __construct() {
      $this->title = 'Case Studies: Share Your Story';
      $this->navsection = 'about';
   }

   function SetupForm() {
       $this->form->addElement('static',   null, null, gettext("If you would like to share your PostgreSQL success story, or would like someone to contact you in relation to PostgreSQL, please either use this contact form, or <a href=\"mailto:press@postgresql.org\">email us</a> directly.</p> <p>Please be aware the fastest method of getting technical assistance without a support contract is through the <a href=\"/community/lists\">PostgreSQL Mailing Lists</a>."));

       $this->form->addElement('text',     'email', gettext("Email:"), array('size' => 30, 'maxlength' => 100));
       $this->form->addElement('text',     'headline', gettext("Headline:"), array('size' => 50, 'maxlength' => 100));
       $this->form->addElement('textarea', 'summary', gettext("Summary:"), array('rows' => 5, 'cols' => 50));
       $this->form->addElement('textarea', 'story', gettext("Story:"), array('rows' => 15, 'cols' => 50));

       // Make all fields required
       $this->form->addRule('email', gettext("The email is required."), 'required', null, 'client');
       $this->form->addRule('headline', gettext("The headline is required."), 'required', null, 'client');
       $this->form->addRule('summary', gettext("The summary is required."), 'required', null, 'client');
       $this->form->addRule('story', gettext("The story is required."), 'required', null, 'client');
       
       // Apply field-specific rules
       $this->form->addRule('email', gettext("The email address you entered does not appear to be valid."), 'email', true, 'client');
       $this->form->addRule('headline', gettext("The headline must be between 3 and 100 characters long."), 'rangelength', array(3, 100), 'client');
       $this->form->addRule('summary', gettext("The summary must be between 3 and 300 characters long."), 'rangelength', array(3, 300), 'client');
       $this->form->addRule('story', gettext("The story must be at least 3 characters long."), 'minlength', 3, 'client');
   }

   function ProcessForm($f) {
       global $_SETTINGS;
       $mailtext  = "The following information has been submitted using the \"Share Your Story\"/\"Request Contact\" form:\n\n" .
           wordwrap('Name:          ' . $f['name'], 76, "\n", true) . "\n" .
           wordwrap('Email address:      ' . $f['email'], 76, "\n", true) . "\n" .
           wordwrap('Description:        ' . $f['description'], 76, "\n", true) . "\n" .
           "Details: \n\n" . wordwrap($f['details'], 76, "\n", true);
       $subject   = "Share Your Story: " . $f['description'];
       $headers   = "From: \"" . $f['name'] . "\" <" . $f['email'] . ">\n";

       @mail($_SETTINGS['notifymail'], $subject, $mailtext, $headers);
   }

   function RenderThanks() {
      $this->tpl->touchBlock('about_submitcontact_block');
      $this->tpl->setVariable('referer', '/about/casestudies/submitcontact');
   }
}
?>
