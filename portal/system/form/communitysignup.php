<?php
class Form_CommunitySignup extends PgForm {
   function __construct() {
      $this->title = 'Community login';
      $this->navsection = 'community';
   }

   function SetupForm() {
       $this->form->addelement('static', null, null, 'To sign up for a free community account, enter your preferred userid and email address. A password will be generated and sent to this address. Note that a community account is only needed if you want to <i>write</i> pages, all content is available for reading without an account.');

       $this->form->addelement('text', 'userid', 'Preferred userid<br/>(min 3 chars)', array('size'=>16, 'maxlength'=>16));
       $this->form->addElement('text', 'email', 'Email address', array('size'=>52, 'maxlength'=>128));

       $this->form->addRule('userid', gettext('The userid is required.'), 'required', null, 'client');
       $this->form->addRule('userid', gettext('Userid must be at least three characters.'), 'minlength', 3, 'client');
       $this->form->addRule('email', gettext('The email is required.'), 'required', null, 'client');
       $this->form->addRule('email', gettext('The email has an invalid format.'), 'email', null, 'client');

       $this->form->registerRule('no_escapes', 'function', 'no_escapes');
       $this->form->addRule('userid', gettext('Invalid character(s) in user id.'), 'no_escapes');
   }

   function ProcessForm($f) {
       $this->pg_query('BEGIN TRANSACTION');
       $rs = $this->pg_query_params('SELECT count(*) FROM users WHERE lower(userid)=lower($1)', array($f['userid']));
       if (pg_fetch_result($rs,0,0) != 0)
           throw new Exception('A user with that userid already exists');
       $rs = $this->pg_query_params('SELECT count(*) FROM users WHERE lower(email)=lower($1)', array($f['email']));
       if (pg_fetch_result($rs,0,0) != 0)
           throw new Exception('A user with that email already exists');

       $pwd = $this->Random_Password(8);
       $mailstr =  "Someone, probably you, has signed up for a postgresql.org community account.\n" .
           "Please use the following details to log in to\n" .
           "http://wwwmaster.postgresql.org/community/profile\n" .
           "to change your password as soon as possible.\n\n\n" .
           "User id:  " . $f['userid'] . "\n" .
           "Password: " . $pwd . "\n\n";
       $this->pg_query_params("SELECT community_login_create($1,$2,'',$3)", array($f['userid'],$pwd,$f['email']));
       $this->pg_query('COMMIT TRANSACTION');

       @mail($f['email'], 'Your postgresql.org community account', $mailstr);
   }
   
   function RenderThanks() {
       $this->tpl->setVariable('referer', '/community/profile');
       $this->tpl->touchBlock('community_signup_block');
   }

   function Random_Password($length) {
       srand(date("s"));
       $possible_charactors = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
       $string = "";
       while(strlen($string)<$length) {
           $string .= substr($possible_charactors, rand()%(strlen($possible_charactors)),1);
       }
       return($string);
   }
}

function no_escapes($elementname, $elementvalue) {
    if ($elementvalue != pg_escape_string($elementvalue))
        return false;
    return true;
}

?>
