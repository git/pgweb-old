<?php
class Form_CommunityResetPwd extends PgForm {
   function __construct() {
      $this->title = 'Reset password';
      $this->navsection = 'community';
   }

   function SetupForm() {
       if (!(isset($_GET['uid']) || isset($_POST['uid'])) || 
           !(isset($_GET['rh']) || isset($_POST['rh'])))
           throw new Exception('Required parameter missing');

       $this->form->addElement('hidden', 'uid', isset($_GET['uid'])?$_GET['uid']:$_POST['uid']);
       $this->form->addElement('hidden', 'rh', isset($_GET['rh'])?$_GET['rh']:$_POST['rh']);
       $this->form->addElement('password', 'pwd1', gettext("Password:"), array('size' => 40, 'maxlength' => 100));
       $this->form->addElement('password', 'pwd2', gettext("Repeat password:"), array('size' => 40, 'maxlength' => 100));

       $this->form->addRule('pwd1', gettext('The password fields must be filled in.'), 'required', null, 'client');
       $this->form->addRule('pwd2', gettext('The password fields must be filled in.'), 'required', null, 'client');
   }

   function ProcessForm($f) {
       if ($f['pwd1'] != $f['pwd2'])
           throw new Exception(gettext('Passwords don\'t match!'));

       $this->pg_query('BEGIN TRANSACTION');
       $res = $this->pg_query_params('SELECT userid FROM users WHERE userid=$1 AND resethash=$2',
           array($f['uid'], $f['rh']));
       if (pg_num_rows($res) != 1) 
           throw new Exception(gettext('Recovery key not found for user!'));

       $res = $this->pg_query_params('UPDATE users SET pwdhash=community_login_make_password($1), resethash=NULL, resethashtime=NULL WHERE userid=$2', array($f['pwd1'], $f['uid']));
       if (pg_affected_rows($res) != 1) 
           throw new Exception(gettext('Failed to reset password!'));

       $this->pg_query('COMMIT');
   }

   function RenderThanks() {
       $this->tpl->setVariable('referer', '/community/lostpwd');
       $this->tpl->touchBlock('community_resetpwd_block');
   }
}
?>
