<?php
class Form_CommunityProfile extends PgForm {
   function __construct() {
      $this->title = 'Community profile';
      $this->navsection = 'community';
      $this->requirelogin = true;
   }

   function SetupForm() {
       $this->form->addElement('static', 'info1', 'Userid', $this->userinfo['userid']);
       $this->form->addElement('text', 'email', 'Email', array('size'=>52, 'maxlength'=>128));
       $this->form->addElement('text', 'fullname', 'Full name', array('size'=>52, 'maxlength'=>'128'));
       $this->form->addElement('textarea', 'authorblurb', 'Author bio', array('cols'=>40, 'rows'=>'5'));
       $this->form->addElement('header', null, 'Change password');
       $this->form->addElement('static', 'info2', null, 'If you don\'t want to change your password, just leave these fields blank to keep your old one.');
       $this->form->addElement('password', 'password', 'New password');
       $this->form->addElement('password', 'password2', 'Verify password');
       $this->form->addElement('static', 'info3', null, 'SSH keys can be used to access certain services. You can paste one or more <b>public</b> keys here in OpenSSH format, one per line. Comments will be removed from the key. Note that linebreaks will make it look strange in the textbox - that\'s ok!');
       $this->form->addelement('textarea', 'sshkey', 'SSH key(s)', array('cols'=>60, 'rows'=>3));

       $rs = $this->pg_query_params("SELECT email,fullname,authorblurb,sshkey FROM users WHERE userid=$1", array($this->userinfo['userid']));
       $this->form->setDefaults(pg_fetch_assoc($rs));

       $this->form->addRule('fullname', gettext('The full name is required.'), 'required', null, 'client');
       $this->form->addRule('email', gettext('The email is required.'), 'required', null, 'client');
       $this->form->addRule('email', gettext('The email was incorrectly formatted.'), 'email', null, 'client');
       $this->form->addRule('sshkey', gettext('The SSH key was incorrectly formatted.'), 'regex', 
		'/^(ssh-(dss|rsa) [a-zA-Z0-9\/=+]+(\s[^\s]+)?[\r\n]*)+$/',
		'client');
       $this->block_elements = array('info1' => 'qf_element', 'info2' => 'qf_element', 'info3' => 'qf_element');
   }

   function ProcessForm($f) {
       $pwd = '';
       if (strlen($f['password'])) {
           if ($f['password'] != $f['password2'])
               throw new Exception('Passwords don\'t match');
           $pwd = $f['password'];
       }
       // Remove multiple linebreaks, and comments (match regexp to above!)
       $sshkey='';
       foreach(preg_split('[\r\n]', $f['sshkey']) as $l) {
          if (preg_match('/^\s*$/', $l))
             continue;
          $sshkey .= preg_replace('/^(ssh-(dss|rsa) [a-zA-Z0-9\/=+]+)(\s[^\s]+)?$/', '\1', $l) . "\n";
       }
       $sshkey = rtrim($sshkey, "\n");
    
       $f['sshkey'] = preg_replace('/[\r\n]+/', "\r", $f['sshkey']);
       $rs = $this->pg_query_params('SELECT community_login_setinfo($1,$2,$3,$4,$5,$6)', array($this->userinfo['userid'], $pwd, $f['fullname'], $f['email'], $f['authorblurb'], $sshkey));
       if (pg_fetch_result($rs, 0,0) != 't') {
           throw new Exception('Failed to update user information');
       }
       $_SESSION['fullname'] = $f['fullname'];
       $_SESSION['authorblurb'] = $f['authorblurb'];
       $_SESSION['email'] = $f['email'];

       $this->redirect_relative = '/community/profile';
   }
   
   function RenderThanks() {
   }
}
?>
