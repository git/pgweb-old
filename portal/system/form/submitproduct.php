<?php

class Form_SubmitProduct extends PgForm {
   function __construct() {
      $this->title = 'Submit Product';
      $this->navsection = 'download';
      $this->requirelogin = true;
   }

   function SetupForm() {
      global $_SETTINGS;
      global $_SESSION;

      $txtf = array('size' => 52);
      $txta = array('cols' => 40, 'rows'=>8);

      $this->form->addElement('static', null, null, gettext("Please enter the product details you want to list:<br />(The product will only be shown after it has been approved based on the project <a href=\"http://wiki.postgresql.org/wiki/Policies\">policies</a>)."));

      $this->form->addElement('header', null, gettext("Publisher"));

      $this->form->addElement('select','product_publisher','Publisher',$this->fetch_publisher_list());

      $this->form->addElement('text','publisher_name',gettext('Name'), $txtf);
      $this->form->addElement('textarea','publisher_address',gettext('Address'), $txta);
      $this->form->addElement('text','publisher_url',gettext('URL'),$txtf);
      $this->form->addElement('text','publisher_email',gettext('Email'), $txtf);
      $this->form->addElement('text','publisher_phone',gettext('Phone'), $txtf);
      $this->form->addElement('select','publisher_orgtype',gettext('Organisation type'),$this->fetch_orgtype_list());

      $this->form->addElement('header', null, gettext("Product"));

      $this->form->addElement('text', 'product_name', gettext("Name"), $txtf);
      $this->form->addElement('text','product_url',gettext('URL'),$txtf);
      $this->form->addElement('select','product_category',gettext('Category'),$this->fetch_category_list());
      $this->form->addElement('select','product_licence',gettext('Licence type'),$this->fetch_licence_list());
      $this->form->addElement('textarea','product_description',gettext('Description'), $txta);
      $this->form->addElement('text','product_price',gettext('Pricing info'), $txtf);

      $this->form->addRule('product_publisher', gettext("The product publisher is required."), 'required', null, 'client');
      $this->form->addRule('product_name', gettext("The product name is required."), 'required', null, 'client');
      $this->form->addRule('product_url','URL required','required',null,'client');
      $this->form->addRule('product_url','URL must be to a webpage','regex','/^https?:\/\//','client');
      $this->form->addRule('product_category','Category required','required',null,'client');
      $this->form->addRule('product_licence','Licence type required','required',null,'client');
      $this->form->addRule('product_description','Description required','required',null,'client');

      // Hack to pass the connection to the callback functions
      $this->form->addFormRule('form_check_publisher');
      $this->form->addFormRule('form_check_product');
   }

   function ProcessForm($f) {
      global $_SETTINGS;
      global $_SESSION;

      $this->pg_query('COMMIT;');

      // Store the publisher if required
      if ($f['product_publisher'] == '-1') {
          $rs = $this->pg_query_params(
              "INSERT INTO organisations(name,address,url,email,orgtype,contact) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id;",
              array($f['publisher_name'],$f['publisher_address'],$f['publisher_url'],$f['publisher_email'],$f['publisher_orgtype'],$_SESSION['userid']));

          if (pg_num_rows($rs) != 1) {
              $this->pg_query('ROLLBACK;');
              throw new Exception('Failed to create the new publisher');
          }

          $f['product_publisher'] = pg_fetch_result($rs, 0, 0);
          pg_free_result($rs);
      }


      // Now store the product
      $rs = $this->pg_query_params(
          "INSERT INTO products(publisher,name,url,category,description,price,licence,contact) VALUES ($1,$2,$3,$4,$5,$6,$7,$8) RETURNING id;",
          array($f['product_publisher'],$f['product_name'],$f['product_url'],$f['product_category'],$f['product_description'],$f['product_price'],$f['product_licence'],$_SESSION['userid']));

      if (pg_num_rows($rs) != 1) {
          $this->pg_query('ROLLBACK;');
          throw new Exception('Failed to create the new product');
      }

      $productId  = pg_fetch_result($rs, 0, 0);
      pg_free_result($rs);

      $this->pg_query('COMMIT;');

      $mailtext  = "A new entry has been added to the products database.\n\n" .
                   "Edit:    {$_SETTINGS['masterserver']}/admin/product-edit.php?id={$productId}\n" .
                   "Approve: {$_SETTINGS['masterserver']}/admin/product-edit.php?action=approve&id={$productId}&publisher={$f['product_publisher']}\n" .
                   "Delete:  {$_SETTINGS['masterserver']}/admin/product-edit.php?action=delete&id={$productId}\n" .
                    "----\nSubmitted by: {$_SESSION['userid']}\nProduct: {$f['product_name']}\nDescription:{$f['product_description']}\n\nNote: Approving this product will also approve the publishing organisation.";

      @mail($_SETTINGS['notifymail'], 'New Product (id: ' . $productId . ')', $mailtext);
   }

   function RenderThanks() {
      $this->tpl->touchBlock('download_submitproduct_block');
      $this->tpl->setVariable('referer', '/download/product-categories');
   }

    function fetch_publisher_list() {
        $rs = $this->pg_query('SELECT id,name FROM organisations ORDER BY name');
        $c = array();
        $c[-1] = gettext('Create new publisher (enter details below)');
        for ($i = 0; $i < pg_num_rows($rs); $i++) {
            $row = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            $c[$row['id']] = $row['name'];
        }
        return $c;
    }

    function fetch_orgtype_list() {
        $c = array();

        $c['c'] = 'Company';
        $c['i'] = 'Individual';
        $c['n'] = 'Not for profit';
        $c['p'] = 'Open source project';

        return $c;
    }

    function fetch_category_list() {
        $rs = $this->pg_query('SELECT id,name FROM product_categories ORDER BY name');
        $c = array();
        for ($i = 0; $i < pg_num_rows($rs); $i++) {
            $row = pg_fetch_array($rs, $i, PGSQL_ASSOC);
            $c[$row['id']] = $row['name'];
        }
        return $c;
    }

    function fetch_licence_list() {
        $c = array();

        $c['c'] = 'Commercial';
        $c['f'] = 'Freeware';
        $c['m'] = 'Multiple';
        $c['o'] = 'Open source';

        return $c;
    }

}

function form_check_publisher($fields) {
    if ($fields['product_publisher'] == '-1') {

        $r = array();

        if (strlen($fields['publisher_name']) == 0)
            $r['publisher_name'] = gettext('Publisher name is required when creating a publisher');

        if (strlen($fields['publisher_url']) == 0)
           $r['publisher_url'] = gettext('Publisher URL is required when creating a publisher');

        $rs = pg_query_params(
            "SELECT id FROM organisations WHERE lower(name) = lower($1);",
            array($fields['publisher_name']));

        if (pg_num_rows($rs) != 0)
            $r['publisher_name'] = gettext('A publisher with this name already exists');

        pg_free_result($rs);

        if (count($r))
            return $r;
        else
            return TRUE;

    } else
        return TRUE;
}

function form_check_product($fields) {
    if ($fields['product_publisher'] != '-1') {

        $r = array();

        $rs = pg_query_params(
            "SELECT id FROM products WHERE lower(name) = lower($1) AND publisher = $2;",
            array($fields['product_name'],$fields['product_publisher']));

        if (pg_num_rows($rs) != 0)
            $r['product_name'] = gettext('A product with this name from this publisher already exists');

        pg_free_result($rs);

        if (count($r))
            return $r;
        else
            return TRUE;

    } else
        return TRUE;
}

        
?>
