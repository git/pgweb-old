<?php
class Form_Vote extends PgForm {
    function __construct() {
        $this->title = 'Vote';
        $this->navsection = 'community';
    }

    function SetupForm() {
        // This is not a regular form
    }

    function ProcessForm($f) {
        $surveyId = isset($_POST['surveyid'])? intval($_POST['surveyid']): 0;
        $answer   = isset($_POST['answer'])? intval($_POST['answer']): 0;
        if ($surveyId > 0 && $answer > 0 && $answer <= MAX_OPTIONS) {
            $this->pg_query('BEGIN TRANSACTION');
            $this->pg_query("DELETE FROM survey_lock WHERE (voted + '15 minutes') < now()");
            $rs = $this->pg_query_params('SELECT count(*) FROM survey_lock WHERE ipaddr=$1', array($_SERVER['REMOTE_ADDR']));
            if (pg_fetch_result($rs, 0, 0) == 0) {
                $this->pg_query_params("UPDATE surveys SET respondants=respondants+1, tot${answer}=tot${answer}+1 WHERE id=$1", array($surveyId));
                $this->pg_query_params('INSERT INTO survey_lock (ipaddr) VALUES ($1)', array($_SERVER['REMOTE_ADDR']));
            }
            $this->pg_query('COMMIT');
        }
        $this->redirect_relative = '/community/survey.' . $surveyId;
    }

    function RenderThanks() {
    }
}
?>
