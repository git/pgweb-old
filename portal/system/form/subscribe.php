<?php
class Form_Subscribe extends PgForm {
   function __construct() {
      $this->title = 'Subscribe to Lists';
      $this->navsection = 'community';
   }

   function SetupForm() {
      // this text is duplicated in template/community/lists.html
      $this->form->addElement('static', null, null, gettext("Please do not subscribe to mailing lists using e-mail accounts protected by mail-back anti-spam systems. These are extremely annoying to the list maintainers and other members, and you may be automatically unsubscribed."));

      $this->form->addElement('text', 'email', gettext("Email address:"), array('size' => 40, 'maxlength' => 100));

      $this->form->addElement('select', 'subunsub', 'Action', array('subscribe' => 'Subscribe', 'unsubscribe' => 'Unsubscribe'));
      $this->form->addElement('select', 'nomail', 'Receive Mail?', array('yes' => 'Yes', 'no' => 'No'));
      $this->form->addElement('select', 'digest', 'Digest Only?', array('no' => 'No', 'yes' => 'Yes'));

      $rs = $this->pg_query("SELECT lists.name,lists.name,lists.grp,1,listgroups.sortkey FROM lists INNER JOIN listgroups ON lists.grp=listgroups.id WHERE active=1 UNION ALL  SELECT '_lg'||CAST(listgroups.id AS text),'--' || listgroups.name, listgroups.id, 0, sortkey FROM listgroups WHERE EXISTS (SELECT * FROM lists WHERE listgroups.id=lists.grp AND lists.active=1) ORDER BY 5, 3,4,2");
      while ($row = pg_fetch_row($rs)) {
          $lists[$row[0]] = $row[1];
      }

      $this->form->addElement('select', 'listname', 'Available Lists', $lists);

      // Make the fields required
      $this->form->addRule('email', gettext("The email is required."), 'required', null, 'client');

      // Field-specific rules
      $this->form->addRule('email', gettext("The email address you entered does not appear to be valid."), 'email', true, 'client');
      $this->form->addRule('listname', gettext("Please choose a valid list."), 'minlength', 2, 'client');
   }

   function ProcessForm($f) {
   	$list = $f['listname'];
   	$subunsub = $f['subunsub'];
   	$nomail = $f['nomail'];
   	$digest = $f['digest'];
    $email = $f['email'];

    if (preg_match('/^_lg/',$list))
        throw new Exception('You must pick an individual list to subscribe to.');

   	if ($subunsub=="subscribe") {
         $mailtext = "\r\n\r\nsubscribe $list $email\r\n";
   		if ($digest=="yes") $mailtext .= "set $list digest $email\r\n";
   		if ($nomail=="no") $mailtext .= "set $list nomail $email\r\n";
   		$mailtext .= "end\r\n";
         $subject = "subscribe to $list";
      }
      else {
         $mailtext = "\r\n\r\nunsubscribe $list $email\r\nend\r\n";
         $subject = "unsubscribe from $list";
      }

   	$headers = "From: <$email>";

   	@mail("majordomo@postgresql.org",$subject,$mailtext,$headers);
   	$this->redir_param = '&subject=' . urlencode($subject) . '&email=' . urlencode($email);
   }

   function RenderThanks() {
      $this->tpl->setVariable('subject', empty($_GET['subject'])? 'None': $_GET['subject']);
      $this->tpl->setVariable('email', empty($_GET['email'])? 'None': $_GET['email']);
      $this->tpl->setVariable('referer', '/community/lists/subscribe');
   }
}
?>
