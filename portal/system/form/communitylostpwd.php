<?php
class Form_CommunityLostPwd extends PgForm {
   function __construct() {
      $this->title = 'Lost password';
      $this->navsection = 'community';
   }

   function SetupForm() {
       $this->form->addelement('static', null, null, 'To recover a lost password, enter your userid or email address in this form, and instructions for resetting your password will be emailed to you.');
       $this->form->addelement('text', 'userid', 'Userid or email', array('size'=>52, 'maxlength'=>128));

       $this->form->addRule('userid', gettext('The userid or email is required.'), 'required', null, 'client');
   }

   function ProcessForm($f) {
       global $_SETTINGS;

       $this->pg_query('BEGIN TRANSACTION');
       $res = $this->pg_query_params('SELECT userid,email FROM users WHERE lower(userid)=lower($1) OR lower(email)=lower($1)', array($f['userid']));
       if (pg_num_rows($res) == 1) {
           $res2 = $this->pg_query_params("UPDATE users SET resethash=md5(pgcrypto.gen_salt('bf')),resethashtime=CURRENT_TIMESTAMP WHERE userid=$1 RETURNING resethash", array(pg_fetch_result($res, 0, 0)));
           if (pg_num_rows($res2) != 1)
               throw new Exception('Failed to store reset hash');

           $mailstr = "Someone, probably you, requested that the password for your community\n" .
               "account on postgresql.org be reset.\n\n" .
               "Please follow the link below to reset your password.\n\n" .
               $_SETTINGS['masterserver'] . '/community/resetpwd?uid=' .
               urlencode(pg_fetch_result($res, 0, 0)) . 
               '&rh=' . urlencode(pg_fetch_result($res2, 0, 0)) . "\n\n";
           @mail(pg_fetch_result($res, 0, 1), 'Your postgresql.org community account', $mailstr);
           $this->pg_query('COMMIT');
       }
       else {
           $this->pg_query('ROLLBACK');
       }
   }
   
   function RenderThanks() {
       $this->tpl->setVariable('referer', '/community/lostpwd');
       $this->tpl->touchBlock('community_lostpwd_block');
   }
}
?>
