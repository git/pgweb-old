<?php
class Form_SubmitNews extends PgForm {
   function __construct() {
      $this->title = 'Submit News';
      $this->navsection = 'about';
      $this->requirelogin = true;
   }

   function SetupForm() {
      $this->form->addElement('static',   null, null, gettext("Please enter the news article you want to post:<br />(The article will only be shown after it has been approved based on the project <a href=\"http://wiki.postgresql.org/wiki/Policies\">policies</a>)."));
      
      $this->form->addElement('text',     'email', gettext("Email:"), array('size' => 30, 'maxlength' => 100));
      $this->form->addElement('text',     'headline', gettext("Headline:"), array('size' => 50, 'maxlength' => 100));
      $this->form->addElement('textarea', 'summary', gettext("Summary:"), array('rows' => 5, 'cols' => 50));
      $this->form->addElement('textarea', 'story', gettext("Story:"), array('rows' => 15, 'cols' => 50));
      
      // Make all fields required
      $this->form->addRule('email', gettext("The email is required."), 'required', null, 'client');
      $this->form->addRule('headline', gettext("The headline is required."), 'required', null, 'client');
      $this->form->addRule('summary', gettext("The summary is required."), 'required', null, 'client');
      $this->form->addRule('story', gettext("The story is required."), 'required', null, 'client');

      // Apply field-specific rules
      $this->form->addRule('email', gettext("The email address you entered does not appear to be valid."), 'email', true, 'client');
      $this->form->addRule('headline', gettext("The headline must be between 3 and 100 characters long."), 'rangelength', array(3, 100), 'client');
      $this->form->addRule('summary', gettext("The summary must be between 3 and 300 characters long."), 'rangelength', array(3, 300), 'client');
      $this->form->addRule('story', gettext("The story must be at least 3 characters long."), 'minlength', 3, 'client');
   }

   function ProcessForm($f) {
      global $_SETTINGS;

      $rs        = $this->pg_query("SELECT nextval('news_id_seq')");
      $newsId    = pg_fetch_result($rs, 0, 0);
      $mailtext  = "A new entry has been added to the news database.\n\n" .
                   "Edit:     {$_SETTINGS['masterserver']}/admin/news-edit.php?id=" . $newsId .
                   "\nApprove:  {$_SETTINGS['masterserver']}/admin/news.php?action=approve&id=" . $newsId .
                   "\nDelete:   {$_SETTINGS['masterserver']}/admin/news.php?action=delete&id=" . $newsId .
                   "\n----\nSubmitted from account: {$this->userinfo['userid']}\nSubmitted by: {$f['email']}\nHeadline: {$f['headline']}\n" .
                   "Summary:\n\n{$f['summary']}\n\nStory:\n\n{$f['story']}\n";

      $this->pg_query('BEGIN TRANSACTION');
      $this->pg_query_params('INSERT INTO news (id, posted_by) VALUES ($1,$2)', array($newsId, $f['email']));
      $this->pg_query_params('INSERT INTO news_text (newsid, headline, summary, story, language) VALUES ($1,$2,$3,$4,$5)',
         array($newsId, $f['headline'], $f['summary'], $f['story'], 'en'));
      $this->pg_query('COMMIT');
      @mail($_SETTINGS['notifymail'], 'New News Entry (id: ' . $newsId . ')', $mailtext);
   }

   function RenderThanks() {
      $this->tpl->touchBlock('about_submitnews_block');
      $this->tpl->setVariable('referer', '/');
   }
}
?>
