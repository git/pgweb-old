<?php
require_once './global/functions.xhtml.php';

class Form_CommunityDocEdit extends PgForm {
    private $pageid;
    private $docs_template;
    function __construct($pageid) {
        $this->title = 'Edit community documentation';
        $this->navsection = 'community';
        $this->requirelogin = true;

        if ($pageid == -1)
            throw new NotFoundException('Page to edit not specified');
        else
            $this->pageid = $pageid;
    }

    function SetupForm() {
        global $_SETTINGS;

        $has_working_copy = 0;

        $rs = $this->pg_query_params('SELECT template FROM communitypages_root WHERE pageid=communitypages_findroot($1)', array($this->pageid));
        if (pg_num_rows($rs) != 1)
            throw new NotFoundException('This page does not have a root');
        $this->docs_template = pg_fetch_result($rs, 0, 0);

        $rs = $this->pg_query_params('SELECT pageid,shorttitle,title,contents,author,ready FROM communitypages_work WHERE pageid=$1', array($this->pageid));
        if (pg_num_rows($rs) == 1) {
            // Found a working copy
            $res = pg_fetch_assoc($rs);
            if ($res['author'] != $this->userinfo['userid'])
                throw new Exception('This page is already checked out for editing by a different user.');
            $has_working_copy=1;
        }
        else {
            // No working copy
            $rs = $this->pg_query_params('SELECT pageid,shorttitle,title,contents,syspage FROM communitypages WHERE pageid=$1', array($this->pageid));
            if (pg_num_rows($rs) != 1)
                throw new NotFoundException('Page not found');
            $res = pg_fetch_assoc($rs);
            if ($res['syspage']==1 && $this->userinfo['communitydoc_superuser'] != 1)
                throw new Exception('This page is flagged as a system page and cannot be edited');
        }

        // find all attached files
        $rs = $this->pg_query_params('SELECT fileid,title FROM communitypages_files WHERE pageid=$1 UNION ALL SELECT fileid,title FROM communitypages_work_files WHERE pageid=$1 ORDER BY 2', array($this->pageid));
        $files = '';
        if (pg_num_rows($rs) > 0) {
            $files = '<ul>';
            while ($fi = pg_fetch_assoc($rs)) {
                $ext = preg_replace('/.*\./', '', $fi['title']);
                if (strlen($ext)) $ext = '.' . $ext;

                $files .= '<li><a href="/communityfiles/' . $fi['fileid'] . $ext . '">' . $fi['title'] . '</a> (reference as /communityfiles/' . $fi['fileid'] . $ext . ')</li>';
            }
            $files .= '</ul>';
        }

        $this->form->addElement('text', 'title', 'Page title', array('size'=>52, 'maxlength'=>128));
        $this->form->addElement('text', 'shorttitle', 'Short title', array('size'=>52, 'maxlength'=>128));
        $this->form->addElement('textarea', 'contents', 'Contents', array('cols'=>55, 'rows'=>20));
        $this->form->addElement('static', 'linklist', 'Available files', $files);
        $this->form->addElement('file', 'ulfile', 'Upload file');
        $maxFileSize = (5*1024*1024); // 5MB, overrides the php.ini
        $this->form->setMaxFileSize($maxFileSize);

        if (!empty($res['ready']) && $res['ready'] == 1) {
            $this->form->addElement('static', 'info1', null, 'This page has been flagged as ready for publishing, and will be published as soon as it has been reviewed by website manager. If you wish to make further changes, please uncheck the box below first.');
        }
        else {
            $this->form->addElement('static', 'info1', null, 'You may save your page several times during editing. Once you are done, check this box and the page will be sent for publishing approval. It will not appear on the website until 
                approved by a website manager. Until this time, you can still make small modifications to the page - but please don\'t flag the page as ready until you are finished.');
        }
        $this->form->addElement('checkbox', 'ready', null, 'This page is ready for publication', array('id' => 'ready'));
        if ($has_working_copy) {
            $this->form->addElement('checkbox', 'release', null, 'Release working copy without saving.');
        }

        $this->form->addElement('submit', null, 'Save');
        $this->submitbutton = false;

        $this->form->addElement('static', 'info1', null, 'Once you have saved the page, you can preview it using the button below. If you don\'t save before you preview, your changes will be lost.');

        $this->form->addElement('button', null, 'Preview', array('onclick' => "document.location.href='{$_SETTINGS['masterserver']}/{$this->docs_template}.{$this->pageid}?preview=1';"));

        $this->form->setDefaults($res);
        $this->block_elements = array('info1' => 'qf_element', 'info2' => 'qf_element', 'linklist' => 'qf_element');

        $this->form->addRule('title', gettext('The title is required'), 'required', null, 'client');
        $this->form->addRule('shorttitle', gettext('the short title is required'), 'required', null, 'client');
        $this->form->addRule('ulfile','Files can not be larger than 2MB','maxfilesize',$maxFileSize);

        $this->include_tinymce = true;
    }

    function ProcessForm($f) {
        $validator = new XHTML_validator();
        if (!$validator->validate_xhtml_snippet($f['contents']))
            throw new Exception($validator->error);

        $this->pg_query('BEGIN TRANSACTION');
        $rs = $this->pg_query_params('SELECT author FROM communitypages_work WHERE pageid=$1', array($this->pageid));
        if (pg_num_rows($rs) == 0) {
            // Create working copy
            $this->pg_query_params('INSERT INTO communitypages_work (pageid,parent,author,title,shorttitle,contents) SELECT pageid,parent,$1,title,shorttitle,contents FROM communitypages WHERE pageid=$2', array($this->userinfo['userid'], $this->pageid));
        }
        else {
            // Found working copy
            $res = pg_fetch_assoc($rs);
            if ($res['author'] != $this->userinfo['userid'])
                throw new Exception('Specified file is checked out for editing by a different author.');
        }

        if (empty($f['ready']) || $f['ready'] != 1) $f['ready']=0;
        if (empty($f['release']) || $f['release'] != 1) $f['release']=0;

        if ($f['release'] == 1) {
            // Release working copy
            $oldrs = $this->pg_query_params('SELECT parent FROM communitypages_work WHERE pageid=$1', array($this->pageid));
            $this->pg_query_params('DELETE FROM communitypages_work_files WHERE pageid=$1', array($this->pageid));
            $this->pg_query_params('DELETE FROM communitypages_work WHERE pageid=$1', array($this->pageid));
            $rs = $this->pg_query_params('SELECT pageid FROM communitypages WHERE pageid=$1', array($this->pageid));
            $this->pg_query('COMMIT');

            // Check if the page existed so we can redirect to an old version of it
            if (pg_num_rows($rs) == 1) {
                $this->redirect_relative = '/' . $this->docs_template . '.' . $this->pageid;
            }
            else {
                $this->redirect_relative = '/' . $this->docs_template . '.' . pg_fetch_result($oldrs, 0, 0);
            }
            return;
        }

        $this->pg_query_params('UPDATE communitypages_work SET ready=$1,shorttitle=$2,title=$3,contents=$4 WHERE pageid=$5', array($f['ready'], $f['shorttitle'], $f['title'], $validator->output, $this->pageid));

        $ulfile = $this->form->getElement('ulfile');
        if ($ulfile->isUploadedFile()) {
            $fileinfo = $ulfile->getValue();

            $rs = $this->pg_query("SELECT nextval('communitypages_files_id_seq')");
            $fileid = pg_fetch_result($rs, 0, 0);

            // pg_query_params is completely broken wrt bytea :-O
            $this->pg_query_params('INSERT INTO communitypages_work_files (fileid,pageid,author,title,mimetype,image) VALUES ($1,$2,$3,$4,$5,NULL)', array($fileid, $this->pageid, $this->userinfo['userid'], $fileinfo['name'], $fileinfo['type']));
            $this->pg_query("UPDATE communitypages_work_files SET image='" . pg_escape_bytea(file_get_contents($fileinfo['tmp_name'])) . "' WHERE fileid=$fileid");

        }

        if ($f['ready']==1) {
            if ($this->userinfo['communitydoc_superuser']) {
                $this->pg_query_params('SELECT communitypages_approve($1)', array($this->pageid));
            }
            else
            {
                global $_SETTINGS;
                $mailtext  = "A community documentation page has been edited by " . $this->userinfo['fullname'] . ' <' . $this->userinfo['email'] . ">\n\n" .
                    "Edit:     " . $_SETTINGS['masterserver'] . '/admin/comdoc-edit.php?id=' . $this->pageid . "  (edit the documentation page)\n" .
                    "Approve:  " . $_SETTINGS['masterserver'] . '/admin/comdocs.php?id=' . $this->pageid . "&doapprove=1 (approve for display on the website)\n" .
                    "Release:  " . $_SETTINGS['masterserver'] . '/admin/comdocs.php?id=' . $this->pageid . "&dorelease=1 (release working copy)\n";

                @mail($_SETTINGS['notifymail'], 'Community doc "' .  $f['shorttitle'] . '" edited by ' . $this->userinfo['fullname'], $mailtext);
            }
        }

        $this->pg_query('COMMIT');
        if ($f['ready'] != 1) {
            $this->redirect_relative = '/community/docedit.' . $this->pageid;
        }
    }

    function RenderThanks() {
       $this->tpl->setVariable('referer', '/community/docedit.' . $this->pageid);
       $this->tpl->touchBlock('community_editpage_block');
    }
}
?>
