<?php
class Form_Login extends PgForm {
   function __construct() {
      $this->title = 'Community login';
   }

   function SetupForm() {
      $this->form->addElement('static',     null, null, gettext('Accessing this resource requires a community login. If you don\'t have one, you can read about it <a href="/community/signup">here</a>. If you don\'t remember your password, click <a href="/community/lostpwd">here</a>.'));
      if (isset($_GET['badpwd']) && $_GET['badpwd'] == '1') {
          $this->form->setElementError('password', gettext('Userid or password was incorrect. Please try again.'));
      }

      $this->form->addElement('text',       'userid', gettext("Userid:"), array('size' => 40, 'maxlength' => 100));
      $this->form->addElement('password',   'password', gettext("Password:"), array('size' => 40, 'maxlength' => 100));
      $this->form->addElement('hidden', 'p', isset($_GET['p'])?$_GET['p']:'');
      
      // Make the fields required
      $this->form->addRule('userid', gettext("The userid is required."), 'required', null, 'client');
      $this->form->addRule('password', gettext("The password is required."), 'required', null, 'client');
   }

   function ProcessForm($f) {
        global $_SETTINGS;

        $rs = $this->pg_query_params("SELECT success,userid,fullname,email,authorblurb,communitydoc_superuser,matrixeditor FROM community_login($1,$2)", array($f['userid'], $f['password']));
        if (pg_num_rows($rs) != 1 || pg_fetch_result($rs,0,0) != 1) {
            if (isset($f['p'])) {
                header('Location: /login?badpwd=1&p=' . urlencode($f['p']));
            }
            else {
                header('Location: /login?badpwd=1');
            }
            exit(0);
        }

        session_start();
        $this->userinfo = pg_fetch_assoc($rs);
        foreach ($this->userinfo as $key=>$val) {
            $_SESSION[$key] = $val;
        }
        $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
        if ($f['p']) {
            header('Location: ' . $f['p']);
        }
        else {
            header('Location: /');
        }
        exit(0);
   }
   
   function RenderThanks() {
   }
}
?>
