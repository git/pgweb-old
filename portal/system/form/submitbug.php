<?php
class Form_SubmitBug extends PgForm {
   function __construct() {
      $this->title = 'Report a bug';
      $this->navsection = 'support';
   }

   function SetupForm() {
       $this->form->addElement('static',     null, null, gettext("Please ensure you have read the <a href=\"/docs/current/static/bug-reporting.html\">bug reporting guidelines</a> before reporting a bug. In particular, please re-read the <a href=\"/docs/current/static/\">documentation</a> to verify that what you are trying is possible. If the documentation is not clear, please report that, too; it is a documentation bug. If a program does something different from what the documentation says, that is also a bug."));
       $this->form->addElement('static',     null, null, gettext('Poor performance is not necessarily a bug. Read the documentation or ask on one of the <a href="/community/lists/">mailing lists</a> for help in tuning your applications. Failing to comply to the SQL standard is not necessarily a bug either, unless compliance for the specific feature is explicitly claimed.'));
       $this->form->addElement('static',     null, null, gettext('Before you continue, check on the <a href="http://wiki.postgresql.org/wiki/Todo">TODO list</a> and in the <a href="/docs/faq/">FAQ</a> to see if your bug is already known. If you cannot decode the information on the TODO list, report your problem so we can clarify the TODO list.'));

       $this->form->addElement('static',     null, null, gettext("To report a security bug, please send an email to <a href=\"mailto:security@postgresql.org\">security@postgresql.org</a>. All other bugs will be forwarded to the <a href=\"http://archives.postgresql.org/pgsql-bugs/\">pgsql-bugs</a> mailing list where they will be publicly archived."));
      $this->form->addElement('static',     'info', null, gettext('Make sure you are running the latest available minor release for your major <a href="/support/versioning">version</a> before reporting a bug. The current list of supported versions is ') . $this->get_supported_versions() . '.');
      $this->form->addElement('static',	     null, null, gettext("This bug report form should only be used for reporting bugs and problems with the PostgreSQL database.  Problems with database connectors such as ODBC and JDBC, graphical administration tools such as pgAdmin or other external projects <b>should not be reported here</b>; please report to those projects directly.  For products closely connected with PostgreSQL, there may be an appropriate <a href='/community/lists'>mailing list</a> available."));

      $this->form->addElement('text',       'name', gettext("Name:"), array('size' => 40, 'maxlength' => 100));
      $this->form->addElement('text',       'email', gettext("Email:"), array('size' => 40, 'maxlength' => 100));
      $this->form->addElement('text',       'version', gettext("PostgreSQL version:"),  array('size' => 15, 'maxlength' => 15));
      $this->form->addElement('text',       'os', gettext("Operating system:"), array('size' => 40, 'maxlength' => 150));
      $this->form->addElement('text',       'description', gettext("Short description:"), array('size' => 40, 'maxlength' => 100));
      $this->form->addElement('textarea',   'details', gettext("Details:"), array('rows' => 15, 'cols' => 55));

      // Make the fields required
      $this->form->addRule('email', gettext("The email is required."), 'required', null, 'client');
      $this->form->addRule('version', gettext("The PostgreSQL version is required."), 'required', null, 'client');
      $this->form->addRule('os', gettext("The operating system info is required."), 'required', null, 'client');
      $this->form->addRule('description', gettext("The short description is required"), 'required', null, 'client');
      $this->form->addRule('details', gettext("The details are required."), 'required', null, 'client');

      // Field-specific rules
      $this->form->addRule('email', gettext("The email address you entered does not appear to be valid."), 'email', true, 'client');
      $this->form->addRule('version', gettext("The PostgreSQL version you entered must be between 1 and 15 characters long."), 'rangelength', array(1, 15), 'client');
      $this->form->addRule('os', gettext("The operating system version must be between 3 and 150 characters long."), 'rangelength', array(3, 150), 'client');
      $this->form->addRule('description', gettext("The short description must be between 3 and 100 characters long."), 'rangelength', array(3, 100), 'client');
      $this->form->addRule('details', gettext("The bug details must be at least 3 characters long."), 'minlength', 3, 'client');
      $this->form->registerRule('validOption', 'callback', 'array_key_exists');
   }

   function ProcessForm($f) {
        global $_SETTINGS;

        $rs        = $this->pg_query("SELECT nextval('bug_id_seq')");
        $bugId     = pg_fetch_result($rs, 0, 0);
        $mailtext  = "The following bug has been logged online:\n\n" .
                     "Bug reference:      {$bugId}\n" .
                     wordwrap('Logged by:          ' . $f['name'], 76, "\n", true) . "\n" .
                     wordwrap('Email address:      ' . $f['email'], 76, "\n", true) . "\n" .
                     wordwrap('PostgreSQL version: ' . $f['version'], 76, "\n", true) . "\n" .
                     wordwrap('Operating system:   ' . $f['os'], 76, "\n", true) . "\n" .
                     wordwrap('Description:        ' . $f['description'], 76, "\n", true) . "\n" .
                     "Details: \n\n" . wordwrap($f['details'], 76, "\n", true);
        $subject   = "BUG #" . $bugId . ": " . $f['description'];

        // Set the From name and charset here. This *should* be utf-8 because that's what the 
        // form is dispatched as, but browsers don't always play by the rules so this isn't infallible.
        $headers   = "From: \"" . $f['name'] . "\" <" . $f['email'] . ">\nContent-Type: text/plain; charset=utf-8\n";

        @mail($_SETTINGS['bugsmail'], $subject, $mailtext, $headers);
        
        $this->redir_param = '&id=' . $bugId;
   }
   
   function RenderThanks() {
      $this->tpl->setVariable('bug_id', empty($_GET['id'])? 'None': $_GET['id']);
      $this->tpl->setVariable('referer', '/support/submitbug');
   }

   function get_supported_versions() {
       global $_SETTINGS;
       $s = '';
       foreach ($_SETTINGS['versions'] as $v=>$d) {
           if ($d['frontpage'] != 1) continue;
           $s .= $d['version'] . ', ';
       }
       return substr($s, 0, strlen($s)-2);
   }
}
?>
