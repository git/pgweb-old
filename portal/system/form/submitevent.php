<?php
class Form_SubmitEvent extends PgForm {
   function __construct() {
      $this->title = 'Submit Event';
      $this->navsection = 'about';
      $this->requirelogin = true;
   }

   function SetupForm() {
      global $_SETTINGS;

      $this->form->applyFilter(array('start', 'end'), 'intval');

      $this->form->addElement('static',     null, null, gettext("Please enter the event details you want to post:<br />(The event will only be shown after it has been approved based on the project <a href=\"http://wiki.postgresql.org/wiki/Policies\">policies</a>)."));

      $this->form->addElement('text',       'organisation', gettext("Organisation/company:"), array('size' => 30, 'maxlength' => 100));
      $this->form->addElement('text',       'email', gettext("Email:"), array('size' => 30, 'maxlength' => 100));
      $this->form->addElement('text',       'headline', gettext("Event:"), array('size' => 50, 'maxlength' => 100));
      $this->form->addElement('checkbox',   'training', null, gettext("is training event"), array('id' => 'training'));
      $this->form->addElement('text',       'city', gettext("City:"), array('size' => 50, 'maxlength' => 100));
      $this->form->addElement('text',       'state', gettext("State:"), array('size' => 50, 'maxlength' => 100));
      $this->form->addElement('select',       'country', gettext("Country:"), $this->fetch_countries_list());
      $this->form->addElement('date',       'start', gettext("Start Date:"), array(
          'language'       => $this->language,
          'format'         => 'd M Y',
          'minYear'        => date('Y'),
          'maxYear'        => date('Y') + 2,
          'addEmptyOption' => true
      ));
      $this->form->addElement('date',       'end', gettext("End Date:"), array(
          'language'       => $this->language,
          'format'         => 'd M Y',
          'minYear'        => date('Y'),
          'maxYear'        => date('Y') + 2,
          'addEmptyOption' => true
      ));
      $this->form->addElement('textarea', 'summary', gettext("Summary:"), array('rows' => 5, 'cols' => 50));
      $this->form->addElement('textarea', 'story', gettext("Details:"), array('rows' => 15, 'cols' => 50));

      // Make all fields required
      $this->form->addRule('organisation', gettext("The organisation/company is required."), 'required', null, 'client');
      $this->form->addRule('email', gettext("The email is required."), 'required', null, 'client');
      $this->form->addRule('headline', gettext("The event title is required."), 'required', null, 'client');
      $this->form->addRule('city', gettext("The city is required."), 'required', null, 'client');
      $this->form->addRule('country', gettext("The country is required."), 'required', null, 'client');
      $this->form->addRule('summary', gettext("The summary is required."), 'required', null, 'client');
      $this->form->addRule('story', gettext("The details are required."), 'required', null, 'client');
      $this->form->addGroupRule('start', gettext("The start date is required."), 'required', null, 0, 'client');
      $this->form->addGroupRule('end', gettext("The end date is required."), 'required', null, 0, 'client');

      // Field-specific rules
      $this->form->addRule('email', gettext("The email address you entered does not appear to be valid."), 'email', true, 'client');
      $this->form->addRule('headline', gettext("The event title must be between 3 and 100 characters long."), 'rangelength', array(3, 100), 'client');
      $this->form->addRule('city', gettext("The city must be between 3 and 100 characters long."), 'rangelength', array(3, 100), 'client');
      $this->form->addRule('summary', gettext("The summary must be between 3 and 300 characters long."), 'rangelength', array(3, 300), 'client');
      $this->form->addRule('story', gettext("The event details must be at least 3 characters long."), 'minlength', 3, 'client');

      // Check the date fields
      $this->form->addFormRule('form_check_event_dates');
   }

   function ProcessForm($f) {
      global $_SETTINGS;

   	$countries = $this->fetch_countries_list();
      $startDate = sprintf('%04d-%02d-%02d', $f['start']['Y'], $f['start']['M'], $f['start']['d']);
      $endDate   = sprintf('%04d-%02d-%02d', $f['end']['Y'], $f['end']['M'], $f['end']['d']);
      $training  = empty($f['training'])? 'f': 't';
      $rs        = $this->pg_query("SELECT nextval('events_id_seq')");
      $eventId   = pg_fetch_result($rs, 0, 0);
      $mailtext  = "A new entry has been added to the events database.\n\n" .
                   "Edit:    {$_SETTINGS['masterserver']}/admin/event-edit.php?id={$eventId}\n" .
                   "Approve: {$_SETTINGS['masterserver']}/admin/events.php?action=approve&id={$eventId}\n" .
                   "Delete:  {$_SETTINGS['masterserver']}/admin/events.php?action=delete&id={$eventId}\n" .
                   "----\nSubmitted by: {$f['email']}\nEvent: {$f['headline']}\n" .
                   "Training event: " . ('f' == $training? 'No': 'Yes') . "\n" .
                   "Location: City: {$f['city']}, State: {$f['state']}, Country: {$countries[$f['country']]}\n" .
                   "Summary:\n\n{$f['summary']}\n\nDetails:\n\n{$f['story']}\n";
      $this->pg_query('BEGIN TRANSACTION');
      $this->pg_query_params('INSERT INTO events (id,posted_by,start_date,end_date,training,organisation) VALUES ($1,$2,$3,$4,$5,$6)',
         array($eventId, $f['email'], $startDate, $endDate, $training, $f['organisation']));
      $this->pg_query_params('INSERT INTO events_location (eventid,country,state,city) VALUES ($1,$2,$3,$4)',
         array($eventId, $f['country'], $f['state'], $f['city']));
      $this->pg_query_params('INSERT INTO events_text (eventid,event,summary,details,language) VALUES ($1,$2,$3,$4,$5)',
         array($eventId, $f['headline'], $f['summary'], $f['story'], 'en'));
      $this->pg_query('COMMIT');
      @mail($_SETTINGS['notifymail'], 'New Event (id: ' . $eventId . ')', $mailtext);
   }

   function RenderThanks() {
      $this->tpl->touchBlock('about_submitevent_block');
      $this->tpl->setVariable('referer', '/');
   }

   function fetch_countries_list() {
      $rs = $this->pg_query('SELECT id,name FROM countries ORDER BY id');
      $c = array();
      for ($i = 0; $i < pg_num_rows($rs); $i++) {
         $countries = pg_fetch_array($rs, $i, PGSQL_ASSOC);
         $c[$countries['id']] = $countries['name'];
      }
      return $c;
   }
}

function form_check_event_dates(&$v)
{
    $errors = array();
    if (!checkdate($v['start']['M'], $v['start']['d'], $v['start']['Y'])) {
        $errors['start'] = gettext("The start date is invalid.");
    } else {
        $start = mktime(0, 0, 0, $v['start']['M'], $v['start']['d'], $v['start']['Y']);
    }
    if (!checkdate($v['end']['M'], $v['end']['d'], $v['end']['Y'])) {
        $errors['end'] = gettext("The end date is invalid.");
    } else {
        $end = mktime(0, 0, 0, $v['end']['M'], $v['end']['d'], $v['end']['Y']);
    }
    if (empty($errors)) {
        if ($start > $end) {
            $errors['end'] = gettext("End date cannot be earlier than start date.");
        } elseif (time() > $end) {
            $errors['end'] = gettext("End date cannot be earlier than today.");
        }
    }
    return empty($errors)? true: $errors;
}
?>
