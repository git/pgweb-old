<?
   $base = "http://search.postgresql.org/search";
   if ($_GET[ul] == "http://www.postgresql.org/%") {
      // General web search
      redirect("q=" . urlencode($_GET[q]));
   }
   elseif ($_GET[m] == 1) {
      redirect("m=1&q=" . urlencode($_GET[q]) . "&l=" . urlencode($_GET[l]) . "&d=" . urlencode($_GET[d]) . "&s=" . urlencode($_GET[s]));
   }
   elseif (substr($_GET[ul],0,26) == "http://www.postgresql.org/") {
      redirect("q=" . urlencode($_GET[q]) . "&u=" . rtrim(substr($_GET[ul],25),'%'));
   }
   else {
      echo 'Unknown search URL.';
   }

   function redirect($qs) {
      header("Location: http://search.postgresql.org/search?" . $qs);
   }
?>
