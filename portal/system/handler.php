<?php
// Main file for dispatching pages
//
// $Id: handler.php,v 1.21 2007-08-14 12:03:45 mha Exp $
//
function __autoload($class) {
   if ($class == 'HTML_Template_Sigma') {
       require_once 'HTML/Template/Sigma.php';
       return;
   }
   $lcclass = strtolower($class);
   if (substr($lcclass,0,5) == 'page_') {
      require_once './page/' . substr($lcclass,5) . '.php';
   }
   elseif (substr($lcclass,0,5) == 'form_') {
      require_once './form/' . substr($lcclass,5) . '.php';
   }
   elseif (substr($lcclass,0,6) == 'admin_') {
       require_once '../admin/' . substr($lcclass,6) . '.php';
   }
   else {
      require_once "./global/$lcclass.php";
   }
}

require_once './global/settings.php';

// Override config that really must be set
ini_set('session.use_cookies','1');
ini_set('session.use_only_cookies','1');


try {
    if (isset($_GET['page']) && $_GET['page'] == 'submitthanks') {
        // Special case. Ugly, but backwards compatible ;-)
        $pgpage = Dispatcher::Dispatch($_GET['action']);
        $pgpage->thanks = true;
    }
    else
        $pgpage = Dispatcher::Dispatch(isset($_GET['page'])?$_GET['page']:'index');

    $pgpage->setLanguage(isset($_GET['lang'])?$_GET['lang']:null);

    if ($pgpage->requirelogin)
        $pgpage->ValidateLogin();

    $pgpage->PreRender();
    $pgpage->DoRender();
}
catch (NotFoundException $nfe) {
    $errp = new ErrorPage(404, 'File Not Found', $nfe->getMessage(), isset($_GET['page'])?$_GET['page']:'index');
    $errp->ShowError();
}
catch (Exception $ex) {
    $errp = new ErrorPage(500, 'Internal Server Error', $ex->getMessage(), isset($_GET['page'])?$_GET['page']:'index');
    $errp->ShowError();
}

$pgpage->Show();

?>
