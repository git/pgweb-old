#!/usr/bin/perl -w

use strict;
use URI::Escape;

sub sysdie { die "$0: @_: $!\n" }

###

use File::Temp 'tempfile';
use File::stat;

# main

die "Usage: $0 directory\n" if (@ARGV != 1);

my $DIR = $ARGV[0];

die "$DIR is not a directory\n" if (! -d $DIR);
chdir $DIR or die "Cannot change directory to $DIR\n";

while (my $in_filename = glob("msg*.php"))
{
	next if (! -f $in_filename);

	my $changes = 0;

	open(my $IN, '<', $in_filename) or sysdie "Cannot open $in_filename";

	(my $OUT, my $out_filename) = tempfile(UNLINK => 1, DIR => '.');
	while (<$IN>)
	{
		# prevent running on a file we already processed, by
		# adding our marker at the top of the file
		last if (m/^<!-- msgid2linkv2 -->/);
		if (m/^<!-- MHonArc/)
		{
			print $OUT "<!-- msgid2linkv2 -->\n";
		}

		if (!$changes && m/>Message-id(<.*>)?:/ && !m/<a href=/)
		{
			s/\(dot\)/./g;
			s/\(at\)/@/g;
			m/^(.*&lt;)(.*)(&gt;)(<\/li>)$/;
			my $lead = $1;
			my $link = uri_escape($2);
			my $ulink = $2;
			my $trailer = $3;
			my $eol = $4;
			print $OUT qq{$lead<a href="http://archives.postgresql.org/message-id/$link">$ulink</a>$trailer &lt;<a href="http://archives.postgresql.org/msgtxt.php?id=$link">text/plain</a>>$eol};
			$changes = 1;
		}
		else
		{
			print $OUT $_;
		}
	}
	close $IN;
	close $OUT;

	if ($changes)
	{
		my $stat_in_file = stat($in_filename);
		rename $out_filename, $in_filename or
			sysdie "Cannot rename $out_filename to $in_filename";
		# restore file permissions
		chmod $stat_in_file->mode & 07777, $in_filename;
	}
	else
	{
		unlink $out_filename;
	}
}
