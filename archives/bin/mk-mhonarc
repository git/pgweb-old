#!/bin/sh
#
# $Id$
# 
# Script to refresh the HTML archives from the Majordomo mboxes.
#

. $HOME/etc/archives.conf

export TZ=GMT
export LC_ALL=C

temp=""
heldlock=""
retval=1	# unless overridden later
# Need single quotes here to avoid expansion of variables until execution
trap 'rm -f $heldlock $REMOVE_FORCE_FILE $temp; exit $retval' \
	EXIT QUIT INT TERM HUP

lockfile -r 0 $LOCKFILE || {
        echo "Can't create mhonarc lockfile: $?" >&2
        exit 1
}
heldlock=$LOCKFILE

if [ ! -d $ARCHIVES_ROOT ]; then
	mkdir -p $ARCHIVES_ROOT || exit 1
fi
if [ ! -d $ARCHIVES_TMP ]; then
	mkdir -p $ARCHIVES_TMP || exit 1
fi
cd $ARCHIVES_ROOT

if [ -f $FORCE_FILE ]; then
	force=1
else
	force=0
fi

# $ARCHIVES_BIN/generate-list-descriptions

process_one_mbox() {
    local listdir path mbox ymon year month ydashm rcfile doit gzipped
    listdir=$1
    mbox=$2
    path=$listdir/$mbox
    ymon=${mbox##*.}		# extract yyyymm
    # Some lists have weekly archives, so we'd get something like 2009032 here
    # Split the week number for later
    if /usr/bin/test -l $ymon -gt 6; then
      submbox=${ymon##??????}
      ymon=${ymon%%?}
    else
      submbox=
    fi
    year=${ymon%%??}		# extract yyyy
    month=${ymon##????}		# extract mm
    if [ -z "$submbox" ]; then
      ydashm=${year}-${month}	# construct yyyy-mm
    else
      ydashm=${year}-${month}-${submbox} # yyyy-mm-s
    fi

    if [ "$ymon" -le 200607 ]; then
      rcfile=$ARCHIVES_BIN/resource-2006-07
    else
      rcfile=$ARCHIVES_BIN/resource-current
    fi

    if [ ! -d "$ydashm" ]; then
      mkdir $ydashm
    fi

    doit=0

    # In forced mode we clean the dir previous to running mhonarc, to cause it
    # to completely regenerate the archive for this month.  If we run it over
    # the existing directory, it will skip rewriting the message pages which is
    # not what we want.  In the normal case this is a good thing, because it
    # saves a lot of work.
    if [ $force -eq 1 -a ! -f $ydashm/forced_run_done ]; then
      doit=1
      rm -f $ydashm/*
    fi
    if [ $doit -eq 0 -a ! -f $ydashm/index.php ]; then
      doit=1
    fi
    if [ $doit -eq 0 -a $path -nt $ydashm/index.php ]; then
      doit=1
    fi
    if [ $doit -eq 1 ]; then
      temp=`mktemp -p $ARCHIVES_TMP`
      $MHONARC -add -quiet -rcfile $rcfile -outdir $ydashm $path \
	      -definevar LIST=$list \
	      -definevar YEAR=$year \
	      -definevar MONTH=$month \
	      > $temp 2>&1
      $ARCHIVES_BIN/msgid2link.pl $ydashm >> $temp 2>/dev/null
      $ARCHIVES_BIN/createmsgid.pl -i $ydashm -o $ARCHIVES_ROOT/msgid \
              >> $temp 2>/dev/null
      if [ -s $temp ]; then
	echo "START -- error processing $mbox"
	cat $temp
	echo -e "END   -- error processing $mbox\n\n"
      fi
      rm $temp
      temp=
      date > $ydashm/last-updated
      if [ ! -d "mbox" ]; then
        mkdir mbox
      fi
      gzipped=mbox/${list}.${ydashm}.gz
      if [ -f $path.gz ]; then
        cp $path.gz $gzipped
      else
        gzip $path -f -c > $gzipped
      fi
      chmod 755 $gzipped
      $ARCHIVES_BIN/splitmbox $gzipped
      if [ $force -eq 1 ]; then
	touch $ydashm/forced_run_done
      fi
    fi
    date > $ydashm/last-checked
}

for list in `ls $MAJORDOMO_FILES`; do
  cd $ARCHIVES_ROOT
  listdir=$MAJORDOMO_FILES/$list/files/public/archive

  if [ ! -d $listdir ]; then
    continue
  fi
  # If the list is present in the "blacklist file", do not archive it, and
  # send a complain by email, but throttle it so that it's not too obnoxious.
  if grep "$list" $ARCHIVES_BIN/blacklist >/dev/null 2>&1; then
    complainfile=${list}_complained
    if [ -f $complainfile ]; then
      # already complained, but remove file if it's too old.  This makes us
      # complain once every 12 hours
      age=`stat --format=%Y $complainfile`
      now=`date +%s`
      if [ $(($now - $age)) -gt $((60 * 60 * 12)) ]; then
	rm $complainfile
      fi
      continue
    fi
    # use a subscribed address so that it is not held for moderation
    echo "warning: sensitive list $list found on archives" | mail -s "sensitive list found" $DESTINATION_ADDRESS -- -f $SOURCE_ADDRESS
    touch $complainfile
    continue
  fi
  if [ ! -d "$list" ]; then
    mkdir $list
  fi
  cd $list

  for mbox in `ls --reverse $listdir`; do
    mbox=${mbox%%.gz}		# remove possible trailing .gz

    process_one_mbox $listdir $mbox
  done

  # XXX maybe this can be done better with mod_rewrite?
  # Ensure that index.php exists and is a symlink
  test -f index.php && rm index.php
  test ! -e index.php && ln -s ../list_index.php index.php

  # create the list of extant months
  $ARCHIVES_BIN/list_dates.pl $listdir $list > months

  date > last-updated
done

touch $ARCHIVES_ROOT/timestamp
date > $ARCHIVES_ROOT/last-updated

if [ $force -eq 1 ]; then
  find $ARCHIVES_ROOT -name forced_run_done -type f -delete
  REMOVE_FORCE_FILE=$FORCE_FILE
fi
retval=0
