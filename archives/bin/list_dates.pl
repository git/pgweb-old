#!/usr/bin/perl -w

use File::Find;

$listdir = shift;
$list = shift;

find({ wanted => \&wanted, preprocess => \&sortfiles }, $listdir);

exit;

sub sortfiles {
	# sort in reverse 
	sort { $b cmp $a } @_;
}

# note that we need to cope with weekly archives too (seventh digit)
sub wanted {
  return unless /^$list/;
  /$list.(\d{4})(\d{2})(\d?)/;
  $year = $1;
  $month = $2;
  $sub = $3 if defined $3;
  print "$year $month $sub\n";
}
