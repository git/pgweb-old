<?php
ini_set('include_path', $_SERVER['DOCUMENT_ROOT']."/../pearlib");

require 'HTML/Template/Sigma.php';

$tmpldir = $_SERVER['DOCUMENT_ROOT'] . "/../templates";

$tpl =& new HTML_Template_Sigma($tmpldir."/html");

$message_page = false;
if (strstr($_SERVER['PHP_SELF'], "message-id") ||
	strstr($_SERVER['PHP_SELF'], "/msg"))
	$message_page = true;

$tpl->loadTemplateFile('msgheader.html');
$tpl->setVariable('title', $title);
if ($message_page)
	$robots = "nofollow, index, archive";
else
	$robots = "follow, noindex, noarchive";
$tpl->setVariable('robots', $robots);

if (!isset($base))
{
	$tpl->hideBlock('baseblock');
}
else
{
	$tpl->setCurrentBlock('baseblock');
	$tpl->setVariable('base', $base);
	$tpl->parse('baseblock');
}
$tpl->parse();
$tpl->show();

// extract the list name from the URL.  When loading a message via message-Id,
// use $base instead because PHP_SELF does not contain a usable URL.
if (isset($base))
	list($listname, $dummy) = sscanf($base, "/%[^/]s/%[0-9-]s/");
else
	list($listname) = sscanf($_SERVER['PHP_SELF'], "/%[^/]s");

/* fill the left-side menu with all the group names, expanding lists
 * for the group this list belongs */
$groups = json_decode(file_get_contents($tmpldir."/groups.json"), true);
$lists = json_decode(file_get_contents($tmpldir."/lists.json"), true);
ksort($groups);
$mygroup = $lists[$listname]["group"];

$tpl->loadTemplateFile('top_config.html');

foreach ($groups as $group) {
        $tpl->setCurrentBlock('top_listgroup');
        $tpl->setVariable('top_groupname', $group["name"]);
        $tpl->setVariable('top_group_firstlist', $group["lists"][0]);

        /* When we detect the group that this list belongs to, expand the lists
         * on the group */
        if ($group["id"] == $mygroup) {
                $tpl->setCurrentBlock('top_list');
                foreach ($group["lists"] as $list) {
                        // use the shortdesc if it exists
                        if (isset($lists[$list]["shortdesc"]))
                                $desc = $lists[$list]["shortdesc"];
                        else
                                $desc = $list;
                        $tpl->setVariable('top_name', $list);
                        $tpl->setVariable('top_desc', $desc);
                        $tpl->parse('top_list');
                }
        }
        $tpl->parse('top_listgroup');
}

$tpl->parse();
$tpl->show();

?>
