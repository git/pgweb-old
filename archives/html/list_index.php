<?php

ini_set('include_path', $_SERVER['DOCUMENT_ROOT']."/../pearlib");

if ($_SERVER['PHP_SELF'] == '/list_index2.php') {
	echo "go away";
	exit;
}

require 'HTML/Template/Sigma.php';

$tmpldir = $_SERVER['DOCUMENT_ROOT'] . "/../templates";

$months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
	'Oct', 'Nov', 'Dec');

$tpl =& new HTML_Template_Sigma($tmpldir."/html");
$tpl->loadTemplateFile('list_index.html');

// generate the months list, using the months file
$f = fopen("months", 'r');
if ($f) {
	$prevyear = 0;
	while ($line = fgets($f)) {
		/* some lists have weekly archives; the "months" file will have 
		 * 3 columns in that case 
		 */
		list ($year, $month, $sub) = sscanf($line, "%s %s %s");
		$month = rtrim($month);

		$tpl->setCurrentBlock('month');
		$tpl->setVariable('year', $year);
		if ($prevyear == $year)
			$yeartext = "";
		else $yeartext = $year;
		$tpl->setVariable('year_text', $yeartext);
		if (isset($sub)) {
		  $tpl->setVariable('month_text', $months[$month - 1].",&nbsp;week&nbsp;".$sub);
		  $tpl->setVariable('month', $month."-".$sub);
		}
		else {
		  $tpl->setVariable('month_text', $months[$month - 1]);
		  $tpl->setVariable('month', $month);
		}
		$tpl->parse('month');
		$prevyear = $year;
	}
	fclose($f);
}

// extract the list name from the URL
list($listname) = sscanf($_SERVER['PHP_SELF'], "/%[^/]s");

/* fill the left-side menu with all the group names, expanding lists
 * for the group this list belongs */
$groups = json_decode(file_get_contents($tmpldir."/groups.json"), true);
$lists = json_decode(file_get_contents($tmpldir."/lists.json"), true);
ksort($groups);
$mygroup = $lists[$listname]["group"];

foreach ($groups as $group) {
	$tpl->setCurrentBlock('top_listgroup');
	$tpl->setVariable('top_groupname', $group["name"]);
	$tpl->setVariable('top_group_firstlist', $group["lists"][0]);

	/* When we detect the group that this list belongs to, expand the lists
	 * on the group */
	if ($group["id"] == $mygroup) {
		$tpl->setCurrentBlock('top_list');
		foreach ($group["lists"] as $list) {
			// use the shortdesc if it exists
			if (isset($lists[$list]["shortdesc"]))
				$desc = $lists[$list]["shortdesc"];
			else
				$desc = $list;
			$tpl->setVariable('top_name', $list);
			$tpl->setVariable('top_desc', $desc);
			$tpl->parse('top_list');
		}
	}
	$tpl->parse('top_listgroup');
}

// finally, set the list name and description
$tpl->setVariable('list', $listname);
$tpl->setVariable('description', $lists[$listname]["description"]);

$tpl->parse();
$tpl->show();

?>
