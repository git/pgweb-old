<?php

ini_set('include_path', '../pearlib');

require 'HTML/Template/Sigma.php';

$tpl =& new HTML_Template_Sigma('../templates/html');
$tpl->loadTemplateFile('index.html');

$groups = json_decode(file_get_contents("../templates/groups.json"), true);
$lists = json_decode(file_get_contents("../templates/lists.json"), true);

ksort($groups);

foreach ($groups as $group) {
	/* This is for index.html.  We want a title for each group, and 
	 * the lists that are part of it. */
	$tpl->parse('listgroup');
	$tpl->setCurrentBlock('listgroup');
	$tpl->setVariable('groupname', $group["name"]);

	foreach ($group["lists"] as $list) {
		$tpl->setCurrentBlock('list');
		$tpl->setVariable('name', $list);
		$tpl->setVariable('description', $lists[$list]["description"]);
		$tpl->parseCurrentBlock();
	}

	/* This is for the menu on top_config.html. On this page we only
	 * link to the first list on each group. */
	$tpl->parse('top_listgroup');
	$tpl->setCurrentBlock('top_listgroup');
	$tpl->setVariable('top_groupname', $group["name"]);
	$tpl->setVariable('top_group_firstlist', $group["lists"][0]);
}

$tpl->parseCurrentBlock();

$tpl->show();

?>
